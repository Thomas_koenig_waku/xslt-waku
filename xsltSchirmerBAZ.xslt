<?xml version="1.0" encoding="UTF-8"?>
<!-- 09.02.2016 -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:script="Myscript" extension-element-prefixes="script">
	<xsl:output version="1.0" encoding="UTF-8" indent="yes" omit-xml-declaration="no" method="xml"/>
	<!--xsl:output version="1.0" encoding="UTF-8"  omit-xml-declaration="no" indent="yes" method="xml"/-->
	<!--xsl:output version="1.0" encoding="ISO-8859-1"  omit-xml-declaration="no" indent="yes" method="xml"/-->
	
	<!-- _______________________________________________________ Root ____________________________________________________________ -->
	<msxsl:script language="VBScript" implements-prefix="script">
		<![CDATA[ 

 	     Dim currentLocale
	     ' Get the current locale
	     currentLocale = GetLocale

            Function SetLocaleFirst()
              Dim original
              original = SetLocale("en-gb")
              SetLocaleFirst = ""
            End Function
        
            Function SetLocaleEnd()
                Dim original
                original = SetLocale(currentLocale)
                SetLocaleEnd = ""
            End Function


	     Dim cntRod
	     Dim cntPiece
	     Dim cntMu
	     cntRod = 0
	     cntPiece=0	
	     cntMu=0	

            Function IncRod()
                cntRod = cntRod + 1
                IncRod = cntRod
            End Function

            Function SetRod(v)
                cntRod = v
                SetRod = cntRod
            End Function
            
            Function IncPiece()
                cntPiece = cntPiece + 1
                IncPiece = cntPiece
            End Function

            Function SetPiece(v)
                cntPiece = v
                SetPiece = cntPiece
            End Function
            
            Function IncMu()
                cntMu = cntMu + 1
                IncMu = cntMu
            End Function
            
            Function SetMu(v)
                cntMu = v
                SetMu = cntMu
            End Function

            Function RFill(name, sign, times)
                Dim sStr
                sStr = name & String(times, sign)
                RFill = Left(sStr, times)
            End Function
            
            Function LFill(name, sign, times)
              Dim s
              s = String(times, sign) & name
              LFill = Right(s, times)
            End Function
 
             Function GetDecDel()
                Dim s
                s = CStr(3 / 2)
                s = Mid(s, 2, 1)
                GetDecDel = s
            End Function

            Function GetDoubleStr(value, decDel)
                Dim dD
                Dim p
                Dim forV
                Dim afterV
                GetDoubleStr = value
                p = 0
                If InStr(value, ".") > 0 Or InStr(value, ",") > 0 Then
                    If InStr(value, ".") > 0 Then
                        p = InStr(value, ".")
                        dD = "."
                    End If
                    If InStr(value, ",") > 0 Then
                        p = InStr(value, ",")
                        dD = ","
                    End If
                    forV = CLng(Left(value, p - 1))
                    afterV = CLng(Mid(value, p + 1, 5))
                    GetDoubleStr = CStr(forV) & decDel & CStr(afterV)
                End If
            End Function

             Function GetFieldNo(fitting)
                Dim v
                GetFieldNo = "0"
                'Feldnummer#Flügeltyp#Sicherheit#Verschlusstyp#Grifftyp#Dornmass#FlügelBand
                v = Split(fitting, "#")
                If UBound(v) >= 6 Then
                    If IsNumeric(v(0)) Then GetFieldNo = CStr(CLng(v(0)))
                End If
            End Function

            Function GetOperationX(pos, length, weldA, inv, decDel, role)
                Dim p
                Dim l
                Dim w
                GetOperationX = 0
                If Not IsNumeric(pos) Then Exit Function
                If Not IsNumeric(length) Then Exit Function
                If Not IsNumeric(weldA) Then Exit Function
                p = CDbl(GetDoubleStr(pos, decDel))
                l = CDbl(GetDoubleStr(length, decDel))
                w = CDbl(GetDoubleStr(weldA, decDel))
                If inv = "1" Then
                    p = l - (p + w)
                Else
                    p = p + w
                End If
                'If role = "SASH" Then p = p - 1 / 2
                GetOperationX = GetDoubleStr(CStr(p), ".")
            End Function
            
            Function GetMachRef(mat)
                Dim prSys
                Dim prZusatz
                Dim prBez
                Dim i
                
                GetMachRef = mat
                
                prSys = Left(mat, 2)
                prBez = mId(mat, 4)
                i = InStr(prBez, "_") + 1
                prZusatz = ""
                If i > 1 Then prZusatz = Left(prBez, i)
                
                Select Case UCase(prSys)
					Case "AP": prBez = Left(prBez, 6)
                    Case "KO": prBez = Left(prBez, 4)
                    Case "TR": prBez = Left(prBez, 6)
				End Select
                If InStr(mat, "810316") Then prBez = "8103"
		If InStr(mat, "140033_FÜD") Then prBez = "140033FUED"
		If InStr(mat, "810416") Then prBez = "8104"
				
		
                GetMachRef = prBez
            End Function

            Function ReplaceSigns(s)
                Dim str
                ReplaceSigns = s
                str = CStr(s)
                If InStr(str, "ß") > 0 Then str = Replace(str, "ß", "ss")
                If InStr(str, "ä") > 0 Then str = Replace(str, "ä", "ae")
                If InStr(str, "ö") > 0 Then str = Replace(str, "ö", "oe")
                If InStr(str, "ü") > 0 Then str = Replace(str, "ü", "ue")
                If InStr(str, "Ä") > 0 Then str = Replace(str, "Ä", "Ae")
                If InStr(str, "Ö") > 0 Then str = Replace(str, "Ö", "Oe")
                If InStr(str, "Ü") > 0 Then str = Replace(str, "Ü", "Ue")
                ReplaceSigns = str
            End Function
			
			Function GetSteelReference(ref)
				ref = CStr(ref)
				GetSteelReference = ""
				'@WAKU
				'Hier bitte selbstaendig erweitern!
				GetSteelReference=ref
				If InStr(ref, "KO-V045") Then GetSteelReference = "45"
				If InStr(ref, "KO-V047") Then GetSteelReference = "47"
				If InStr(ref, "KO-V047fix") Then GetSteelReference = "47 F"
				If InStr(ref, "KO-V048") Then GetSteelReference = "48"
				If InStr(ref, "KO-V049") Then GetSteelReference = "49"
				If InStr(ref, "KO-V063") Then GetSteelReference = "63"
				If InStr(ref, "KO-V050") Then GetSteelReference = "50"
				If InStr(ref, "KO-V053") Then GetSteelReference = "53"
				If InStr(ref, "KO-V039") Then GetSteelReference = "39"
				If InStr(ref, "TR-810308") Then GetSteelReference = "03"
				If InStr(ref, "TR-811408") Then GetSteelReference = "14"
				If InStr(ref, "TR-810408") Then GetSteelReference = "04"
				If InStr(ref, "TR-820708") Then GetSteelReference = "07"
				If InStr(ref, "TR-810108") Then GetSteelReference = "01"
				If InStr(ref, "TR-810208") Then GetSteelReference = "02"
				If InStr(ref, "TR-832208") Then GetSteelReference = "22"
				If InStr(ref, "TR-830108") Then GetSteelReference = "8301"

				If InStr(ref, "ZU-XPS") Then GetSteelReference = "X"
				If InStr(ref, "TR-892031") Then GetSteelReference = "X"
				If InStr(ref, "TR-892131") Then GetSteelReference = "X"
				If InStr(ref, "TR-892731") Then GetSteelReference = "X"
				If InStr(ref, "ZU-XPS873207") Then GetSteelReference = "HSX"
				
				
			End Function
	

			
			'Function GetSteelLength(SteelRef1, SteelLength1, SteelRef2, SteelLength2)
			'	SteelRef1 = CStr(SteelRef1)
			'	SteelLength1 = CDbl(SteelLength1)
			'	SteelRef2 = CStr(SteelRef2)
			'	SteelLength2 = CDbl(SteelLength2)
			'	Dim GetSteelReference1
			'	Dim GetSteelReference2
			'   
			'	GetSteelReference1 = GetSteelReference(SteelRef1)
			'	GetSteelReference2 = GetSteelReference(SteelRef2)
			'   
			'	GetSteelLength = "0"
			'	If Len(SteelRef2) > 0 Then
			'		'2 Stähle enthalten
			'		If GetSteelReference1 <> "X" And GetSteelReference1 <> "F" Then
			'			'dann nimm Stahl 1
			'			GetSteelLength = SteelLength1
			'		Else
			'			GetSteelLength = SteelLength2
			'		End If
			'	Else
			'		'nur 1 Stahl enthalten
			'		If Len(SteelLength1) > 0 Then
			'			GetSteelLength = SteelLength1
			'		End If
			'	End If
			'End Function
            
			
 

        ]]>
	</msxsl:script>
	<xsl:variable name="decDel" select="script:GetDecDel()"/>


	<!-- PARAMS USED TO OBTAIN EXTERNAL VB INFORMATION -->
	<xsl:param name="lot" select="ProductionLot/@ProductionLot"/>
	<xsl:param name="set" select="'1'"/>
	<xsl:param name="machines" select="'#1#51#52#53#'"/>
	<xsl:param name="machinesZu" select="'#5#'"/>	
	
	<!--SD20110202 invFrOp = 1, da alle Bearbeitunge auf dem Rahmen von der anderen Seite kamen-->
	<xsl:param name="invFrOp" select="1"/>
	<!--SD20110215 invShOp = 0 da die Bearbeitungen invertiert waren-->
	<xsl:param name="invShOp" select="0"/>
	<xsl:param name="invMuOp" select="1"/>
	<xsl:param name="invShStOp" select="0"/>
	<xsl:param name="invSteelOp" select="0"/>
	<xsl:param name="separation" select="5"/>
	<!-- separation = 1  - Rahmen und Flügel werden in eine Datei geschrieben  (wird nicht mehr unterstützt)  -->
	<!-- separation = 2  - nur Rahmendatei wird erzeugt -->
	<!-- separation = 3  - nur Flügeldatei wird erzeugt-->
	<!-- separation = 4  - nur Teiler + Stulp+ Zusatzprofile -datei wird erzeugt-->
	<!-- separation = 5  - nur  Zusatzprofile -datei wird erzeugt-->

	<xsl:param name="addWeldingLost" select="1"/>
	<xsl:param name="goodWaste" select="800"/>

	<xsl:param name="file">
		<xsl:value-of select="concat(format-number($lot,'000000'), format-number($set,'00'))"/>
		<xsl:choose>
			<xsl:when test="$separation = 2">
				<xsl:value-of select="'Ra.xml'"/>
			</xsl:when>
			<xsl:when test="$separation = 3">
				<xsl:value-of select="'Fl.xml'"/>
			</xsl:when>
			<xsl:when test="$separation = 4">
				<xsl:value-of select="'Pf.xml'"/>
			</xsl:when>
			<xsl:when test="$separation = 5">
				<xsl:value-of select="'Zu.xml'"/>
			</xsl:when>			
			<xsl:otherwise>
				<xsl:value-of select="'.xml'"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:param>

	<!--SD20110526 SteelClass hinzugefügt, wird für richtige Auswertung der Stahllaenge benötigt-->
	<xsl:param name ="SteelClassPrio1">
		<xsl:value-of select ="'Manueller Stahl'"/>
	</xsl:param>
	<xsl:param name ="SteelClassPrio2">
		<xsl:value-of select ="'RVBStahl'"/>
	</xsl:param>
	<xsl:param name ="SteelClassPrio3">
		<xsl:value-of select ="''"/>
	</xsl:param>

	<xsl:template match="/">
		<xsl:value-of select="script:SetLocaleFirst()"/>
		<xsl:variable name="cntRod">
			<xsl:choose>
				<xsl:when test="$separation = 2">
					<xsl:value-of select="count(descendant::ProductionSet[@ProductionSetNumber=$set]/Machine[contains($machines,concat('#',@machineId,'#'))]/descendant::Rod[@rol = 'FRAME'])"/>
				</xsl:when>
				<xsl:when test="$separation = 3">
					<xsl:value-of select="count(descendant::ProductionSet[@ProductionSetNumber=$set]/Machine[contains($machines,concat('#',@machineId,'#'))]/descendant::Rod[@rol = 'SASH'])"/>
				</xsl:when>
				<xsl:when test="$separation = 4">
					<xsl:value-of select="count(descendant::ProductionSet[@ProductionSetNumber=$set]/Machine[contains($machines,concat('#',@machineId,'#'))]/descendant::Rod[@rol != 'FRAME' and @rol != 'SASH'])"/>
				</xsl:when>
				<xsl:when test="$separation = 5">
					<xsl:value-of select="count(descendant::ProductionSet[@ProductionSetNumber=$set]/Machine[contains($machinesZu,concat('#',@machineId,'#'))]/descendant::Rod[@rol != 'FRAME' and @rol != 'SASH'])"/>
				</xsl:when>				
				<xsl:otherwise>
					<xsl:value-of select="count(descendant::ProductionSet[@ProductionSetNumber=$set]/Machine[contains($machines,concat('#',@machineId,'#'))]/descendant::Rod)"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:if test="$cntRod &gt; 0">
			<Datei>
				<xsl:attribute name="Name">
					<xsl:value-of select="$file"/>
				</xsl:attribute>
				<!-- Dateiname -->
				<OptiZuschnittdaten>
					<Optidaten>
						<xsl:attribute name="Name">
							<xsl:value-of select="$file"/>
						</xsl:attribute>
						<!-- Dateiname -->
						<xsl:attribute name="Lauf">
							<xsl:value-of select="'Hier Infos reinschreiben.'"/>
						</xsl:attribute>
						<!-- Hinweistext max 255 Zeichen -->
						<xsl:choose>
							<xsl:when test="$separation=2">
								<!-- Rahmen -->
								<xsl:apply-templates select="descendant::ProductionSet[@ProductionSetNumber=$set]/Machine[contains($machines,concat('#',@machineId,'#'))]/descendant::Rod[@rol = 'FRAME']">
									<xsl:sort select="ancestor::Machine/@machineId" data-type="number" order="descending"/>
									<xsl:with-param name="kind" select="'PVC'"/>
								</xsl:apply-templates>
							</xsl:when>
							<xsl:when test="$separation=3">
								<!-- Flügel -->
								<xsl:apply-templates select="descendant::ProductionSet[@ProductionSetNumber=$set]/Machine[contains($machines,concat('#',@machineId,'#'))]/descendant::Rod[@rol = 'SASH']">
									<xsl:sort select="ancestor::Machine/@machineId" data-type="number" order="descending"/>
									<xsl:with-param name="kind" select="'PVC'"/>
								</xsl:apply-templates>
							</xsl:when>
							<xsl:when test="$separation=4">
								<!-- Teiler und Zusatzprofile -->
								<xsl:apply-templates select="descendant::ProductionSet[@ProductionSetNumber=$set]/Machine[contains($machines,concat('#',@machineId,'#'))]/descendant::Rod[@rol != 'FRAME' and @rol != 'SASH']">
									<xsl:sort select="ancestor::Machine/@machineId" data-type="number" order="descending"/>
									<xsl:with-param name="kind" select="'PVC'"/>
								</xsl:apply-templates>
							</xsl:when>
							<xsl:when test="$separation=5">
								<!-- Teiler und Zusatzprofile -->
								<xsl:apply-templates select="descendant::ProductionSet[@ProductionSetNumber=$set]/Machine[contains($machinesZu,concat('#',@machineId,'#'))]/descendant::Rod[@rol != 'FRAME' and @rol != 'SASH']">
									<xsl:sort select="ancestor::Machine/@machineId" data-type="number" order="descending"/>
									<xsl:with-param name="kind" select="'PVC'"/>
								</xsl:apply-templates>
							</xsl:when>
							
							<xsl:otherwise>
								<xsl:apply-templates select="descendant::ProductionSet[@ProductionSetNumber=$set]/Machine[contains($machines,concat('#',@machineId,'#'))]/descendant::Rod">
									<xsl:sort select="ancestor::Machine/@machineId" data-type="number" order="descending"/>
									<xsl:with-param name="kind" select="'PVC'"/>
								</xsl:apply-templates>
							</xsl:otherwise>
						</xsl:choose>
					</Optidaten>
					<xsl:variable name="r" select="script:SetRod(string(0))"/>
					<xsl:variable name="p" select="script:SetPiece(string(0))"/>
					<xsl:variable name="m" select="script:SetMu(string(0))"/>
					<StahlOptidaten>
						<xsl:attribute name="Name">
							<xsl:value-of select="$file"/>
						</xsl:attribute>
						<!-- Dateiname -->
						<xsl:attribute name="Lauf">
							<xsl:value-of select="'Hier Infos reinschreiben.'"/>
						</xsl:attribute>
						<!-- Hinweistext max 255 Zeichen -->
						<xsl:choose>
							<xsl:when test="$separation=2">
								<!-- Rahmen -->
								<xsl:apply-templates select="descendant::ProductionSet[@ProductionSetNumber=$set]/Machine[contains($machines,concat('#',@machineId,'#'))]/descendant::Rod[@rol = 'FRAME']">
									<xsl:sort select="ancestor::Machine/@machineId" data-type="number" order="descending"/>
									<xsl:with-param name="kind" select="'ST'"/>
								</xsl:apply-templates>
							</xsl:when>
							<xsl:when test="$separation=3">
								<!-- Flügel -->
								<xsl:apply-templates select="descendant::ProductionSet[@ProductionSetNumber=$set]/Machine[contains($machines,concat('#',@machineId,'#'))]/descendant::Rod[@rol = 'SASH']">
									<xsl:sort select="ancestor::Machine/@machineId" data-type="number" order="descending"/>
									<xsl:with-param name="kind" select="'ST'"/>
								</xsl:apply-templates>
							</xsl:when>
							<xsl:when test="$separation=4">
								<!-- Teiler und Zusatzprofile -->
								<xsl:apply-templates select="descendant::ProductionSet[@ProductionSetNumber=$set]/Machine[contains($machines,concat('#',@machineId,'#'))]/descendant::Rod[@rol != 'FRAME' and @rol != 'SASH']">
									<xsl:sort select="ancestor::Machine/@machineId" data-type="number" order="descending"/>
									<xsl:with-param name="kind" select="'ST'"/>
								</xsl:apply-templates>
							</xsl:when>
							<xsl:when test="$separation=5">
								<!-- Teiler und Zusatzprofile -->
								<xsl:apply-templates select="descendant::ProductionSet[@ProductionSetNumber=$set]/Machine[contains($machinesZu,concat('#',@machineId,'#'))]/descendant::Rod[@rol != 'FRAME' and @rol != 'SASH']">
									<xsl:sort select="ancestor::Machine/@machineId" data-type="number" order="descending"/>
									<xsl:with-param name="kind" select="'ST'"/>
								</xsl:apply-templates>
							</xsl:when>
							
							<xsl:otherwise>
								<xsl:apply-templates select="descendant::ProductionSet[@ProductionSetNumber=$set]/Machine[contains($machines,concat('#',@machineId,'#'))]/descendant::Rod">
									<xsl:sort select="ancestor::Machine/@machineId" data-type="number" order="descending"/>
									<xsl:with-param name="kind" select="'ST'"/>
								</xsl:apply-templates>
							</xsl:otherwise>
						</xsl:choose>
					</StahlOptidaten>
				</OptiZuschnittdaten>
			</Datei>
		</xsl:if>
		<xsl:value-of select="script:SetLocaleEnd()"/>
	</xsl:template>

	<xsl:template match="Rod">
		<xsl:param name="kind"/>
		<xsl:variable name="invOp">
			<xsl:choose>
				<xsl:when test="@rol='FRAME'">
					<xsl:value-of select="$invFrOp"/>
				</xsl:when>
				<xsl:when test="@rol='SASH'">
					<xsl:value-of select="$invShOp"/>
				</xsl:when>
				<xsl:when test="@rol='SASH STOP'">
					<xsl:value-of select="$invShStOp"/>
				</xsl:when>
				<xsl:when test="@rol='MULLION'">
					<xsl:value-of select="$invMuOp"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="0"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
	<!--Testbereich-->
		<xsl:attribute name ="invOp">
			<xsl:value-of select ="$invOp"/>
		</xsl:attribute>
		<!--Testbereich-->

		<xsl:if test="$separation=1 or $separation=2">
			<xsl:if test="@rol='FRAME'">
				<xsl:call-template name="RodInst">
					<xsl:with-param name="inst" select="1"/>
					<xsl:with-param name="invOp" select="$invOp"/>
					<xsl:with-param name="role" select="@rol"/>
					<xsl:with-param name="kind" select="$kind"/>
				</xsl:call-template>
				<xsl:if test="@instances = 2">
					<xsl:call-template name="RodInst">
						<xsl:with-param name="inst" select="2"/>
						<xsl:with-param name="invOp" select="$invOp"/>
						<xsl:with-param name="role" select="@rol"/>
						<xsl:with-param name="kind" select="$kind"/>
					</xsl:call-template>
				</xsl:if>
			</xsl:if>
		</xsl:if>
		<xsl:if test="$separation=1 or $separation=3">
			<xsl:if test="@rol='SASH'">
				<xsl:call-template name="RodInst">
					<xsl:with-param name="inst" select="1"/>
					<xsl:with-param name="invOp" select="$invOp"/>
					<xsl:with-param name="role" select="@rol"/>
					<xsl:with-param name="kind" select="$kind"/>
				</xsl:call-template>
				<xsl:if test="@instances = 2">
					<xsl:call-template name="RodInst">
						<xsl:with-param name="inst" select="2"/>
						<xsl:with-param name="invOp" select="$invOp"/>
						<xsl:with-param name="role" select="@rol"/>
						<xsl:with-param name="kind" select="$kind"/>
					</xsl:call-template>
				</xsl:if>
			</xsl:if>
		</xsl:if>
		<xsl:if test="$separation=4">
			<xsl:if test="@rol != 'FRAME' and @rol != 'SASH'">
				<xsl:call-template name="RodInst">
					<xsl:with-param name="inst" select="1"/>
					<xsl:with-param name="invOp" select="$invOp"/>
					<xsl:with-param name="role" select="@rol"/>
					<xsl:with-param name="kind" select="$kind"/>
				</xsl:call-template>
			</xsl:if>
		</xsl:if>
		<xsl:if test="$separation=5">
			<xsl:if test="@rol != 'FRAME' and @rol != 'SASH'">
				<xsl:call-template name="RodInst">
					<xsl:with-param name="inst" select="1"/>
					<xsl:with-param name="invOp" select="$invOp"/>
					<xsl:with-param name="role" select="@rol"/>
					<xsl:with-param name="kind" select="$kind"/>
				</xsl:call-template>
			</xsl:if>
		</xsl:if>
		
	</xsl:template>

	<xsl:template name="RodInst">
		<xsl:param name="inst"/>
		<xsl:param name="invOp"/>
		<xsl:param name="role"/>
		<xsl:param name="kind"/>
		<xsl:param name="reference"/>
		<xsl:variable name="rodNo" select="script:IncRod()"/>
		<xsl:variable name="stRef">
			<xsl:value-of select="script:GetMachRef(string(descendant::Steel/@reference))"/>
		</xsl:variable>
		<xsl:if test="$kind='PVC'">
			<Stabdaten>
				<xsl:attribute name="StabNr">
					<xsl:value-of select="$rodNo"/>
				</xsl:attribute>
				<!-- Stabnummer für jede Instanz hochzählen -->
				<xsl:attribute name="Rohlaenge">
					<xsl:value-of select="format-number(@length,'0.0')"/>
				</xsl:attribute>
				<!-- Laufbahn 1=Einzelstab hinten, 2=Einzelstab vorn, 3=Doppelbahnbearbeitung, 4=Doppelbahnbearbeitung hinten, 5=Doppelbahnbearbeitung vorn -->
				<!-- ???? muss noch eindeutig geklärt werden -->
				<xsl:attribute name="Laufbahn">
					<xsl:value-of select="1"/>
				</xsl:attribute>
				<xsl:attribute name="Bezeichnung">
					<xsl:value-of select="script:ReplaceSigns(string(@Description))"/>
				</xsl:attribute>
				<!-- momnetan Profilbeschreibung -->
				<xsl:attribute name="Kommentar"/>
				<!--OPTIONAL soll leer bleiben  max 255 Zeichen -->
				<xsl:attribute name="ProfilName">
					<xsl:value-of select="script:GetMachRef(string(@reference))"/>
				</xsl:attribute>
				
				<!--Farbe-->
				<xsl:variable name ="color">
					<xsl:value-of select="@color"/>
				</xsl:variable>
				<xsl:attribute name="Farbe">
					<xsl:value-of select="$color"/>
				</xsl:attribute>
				
				<xsl:attribute name="ResteLaenge">
					<xsl:value-of select="format-number(@waste,'0.0')"/>
				</xsl:attribute>
				<!-- Kennung für Reststück 1=Rest, 2=Abfall -->
				<xsl:attribute name="ResteKennung">
					<xsl:choose>
						<!--xsl:when test="@kindwaste=1"-->
						<!-- RK101123 auf goodWaste geändert, da hier sie Stammdaten schienbar noch nicht so weit sind -->
						<xsl:when test="@waste &lt; $goodWaste">
							<xsl:value-of select="2"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="1"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>
				<xsl:attribute name="ResteTeileNr">
					<xsl:value-of select="format-number(10000 - $rodNo  ,'0')"/>
				</xsl:attribute>
				<xsl:attribute name="StahlName">
					<xsl:value-of select="$stRef"/>
				</xsl:attribute>
				<!--SD20110215 Bei Reststücken soll Etikettendaten leet bleiben-->
				<!--<Etikettendaten>
					<xsl:attribute name="TeileNr">
						<xsl:value-of select="format-number(10000 - ($rodNo),'0')"/>
					</xsl:attribute>
					 Teilenummer der Stabinstanz 
					BM$010SF$010/opt/storage/8001.xml$010RM$010T0$010Laenge 500mm$010T1$010Teil 1$010
					<TintenDruckdaten>
						
						
						<xsl:text>BM$010SF$010/opt/storage/</xsl:text>
						 Dateiname 
						xsl:value-of select="concat(format-number($lot,'000000'), format-number($set,'00'),'.xml')"/
						 RK101208 lt. Herrn Buesge soll hier die Profilbezeichnung rein, da für jede Referenze ein Layout definiert ist 
						<xsl:value-of select="concat(script:GetMachRef(string(ancestor::Rod/@reference)),'.xml')"/>
						 Kennung Text 0 
						<xsl:text>$010RM$010T0$010</xsl:text>
						Länge
						<xsl:value-of select="concat(format-number(@waste,'0'), 'mm')"/>
						<xsl:text>$010</xsl:text>
						 Kennung Text 1 
						<xsl:text>T1$010</xsl:text>
						Profilbezeichnung
						<xsl:value-of select="script:GetMachRef(string(@reference))"/>
						<xsl:text>$010</xsl:text>
					</TintenDruckdaten>
				</Etikettendaten>-->
				<xsl:apply-templates select="child::CutPiece/CutInstance[$inst]">
					<xsl:with-param name="rodNo" select="$rodNo"/>
					<xsl:with-param name="invOp" select="$invOp"/>
					<xsl:with-param name="inst" select="$inst"/>
					<xsl:with-param name="role" select="@rol"/>
					<xsl:with-param name="kind" select="$kind"/>
					<xsl:with-param name="stRef" select="$stRef"/>
					<xsl:with-param name="reference" select="@reference"/>
					<xsl:with-param name="color" select="$color"/>
				</xsl:apply-templates>
			</Stabdaten>
		</xsl:if>
		<xsl:if test="$kind='ST'">
			<StahlStabdaten>
				<xsl:attribute name="StabNr">
					<xsl:value-of select="$rodNo"/>
				</xsl:attribute>
				<!-- Stabnummer für jede Instanz hochzählen -->
				<xsl:attribute name="Rohlaenge">
					<xsl:value-of select="format-number(@length,'0.0')"/>
				</xsl:attribute>
				<!-- Laufbahn 1=Einzelstab hinten, 2=Einzelstab vorn, 3=Doppelbahnbearbeitung, 4=Doppelbahnbearbeitung hinten, 5=Doppelbahnbearbeitung vorn -->
				<!-- ???? muss noch eindeutig geklärt werden -->
				<xsl:attribute name="Laufbahn">
					<xsl:value-of select="1"/>
				</xsl:attribute>
				<xsl:attribute name="Bezeichnung">
					<xsl:value-of select="script:ReplaceSigns(string(@Description))"/>
				</xsl:attribute>
				<xsl:attribute name="Kommentar"/>
				<!--OPTIONAL soll leer bleiben  max 255 Zeichen -->
				<xsl:attribute name="StahlName">
					<xsl:value-of select="$stRef"/>
				</xsl:attribute>
				<xsl:attribute name="Restelaenge">
					<xsl:value-of select="format-number(@waste,'0.0')"/>
				</xsl:attribute>
				<!-- Kennung für Reststück 1=Rest, 2=Abfall -->
				<xsl:attribute name="Restekennung">
					<xsl:choose>
						<xsl:when test="@waste &lt; $goodWaste">
							<xsl:value-of select="'2'"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="'1'"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>
				<xsl:attribute name="ResteTeileNr">
					<xsl:value-of select="format-number(10000 - $rodNo,'0')"/>
				</xsl:attribute>
				<xsl:apply-templates select="child::CutPiece/CutInstance[$inst]">
					<xsl:with-param name="rodNo" select="$rodNo"/>
					<xsl:with-param name="invOp" select="$invSteelOp"/>
					<xsl:with-param name="inst" select="$inst"/>
					<xsl:with-param name="role" select="@rol"/>
					<xsl:with-param name="kind" select="$kind"/>
					<xsl:with-param name="stRef" select="$stRef"/>
				</xsl:apply-templates>
			</StahlStabdaten>
		</xsl:if>
	</xsl:template>

	<xsl:template match="CutInstance">
		<xsl:param name="rodNo"/>
		<xsl:param name="invOp"/>
		<xsl:param name="inst"/>
		<xsl:param name="role"/>
		<xsl:param name="kind"/>
		<xsl:param name="stRef"/>
		<xsl:param name="reference"/>
		<xsl:param name="color"/>

		<xsl:variable name="pieceNo" select="script:IncPiece()"/>
		<xsl:variable name ="Stahllaenge">
			<xsl:choose>
				<xsl:when test ="Steels/Steel[@class = $SteelClassPrio1]">
					<xsl:value-of select ="Steels/Steel[@class = $SteelClassPrio1]/@length"/>
				</xsl:when>
				<xsl:when test ="Steels/Steel[@class = $SteelClassPrio2]">
					<xsl:value-of select ="Steels/Steel[@class = $SteelClassPrio2]/@length"/>
				</xsl:when>
				<xsl:when test ="Steels/Steel[@class = $SteelClassPrio3]">
					<xsl:value-of select ="Steels/Steel[@class = $SteelClassPrio3]/@length"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select ="Steels/Steel/@length"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:variable name ="SteelReference1">
			<xsl:value-of select ="script:GetSteelReference(string(Steels/Steel[1]/@reference))"/>
		</xsl:variable>
		<xsl:variable name ="SteelReference2">
			<xsl:value-of select ="script:GetSteelReference(string(Steels/Steel[2]/@reference))"/>
		</xsl:variable>
		<xsl:variable name ="SteelReference3">
			<xsl:value-of select ="script:GetSteelReference(string(Steels/Steel[3]/@reference))"/>
		</xsl:variable>
		<!--Testbereich-->
		<!--<xsl:value-of select ="concat(
										  'GetSteelLength(',
										  $SteelReference1,',',
										  $SteelLength1,',',
										  $SteelReference2,',',
										  $SteelLength2,
										  ')')"/>-->

		<xsl:if test="$kind='PVC'">
			<Teiledaten>
				<xsl:attribute name="StabNr">
					<xsl:value-of select="$rodNo"/>
				</xsl:attribute>
				<!-- lfd. Stabnummer -->
				<xsl:attribute name="TeileNr">
					<xsl:value-of select="$pieceNo"/>
				</xsl:attribute>

				<!--Testbereich-->
				<!--<xsl:attribute name="absoluteNumber">
					<xsl:value-of select="@absoluteNumber"/>
				</xsl:attribute>-->
				
				<!-- Teilenummer der Stabinstanz -->
				<xsl:attribute name="WagenNr">
					<xsl:choose>
						<xsl:when test="@container &gt; 0">
							<xsl:value-of select="@container"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="0"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>
				<xsl:attribute name="FachNr">
					<xsl:choose>
						<xsl:when test="@slot &gt; 0">
							<xsl:value-of select="format-number(@slot,'00')"/>
							<!--SD20110210 weg lassen, da es sonst nicht angezeigt wird-->
							<!--<xsl:choose>
								<xsl:when test="@angle = 90"><xsl:value-of select="'r'"/></xsl:when>
								<xsl:when test="@angle = 180"><xsl:value-of select="'o'"/></xsl:when>
								<xsl:when test="@angle = 270"><xsl:value-of select="'l'"/></xsl:when>
								<xsl:otherwise><xsl:value-of select="'u'"/></xsl:otherwise>
							</xsl:choose>-->
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="'0'"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>
				<xsl:attribute name="Bezeichnung">
					<!--  OPTIONAL bleibt leer max 255 Zeichen -->
					<xsl:choose>
						<!-- Teile- Typ  -->
						<xsl:when test="$role='SASH STOP '">
							<xsl:value-of select="'ST'"/>
						</xsl:when>
						<!-- ST = Stulp -->
						<xsl:when test="$role='MULLION'">
							<xsl:choose>
								<xsl:when test="@angle = 0 or @angle=180 or @angle=360">
									<xsl:value-of select="'KA'"/>
								</xsl:when>
								<!-- KA = Kämpfer -->
								<xsl:when test="@angle = 90 or @angle=270">
									<xsl:value-of select="'PF'"/>
								</xsl:when>
								<!-- PF = Pfosten -->
								<xsl:otherwise>
									<xsl:value-of select="'SR'"/>
								</xsl:otherwise>
								<!-- SR = schräger Teiler -->
							</xsl:choose>
						</xsl:when>
						<xsl:otherwise>
							<xsl:choose>
								<xsl:when test="$role='FRAME'">
									<xsl:value-of select="'R'"/>
								</xsl:when>
								<xsl:when test="$role='SASH'">
									<xsl:value-of select="'F'"/>
								</xsl:when>
							</xsl:choose>
							<xsl:choose>
								<xsl:when test="@angle = 0 or @angle=360">
									<xsl:value-of select="'U'"/>
								</xsl:when>
								<!-- U = unten -->
								<xsl:when test="@angle = 90">
									<xsl:value-of select="'R'"/>
								</xsl:when>
								<!-- R = rechts -->
								<xsl:when test="@angle = 180">
									<xsl:value-of select="'O'"/>
								</xsl:when>
								<!-- O = oben -->
								<xsl:when test="@angle = 270">
									<xsl:value-of select="'L'"/>
								</xsl:when>
								<!-- L =links -->
								<xsl:otherwise>
									<xsl:value-of select="'S'"/>
								</xsl:otherwise>
								<!-- SR = schräges Teil -->
							</xsl:choose>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>
				<xsl:attribute name="Kommission">
					<xsl:value-of select="concat(@orderNumber,'.',@version)"/>
				</xsl:attribute>
				<!--  OPTIONAL wird mit Auftrag . Version gefüllt -->
				<xsl:attribute name="Position">
					<xsl:value-of select="@nomenclature"/>
				</xsl:attribute>
				<!--  OPTIONAL wird mit Nomenclature gefüllt -->

				<!-- RK101018 wenn außenöffnend dann invOp invertieren -->
				<xsl:variable name="invOpAO">
					<xsl:choose>
						<!--SD20110520 außenöffnend Flügel hinzugefügt-->
						<xsl:when test ="descendant::Operation[contains(@name,'MODELL_AO')] and $role = 'SASH'">
							<!--0-->
							<xsl:choose>
								<xsl:when test="$invOp=1">
									<xsl:value-of select="1"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="0"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:when>
						<xsl:when test="descendant::Operation[contains(@name,'MODELL_AO')]">
							<!-- außenöffnend -->
							<xsl:choose>
								<xsl:when test="$invOp=1">
									<xsl:value-of select="0"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="1"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$invOp"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>

				<!--Testbereich-->
				<!--xsl:attribute name ="invOpAO"-->
					<!--xsl:value-of select ="$invOpAO"/-->
				<!--/xsl:attribute-->
				<!--Testbereich-->



				


				<xsl:choose>
					<!-- An- und Abschnittwinkel -->
					<xsl:when test="$invOpAO=1">
						<xsl:attribute name="AnGehrung">
							<xsl:value-of select="format-number(parent::CutPiece/@angleB,'0.0')"/>
						</xsl:attribute>
						<xsl:attribute name="AbGehrung">
							<xsl:value-of select="format-number(parent::CutPiece/@angleA,'0.0')"/>
						</xsl:attribute>																								
					</xsl:when>
					<xsl:otherwise>
						<xsl:attribute name="AnGehrung">
							<xsl:value-of select="format-number(parent::CutPiece/@angleA,'0.0')"/>
						</xsl:attribute>
						<xsl:attribute name="AbGehrung">
							<xsl:value-of select="format-number(parent::CutPiece/@angleB,'0.0')"/>
						</xsl:attribute>
					</xsl:otherwise>
				</xsl:choose>
				<!-- Winkelangaben für Spitz- oder Kappschnitt -->

				
				<xsl:variable name="AngleOnCutID">
					<xsl:choose>
						<xsl:when test="$reference = 'KO-2621_GS_R'">3</xsl:when>
						<xsl:when test="$invOpAO=1">
							<xsl:choose>
								<xsl:when test="@incrementB">4</xsl:when>
								<xsl:otherwise>0</xsl:otherwise>
							</xsl:choose>
						</xsl:when>
					<xsl:otherwise>
							<xsl:choose>
								<xsl:when test="@incrementA">4</xsl:when>
								<xsl:otherwise>0</xsl:otherwise>
							</xsl:choose>					
					</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>

				<xsl:variable name="AngleOffCutID">
					<xsl:choose>
						<xsl:when test="$reference = 'KO-2621_GS_R'">3</xsl:when>
						<xsl:when test="$invOpAO=1">
							<xsl:choose>
								<xsl:when test="@incrementA">4</xsl:when>
								<xsl:otherwise>0</xsl:otherwise>
							</xsl:choose>
						</xsl:when>
					<xsl:otherwise>
							<xsl:choose>
								<xsl:when test="@incrementB">4</xsl:when>
								<xsl:otherwise>0</xsl:otherwise>
							</xsl:choose>					
					</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>


			     <xsl:attribute name="AngleOnCutID">
			     	<xsl:value-of select="$AngleOnCutID"></xsl:value-of>
			     </xsl:attribute>
			     <xsl:attribute name="AngleOffCutID">
			     	<xsl:value-of select="$AngleOffCutID"></xsl:value-of>
			     </xsl:attribute>
				
				<xsl:variable name="KappHoeheAnschnitt">
					<xsl:choose>						
						<xsl:when test="$invOpAO=1">
							<xsl:choose>
								<xsl:when test="@incrementB">
									<xsl:value-of select="@incrementB"></xsl:value-of>
								</xsl:when>
								<xsl:otherwise>0</xsl:otherwise>
							</xsl:choose>
						</xsl:when>
					<xsl:otherwise>
							<xsl:choose>
								<xsl:when test="@incrementA">
									<xsl:value-of select="@incrementA"></xsl:value-of>
								</xsl:when>
								<xsl:otherwise>0</xsl:otherwise>
							</xsl:choose>					
					</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>

				<xsl:variable name="KappHoeheAbschnitt">
					<xsl:choose>						
						<xsl:when test="$invOpAO=1">
							<xsl:choose>
								<xsl:when test="@incrementA">
									<xsl:value-of select="@incrementA"></xsl:value-of>
								</xsl:when>
								<xsl:otherwise>0</xsl:otherwise>
							</xsl:choose>
						</xsl:when>
					<xsl:otherwise>
							<xsl:choose>
								<xsl:when test="@incrementB">
									<xsl:value-of select="@incrementB"></xsl:value-of>
								</xsl:when>
								<xsl:otherwise>0</xsl:otherwise>
							</xsl:choose>					
					</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>							
				
				
								
				<xsl:attribute name="Laenge">
					<xsl:value-of select="format-number(parent::CutPiece/@length -$KappHoeheAnschnitt - $KappHoeheAbschnitt ,'0.0')"/>
				</xsl:attribute>
				
				
				
				<xsl:attribute name="KappHoeheAnschnitt">
					<xsl:value-of select="$KappHoeheAnschnitt"/>
				</xsl:attribute>
				<xsl:attribute name="KappHoeheAbschnitt">
					<xsl:value-of select="$KappHoeheAbschnitt"/>
				</xsl:attribute>
				
				<!-- Barcode -->
				<!-- vorerst Standard: -->
				<!-- Rechtecke 6st Lot, 2st Set, 4st SqNo, 2st Lage -->
				<!-- Teiler + Stulp 6st Lot, 2st Set, 4st MId, 4st abs.Teilenummer -->
				<xsl:variable name="barCode">
					<xsl:value-of select="script:LFill(string($lot),string(0), string(6))"/>
					<xsl:value-of select="script:LFill(string($set),string(0), string(2))"/>
					<xsl:choose>
						<xsl:when test="$role = 'FRAME' or $role='SASH'">
							<xsl:value-of select="script:LFill(string(@squareNumberInSet),string(0), string(4))"/>
							<xsl:value-of select="script:LFill(string(@positionInSquare),string(0), string(2))"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="script:LFill(string(ancestor::Machine/@machineId),string(0), string(4))"/>
							<xsl:value-of select="script:LFill(string(@absoluteNumber),string(0), string(4))"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:attribute name="Barcode">
					<xsl:value-of select="$barCode"/>
				</xsl:attribute>
				<!-- Stahlpart ist zwar alles OPTIONAL wir aber generell gefüllt -->
				<!-- Stahlanwahl: 0=kein Stahl, 1=Handeinschub, 2=automatischer Einschub 3=Handeinschub ohne Stahlzuschnittcenter -->
				<xsl:variable name="noSteel">
					<xsl:choose>
						<xsl:when test="parent::CutPiece/@angleA &lt; 45 or parent::CutPiece/@angleB &lt; 45">
							<xsl:value-of select="1"/>
						</xsl:when>
						<!--SD20110222 die Abfrage der Länge des Stahlsd soll weg! laut ThKö-->
						<!--<xsl:when test="Steels/child::Steel/@length &lt; 300">
							<xsl:value-of select="1"/>
						</xsl:when>-->
						<xsl:when test="descendant::Operation[@name='NO_STEEL_OLIVE']">
							<xsl:value-of select="1"/>
						</xsl:when>
						<xsl:when test="descendant::Operation[@name='NO_STEEL']">
							<xsl:value-of select="1"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="0"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<!--xsl:attribute name="test"><xsl:value-of select="$noSteel"/></xsl:attribute-->
				<xsl:choose>
					<!--SD20110222 die Abfrage der Länge des Stahls soll weg! laut ThKö-->
					<!--<xsl:when test="Steels/child::Steel and Steels/child::Steel/@length &gt;= 300">-->
					<xsl:when test="Steels/child::Steel">
						<!--xsl:attribute name="Stahlanwahl"><xsl:value-of select="2"/></xsl:attribute-->
						<!--  2 = automatischer Einschub -->
						<!-- RK101123 Stahlanwahl ist bei Waku immer = 1 -->
						<!-- RK110124 Stahlanwahl ist bei Waku immer = 3  Handeinschub ohne Stahlzuschnittcenter-->
						<xsl:attribute name="Stahlanwahl">
							<xsl:value-of select="3"/>
						</xsl:attribute>
						<!--  1 = Handeinschub -->
						<xsl:attribute name="Stahllaenge">
							<!--<xsl:value-of select="format-number(Steels/child::Steel/@length,'0.0')"/>-->
							<!--SD20110525 nun nach Function GetSteelLength(SteelRef1, SteelLength1, SteelRef2, SteelLength2)-->
							<xsl:value-of select ="$Stahllaenge"/>
						</xsl:attribute>
						<!--  Länge in mm -->
						<xsl:attribute name="Stahlnummer">
							<!--<xsl:value-of select="$stRef"/>-->
							<!--SD20110216 den Stahl der im Teil ist erzeugen-->
							<!--<xsl:value-of select="Steels/Steel/@reference"/>-->
							<!--SD20110525 nun nach GetSteelReference(ref)-->
							<xsl:value-of select ="$SteelReference1"/>
							<xsl:value-of select ="' '"/>
							<xsl:value-of select ="$SteelReference2"/>
							<xsl:value-of select ="' '"/>
							<xsl:value-of select ="$SteelReference3"/>
							<!--Testbereich-->
							<!--<xsl:text>Testbereich</xsl:text>
							<xsl:value-of select ="concat(
										  'GetSteelLength(',
										  $SteelReference1,',',
										  $SteelLength1,',',
										  $SteelReference2,',',
										  $SteelLength2,',',
										  ')')"/>-->
							
						</xsl:attribute>
						<xsl:attribute name="Stahleinschubtiefe">
							<xsl:value-of select="format-number(((ancestor::CutPiece/@length - $Stahllaenge) div 2),'0.0') "/>
						</xsl:attribute>
						<!-- Einschubtiefe in mm -->
						<xsl:attribute name="StahlKammer1">
							<!-- RK110121 soll lt. Email D.Büsge benutzt werden um Fix bez. Individualstahl anzuzeigen-->
							<xsl:choose>
								<xsl:when test="contains(Steels/child::Steel/@reference,'fix')">
									<xsl:value-of select="0"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="1"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
					</xsl:when>
					<xsl:otherwise>
						<xsl:attribute name="Stahlanwahl">
							<xsl:value-of select="0"/>
						</xsl:attribute>
						<!--  kein Stahl -->
						<xsl:attribute name="Stahllaenge">
							<xsl:value-of select="'0.0'"/>
						</xsl:attribute>
						<xsl:attribute name="Stahlnummer"/>
						<xsl:attribute name="Stahleinschubtiefe">
							<xsl:value-of select="'0.0'"/>
						</xsl:attribute>
						<xsl:attribute name="StahlKammer1">
							<xsl:value-of select="0"/>
						</xsl:attribute>
					</xsl:otherwise>
				</xsl:choose>

				<xsl:if test="count(descendant::Operation[contains(@tool, 'SB#')]) &gt; 0">
					<Werkzeugdaten>
						<xsl:attribute name="TeileNr">
							<xsl:value-of select="$pieceNo"/>
						</xsl:attribute>
						<!-- Teilenummer der Stabinstanz -->
						<xsl:apply-templates select="Operations/child::Operation[contains(@tool, 'SB#')]">
							<xsl:sort select="substring(@tool,5)" data-type="number"/>
							<xsl:sort select="@X" data-type="number"/>
							<xsl:with-param name="invOp" select="$invOpAO"/>
							<xsl:with-param name="inst" select="$inst"/>
							<xsl:with-param name="role" select="$role"/>
							<xsl:with-param name="noSteel" select="$noSteel"/>
						</xsl:apply-templates>
					</Werkzeugdaten>
				</xsl:if>
				<Etikettendaten>
					<!-- Teilenummer der Stabinstanz -->
					<xsl:attribute name="TeileNr">
						<xsl:value-of select="format-number($pieceNo,'0')"/>
					</xsl:attribute>

					<!-- Lage des Teiles -->
					<xsl:variable name="lage">
						<xsl:choose>
							<xsl:when test="$role = 'FRAME' or $role='SASH'">
								<xsl:choose>
									<xsl:when test="@angle=90">
										<xsl:value-of select="'R'"/>
									</xsl:when>
									<xsl:when test="@angle=180">
										<xsl:value-of select="'O'"/>
									</xsl:when>
									<xsl:when test="@angle=270">
										<xsl:value-of select="'L'"/>
									</xsl:when>
									<xsl:when test="@angle=360">
										<xsl:value-of select="'U'"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="'S'"/>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:when>
							<xsl:otherwise>
								<xsl:choose>
									<xsl:when test="@angle=90">
										<xsl:value-of select="'SE'"/>
									</xsl:when>
									<xsl:when test="@angle=180">
										<xsl:value-of select="'WA'"/>
									</xsl:when>
									<xsl:when test="@angle=270">
										<xsl:value-of select="'SE'"/>
									</xsl:when>
									<xsl:when test="@angle=360">
										<xsl:value-of select="'WA'"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="'S'"/>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>

					<xsl:variable name="fieldNo">
						<xsl:value-of select="script:GetFieldNo(string(@fittingCode))"/>
					</xsl:variable>

					<Druckdaten>
						<xsl:choose>
							<xsl:when test ="$role = 'FRAME' or $role = 'SASH'">
								<!--1. Zeile-->
								<xsl:value-of select ="'DIR1:AN1:NASC-2:MAG1,1'"/>
								<xsl:text>&#xA;</xsl:text>
								<!--2. Zeile-->
								<xsl:value-of select ="'PP015,230:FT&quot;Swiss 721 BT&quot;,13:PT&quot;'"/>
								<!-- Auftragsnummer-->
								<xsl:value-of select="script:RFill(string(@orderNumber),' ','10')"/>
								<xsl:text> / </xsl:text>
								<!--Position-->
								<xsl:value-of select="script:RFill(string(@nomenclature),' ','6')"/>
								<xsl:text> / </xsl:text>
								<!--Lage-->
								<xsl:value-of select="script:RFill(string($lage),' ',string(5))"/>
								<xsl:value-of select ="'&quot;'"/>
								<xsl:text>&#xA;</xsl:text>
								<!--3. Zeile-->
								<xsl:value-of select ="'PP015,200:FT&quot;Swiss 721 BT&quot;,9:PT&quot;'"/>
								<!--ProfillÃ¤nge-->
								<xsl:value-of select ="format-number(ancestor::CutPiece/@length,'0.0')"/>
								<xsl:text> / </xsl:text>
								<!--Farbe-->
								<xsl:value-of select ="$color"/>
								<xsl:text> / </xsl:text>
								<!--Amierung = LÃ¤nge d. Arm.-->
								<xsl:value-of select ="$SteelReference1"/>
								<xsl:value-of select ="' '"/>
								<xsl:value-of select ="$SteelReference2"/>
								<xsl:text>=</xsl:text>
								<xsl:value-of select="format-number($Stahllaenge,'0.0')"/>
								<xsl:text> / </xsl:text>
								<!--Beschlagsgarnitur (nur eine wenn mehrere!)-->
								<xsl:value-of select ="@prefOpenCode"/>
								<xsl:value-of select ="'&quot;'"/>
								<xsl:text>&#xA;</xsl:text>
								<!--4. Zeile-->
								<xsl:value-of select ="'PP015,160:FT&quot;Swiss 721 BT&quot;,13:PT&quot;'"/>
								<!--Wagen / Fach // Los-Zuschnittlos / Teile Nr.-->
								<!--Wagen-->
								<xsl:choose>
									<xsl:when test="@container &gt; 0">
										<xsl:value-of select="concat('W',script:LFill(string(@container),'0','3'))"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="'W000'"/>
									</xsl:otherwise>
								</xsl:choose>
								<xsl:text> / </xsl:text>
								<!--Fach-->
								<xsl:choose>
									<xsl:when test="@slot &gt; 0">
										<xsl:value-of select="concat('F',script:LFill(string(@slot),'0','2'))"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="'F00'"/>
									</xsl:otherwise>
								</xsl:choose>
								<xsl:text> // </xsl:text>
								<!--Los-->
								<xsl:value-of select ="$lot"/>
								<xsl:text>-</xsl:text>
								<!--Zuschnittlos-->
								<xsl:value-of select ="$set"/>
								<xsl:text> / </xsl:text>
								<!--Teile Nr.-->
								<xsl:value-of select ="$pieceNo"/>
								<xsl:value-of select ="'&quot;'"/>
								<xsl:text>&#xA;</xsl:text>
								<!--5. Zeile-->
								<!--SD20110527 auskommentiert, da nicht benötigt-->
								<!--<xsl:value-of select ="'PP015,100:PL 600, 50'"/>-->
								<!--<xsl:text>&#xA;</xsl:text>-->
								<!--6. Zeile-->
								<xsl:value-of select ="'PP015,50:FT&quot;Swiss 721 BT&quot;,10:PT&quot;'"/>
								<!--Kunde-->
								<xsl:value-of select ="@customerName"/>
								<xsl:value-of select ="'&quot;'"/>
								<xsl:text>&#xA;</xsl:text>
								<!--7. Zeile-->
								<xsl:value-of select ="'PP015,20:FT&quot;Swiss 721 BT&quot;,8:PT&quot;'"/>
								<!--AuÃŸendienst / Zusatzinformation / Kommission-->
								<!--AuÃŸendienst-->
								<xsl:value-of select ="script:RFill(string(@vertreter),' ','18')"/>
								<xsl:text> / </xsl:text>
								<!--Zusatzinformation -->
								<xsl:value-of select ="script:RFill(string(@userData1),' ','18')"/>
								<xsl:text> / </xsl:text>
								<!--Kommission-->
								<xsl:value-of select ="script:RFill(string(@reference),' ','18')"/>
								<xsl:value-of select ="'&quot;'"/>
								<xsl:text>&#xA;</xsl:text>
								<!--8. Zeile-->
								<xsl:value-of select ="'PP534,180:BT&quot;DATAMATRIX&quot;:BM7:BF OFF:PB&quot;'"/>
								<!--2D-Code-->
								<!--Barcode-->
								<xsl:value-of select="$barCode"/>
								<xsl:value-of select ="'&quot;'"/>
								<xsl:text>&#xA;</xsl:text>
								<!--9. Zeile-->
								<xsl:value-of select ="'PF$013'"/>
							</xsl:when>
							<xsl:when test ="$role = 'MULLION'">
								<!--1. Zeile-->
								<xsl:value-of select ="'DIR1:AN1:NASC-2:MAG1,1'"/>
								<xsl:text>&#xA;</xsl:text>
								<!--2. Zeile-->
								<xsl:value-of select ="'PP015,230:FT&quot;Swiss 721 BT&quot;,13:PT&quot;'"/>
								<!-- Auftragsnummer-->
								<xsl:value-of select="script:RFill(string(@orderNumber),' ','10')"/>
								<xsl:text> / </xsl:text>
								<!--Position-->
								<xsl:value-of select="script:RFill(string(@nomenclature),' ','6')"/>
								<xsl:text> / </xsl:text>
								<!--Lage-->
								<xsl:value-of select="script:RFill(string($lage),' ',string(5))"/>
								<xsl:value-of select ="'&quot;'"/>
								<xsl:text>&#xA;</xsl:text>
								<!--3. Zeile-->
								<xsl:value-of select ="'PP015,200:FT&quot;Swiss 721 BT&quot;,9:PT&quot;'"/>
								<!--ProfillÃ¤nge-->
								<xsl:value-of select ="format-number(ancestor::CutPiece/@length,'0.0')"/>
								<xsl:text> / </xsl:text>
								<!--Farbe-->
								<xsl:value-of select ="$color"/>
								<xsl:text> / </xsl:text>
								<!--Amierung = LÃ¤nge d. Arm.-->
								<xsl:value-of select ="$SteelReference1"/>
								<xsl:value-of select ="' '"/>
								<xsl:value-of select ="$SteelReference2"/>
								<xsl:text>=</xsl:text>
								<xsl:value-of select="format-number($Stahllaenge,'0.0')"/>
								<xsl:text> / </xsl:text>
								<!--Beschlagsgarnitur (nur eine wenn mehrere!)-->
								<xsl:value-of select ="@prefOpenCode"/>
								<xsl:value-of select ="'&quot;'"/>
								<xsl:text>&#xA;</xsl:text>
								<!--4. Zeile-->
								<xsl:value-of select ="'PP015,160:FT&quot;Swiss 721 BT&quot;,13:PT&quot;'"/>
								<!--Wagen / Fach // Los-Zuschnittlos / Teile Nr.-->
								<!--Wagen-->
								<xsl:choose>
									<xsl:when test="@container &gt; 0">
										<xsl:value-of select="concat('W',script:LFill(string(@container),'0','3'))"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="'W000'"/>
									</xsl:otherwise>
								</xsl:choose>
								<xsl:text> / </xsl:text>
								<!--Fach-->
								<xsl:choose>
									<xsl:when test="@slot &gt; 0">
										<xsl:value-of select="concat('F',script:LFill(string(@slot),'0','2'))"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="'F00'"/>
									</xsl:otherwise>
								</xsl:choose>
								<xsl:text> // </xsl:text>
								<!--Los-->
								<xsl:value-of select ="$lot"/>
								<xsl:text>-</xsl:text>
								<!--Zuschnittlos-->
								<xsl:value-of select ="$set"/>
								<xsl:text> / </xsl:text>
								<!--Teile Nr.-->
								<xsl:value-of select ="$pieceNo"/>
								<xsl:value-of select ="'&quot;'"/>
								<xsl:text>&#xA;</xsl:text>
								<!--5. Zeile-->
								<xsl:value-of select ="'PP534,180:BT&quot;DATAMATRIX&quot;:BM7:BF OFF:PB&quot;'"/>
								<!--2D-Code-->
								<!--Barcode-->
								<xsl:value-of select="$barCode"/>
								<xsl:value-of select ="'&quot;'"/>
								<xsl:text>&#xA;</xsl:text>
								<!--6. Zeile-->
								<xsl:value-of select ="'PF$013'"/>
							</xsl:when>
							<xsl:otherwise>
								<!--Testweise-->
								<xsl:value-of select ="'PP350,045:DIR1:AN4:FT&quot;SW030RSN&quot;:PT&quot;0001&quot;
PP018,240:DIR1:AN4:FT&quot;Zurich Extra Condensed BT&quot;:PT&quot;A Nr: 210111671  S-Nr: 104143&quot;
PP450,240:DIR1:AN4:FT&quot;Swiss 721 Bold Condensed BT&quot;:PT&quot;W: 1   F: 12&quot;
PP018,210:DIR1:AN4:FT&quot;Zurich Extra Condensed BT&quot;:PT&quot;Pos.: 8   &quot;
PP500,210:DIR1:AN4:FT&quot;Zurich Extra Condensed BT&quot;:PT&quot; 8337 82mm &quot;
PP030,095:DIR1:AN1:BT&quot;CODE39&quot;:BH80:BF&quot;SW020BSN&quot;:BF ON:PB&quot;1297044001100006&quot;
PP500,075:DIR1:AN4:FT&quot;Zurich Extra Condensed BT&quot;:PT&quot;St-L: 796  mm &quot;
PP500,045:DIR1:AN4:FT&quot;Zurich Extra Condensed BT&quot;:PT&quot;Ra-L: 936  mm&quot;
PP018,075:DIR1:AN4:FT&quot;Zurich Extra Condensed BT&quot;:PT&quot;DKL/FOF/DKR/ dreh kipp li&quot;
PP350,075:DIR1:AN4:FT&quot;Zurich Extra Condensed BT&quot;:PT&quot;PFOSTE&quot;
PP018,045:DIR1:AN4:FT&quot;Zurich Extra Condensed BT&quot;:PT&quot;MultiMatic GS       &quot;
PP018,015:DIR1:AN4:FT&quot;Zurich Extra Condensed BT&quot;:PT&quot;Entw.Art:MPF vorne       - Anschlag  &quot;
PF$13'"/>
							</xsl:otherwise>
						</xsl:choose>
					</Druckdaten>

					<TintenDruckdaten>
						<xsl:choose>
							<xsl:when test ="$role = 'FRAME'">
								<xsl:text>BM$010SF$010/opt/storage/</xsl:text>
								<!-- Dateiname -->
								<!--xsl:value-of select="concat(format-number($lot,'000000'), format-number($set,'00'),'.xml')"/-->
								<!-- RK101208 lt. Herrn Buesge soll hier die Profilbezeichnung rein, da fÃ¼r jede Referenze ein Layout definiert ist -->
								<xsl:value-of select="concat(script:GetMachRef(string(ancestor::Rod/@reference)),'.xml')"/>
								<!-- Kennung Text 0 -->
								<xsl:text>$010RM$010T0$010</xsl:text>
								<!-- Auftragsnummer-->
								<xsl:value-of select="script:RFill(string(@orderNumber),' ','10')"/>
								<xsl:text> / </xsl:text>
								<!--Position-->
								<xsl:value-of select="script:RFill(string(@nomenclature),' ','6')"/>
								<xsl:text> / </xsl:text>
								<!--Lage-->
								<xsl:value-of select="script:RFill(string($lage),' ',string(5))"/>
								<xsl:text>$010</xsl:text>
								<!-- Kennung Text 1 -->
								<xsl:text>T1$010</xsl:text>
								<!--ProfillÃ¤nge-->
								<xsl:value-of select ="format-number(ancestor::CutPiece/@length,'0.0')"/>
								<xsl:text> / </xsl:text>
								<!--Farbe-->
								<xsl:value-of select ="$color"/>
								<xsl:text> / </xsl:text>
								<!--Amierung = LÃ¤nge d. Arm.-->
								<xsl:value-of select ="$SteelReference1"/>
								<xsl:value-of select ="' '"/>
								<xsl:value-of select ="$SteelReference2"/>
								<xsl:text>=</xsl:text>
								<xsl:value-of select="format-number($Stahllaenge,'0.0')"/>
								<xsl:text> / </xsl:text>
								<!--Beschlagsgarnitur (nur eine wenn mehrere!)-->
								<xsl:value-of select ="@prefOpenCode"/>
								<xsl:text>$010</xsl:text>
								<!-- Kennung Text 2 -->
								<xsl:text>T2$010</xsl:text>
								<!--Wagen / Fach // Los-Zuschnittlos / Teile Nr.-->
								<!--Wagen-->
								<xsl:choose>
									<xsl:when test="@container &gt; 0">
										<xsl:value-of select="concat('W',script:LFill(string(@container),'0','3'))"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="'W000'"/>
									</xsl:otherwise>
								</xsl:choose>
								<xsl:text> / </xsl:text>
								<!--Fach-->
								<xsl:choose>
									<xsl:when test="@slot &gt; 0">
										<xsl:value-of select="concat('F',script:LFill(string(@slot),'0','2'))"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="'F00'"/>
									</xsl:otherwise>
								</xsl:choose>
								<xsl:text> // </xsl:text>
								<!--Los-->
								<xsl:value-of select ="$lot"/>
								<xsl:text>-</xsl:text>
								<!--Zuschnittlos-->
								<xsl:value-of select ="$set"/>
								<xsl:text> / </xsl:text>
								<!--Teile Nr.-->
								<xsl:value-of select ="$pieceNo"/>
								<xsl:text>$010</xsl:text>
								<!-- Kennung Text 3 -->
								<xsl:text>T3$010</xsl:text>
								<!--2D-Code-->
								<!--Barcode-->
								<xsl:value-of select="$barCode"/>
								<xsl:text>$010</xsl:text>
								<!-- Kennung Text 4 -->
								<xsl:text>T4$010</xsl:text>
								<!--Kunde-->
								<xsl:value-of select ="@customerName"/>
								<xsl:text>$010</xsl:text>
								<!-- Kennung Text 5 -->
								<xsl:text>T5$010</xsl:text>
								<!--AuÃŸendienst / Zusatzinformation / Kommission-->
								<!--AuÃŸendienst-->
								<xsl:value-of select ="script:RFill(string(@vertreter),' ','18')"/>
								<xsl:text> / </xsl:text>
								<!--Zusatzinformation -->
								<xsl:value-of select ="script:RFill(string(@userData1),' ','18')"/>
								<xsl:text> / </xsl:text>
								<!--Kommission-->
								<xsl:value-of select ="script:RFill(string(@reference),' ','18')"/>
								<xsl:text>$010</xsl:text>
							</xsl:when>
							<xsl:when test ="$role = 'MULLION'">
								<xsl:text>BM$010SF$010/opt/storage/</xsl:text>
								<!-- Dateiname -->
								<!--xsl:value-of select="concat(format-number($lot,'000000'), format-number($set,'00'),'.xml')"/-->
								<!-- RK101208 lt. Herrn Buesge soll hier die Profilbezeichnung rein, da fÃ¼r jede Referenze ein Layout definiert ist -->
								<xsl:value-of select="concat(script:GetMachRef(string(ancestor::Rod/@reference)),'.xml')"/>
								<!-- Kennung Text 0 -->
								<xsl:text>$010RM$010T0$010</xsl:text>
								<!-- Auftragsnummer-->
								<xsl:value-of select="script:RFill(string(@orderNumber),' ','10')"/>
								<xsl:text> / </xsl:text>
								<!--Position-->
								<xsl:value-of select="script:RFill(string(@nomenclature),' ','6')"/>
								<xsl:text> / </xsl:text>
								<!--Lage-->
								<xsl:value-of select="script:RFill(string($lage),' ',string(5))"/>
								<xsl:text>$010</xsl:text>
								<!-- Kennung Text 1 -->
								<xsl:text>T1$010</xsl:text>
								<!--ProfillÃ¤nge-->
								<xsl:value-of select ="format-number(ancestor::CutPiece/@length,'0.0')"/>
								<xsl:text> / </xsl:text>
								<!--Farbe-->
								<xsl:value-of select ="$color"/>
								<xsl:text> / </xsl:text>
								<!--Amierung = LÃ¤nge d. Arm.-->
								<xsl:value-of select ="$SteelReference1"/>
								<xsl:value-of select ="' '"/>
								<xsl:value-of select ="$SteelReference2"/>
								<xsl:text>=</xsl:text>
								<xsl:value-of select="format-number($Stahllaenge,'0.0')"/>
								<xsl:text> / </xsl:text>
								<!--Beschlagsgarnitur (nur eine wenn mehrere!)-->
								<xsl:value-of select ="@prefOpenCode"/>
								<xsl:text>$010</xsl:text>
								<!-- Kennung Text 2 -->
								<xsl:text>T2$010</xsl:text>
								<!--Wagen / Fach // Los-Zuschnittlos / Teile Nr.-->
								<!--Wagen-->
								<xsl:choose>
									<xsl:when test="@container &gt; 0">
										<xsl:value-of select="concat('W',script:LFill(string(@container),'0','3'))"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="'W000'"/>
									</xsl:otherwise>
								</xsl:choose>
								<xsl:text> / </xsl:text>
								<!--Fach-->
								<xsl:choose>
									<xsl:when test="@slot &gt; 0">
										<xsl:value-of select="concat('F',script:LFill(string(@slot),'0','2'))"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="'F00'"/>
									</xsl:otherwise>
								</xsl:choose>
								<xsl:text> // </xsl:text>
								<!--Los-->
								<xsl:value-of select ="$lot"/>
								<xsl:text>-</xsl:text>
								<!--Zuschnittlos-->
								<xsl:value-of select ="$set"/>
								<xsl:text> / </xsl:text>
								<!--Teile Nr.-->
								<xsl:value-of select ="$pieceNo"/>
								<xsl:text>$010</xsl:text>
								<!-- Kennung Text 3 -->
								<xsl:text>T3$010</xsl:text>
								<!--AuÃŸendienst / Zusatzinformation / Kommission-->
								<!--AuÃŸendienst-->
								<xsl:value-of select ="script:RFill(string(@vertreter),' ','18')"/>
								<xsl:text> / </xsl:text>
								<!--Zusatzinformation -->
								<xsl:value-of select ="script:RFill(string(@userData1),' ','18')"/>
								<xsl:text> / </xsl:text>
								<!--Kommission-->
								<xsl:value-of select ="script:RFill(string(@reference),' ','18')"/>
								<xsl:text>$010</xsl:text>
								<!-- Kennung Text 4 -->
								<xsl:text>T4$010</xsl:text>
								<!--Kunde-->
								<xsl:value-of select ="@customerName"/>
								<xsl:text>$010</xsl:text>
								<!-- Kennung Text 5 -->
								<xsl:text>T5$010</xsl:text>
								<!--2D-Code-->
								<!--Barcode-->
								<xsl:value-of select="$barCode"/>
								<xsl:text>$010</xsl:text>
							</xsl:when>
							<xsl:when test ="$role = 'SASH'">
								<xsl:text>BM$010SF$010/opt/storage/</xsl:text>
								<!-- Dateiname -->
								<!--xsl:value-of select="concat(format-number($lot,'000000'), format-number($set,'00'),'.xml')"/-->
								<!-- RK101208 lt. Herrn Buesge soll hier die Profilbezeichnung rein, da fÃ¼r jede Referenze ein Layout definiert ist -->
								<xsl:value-of select="concat(script:GetMachRef(string(ancestor::Rod/@reference)),'.xml')"/>
								<!-- Kennung Text 0 -->
								<xsl:text>$010RM$010T0$010</xsl:text>
								<!-- Auftragsnummer-->
								<xsl:value-of select="script:RFill(string(@orderNumber),' ','10')"/>
								<xsl:text> / </xsl:text>
								<!--Position-->
								<xsl:value-of select="script:RFill(string(@nomenclature),' ','6')"/>
								<xsl:text> / </xsl:text>
								<!--Lage-->
								<xsl:value-of select="script:RFill(string($lage),' ',string(5))"/>
								<xsl:text> / </xsl:text>
								<!--Feld-->
								<xsl:value-of select ="script:GetFieldNo(string(@fittingCode))"/>
								<xsl:text>$010</xsl:text>
								<!-- Kennung Text 1 -->
								<xsl:text>T1$010</xsl:text>
								<!--ProfillÃ¤nge-->
								<xsl:value-of select ="format-number(ancestor::CutPiece/@length,'0.0')"/>
								<xsl:text> / </xsl:text>
								<!--Farbe-->
								<xsl:value-of select ="$color"/>
								<xsl:text> / </xsl:text>
								<!--Amierung = LÃ¤nge d. Arm.-->
								<xsl:value-of select ="$SteelReference1"/>
								<xsl:value-of select ="' '"/>
								<xsl:value-of select ="$SteelReference2"/>
								<xsl:text>=</xsl:text>
								<xsl:value-of select="format-number($Stahllaenge,'0.0')"/>
								<xsl:text> / </xsl:text>
								<!--Beschlagsgarnitur (nur eine wenn mehrere!)-->
								<xsl:value-of select ="@prefOpenCode"/>
								<xsl:text>$010</xsl:text>
								<!-- Kennung Text 2 -->
								<xsl:text>T2$010</xsl:text>
								<!--Wagen / Fach // Los-Zuschnittlos / Teile Nr.-->
								<!--Wagen-->
								<xsl:choose>
									<xsl:when test="@container &gt; 0">
										<xsl:value-of select="concat('W',script:LFill(string(@container),'0','3'))"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="'W000'"/>
									</xsl:otherwise>
								</xsl:choose>
								<xsl:text> / </xsl:text>
								<!--Fach-->
								<xsl:choose>
									<xsl:when test="@slot &gt; 0">
										<xsl:value-of select="concat('F',script:LFill(string(@slot),'0','2'))"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="'F00'"/>
									</xsl:otherwise>
								</xsl:choose>
								<xsl:text> // </xsl:text>
								<!--Los-->
								<xsl:value-of select ="$lot"/>
								<xsl:text>-</xsl:text>
								<!--Zuschnittlos-->
								<xsl:value-of select ="$set"/>
								<xsl:text> / </xsl:text>
								<!--Teile Nr.-->
								<xsl:value-of select ="$pieceNo"/>
								<xsl:text>$010</xsl:text>
								<!-- Kennung Text 3 -->
								<xsl:text>T3$010</xsl:text>
								<!--AuÃŸendienst / Zusatzinformation / Kommission-->
								<!--AuÃŸendienst-->
								<xsl:value-of select ="script:RFill(string(@salesmanName),'*','18')"/>
								<xsl:text> / </xsl:text>
								<!--Zusatzinformation -->
								<xsl:value-of select ="script:RFill(string(@userData1),'*','18')"/>
								<xsl:text> / </xsl:text>
								<!--Kommission-->
								<xsl:value-of select ="script:RFill(string(@reference),'*','18')"/>
								<xsl:text>$010</xsl:text>
								<!-- Kennung Text 4 -->
								<xsl:text>T4$010</xsl:text>
								<!--Kunde-->
								<xsl:value-of select ="@customerName"/>
								<xsl:text>$010</xsl:text>
								<!-- Kennung Text 5 -->
								<xsl:text>T5$010</xsl:text>
								<!--2D-Code-->
								<!--Barcode-->
								<xsl:value-of select="$barCode"/>
								<xsl:text>$010</xsl:text>
							</xsl:when>
						</xsl:choose>
					</TintenDruckdaten>
				</Etikettendaten>
			</Teiledaten>
		</xsl:if>
		<xsl:if test="$kind='ST'">
			<xsl:variable name="nu" select="@number"/>
			<xsl:variable name="ve" select="@version"/>
			<xsl:variable name="po" select="@position"/>
			<xsl:variable name="in" select="@instance"/>
			<xsl:variable name="id" select="Steels/child::Steel/@Id"/>
			<xsl:variable name="steelP" select="//descendant::CutInstance[@number= $nu and @version= $ve and @position= $po and @instance= $in and @Id= $id]"/>
			<!-- Stahl weglassen wenn An- bzw- Abschnittwinkel < 45 -->
			<!-- auch weglassen, wenn Stahllänge < 300 mm ist -->
			<xsl:variable name="noSt">
				<xsl:choose>
					<xsl:when test="parent::CutPiece/@angleA &lt; 45 or parent::CutPiece/@angleB &lt; 45">
						<xsl:value-of select="1"/>
					</xsl:when>
					<xsl:when test="Steels/child::Steel/@length &lt; 300">
						<xsl:value-of select="1"/>
					</xsl:when>
					<xsl:when test="descendant::Operation[@name='NO_STEEL_OLIVE']">
						<xsl:value-of select="1"/>
					</xsl:when>
					<xsl:when test="descendant::Operation[@name='NO_STEEL']">
						<xsl:value-of select="1"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="0"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:if test="$noSt=0 and Steels/child::Steel">
				<StahlTeiledaten>
					<xsl:attribute name="StabNr">
						<xsl:value-of select="$rodNo"/>
					</xsl:attribute>
					<!-- lfd. Stabnummer -->
					<xsl:attribute name="TeileNr">
						<xsl:value-of select="$pieceNo"/>
					</xsl:attribute>
					<xsl:attribute name="Bezeichnung">
						<!--<xsl:value-of select="$stRef"/>-->
						<!--SD20110216 den Stahl der im Teil ist erzeugen-->
						<!--<xsl:value-of select="Steels/Steel/@reference"/>-->
						<!--SD20110525 nun nach GetSteelReference(ref)-->
						<xsl:value-of select ="$SteelReference1"/>
						<xsl:value-of select ="' '"/>
						<xsl:value-of select ="$SteelReference2"/>
					</xsl:attribute>
					<xsl:attribute name="Kommentar"/>
					<!--  OPTIONAL bleibt leer max 255 Zeichen -->
					<xsl:attribute name="Kommission">
						<xsl:value-of select="concat(@orderNumber,'.',@version)"/>
					</xsl:attribute>
					<!--  OPTIONAL wird mit Auftrag . Version gefüllt -->
					<xsl:attribute name="Position">
						<xsl:value-of select="@nomenclature"/>
					</xsl:attribute>
					<!--  OPTIONAL wird mit Nomenclature gefüllt -->
					<xsl:attribute name="Laenge">
						<!--  Länge in mm -->
						<xsl:choose>
							<xsl:when test="Steels/child::Steel">
								<xsl:value-of select="format-number($Stahllaenge,'0.0')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="0"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:attribute>
					<xsl:attribute name="AnGehrung">
						<xsl:value-of select="'90'"/>
					</xsl:attribute>
					<xsl:attribute name="AbGehrung">
						<xsl:value-of select="'90'"/>
					</xsl:attribute>
					<!-- Winkelangaben für Spitz- oder Kappschnitt -->
					<xsl:attribute name="AngleOnCutID">
						<xsl:value-of select="'0'"/>
					</xsl:attribute>
					<xsl:attribute name="AngleOffCutID">
						<xsl:value-of select="'0'"/>
					</xsl:attribute>
					<!-- Stahlpart ist zwar alles OPTINAL wir aber generell gefüllt -->
					<!-- Stahlanwahl: 0=kein Stahl, 1=Handeinschub, 2=automatischer Einschub, 3=Handeinschub ohne Stahlzuschnittcenter -->
					<xsl:variable name="toShort">
						<xsl:choose>
							<xsl:when test="parent::CutPiece/@length &lt; 500">
								<xsl:value-of select="1"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="0"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					<xsl:choose>
						<xsl:when test="Steels/child::Steel">
							<!--  immer Handeinschub bei Waku -->
							<!-- RK110124 Stahlanwahl ist bei Waku immer = 3  Handeinschub ohne Stahlzuschnittcenter-->
							<xsl:attribute name="Stahlanwahl">
								<xsl:value-of select="3"/>
							</xsl:attribute>
							<!-- Einschubtiefe in mm -->
							<xsl:attribute name="Stahleinschubtiefe">
								<xsl:value-of select="format-number(((ancestor::CutPiece/@length - $Stahllaenge) div 2),'0.0') "/>
							</xsl:attribute>
						</xsl:when>
						<xsl:otherwise>
							<xsl:attribute name="Stahlanwahl">
								<xsl:value-of select="0"/>
							</xsl:attribute>
							<!--  kein Stahl -->
							<xsl:attribute name="Stahleinschubtiefe">
								<xsl:value-of select="'0.0'"/>
							</xsl:attribute>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:attribute name="PVCTeileNr">
						<xsl:value-of select="$pieceNo"/>
					</xsl:attribute>
					<!--xsl:attribute name="PVCLosName"><xsl:value-of select="$file"/></xsl:attribute-->
				</StahlTeiledaten>
			</xsl:if>
		</xsl:if>
	</xsl:template>

	<xsl:template match="Operation">
		<xsl:param name="invOp"/>
		<xsl:param name="inst"/>
		<xsl:param name="role"/>
		<xsl:param name="noSteel"/>
		<xsl:variable name="opAllowed">
			<xsl:value-of select="1"/>
		</xsl:variable>
		<xsl:if test="$opAllowed=1">
			<ProfilBearb>
				<!-- RK101014 Manchen Schließteilbearbeitungen werden im Kömmerling PS mit anderen Wkz gemacht -->
				<xsl:variable name="Wkz">
					<xsl:choose>
						<xsl:when test="starts-with(ancestor::Rod[1]/@reference,'KO')">
							<xsl:choose>
								<xsl:when test="@tool = 'SB#146'">
									<xsl:value-of select="'158'"/>
								</xsl:when>
								<!-- KSB56 -->
								<xsl:when test="@tool = 'SB#176'">
									<xsl:value-of select="'188'"/>
								</xsl:when>
								<!-- KSB56 2.S -->
								<xsl:when test="@tool = 'SB#150'">
									<xsl:value-of select="'160'"/>
								</xsl:when>
								<!-- KFRZV02 -->
								<xsl:when test="@tool = 'SB#180'">
									<xsl:value-of select="'190'"/>
								</xsl:when>
								<!-- KFRZV02 2.S -->
								<xsl:when test="@tool = 'SB#151'">
									<xsl:value-of select="'163'"/>
								</xsl:when>
								<!-- KSRBPS -->
								<xsl:when test="@tool = 'SB#181'">
									<xsl:value-of select="'193'"/>
								</xsl:when>
								<!-- KSRBPS 2.S -->
								<xsl:when test="@tool = 'SB#155'">
									<xsl:value-of select="'165'"/>
								</xsl:when>
								<!-- RTSP -->
								<xsl:when test="@tool = 'SB#185'">
									<xsl:value-of select="'195'"/>
								</xsl:when>
								<!-- RTSP 2.S -->
								<xsl:when test="@tool = 'SB#156'">
									<xsl:value-of select="'162'"/>
								</xsl:when>
								<!-- KFRUP01 -->
								<xsl:when test="@tool = 'SB#186'">
									<xsl:value-of select="'192'"/>
								</xsl:when>
								<!-- KFRUP01 2.S -->
								<xsl:otherwise>
									<xsl:value-of select="substring(@tool,4)"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="substring(@tool,4)"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<!-- Testbereich -->
				<!--xsl:attribute name="Test"><xsl:value-of select="concat('Tool: ',$Wkz, '  Side: ', @side,'  orgTool: ', @tool , '  InvOp: ', $invOp)"/>/></xsl:attribute-->
				<!-- Ende Testbereich -->
				<xsl:attribute name="BNr">
					<!-- Bearbeitungsnummer ist unsere Werkzeugnummer-->
					<xsl:value-of select="$Wkz"/>
				</xsl:attribute>
				<xsl:variable name="xPos">
					<xsl:choose>
						<xsl:when test="$addWeldingLost=1">
							<xsl:value-of select="format-number(script:GetOperationX(string(@X),string(ancestor::CutPiece/@length), string(ancestor::CutInstance/@weldAddedA), string($invOp),string($decDel),string($role)),'0.0')"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="format-number(script:GetOperationX(string(@X),string(ancestor::CutPiece/@length), string(0), string($invOp),string($decDel),string($role)),'0.0')"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:attribute name="XPos">
					<xsl:value-of select="$xPos"/>
				</xsl:attribute>
				<!-- Bearbeitungsposition auf der Längsachse -->
				<xsl:attribute name="Bezeichnung">
					<xsl:value-of select="script:ReplaceSigns(string(@description))"/>
				</xsl:attribute>
				<!-- Bearbeitungsbeschreibung max 255 Zeichen  wird von uns nicht genutzt-->
				<xsl:attribute name="Kommentar"/>
				<!-- max 255 Zeichen   wird von uns nicht genutzt-->
			</ProfilBearb>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>