<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="\\192.168.1.202\share\XSLT\xsltUrbanAKS6400.xslt"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:math="urn:schemas-cagle-com:math" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:script="Myscript">
	<xsl:output version="4.0" omit-xml-declaration="yes" indent="yes" method="text"/>
	<!-- _______________________________________________________ Root ____________________________________________________________ -->
	<msxsl:script language="VBScript" implements-prefix="script"><![CDATA[ 
		Dim sSort
		Dim cnt
 		Dim currentLocale
 		Dim op
		
 		 		
		' Get the current locale
		currentLocale = GetLocale

		Function SetLocaleFirst()
		  Dim original
		  original = SetLocale("en-gb")
		  SetLocaleFirst = ""
  		End Function
	
		Function SetLocaleEnd()
			Dim original
			original = SetLocale(currentLocale)
			SetLocaleEnd = ""
		End Function

        Function RFill(name, sign, times)
            Dim sStr
            sStr = name & String(times, sign)
            RFill = Left(sStr, times)
        End Function
        
        Function LFill(name, sign, times)
            Dim s
            s = String(times, sign) & name
            LFill = Right(s, times)
        End Function

        Function SortSquareAdd(sqNo)
            sSort = sSort & "#" & CStr(sqNo)
            SortSquareAdd = sSort
        End Function
        
        Function SortSquareSetBack()
            sSort = ""
            SortSquareSetBack = sSort
        End Function
        
        Function GetSortSquare()
            GetSortSquare = sSort
        End Function

        Function GetPartner(sort, sq, p)
            Dim v
            Dim i
            Dim s
            GetPartner = ""
            v = Split(sort, "#")
            If UBound(v) > 1 Then
                For i = 0 To UBound(v)
                    If v(i) = sq Then Exit For
                Next
                If p = "P" Then
                    i = i - 1
                    If i > 0 Then GetPartner = v(i)
                Else
                    i = i + 1
                    If i <= UBound(v) Then GetPartner = v(i)
                End If
            End If
        End Function

        Function IncCnt()
            cnt = cnt + 1
            IncCnt = cnt
        End Function
        
        Function SetCntBack()
            cnt = 0
            SetCntBack = cnt
        End Function
        
		Dim verarbeiteteKaempfer
		Function CheckVerarbeiteteKaempfer(Kaempfer)
			Kaempfer = CStr(Kaempfer)
			verarbeiteteKaempfer = CStr(verarbeiteteKaempfer)
			CheckVerarbeiteteKaempfer = 0
			If Not (InStr(verarbeiteteKaempfer, Kaempfer) > 0) Then
				CheckVerarbeiteteKaempfer = 1
				verarbeiteteKaempfer = verarbeiteteKaempfer & "#" & Kaempfer
			End If
		End Function

		Function InitVerarbeiteteKaempfer()
			verarbeiteteKaempfer = ""
			InitVerarbeiteteKaempfer = verarbeiteteKaempfer
		End Function
		
		Function GetKaempfertyp(material, angle)
			'@WAKU
			'Hier bitte selbstaendig erweitern!
			'0 = Kein
			'3 = waagerecht vorgefertigt
			'4 = senkrecht vorgefertigt
			'7 = waagerecht vorgefertigt - nicht verputzen
			'8 = senkrecht vorgefertigt - nicht verputzen
			GetKaempfertyp = 0
		    
			'If InStr(material, "_GS") > 0 Then
				'geschweisste Teiler
				'waagerechte Teiler Kaempfertyp = 3
				If angle = 270 Or angle = 90 Then
					If InStr(material, "2621") Then GetKaempfertyp = 3
				End If
		        
				'senkrechte Teiler Kaempfertyp = 4
				If angle = 360 Or angle = 180 Then
					If InStr(material, "2621") Then GetKaempfertyp = 4
				End If
			'End If
		End Function
		
		Function getKaempfer(HorizontaleTeiler, VertikaleTeiler)
			Dim HorizontaleTeiler1
			Dim HorizontaleTeiler2
			Dim VertikaleTeiler1
			Dim VertikaleTeiler2
			Dim Teiler
			'Horizontale Teiler
			Teiler = Split(HorizontaleTeiler, "#")
			If UBound(Teiler) > 0 Then
				If Teiler(0) = "" Then
					HorizontaleTeiler1 = "            ;0000.00;0;"
				Else
					HorizontaleTeiler1 = Teiler(0)
				End If
			Else
				HorizontaleTeiler1 = "            ;0000.00;0;"
			End If
		        
			If UBound(Teiler) > 1 Then
				If Teiler(1) = "" Then
					HorizontaleTeiler2 = "            ;0000.00;0;"
				Else
					HorizontaleTeiler2 = Teiler(1)
				End If
			Else
				HorizontaleTeiler2 = "            ;0000.00;0;"
			End If
		    
			'Vertikale Teiler
			Teiler = Split(VertikaleTeiler, "#")
			If UBound(Teiler) > 0 Then
				If Teiler(0) = "" Then
					VertikaleTeiler1 = "            ;0000.00;0;"
				Else
					VertikaleTeiler1 = Teiler(0)
				End If
			Else
				VertikaleTeiler1 = "            ;0000.00;0;"
			End If
			If UBound(Teiler) > 1 Then
				If Teiler(1) = "" Then
					VertikaleTeiler2 = "            ;0000.00;0;"
				Else
					VertikaleTeiler2 = Teiler(1)
				End If
			Else
				VertikaleTeiler2 = "            ;0000.00;0;"
			End If
		    
			getKaempfer = HorizontaleTeiler1 & HorizontaleTeiler2 & VertikaleTeiler1 & VertikaleTeiler2
		    
		End Function
		
		Dim esl(12)
		
		Function InitESL()
			Dim i
			InitESL = ""
			For i = 1 To UBound(esl)
				esl(i) = 0
			Next
		End Function	
		
		Function setESL(index, value)
			setESL = ""
			esl(index) = value
		End Function

		Function GetESL()
			Dim i
			GetESL = ""
			For i = 1 To UBound(esl)
				GetESL = GetESL & esl(i)
			Next
		End Function	
		
		Function GetUrbanProfilbezeichnung(ref)
			GetUrbanProfilbezeichnung = ""
			'@WAKU: hier bitte selbständig erweitern
			'-----------------------------
			'TROCAL     ------------------
			'-----------------------------

If InStr(ref, "TR-810316 2GO") Then GetUrbanProfilbezeichnung = "810300CC"
If InStr(ref, "TR-810316 2OH") Then GetUrbanProfilbezeichnung = "810300CC"
If InStr(ref, "TR-810316 654") Then GetUrbanProfilbezeichnung = "810300A"
If InStr(ref, "TR-810316 RR") Then GetUrbanProfilbezeichnung = "810300CC"
If InStr(ref, "TR-810316 WeR") Then GetUrbanProfilbezeichnung = "810300A"
If InStr(ref, "TR-810316RALT") Then GetUrbanProfilbezeichnung = "810300A"
If InStr(ref, "TR-810316WE/RALT") Then GetUrbanProfilbezeichnung = "810300A"
If InStr(ref, "TR-810316 AG_167") Then GetUrbanProfilbezeichnung = "810300WC"
If InStr(ref, "TR-810316 AG_83") Then GetUrbanProfilbezeichnung = "810300WC"
If InStr(ref, "TR-810316 BR") Then GetUrbanProfilbezeichnung = "810300WC"
If InStr(ref, "TR-810316 GO") Then GetUrbanProfilbezeichnung = "810300WC"
If InStr(ref, "TR-810316 GR") Then GetUrbanProfilbezeichnung = "810300WC"
If InStr(ref, "TR-810316 MA") Then GetUrbanProfilbezeichnung = "810300WC"
If InStr(ref, "TR-810316 OH") Then GetUrbanProfilbezeichnung = "810300WC"
If InStr(ref, "TR-810316 R_LK1") Then GetUrbanProfilbezeichnung = "810300WC"
If InStr(ref, "TR-810316 R_LK3") Then GetUrbanProfilbezeichnung = "810300WC"
If InStr(ref, "TR-810316 R_LK4") Then GetUrbanProfilbezeichnung = "810300WC"
If InStr(ref, "TR-810316 R_LK5") Then GetUrbanProfilbezeichnung = "810300WC"
If InStr(ref, "TR-810316 R_LK6") Then GetUrbanProfilbezeichnung = "810300WC"
If InStr(ref, "TR-810316 SG_83") Then GetUrbanProfilbezeichnung = "810300WC"
If InStr(ref, "TR-810316RAL/WET") Then GetUrbanProfilbezeichnung = "810300WW"
If InStr(ref, "TR-810366 2GO") Then GetUrbanProfilbezeichnung = "810300CCD"
If InStr(ref, "TR-810366 2OH") Then GetUrbanProfilbezeichnung = "810300CCD"
If InStr(ref, "TR-810366 654") Then GetUrbanProfilbezeichnung = "810300WWD"
If InStr(ref, "TR-810366 BR") Then GetUrbanProfilbezeichnung = "810300WCD"
If InStr(ref, "TR-810366 GO") Then GetUrbanProfilbezeichnung = "810300WCD"
If InStr(ref, "TR-810366 GR") Then GetUrbanProfilbezeichnung = "810300WCD"
If InStr(ref, "TR-810366 MA") Then GetUrbanProfilbezeichnung = "810300WCD"
If InStr(ref, "TR-810366 OH") Then GetUrbanProfilbezeichnung = "810300WCD"
If InStr(ref, "TR-810366 AG_167") Then GetUrbanProfilbezeichnung = "810300WCD"
If InStr(ref, "TR-810366 AG_83") Then GetUrbanProfilbezeichnung = "810300WCD"
If InStr(ref, "TR-810366 SG_167") Then GetUrbanProfilbezeichnung = "810300WCD"
If InStr(ref, "TR-810366 SG_83") Then GetUrbanProfilbezeichnung = "810300WCD"
If InStr(ref, "TR-810366 RR") Then GetUrbanProfilbezeichnung = "810300CCD"
If InStr(ref, "TR-810366 RWe") Then GetUrbanProfilbezeichnung = "810300WCD"
If InStr(ref, "TR-810366 WeR") Then GetUrbanProfilbezeichnung = "810300WWD"
If InStr(ref, "TR-810366RAL/WET") Then GetUrbanProfilbezeichnung = "810300WWD"
If InStr(ref, "TR-810366RALT") Then GetUrbanProfilbezeichnung = "810300WW"
If InStr(ref, "TR-810416 2GO") Then GetUrbanProfilbezeichnung = "810400CC"
If InStr(ref, "TR-810416 2OH") Then GetUrbanProfilbezeichnung = "810400CC"
If InStr(ref, "TR-810416 654") Then GetUrbanProfilbezeichnung = "810400A"
If InStr(ref, "TR-810416 WeR") Then GetUrbanProfilbezeichnung = "810400A"
If InStr(ref, "TR-810416RAL/WET") Then GetUrbanProfilbezeichnung = "810400A"
If InStr(ref, "TR-810416 AG_167") Then GetUrbanProfilbezeichnung = "810400WC"
If InStr(ref, "TR-810416 AG_83") Then GetUrbanProfilbezeichnung = "810400WC"
If InStr(ref, "TR-810416 BR") Then GetUrbanProfilbezeichnung = "810400WC"
If InStr(ref, "TR-810416 GO") Then GetUrbanProfilbezeichnung = "810400WC"
If InStr(ref, "TR-810416 GR") Then GetUrbanProfilbezeichnung = "810400WC"
If InStr(ref, "TR-810416 MA") Then GetUrbanProfilbezeichnung = "810400WC"
If InStr(ref, "TR-810416 OH") Then GetUrbanProfilbezeichnung = "810400WC"
If InStr(ref, "TR-810416 R_LK1") Then GetUrbanProfilbezeichnung = "810400WC"
If InStr(ref, "TR-810416 R_LK3") Then GetUrbanProfilbezeichnung = "810400WC"
If InStr(ref, "TR-810416 R_LK4") Then GetUrbanProfilbezeichnung = "810400WC"
If InStr(ref, "TR-810416 R_LK5") Then GetUrbanProfilbezeichnung = "810400WC"
If InStr(ref, "TR-810416 R_LK6") Then GetUrbanProfilbezeichnung = "810400WC"
If InStr(ref, "TR-810416 RR") Then GetUrbanProfilbezeichnung = "810400CC"
If InStr(ref, "TR-810416 SG_83") Then GetUrbanProfilbezeichnung = "810400WC"
If InStr(ref, "TR-810416RAL/WET") Then GetUrbanProfilbezeichnung = "810400WW"
If InStr(ref, "TR-810416RALT") Then GetUrbanProfilbezeichnung = "810400WW"
If InStr(ref, "TR-810466 2GO") Then GetUrbanProfilbezeichnung = "810400CCD"
If InStr(ref, "TR-810466 2OH") Then GetUrbanProfilbezeichnung = "810400CCD"
If InStr(ref, "TR-810466 654") Then GetUrbanProfilbezeichnung = "810400WWD"
If InStr(ref, "TR-810466 BR") Then GetUrbanProfilbezeichnung = "810400WCD"
If InStr(ref, "TR-810466 GO") Then GetUrbanProfilbezeichnung = "810400WCD"
If InStr(ref, "TR-810466 GR") Then GetUrbanProfilbezeichnung = "810400WCD"
If InStr(ref, "TR-810466 MA") Then GetUrbanProfilbezeichnung = "810400WCD"
If InStr(ref, "TR-810466 OH") Then GetUrbanProfilbezeichnung = "810400WCD"
If InStr(ref, "TR-810466 AG_167") Then GetUrbanProfilbezeichnung = "810400WCD"
If InStr(ref, "TR-810466 AG_83") Then GetUrbanProfilbezeichnung = "810400WCD"
If InStr(ref, "TR-810466 SG_167") Then GetUrbanProfilbezeichnung = "810400WCD"
If InStr(ref, "TR-810466 SG_83") Then GetUrbanProfilbezeichnung = "810400WCD"
If InStr(ref, "TR-810466 RR") Then GetUrbanProfilbezeichnung = "810400CCD"
If InStr(ref, "TR-810466 RWe") Then GetUrbanProfilbezeichnung = "810400WCD"
If InStr(ref, "TR-810466 WeR") Then GetUrbanProfilbezeichnung = "810400WWD"
If InStr(ref, "TR-810466RAL/WET") Then GetUrbanProfilbezeichnung = "810400WW"
If InStr(ref, "TR-810466RALT") Then GetUrbanProfilbezeichnung = "810400WW"
If InStr(ref, "TR-810866 2GO") Then GetUrbanProfilbezeichnung = "810800CCD"
If InStr(ref, "TR-810866 2OH") Then GetUrbanProfilbezeichnung = "810800CCD"
If InStr(ref, "TR-810866 654") Then GetUrbanProfilbezeichnung = "810800WWD"
If InStr(ref, "TR-810866 BR") Then GetUrbanProfilbezeichnung = "810800WCD"
If InStr(ref, "TR-810866 GO") Then GetUrbanProfilbezeichnung = "810800WCD"
If InStr(ref, "TR-810866 GR") Then GetUrbanProfilbezeichnung = "810800WCD"
If InStr(ref, "TR-810866 MA") Then GetUrbanProfilbezeichnung = "810800WCD"
If InStr(ref, "TR-810866 OH") Then GetUrbanProfilbezeichnung = "810800WCD"
If InStr(ref, "TR-810866 AG_167") Then GetUrbanProfilbezeichnung = "810800WCD"
If InStr(ref, "TR-810866 AG_83") Then GetUrbanProfilbezeichnung = "810800WCD"
If InStr(ref, "TR-810866 SG_167") Then GetUrbanProfilbezeichnung = "810800WCD"
If InStr(ref, "TR-810866 SG_83") Then GetUrbanProfilbezeichnung = "810800WCD"
If InStr(ref, "TR-810866 RR") Then GetUrbanProfilbezeichnung = "810800CCD"
If InStr(ref, "TR-810866 RWe") Then GetUrbanProfilbezeichnung = "810800WCD"
If InStr(ref, "TR-810866 WeR") Then GetUrbanProfilbezeichnung = "810800WWD"
If InStr(ref, "TR-810866RAL/WET") Then GetUrbanProfilbezeichnung = "810800WW"
If InStr(ref, "TR-810866RALT") Then GetUrbanProfilbezeichnung = "810800WW"
If InStr(ref, "TR-820680 2GO") Then GetUrbanProfilbezeichnung = "820600A"
If InStr(ref, "TR-820680 2OH") Then GetUrbanProfilbezeichnung = "820600A"
If InStr(ref, "TR-820680 654") Then GetUrbanProfilbezeichnung = "820600A"
If InStr(ref, "TR-820680 RR") Then GetUrbanProfilbezeichnung = "820600A"
If InStr(ref, "TR-820680 WeR") Then GetUrbanProfilbezeichnung = "820600A"
If InStr(ref, "TR-820680RAL/WET") Then GetUrbanProfilbezeichnung = "820600A"
If InStr(ref, "TR-820680RALT") Then GetUrbanProfilbezeichnung = "820600A"
If InStr(ref, "TR-820680WE/RALT") Then GetUrbanProfilbezeichnung = "820600A"
If InStr(ref, "TR-820680_G 2GO") Then GetUrbanProfilbezeichnung = "820600GA"
If InStr(ref, "TR-820680_G 2OH") Then GetUrbanProfilbezeichnung = "820600GA"
If InStr(ref, "TR-820680_G 654") Then GetUrbanProfilbezeichnung = "820600GA"
If InStr(ref, "TR-820680_G RR") Then GetUrbanProfilbezeichnung = "820600GA"
If InStr(ref, "TR-820680_G WeR") Then GetUrbanProfilbezeichnung = "820600GA"
If InStr(ref, "TR-820680_GRAL/WET") Then GetUrbanProfilbezeichnung = "820600GA"
If InStr(ref, "TR-820680_GWE/RALT") Then GetUrbanProfilbezeichnung = "820600GA"
If InStr(ref, "TR-820680_GRALT") Then GetUrbanProfilbezeichnung = "820600GA"
If InStr(ref, "TR-820690 2GO") Then GetUrbanProfilbezeichnung = "820600CCD"
If InStr(ref, "TR-820690 2OH") Then GetUrbanProfilbezeichnung = "820600CCD"
If InStr(ref, "TR-820690 654") Then GetUrbanProfilbezeichnung = "820600WWD"
If InStr(ref, "TR-820690 BR") Then GetUrbanProfilbezeichnung = "820600WCD"
If InStr(ref, "TR-820690 GO") Then GetUrbanProfilbezeichnung = "820600WCD"
If InStr(ref, "TR-820690 GR") Then GetUrbanProfilbezeichnung = "820600WCD"
If InStr(ref, "TR-820690 MA") Then GetUrbanProfilbezeichnung = "820600WCD"
If InStr(ref, "TR-820690 OH") Then GetUrbanProfilbezeichnung = "820600WCD"
If InStr(ref, "TR-820690 AG_167") Then GetUrbanProfilbezeichnung = "820600WCD"
If InStr(ref, "TR-820690 AG_83") Then GetUrbanProfilbezeichnung = "820600WCD"
If InStr(ref, "TR-820690 SG_167") Then GetUrbanProfilbezeichnung = "820600WCD"
If InStr(ref, "TR-820690 SG_83") Then GetUrbanProfilbezeichnung = "820600WCD"
If InStr(ref, "TR-820690 RR") Then GetUrbanProfilbezeichnung = "820600CCD"
If InStr(ref, "TR-820690 RWe") Then GetUrbanProfilbezeichnung = "820600WCD"
If InStr(ref, "TR-820690 WeR") Then GetUrbanProfilbezeichnung = "820600WWD"
If InStr(ref, "TR-820690RAL/WET") Then GetUrbanProfilbezeichnung = "820600WWD"
If InStr(ref, "TR-820690RALT") Then GetUrbanProfilbezeichnung = "820600WW"
If InStr(ref, "TR-820690_G 2GO") Then GetUrbanProfilbezeichnung = "820600GCCD"
If InStr(ref, "TR-820690_G 2OH") Then GetUrbanProfilbezeichnung = "820600GCCD"
If InStr(ref, "TR-820690_G 654") Then GetUrbanProfilbezeichnung = "820600GWWD"
If InStr(ref, "TR-820690_G BR") Then GetUrbanProfilbezeichnung = "820600GWCD"
If InStr(ref, "TR-820690_G GO") Then GetUrbanProfilbezeichnung = "820600GWCD"
If InStr(ref, "TR-820690_G GR") Then GetUrbanProfilbezeichnung = "820600GWCD"
If InStr(ref, "TR-820690_G MA") Then GetUrbanProfilbezeichnung = "820600GWCD"
If InStr(ref, "TR-820690_G OH") Then GetUrbanProfilbezeichnung = "820600GWCD"
If InStr(ref, "TR-820690_G AG_167") Then GetUrbanProfilbezeichnung = "820600GWCD"
If InStr(ref, "TR-820690_G AG_83") Then GetUrbanProfilbezeichnung = "820600GWCD"
If InStr(ref, "TR-820690_G SG_167") Then GetUrbanProfilbezeichnung = "820600GWCD"
If InStr(ref, "TR-820690_G SG_83") Then GetUrbanProfilbezeichnung = "820600GWCD"
If InStr(ref, "TR-820690_G RR") Then GetUrbanProfilbezeichnung = "820600GCCD"
If InStr(ref, "TR-820690_G RWe") Then GetUrbanProfilbezeichnung = "820600GWCD"
If InStr(ref, "TR-820690_G WeR") Then GetUrbanProfilbezeichnung = "820600GWWD"
If InStr(ref, "TR-820690_GRAL/WET") Then GetUrbanProfilbezeichnung = "820600WWD"
If InStr(ref, "TR-820690_GRALT") Then GetUrbanProfilbezeichnung = "820600WW"
If InStr(ref, "TR-822190 2GO") Then GetUrbanProfilbezeichnung = "822100CCD"
If InStr(ref, "TR-822190 2OH") Then GetUrbanProfilbezeichnung = "822100CCD"
If InStr(ref, "TR-822190 654") Then GetUrbanProfilbezeichnung = "822100WWD"
If InStr(ref, "TR-822190 BR") Then GetUrbanProfilbezeichnung = "822100WCD"
If InStr(ref, "TR-822190 GO") Then GetUrbanProfilbezeichnung = "822100WCD"
If InStr(ref, "TR-822190 GR") Then GetUrbanProfilbezeichnung = "822100WCD"
If InStr(ref, "TR-822190 MA") Then GetUrbanProfilbezeichnung = "822100WCD"
If InStr(ref, "TR-822190 OH") Then GetUrbanProfilbezeichnung = "822100WCD"
If InStr(ref, "TR-822190 AG_167") Then GetUrbanProfilbezeichnung = "822100WCD"
If InStr(ref, "TR-822190 AG_83") Then GetUrbanProfilbezeichnung = "822100WCD"
If InStr(ref, "TR-822190 SG_167") Then GetUrbanProfilbezeichnung = "822100WCD"
If InStr(ref, "TR-822190 SG_83") Then GetUrbanProfilbezeichnung = "822100WCD"
If InStr(ref, "TR-822190 RR") Then GetUrbanProfilbezeichnung = "822100CCD"
If InStr(ref, "TR-822190 RWe") Then GetUrbanProfilbezeichnung = "822100WCD"
If InStr(ref, "TR-822190 WeR") Then GetUrbanProfilbezeichnung = "822100WWD"
If InStr(ref, "TR-822190RAL/WET") Then GetUrbanProfilbezeichnung = "822100WWD"
If InStr(ref, "TR-822190RALT") Then GetUrbanProfilbezeichnung = "822100WW"
If InStr(ref, "TR-822190(A) 2GO") Then GetUrbanProfilbezeichnung = "822100A"
If InStr(ref, "TR-822190(A) 2OH") Then GetUrbanProfilbezeichnung = "822100A"
If InStr(ref, "TR-822190(A) 654") Then GetUrbanProfilbezeichnung = "822100A"
If InStr(ref, "TR-822190(A) RR") Then GetUrbanProfilbezeichnung = "822100A"
If InStr(ref, "TR-822190(A) WeR") Then GetUrbanProfilbezeichnung = "822100A"
If InStr(ref, "TR-822190(A)RAL/WET") Then GetUrbanProfilbezeichnung = "822100A"
If InStr(ref, "TR-822190(A)RALT") Then GetUrbanProfilbezeichnung = "822100A"
If InStr(ref, "TR-822190(A)_G 2GO") Then GetUrbanProfilbezeichnung = "822100GA"
If InStr(ref, "TR-822190(A)_G 2OH") Then GetUrbanProfilbezeichnung = "822100GA"
If InStr(ref, "TR-822190(A)_G 654") Then GetUrbanProfilbezeichnung = "822100GA"
If InStr(ref, "TR-822190(A)_G RR") Then GetUrbanProfilbezeichnung = "822100GA"
If InStr(ref, "TR-822190(A)_G WeR") Then GetUrbanProfilbezeichnung = "822100GA"
If InStr(ref, "TR-822190(A)_GRAL/WET") Then GetUrbanProfilbezeichnung = "822100GA"
If InStr(ref, "TR-822190(A)_GRALT") Then GetUrbanProfilbezeichnung = "822100GA"
If InStr(ref, "TR-822190_G 2GO") Then GetUrbanProfilbezeichnung = "822100GCCD"
If InStr(ref, "TR-822190_G 2OH") Then GetUrbanProfilbezeichnung = "822100GCCD"
If InStr(ref, "TR-822190_G 654") Then GetUrbanProfilbezeichnung = "822100GWWD"
If InStr(ref, "TR-822190_G BR") Then GetUrbanProfilbezeichnung = "822100GWCD"
If InStr(ref, "TR-822190_G GO") Then GetUrbanProfilbezeichnung = "822100GWCD"
If InStr(ref, "TR-822190_G GR") Then GetUrbanProfilbezeichnung = "822100GWCD"
If InStr(ref, "TR-822190_G MA") Then GetUrbanProfilbezeichnung = "822100GWCD"
If InStr(ref, "TR-822190_G OH") Then GetUrbanProfilbezeichnung = "822100GWCD"
If InStr(ref, "TR-822190_G AG_167") Then GetUrbanProfilbezeichnung = "822100GWCD"
If InStr(ref, "TR-822190_G AG_83") Then GetUrbanProfilbezeichnung = "822100GWCD"
If InStr(ref, "TR-822190_G SG_167") Then GetUrbanProfilbezeichnung = "822100GWCD"
If InStr(ref, "TR-822190_G SG_83") Then GetUrbanProfilbezeichnung = "822100GWCD"
If InStr(ref, "TR-822190_G RR") Then GetUrbanProfilbezeichnung = "822100GCCD"
If InStr(ref, "TR-822190_G RWe") Then GetUrbanProfilbezeichnung = "822100GWCD"
If InStr(ref, "TR-822190_G WeR") Then GetUrbanProfilbezeichnung = "822100GWWD"
If InStr(ref, "TR-822190_GRAL/WET") Then GetUrbanProfilbezeichnung = "822100GWWD"
If InStr(ref, "TR-822190_GRALT") Then GetUrbanProfilbezeichnung = "822100GWW"
If InStr(ref, "TR-820790 2GO") Then GetUrbanProfilbezeichnung = "820700CCD"
If InStr(ref, "TR-820790 2OH") Then GetUrbanProfilbezeichnung = "820700CCD"
If InStr(ref, "TR-820790 654") Then GetUrbanProfilbezeichnung = "820700WWD"
If InStr(ref, "TR-820790 BR") Then GetUrbanProfilbezeichnung = "820700WCD"
If InStr(ref, "TR-820790 GO") Then GetUrbanProfilbezeichnung = "820700WCD"
If InStr(ref, "TR-820790 GR") Then GetUrbanProfilbezeichnung = "820700WCD"
If InStr(ref, "TR-820790 MA") Then GetUrbanProfilbezeichnung = "820700WCD"
If InStr(ref, "TR-820790 OH") Then GetUrbanProfilbezeichnung = "820700WCD"
If InStr(ref, "TR-820790 AG_167") Then GetUrbanProfilbezeichnung = "820700WCD"
If InStr(ref, "TR-820790 AG_83") Then GetUrbanProfilbezeichnung = "820700WCD"
If InStr(ref, "TR-820790 SG_167") Then GetUrbanProfilbezeichnung = "820700WCD"
If InStr(ref, "TR-820790 SG_83") Then GetUrbanProfilbezeichnung = "820700WCD"
If InStr(ref, "TR-820790 RR") Then GetUrbanProfilbezeichnung = "820700CCD"
If InStr(ref, "TR-820790 RWe") Then GetUrbanProfilbezeichnung = "820700WCD"
If InStr(ref, "TR-820790 WeR") Then GetUrbanProfilbezeichnung = "820700WWD"
If InStr(ref, "TR-820790RAL/WET") Then GetUrbanProfilbezeichnung = "820700WWD"
If InStr(ref, "TR-820790RALT") Then GetUrbanProfilbezeichnung = "820700WW"
If InStr(ref, "TR-820790WE/RALT") Then GetUrbanProfilbezeichnung = "820700WWD"
If InStr(ref, "TR-820790_G 2GO") Then GetUrbanProfilbezeichnung = "820700GCCD"
If InStr(ref, "TR-820790_G 2OH") Then GetUrbanProfilbezeichnung = "820700GCCD"
If InStr(ref, "TR-820790_G 654") Then GetUrbanProfilbezeichnung = "820700GWWD"
If InStr(ref, "TR-820790_G BR") Then GetUrbanProfilbezeichnung = "820700GWCD"
If InStr(ref, "TR-820790_G GO") Then GetUrbanProfilbezeichnung = "820700GWCD"
If InStr(ref, "TR-820790_G GR") Then GetUrbanProfilbezeichnung = "820700GWCD"
If InStr(ref, "TR-820790_G MA") Then GetUrbanProfilbezeichnung = "820700GWCD"
If InStr(ref, "TR-820790_G OH") Then GetUrbanProfilbezeichnung = "820700GWCD"
If InStr(ref, "TR-820790_G AG_167") Then GetUrbanProfilbezeichnung = "820700GWCD"
If InStr(ref, "TR-820790_G AG_83") Then GetUrbanProfilbezeichnung = "820700GWCD"
If InStr(ref, "TR-820790_G SG_167") Then GetUrbanProfilbezeichnung = "820700GWCD"
If InStr(ref, "TR-820790_G SG_83") Then GetUrbanProfilbezeichnung = "820700GWCD"
If InStr(ref, "TR-820790_G RR") Then GetUrbanProfilbezeichnung = "820700GCCD"
If InStr(ref, "TR-820790_G RWe") Then GetUrbanProfilbezeichnung = "820700GWCD"
If InStr(ref, "TR-820790_G WeR") Then GetUrbanProfilbezeichnung = "820700GWWD"
If InStr(ref, "TR-820790_GRAL/WET") Then GetUrbanProfilbezeichnung = "820700GWWD"
If InStr(ref, "TR-820790_GRALT") Then GetUrbanProfilbezeichnung = "820700GWW"
If InStr(ref, "TR-820790_GWE/RALT") Then GetUrbanProfilbezeichnung = "820700GWWD"
If InStr(ref, "TR-810366 R_LK1") Then GetUrbanProfilbezeichnung = "810300WCD"
If InStr(ref, "TR-810366 R_LK3") Then GetUrbanProfilbezeichnung = "810300WCD"
If InStr(ref, "TR-810366 R_LK4") Then GetUrbanProfilbezeichnung = "810300WCD"
If InStr(ref, "TR-810366 R_LK5") Then GetUrbanProfilbezeichnung = "810300WCD"
If InStr(ref, "TR-810366 R_LK6") Then GetUrbanProfilbezeichnung = "810300WCD"
If InStr(ref, "TR-810466 R_LK1") Then GetUrbanProfilbezeichnung = "810400WCD"
If InStr(ref, "TR-810466 R_LK3") Then GetUrbanProfilbezeichnung = "810400WCD"
If InStr(ref, "TR-810466 R_LK4") Then GetUrbanProfilbezeichnung = "810400WCD"
If InStr(ref, "TR-810466 R_LK5") Then GetUrbanProfilbezeichnung = "810400WCD"
If InStr(ref, "TR-810466 R_LK6") Then GetUrbanProfilbezeichnung = "810400WCD"
If InStr(ref, "TR-820690 R_LK1") Then GetUrbanProfilbezeichnung = "820600WCD"
If InStr(ref, "TR-820690 R_LK3") Then GetUrbanProfilbezeichnung = "820600WCD"
If InStr(ref, "TR-820690 R_LK4") Then GetUrbanProfilbezeichnung = "820600WCD"
If InStr(ref, "TR-820690 R_LK5") Then GetUrbanProfilbezeichnung = "820600WCD"
If InStr(ref, "TR-820690 R_LK6") Then GetUrbanProfilbezeichnung = "820600WCD"
If InStr(ref, "TR-820690_G R_LK1") Then GetUrbanProfilbezeichnung = "820600GWCD"
If InStr(ref, "TR-820690_G R_LK3") Then GetUrbanProfilbezeichnung = "820600GWCD"
If InStr(ref, "TR-820690_G R_LK4") Then GetUrbanProfilbezeichnung = "820600GWCD"
If InStr(ref, "TR-820690_G R_LK5") Then GetUrbanProfilbezeichnung = "820600GWCD"
If InStr(ref, "TR-820690_G R_LK6") Then GetUrbanProfilbezeichnung = "820600GWCD"
If InStr(ref, "TR-820790 R_LK1") Then GetUrbanProfilbezeichnung = "820700WCD"
If InStr(ref, "TR-820790 R_LK3") Then GetUrbanProfilbezeichnung = "820700WCD"
If InStr(ref, "TR-820790 R_LK4") Then GetUrbanProfilbezeichnung = "820700WCD"
If InStr(ref, "TR-820790 R_LK5") Then GetUrbanProfilbezeichnung = "820700WCD"
If InStr(ref, "TR-820790 R_LK6") Then GetUrbanProfilbezeichnung = "820700WCD"
If InStr(ref, "TR-820790_G R_LK1") Then GetUrbanProfilbezeichnung = "820700GWCD"
If InStr(ref, "TR-820790_G R_LK3") Then GetUrbanProfilbezeichnung = "820700GWCD"
If InStr(ref, "TR-820790_G R_LK4") Then GetUrbanProfilbezeichnung = "820700GWCD"
If InStr(ref, "TR-820790_G R_LK5") Then GetUrbanProfilbezeichnung = "820700GWCD"
If InStr(ref, "TR-820790_G R_LK6") Then GetUrbanProfilbezeichnung = "820700GWCD"
If InStr(ref, "TR-822190 R_LK1") Then GetUrbanProfilbezeichnung = "822100WCD"
If InStr(ref, "TR-822190 R_LK3") Then GetUrbanProfilbezeichnung = "822100WCD"
If InStr(ref, "TR-822190 R_LK4") Then GetUrbanProfilbezeichnung = "822100WCD"
If InStr(ref, "TR-822190 R_LK5") Then GetUrbanProfilbezeichnung = "822100WCD"
If InStr(ref, "TR-822190 R_LK6") Then GetUrbanProfilbezeichnung = "822100WCD"
If InStr(ref, "TR-822190_G R_LK1") Then GetUrbanProfilbezeichnung = "822100GWCD"
If InStr(ref, "TR-822190_G R_LK3") Then GetUrbanProfilbezeichnung = "822100GWCD"
If InStr(ref, "TR-822190_G R_LK4") Then GetUrbanProfilbezeichnung = "822100GWCD"
If InStr(ref, "TR-822190_G R_LK5") Then GetUrbanProfilbezeichnung = "822100GWCD"
If InStr(ref, "TR-822190_G R_LK6") Then GetUrbanProfilbezeichnung = "822100GWCD"
If InStr(ref, "TR-810366_AÖ 2GO") Then GetUrbanProfilbezeichnung = "810300CCD"
If InStr(ref, "TR-810366_AÖ 2OH") Then GetUrbanProfilbezeichnung = "810300CCD"
If InStr(ref, "TR-810366_AÖ 654") Then GetUrbanProfilbezeichnung = "810300WWD"
If InStr(ref, "TR-810366_AÖ AG_167") Then GetUrbanProfilbezeichnung = "810300WWD"
If InStr(ref, "TR-810366_AÖ AG_83") Then GetUrbanProfilbezeichnung = "810300WWD"
If InStr(ref, "TR-810366_AÖ BR") Then GetUrbanProfilbezeichnung = "810300WWD"
If InStr(ref, "TR-810366_AÖ GO") Then GetUrbanProfilbezeichnung = "810300WWD"
If InStr(ref, "TR-810366_AÖ GR") Then GetUrbanProfilbezeichnung = "810300WWD"
If InStr(ref, "TR-810366_AÖ MA") Then GetUrbanProfilbezeichnung = "810300WWD"
If InStr(ref, "TR-810366_AÖ OH") Then GetUrbanProfilbezeichnung = "810300WWD"
If InStr(ref, "TR-810366_AÖ R_LK1") Then GetUrbanProfilbezeichnung = "810300WWD"
If InStr(ref, "TR-810366_AÖ R_LK3") Then GetUrbanProfilbezeichnung = "810300WWD"
If InStr(ref, "TR-810366_AÖ R_LK4") Then GetUrbanProfilbezeichnung = "810300WWD"
If InStr(ref, "TR-810366_AÖ R_LK5") Then GetUrbanProfilbezeichnung = "810300WWD"
If InStr(ref, "TR-810366_AÖ R_LK6") Then GetUrbanProfilbezeichnung = "810300WWD"
If InStr(ref, "TR-810366_AÖ RR") Then GetUrbanProfilbezeichnung = "810300CCD"
If InStr(ref, "TR-810366_AÖ SG_83") Then GetUrbanProfilbezeichnung = "810300WWD"
If InStr(ref, "TR-810366_AÖ WeR") Then GetUrbanProfilbezeichnung = "810300WCD"
If InStr(ref, "TR-810366_AÖRAL/WET") Then GetUrbanProfilbezeichnung = "810300WWD"
If InStr(ref, "TR-810366_AÖRALT") Then GetUrbanProfilbezeichnung = "810300WWD"
If InStr(ref, "TR-810366_AÖWE/RALT") Then GetUrbanProfilbezeichnung = "810300WW"
If InStr(ref, "TR-810466_AÖ 2GO") Then GetUrbanProfilbezeichnung = "810400CCD"
If InStr(ref, "TR-810466_AÖ 2OH") Then GetUrbanProfilbezeichnung = "810400CCD"
If InStr(ref, "TR-810466_AÖ 654") Then GetUrbanProfilbezeichnung = "810400WWD"
If InStr(ref, "TR-810466_AÖ AG_167") Then GetUrbanProfilbezeichnung = "810400WWD"
If InStr(ref, "TR-810466_AÖ AG_83") Then GetUrbanProfilbezeichnung = "810400WWD"
If InStr(ref, "TR-810466_AÖ BR") Then GetUrbanProfilbezeichnung = "810400WWD"
If InStr(ref, "TR-810466_AÖ GO") Then GetUrbanProfilbezeichnung = "810400WWD"
If InStr(ref, "TR-810466_AÖ GR") Then GetUrbanProfilbezeichnung = "810400WWD"
If InStr(ref, "TR-810466_AÖ MA") Then GetUrbanProfilbezeichnung = "810400WWD"
If InStr(ref, "TR-810466_AÖ OH") Then GetUrbanProfilbezeichnung = "810400WWD"
If InStr(ref, "TR-810466_AÖ R_LK1") Then GetUrbanProfilbezeichnung = "810400WWD"
If InStr(ref, "TR-810466_AÖ R_LK3") Then GetUrbanProfilbezeichnung = "810400WWD"
If InStr(ref, "TR-810466_AÖ R_LK4") Then GetUrbanProfilbezeichnung = "810400WWD"
If InStr(ref, "TR-810466_AÖ R_LK5") Then GetUrbanProfilbezeichnung = "810400WWD"
If InStr(ref, "TR-810466_AÖ R_LK6") Then GetUrbanProfilbezeichnung = "810400WWD"
If InStr(ref, "TR-810466_AÖ RR") Then GetUrbanProfilbezeichnung = "810400CCD"
If InStr(ref, "TR-810466_AÖ SG_83") Then GetUrbanProfilbezeichnung = "810400WWD"
If InStr(ref, "TR-810466_AÖ WeR") Then GetUrbanProfilbezeichnung = "810400WCD"
If InStr(ref, "TR-810466_AÖRAL/WET") Then GetUrbanProfilbezeichnung = "810400WWD"
If InStr(ref, "TR-810466_AÖRALT") Then GetUrbanProfilbezeichnung = "810400WWD"
If InStr(ref, "TR-810466_AÖWE/RALT") Then GetUrbanProfilbezeichnung = "810400WW"
If InStr(ref, "TR-822290 2GO") Then GetUrbanProfilbezeichnung = "822200CCD"
If InStr(ref, "TR-822290 2OH") Then GetUrbanProfilbezeichnung = "822200CCD"
If InStr(ref, "TR-822290 654") Then GetUrbanProfilbezeichnung = "822200WWD"
If InStr(ref, "TR-822290 AG_167") Then GetUrbanProfilbezeichnung = "822200WCD"
If InStr(ref, "TR-822290 AG_83") Then GetUrbanProfilbezeichnung = "822200WCD"
If InStr(ref, "TR-822290 BR") Then GetUrbanProfilbezeichnung = "822200WCD"
If InStr(ref, "TR-822290 GO") Then GetUrbanProfilbezeichnung = "822200WCD"
If InStr(ref, "TR-822290 GR") Then GetUrbanProfilbezeichnung = "822200WCD"
If InStr(ref, "TR-822290 MA") Then GetUrbanProfilbezeichnung = "822200WCD"
If InStr(ref, "TR-822290 OH") Then GetUrbanProfilbezeichnung = "822200WCD"
If InStr(ref, "TR-822290 R_LK1") Then GetUrbanProfilbezeichnung = "822200WCD"
If InStr(ref, "TR-822290 R_LK3") Then GetUrbanProfilbezeichnung = "822200WCD"
If InStr(ref, "TR-822290 R_LK4") Then GetUrbanProfilbezeichnung = "822200WCD"
If InStr(ref, "TR-822290 R_LK5") Then GetUrbanProfilbezeichnung = "822200WCD"
If InStr(ref, "TR-822290 R_LK6") Then GetUrbanProfilbezeichnung = "822200WCD"
If InStr(ref, "TR-822290 RR") Then GetUrbanProfilbezeichnung = "822200CCD"
If InStr(ref, "TR-822290 SG_83") Then GetUrbanProfilbezeichnung = "822200WCD"
If InStr(ref, "TR-822290 WeR") Then GetUrbanProfilbezeichnung = "822200WW"
If InStr(ref, "TR-822290RAL/WET") Then GetUrbanProfilbezeichnung = "822200WWD"
If InStr(ref, "TR-822290RALT") Then GetUrbanProfilbezeichnung = "822200WWD"
If InStr(ref, "TR-822290WE/RALT") Then GetUrbanProfilbezeichnung = "822200WW"
If InStr(ref, "TR-2883 2GO") Then GetUrbanProfilbezeichnung = "2883CCD"
If InStr(ref, "TR-2883 2OH") Then GetUrbanProfilbezeichnung = "2883CCD"
If InStr(ref, "TR-2883 654") Then GetUrbanProfilbezeichnung = "2883A"
If InStr(ref, "TR-2883 AG_167") Then GetUrbanProfilbezeichnung = "2883WCD"
If InStr(ref, "TR-2883 AG_83") Then GetUrbanProfilbezeichnung = "2883WCD"
If InStr(ref, "TR-2883 BR") Then GetUrbanProfilbezeichnung = "2883WCD"
If InStr(ref, "TR-2883 GO") Then GetUrbanProfilbezeichnung = "2883WCD"
If InStr(ref, "TR-2883 GR") Then GetUrbanProfilbezeichnung = "2883WCD"
If InStr(ref, "TR-2883 MA") Then GetUrbanProfilbezeichnung = "2883WCD"
If InStr(ref, "TR-2883 OH") Then GetUrbanProfilbezeichnung = "2883WCD"
If InStr(ref, "TR-2883 R_LK1") Then GetUrbanProfilbezeichnung = "2883WCD"
If InStr(ref, "TR-2883 R_LK3") Then GetUrbanProfilbezeichnung = "2883WCD"
If InStr(ref, "TR-2883 R_LK4") Then GetUrbanProfilbezeichnung = "2883WCD"
If InStr(ref, "TR-2883 R_LK5") Then GetUrbanProfilbezeichnung = "2883WCD"
If InStr(ref, "TR-2883 R_LK6") Then GetUrbanProfilbezeichnung = "2883WCD"
If InStr(ref, "TR-2883 RR") Then GetUrbanProfilbezeichnung = "2883CCD"
If InStr(ref, "TR-2883 SG_83") Then GetUrbanProfilbezeichnung = "2883WCD"
If InStr(ref, "TR-2883 WeR") Then GetUrbanProfilbezeichnung = "2883CCD"
If InStr(ref, "TR-2883_A 2GO") Then GetUrbanProfilbezeichnung = "2883CCD"
If InStr(ref, "TR-2883_A 2OH") Then GetUrbanProfilbezeichnung = "2883CCD"
If InStr(ref, "TR-2883_A 654") Then GetUrbanProfilbezeichnung = "2883A"
If InStr(ref, "TR-2883_A AG_167") Then GetUrbanProfilbezeichnung = "2883WCD"
If InStr(ref, "TR-2883_A AG_83") Then GetUrbanProfilbezeichnung = "2883WCD"
If InStr(ref, "TR-2883_A BR") Then GetUrbanProfilbezeichnung = "2883WCD"
If InStr(ref, "TR-2883_A GO") Then GetUrbanProfilbezeichnung = "2883WCD"
If InStr(ref, "TR-2883_A GR") Then GetUrbanProfilbezeichnung = "2883WCD"
If InStr(ref, "TR-2883_A MA") Then GetUrbanProfilbezeichnung = "2883WCD"
If InStr(ref, "TR-2883_A OH") Then GetUrbanProfilbezeichnung = "2883WCD"
If InStr(ref, "TR-2883_A R_LK1") Then GetUrbanProfilbezeichnung = "2883WCD"
If InStr(ref, "TR-2883_A R_LK3") Then GetUrbanProfilbezeichnung = "2883WCD"
If InStr(ref, "TR-2883_A R_LK4") Then GetUrbanProfilbezeichnung = "2883WCD"
If InStr(ref, "TR-2883_A R_LK5") Then GetUrbanProfilbezeichnung = "2883WCD"
If InStr(ref, "TR-2883_A R_LK6") Then GetUrbanProfilbezeichnung = "2883WCD"
If InStr(ref, "TR-2883_A RR") Then GetUrbanProfilbezeichnung = "2883CCD"
If InStr(ref, "TR-2883_A SG_83") Then GetUrbanProfilbezeichnung = "2883WCD"
If InStr(ref, "TR-2883_A WeR") Then GetUrbanProfilbezeichnung = "2883CCD"
If InStr(ref, "TR-2883_A_F 2GO") Then GetUrbanProfilbezeichnung = "2883CCD"
If InStr(ref, "TR-2883_A_F 2OH") Then GetUrbanProfilbezeichnung = "2883CCD"
If InStr(ref, "TR-2883_A_F 654") Then GetUrbanProfilbezeichnung = "2883A"
If InStr(ref, "TR-2883_A_F AG_167") Then GetUrbanProfilbezeichnung = "2883WCD"
If InStr(ref, "TR-2883_A_F AG_83") Then GetUrbanProfilbezeichnung = "2883WCD"
If InStr(ref, "TR-2883_A_F BR") Then GetUrbanProfilbezeichnung = "2883WCD"
If InStr(ref, "TR-2883_A_F GO") Then GetUrbanProfilbezeichnung = "2883WCD"
If InStr(ref, "TR-2883_A_F GR") Then GetUrbanProfilbezeichnung = "2883WCD"
If InStr(ref, "TR-2883_A_F MA") Then GetUrbanProfilbezeichnung = "2883WCD"
If InStr(ref, "TR-2883_A_F OH") Then GetUrbanProfilbezeichnung = "2883WCD"
If InStr(ref, "TR-2883_A_F R_LK1") Then GetUrbanProfilbezeichnung = "2883WCD"
If InStr(ref, "TR-2883_A_F R_LK3") Then GetUrbanProfilbezeichnung = "2883WCD"
If InStr(ref, "TR-2883_A_F R_LK4") Then GetUrbanProfilbezeichnung = "2883WCD"
If InStr(ref, "TR-2883_A_F R_LK5") Then GetUrbanProfilbezeichnung = "2883WCD"
If InStr(ref, "TR-2883_A_F R_LK6") Then GetUrbanProfilbezeichnung = "2883WCD"
If InStr(ref, "TR-2883_A_F RR") Then GetUrbanProfilbezeichnung = "2883CCD"
If InStr(ref, "TR-2883_A_F SG_83") Then GetUrbanProfilbezeichnung = "2883WCD"
If InStr(ref, "TR-2883_A_F WeR") Then GetUrbanProfilbezeichnung = "2883CCD"
If InStr(ref, "TR-2883_F 2GO") Then GetUrbanProfilbezeichnung = "2883CCD"
If InStr(ref, "TR-2883_F 2OH") Then GetUrbanProfilbezeichnung = "2883CCD"
If InStr(ref, "TR-2883_F 654") Then GetUrbanProfilbezeichnung = "2883A"
If InStr(ref, "TR-2883_F AG_167") Then GetUrbanProfilbezeichnung = "2883WCD"
If InStr(ref, "TR-2883_F AG_83") Then GetUrbanProfilbezeichnung = "2883WCD"
If InStr(ref, "TR-2883_F BR") Then GetUrbanProfilbezeichnung = "2883WCD"
If InStr(ref, "TR-2883_F GO") Then GetUrbanProfilbezeichnung = "2883WCD"
If InStr(ref, "TR-2883_F GR") Then GetUrbanProfilbezeichnung = "2883WCD"
If InStr(ref, "TR-2883_F MA") Then GetUrbanProfilbezeichnung = "2883WCD"
If InStr(ref, "TR-2883_F OH") Then GetUrbanProfilbezeichnung = "2883WCD"
If InStr(ref, "TR-2883_F R_LK1") Then GetUrbanProfilbezeichnung = "2883WCD"
If InStr(ref, "TR-2883_F R_LK3") Then GetUrbanProfilbezeichnung = "2883WCD"
If InStr(ref, "TR-2883_F R_LK4") Then GetUrbanProfilbezeichnung = "2883WCD"
If InStr(ref, "TR-2883_F R_LK5") Then GetUrbanProfilbezeichnung = "2883WCD"
If InStr(ref, "TR-2883_F R_LK6") Then GetUrbanProfilbezeichnung = "2883WCD"
If InStr(ref, "TR-2883_F RR") Then GetUrbanProfilbezeichnung = "2883CCD"
If InStr(ref, "TR-2883_F SG_83") Then GetUrbanProfilbezeichnung = "2883WCD"
If InStr(ref, "TR-2883_F WeR") Then GetUrbanProfilbezeichnung = "2883CCD"
If InStr(ref, "TR-822400 2GO") Then GetUrbanProfilbezeichnung = "822400CC"
If InStr(ref, "TR-822400 2OH") Then GetUrbanProfilbezeichnung = "822400CC"
If InStr(ref, "TR-822400 654") Then GetUrbanProfilbezeichnung = "822400WWD"
If InStr(ref, "TR-822400 AG_167") Then GetUrbanProfilbezeichnung = "822400WC"
If InStr(ref, "TR-822400 AG_83") Then GetUrbanProfilbezeichnung = "822400WC"
If InStr(ref, "TR-822400 BR") Then GetUrbanProfilbezeichnung = "822400WC"
If InStr(ref, "TR-822400 GO") Then GetUrbanProfilbezeichnung = "822400WC"
If InStr(ref, "TR-822400 GR") Then GetUrbanProfilbezeichnung = "822400WC"
If InStr(ref, "TR-822400 MA") Then GetUrbanProfilbezeichnung = "822400WC"
If InStr(ref, "TR-822400 OH") Then GetUrbanProfilbezeichnung = "822400WC"
If InStr(ref, "TR-822400 R_LK1") Then GetUrbanProfilbezeichnung = "822400WC"
If InStr(ref, "TR-822400 R_LK3") Then GetUrbanProfilbezeichnung = "822400WC"
If InStr(ref, "TR-822400 R_LK4") Then GetUrbanProfilbezeichnung = "822400WC"
If InStr(ref, "TR-822400 R_LK5") Then GetUrbanProfilbezeichnung = "822400WC"
If InStr(ref, "TR-822400 R_LK6") Then GetUrbanProfilbezeichnung = "822400WC"
If InStr(ref, "TR-822400 RR") Then GetUrbanProfilbezeichnung = "822400CC"
If InStr(ref, "TR-822400 SG_83") Then GetUrbanProfilbezeichnung = "822400WC"
If InStr(ref, "TR-822400 WeR") Then GetUrbanProfilbezeichnung = "822400CC"
If InStr(ref, "TR-822400RAL/WET") Then GetUrbanProfilbezeichnung = "822400WW"
If InStr(ref, "TR-822400RALT") Then GetUrbanProfilbezeichnung = "822400WW"
If InStr(ref, "TR-822400WE/RALT") Then GetUrbanProfilbezeichnung = "822400WW"
If InStr(ref, "TR-822500 2GO") Then GetUrbanProfilbezeichnung = "822500CCD"
If InStr(ref, "TR-822500 2OH") Then GetUrbanProfilbezeichnung = "822500CCD"
If InStr(ref, "TR-822500 654") Then GetUrbanProfilbezeichnung = "822500WWD"
If InStr(ref, "TR-822500 AG_167") Then GetUrbanProfilbezeichnung = "822500WC"
If InStr(ref, "TR-822500 AG_83") Then GetUrbanProfilbezeichnung = "822500WC"
If InStr(ref, "TR-822500 BR") Then GetUrbanProfilbezeichnung = "822500WC"
If InStr(ref, "TR-822500 GO") Then GetUrbanProfilbezeichnung = "822500WC"
If InStr(ref, "TR-822500 GR") Then GetUrbanProfilbezeichnung = "822500WC"
If InStr(ref, "TR-822500 MA") Then GetUrbanProfilbezeichnung = "822500WC"
If InStr(ref, "TR-822500 OH") Then GetUrbanProfilbezeichnung = "822500WC"
If InStr(ref, "TR-822500 R_LK1") Then GetUrbanProfilbezeichnung = "822500WC"
If InStr(ref, "TR-822500 R_LK3") Then GetUrbanProfilbezeichnung = "822500WC"
If InStr(ref, "TR-822500 R_LK4") Then GetUrbanProfilbezeichnung = "822500WC"
If InStr(ref, "TR-822500 R_LK5") Then GetUrbanProfilbezeichnung = "822500WC"
If InStr(ref, "TR-822500 R_LK6") Then GetUrbanProfilbezeichnung = "822500WC"
If InStr(ref, "TR-822500 RR") Then GetUrbanProfilbezeichnung = "822500CC"
If InStr(ref, "TR-822500 SG_83") Then GetUrbanProfilbezeichnung = "822500WC"
If InStr(ref, "TR-822500 WeR") Then GetUrbanProfilbezeichnung = "822500CC"
If InStr(ref, "TR-822500RAL/WET") Then GetUrbanProfilbezeichnung = "822500WW"
If InStr(ref, "TR-822500RALT") Then GetUrbanProfilbezeichnung = "822500WW"
If InStr(ref, "TR-822500WE/RALT") Then GetUrbanProfilbezeichnung = "822500WW"
If InStr(ref, "TR-810316_AÖ 2GO") Then GetUrbanProfilbezeichnung = "810300CC"
If InStr(ref, "TR-810316_AÖ 2OH") Then GetUrbanProfilbezeichnung = "810300CC"
If InStr(ref, "TR-810316_AÖ 654") Then GetUrbanProfilbezeichnung = "810300WW"
If InStr(ref, "TR-810316_AÖ AG_167") Then GetUrbanProfilbezeichnung = "810300WW"
If InStr(ref, "TR-810316_AÖ AG_83") Then GetUrbanProfilbezeichnung = "810300WW"
If InStr(ref, "TR-810316_AÖ BR") Then GetUrbanProfilbezeichnung = "810300WW"
If InStr(ref, "TR-810316_AÖ GO") Then GetUrbanProfilbezeichnung = "810300WW"
If InStr(ref, "TR-810316_AÖ GR") Then GetUrbanProfilbezeichnung = "810300WW"
If InStr(ref, "TR-810316_AÖ MA") Then GetUrbanProfilbezeichnung = "810300WW"
If InStr(ref, "TR-810316_AÖ OH") Then GetUrbanProfilbezeichnung = "810300WW"
If InStr(ref, "TR-810316_AÖ R_LK1") Then GetUrbanProfilbezeichnung = "810300WW"
If InStr(ref, "TR-810316_AÖ R_LK3") Then GetUrbanProfilbezeichnung = "810300WW"
If InStr(ref, "TR-810316_AÖ R_LK4") Then GetUrbanProfilbezeichnung = "810300WW"
If InStr(ref, "TR-810316_AÖ R_LK5") Then GetUrbanProfilbezeichnung = "810300WW"
If InStr(ref, "TR-810316_AÖ R_LK6") Then GetUrbanProfilbezeichnung = "810300WW"
If InStr(ref, "TR-810316_AÖ RR") Then GetUrbanProfilbezeichnung = "810300CC"
If InStr(ref, "TR-810316_AÖ SG_83") Then GetUrbanProfilbezeichnung = "810300CC"
If InStr(ref, "TR-810316_AÖ WeR") Then GetUrbanProfilbezeichnung = "810300WC"
If InStr(ref, "TR-810316_AÖRAL/WET") Then GetUrbanProfilbezeichnung = "810300WW"
If InStr(ref, "TR-810316_AÖRALT") Then GetUrbanProfilbezeichnung = "810300WW"
If InStr(ref, "TR-810316_AÖWE/RALT") Then GetUrbanProfilbezeichnung = "810300WW"
If InStr(ref, "TR-810416_AÖ 2GO") Then GetUrbanProfilbezeichnung = "810400CC"
If InStr(ref, "TR-810416_AÖ 2OH") Then GetUrbanProfilbezeichnung = "810400CC"
If InStr(ref, "TR-810416_AÖ 654") Then GetUrbanProfilbezeichnung = "810400WW"
If InStr(ref, "TR-810416_AÖ AG_167") Then GetUrbanProfilbezeichnung = "810400WW"
If InStr(ref, "TR-810416_AÖ AG_83") Then GetUrbanProfilbezeichnung = "810400WW"
If InStr(ref, "TR-810416_AÖ BR") Then GetUrbanProfilbezeichnung = "810400WW"
If InStr(ref, "TR-810416_AÖ GO") Then GetUrbanProfilbezeichnung = "810400WW"
If InStr(ref, "TR-810416_AÖ GR") Then GetUrbanProfilbezeichnung = "810400WW"
If InStr(ref, "TR-810416_AÖ MA") Then GetUrbanProfilbezeichnung = "810400WW"
If InStr(ref, "TR-810416_AÖ OH") Then GetUrbanProfilbezeichnung = "810400WW"
If InStr(ref, "TR-810416_AÖ R_LK1") Then GetUrbanProfilbezeichnung = "810400WW"
If InStr(ref, "TR-810416_AÖ R_LK3") Then GetUrbanProfilbezeichnung = "810400WW"
If InStr(ref, "TR-810416_AÖ R_LK4") Then GetUrbanProfilbezeichnung = "810400WW"
If InStr(ref, "TR-810416_AÖ R_LK5") Then GetUrbanProfilbezeichnung = "810400WW"
If InStr(ref, "TR-810416_AÖ R_LK6") Then GetUrbanProfilbezeichnung = "810400WW"
If InStr(ref, "TR-810416_AÖ RR") Then GetUrbanProfilbezeichnung = "810400CC"
If InStr(ref, "TR-810416_AÖ SG_83") Then GetUrbanProfilbezeichnung = "810400WW"
If InStr(ref, "TR-810416_AÖ WeR") Then GetUrbanProfilbezeichnung = "810400WC"
If InStr(ref, "TR-810416_AÖRAL/WET") Then GetUrbanProfilbezeichnung = "810400WW"
If InStr(ref, "TR-810416_AÖRALT") Then GetUrbanProfilbezeichnung = "810400WW"
If InStr(ref, "TR-810416_AÖWE/RALT") Then GetUrbanProfilbezeichnung = "810400WW"

If InStr(ref, "TR-810316_EAÖ GO") Then GetUrbanProfilbezeichnung = "810300WW"
If InStr(ref, "TR-810316_EAÖ GR") Then GetUrbanProfilbezeichnung = "810300WW"
If InStr(ref, "TR-810316_EAÖ MA") Then GetUrbanProfilbezeichnung = "810300WW"
If InStr(ref, "TR-810316_EAÖ OH") Then GetUrbanProfilbezeichnung = "810300WW"
If InStr(ref, "TR-810316_EAÖ R_LK1") Then GetUrbanProfilbezeichnung = "810300WW"
If InStr(ref, "TR-810316_EAÖ R_LK3") Then GetUrbanProfilbezeichnung = "810300WW"
If InStr(ref, "TR-810316_EAÖ R_LK4") Then GetUrbanProfilbezeichnung = "810300WW"
If InStr(ref, "TR-810316_EAÖ R_LK5") Then GetUrbanProfilbezeichnung = "810300WW"
If InStr(ref, "TR-810316_EAÖ R_LK6") Then GetUrbanProfilbezeichnung = "810300WW"
If InStr(ref, "TR-810316_EAÖ BR") Then GetUrbanProfilbezeichnung = "810300WW"
If InStr(ref, "TR-810316_EAÖ AG_83") Then GetUrbanProfilbezeichnung = "810300WW"
If InStr(ref, "TR-810316_EAÖ AG_167") Then GetUrbanProfilbezeichnung = "810300WW"
If InStr(ref, "TR-810316_EAÖ 654") Then GetUrbanProfilbezeichnung = "810300WW"
If InStr(ref, "TR-810316_EAÖ 2OH") Then GetUrbanProfilbezeichnung = "810300CC"
If InStr(ref, "TR-810316_EAÖ 2GO") Then GetUrbanProfilbezeichnung = "810300CC"
If InStr(ref, "TR-810316_EAÖ RR") Then GetUrbanProfilbezeichnung = "810300CC"
If InStr(ref, "TR-810316_EAÖ SG_83") Then GetUrbanProfilbezeichnung = "810300WW"
If InStr(ref, "TR-810316_EAÖ WeR") Then GetUrbanProfilbezeichnung = "810300WC"
If InStr(ref, "TR-810316_EAÖRAL/WET") Then GetUrbanProfilbezeichnung = "810300WW"
If InStr(ref, "TR-810316_EAÖRALT") Then GetUrbanProfilbezeichnung = "810300WW"
If InStr(ref, "TR-810316_EAÖWE/RALT") Then GetUrbanProfilbezeichnung = "810300WW"
If InStr(ref, "TR-810316_ERAL/WET") Then GetUrbanProfilbezeichnung = "810300WW"
If InStr(ref, "TR-810316_ERALT") Then GetUrbanProfilbezeichnung = "810300WW"
If InStr(ref, "TR-810316_EWE/RALT") Then GetUrbanProfilbezeichnung = "810300WW"
If InStr(ref, "TR-810316_E WeR") Then GetUrbanProfilbezeichnung = "810300WW"
If InStr(ref, "TR-810316_E SG_83") Then GetUrbanProfilbezeichnung = "810300WC"
If InStr(ref, "TR-810316_E RR") Then GetUrbanProfilbezeichnung = "810300CC"
If InStr(ref, "TR-810316_E R_LK6") Then GetUrbanProfilbezeichnung = "810300WC"
If InStr(ref, "TR-810316_E R_LK5") Then GetUrbanProfilbezeichnung = "810300WC"
If InStr(ref, "TR-810316_E R_LK4") Then GetUrbanProfilbezeichnung = "810300WC"
If InStr(ref, "TR-810316_E R_LK3") Then GetUrbanProfilbezeichnung = "810300WC"
If InStr(ref, "TR-810316_E R_LK1") Then GetUrbanProfilbezeichnung = "810300WC"
If InStr(ref, "TR-810316_E OH") Then GetUrbanProfilbezeichnung = "810300WC"
If InStr(ref, "TR-810316_E MA") Then GetUrbanProfilbezeichnung = "810300WC"
If InStr(ref, "TR-810316_E GR") Then GetUrbanProfilbezeichnung = "810300WC"
If InStr(ref, "TR-810316_E GO") Then GetUrbanProfilbezeichnung = "810300WC"
If InStr(ref, "TR-810316_E BR") Then GetUrbanProfilbezeichnung = "810300WC"
If InStr(ref, "TR-810316_E AG_83") Then GetUrbanProfilbezeichnung = "810300WC"
If InStr(ref, "TR-810316_E AG_167") Then GetUrbanProfilbezeichnung = "810300WC"
If InStr(ref, "TR-810316_E 654") Then GetUrbanProfilbezeichnung = "810300WW"
If InStr(ref, "TR-810316_E 2OH") Then GetUrbanProfilbezeichnung = "810300CC"
If InStr(ref, "TR-810316_E 2GO") Then GetUrbanProfilbezeichnung = "810300CC"
If InStr(ref, "TR-810416_EAÖ GO") Then GetUrbanProfilbezeichnung = "810400WW"
If InStr(ref, "TR-810416_EAÖ GR") Then GetUrbanProfilbezeichnung = "810400WW"
If InStr(ref, "TR-810416_EAÖ BR") Then GetUrbanProfilbezeichnung = "810400WW"
If InStr(ref, "TR-810416_EAÖ AG_83") Then GetUrbanProfilbezeichnung = "810400WW"
If InStr(ref, "TR-810416_EAÖ AG_167") Then GetUrbanProfilbezeichnung = "810400WW"
If InStr(ref, "TR-810416_EAÖ 654") Then GetUrbanProfilbezeichnung = "810400WW"
If InStr(ref, "TR-810416_EAÖ 2OH") Then GetUrbanProfilbezeichnung = "810400CC"
If InStr(ref, "TR-810416_EAÖ 2GO") Then GetUrbanProfilbezeichnung = "810400CC"
If InStr(ref, "TR-810416_EAÖ MA") Then GetUrbanProfilbezeichnung = "810400WW"
If InStr(ref, "TR-810416_EAÖ OH") Then GetUrbanProfilbezeichnung = "810400WW"
If InStr(ref, "TR-810416_EAÖ R_LK1") Then GetUrbanProfilbezeichnung = "810400WW"
If InStr(ref, "TR-810416_EAÖ R_LK3") Then GetUrbanProfilbezeichnung = "810400WW"
If InStr(ref, "TR-810416_EAÖ R_LK4") Then GetUrbanProfilbezeichnung = "810400WW"
If InStr(ref, "TR-810416_EAÖ R_LK5") Then GetUrbanProfilbezeichnung = "810400WW"
If InStr(ref, "TR-810416_EAÖ R_LK6") Then GetUrbanProfilbezeichnung = "810400WW"
If InStr(ref, "TR-810416_EAÖ RR") Then GetUrbanProfilbezeichnung = "810400CC"
If InStr(ref, "TR-810416_EAÖ SG_83") Then GetUrbanProfilbezeichnung = "810400WW"
If InStr(ref, "TR-810416_EAÖ WeR") Then GetUrbanProfilbezeichnung = "810400WC"
If InStr(ref, "TR-810416_EAÖRAL/WET") Then GetUrbanProfilbezeichnung = "810400WW"
If InStr(ref, "TR-810416_EAÖRALT") Then GetUrbanProfilbezeichnung = "810400WW"
If InStr(ref, "TR-810416_EAÖWE/RALT") Then GetUrbanProfilbezeichnung = "810400WW"
If InStr(ref, "TR-810416_E WeR") Then GetUrbanProfilbezeichnung = "810400WW"
If InStr(ref, "TR-810416_E SG_83") Then GetUrbanProfilbezeichnung = "810400WC"
If InStr(ref, "TR-810416_E RR") Then GetUrbanProfilbezeichnung = "810400CC"
If InStr(ref, "TR-810416_E R_LK6") Then GetUrbanProfilbezeichnung = "810400WC"
If InStr(ref, "TR-810416_E R_LK5") Then GetUrbanProfilbezeichnung = "810400WC"
If InStr(ref, "TR-810416_E R_LK4") Then GetUrbanProfilbezeichnung = "810400WC"
If InStr(ref, "TR-810416_E R_LK3") Then GetUrbanProfilbezeichnung = "810400WC"
If InStr(ref, "TR-810416_ERAL/WET") Then GetUrbanProfilbezeichnung = "810400WW"
If InStr(ref, "TR-810416_ERALT") Then GetUrbanProfilbezeichnung = "810400WW"
If InStr(ref, "TR-810416_EWE/RALT") Then GetUrbanProfilbezeichnung = "810400WW"
If InStr(ref, "TR-810416_E R_LK1") Then GetUrbanProfilbezeichnung = "810400WC"
If InStr(ref, "TR-810416_E OH") Then GetUrbanProfilbezeichnung = "810400WC"
If InStr(ref, "TR-810416_E MA") Then GetUrbanProfilbezeichnung = "810400WC"
If InStr(ref, "TR-810416_E GR") Then GetUrbanProfilbezeichnung = "810400WC"
If InStr(ref, "TR-810416_E GO") Then GetUrbanProfilbezeichnung = "810400WC"
If InStr(ref, "TR-810416_E BR") Then GetUrbanProfilbezeichnung = "810400WC"
If InStr(ref, "TR-810416_E AG_83") Then GetUrbanProfilbezeichnung = "810400WC"
If InStr(ref, "TR-810416_E AG_167") Then GetUrbanProfilbezeichnung = "810400WC"
If InStr(ref, "TR-810416_E 654") Then GetUrbanProfilbezeichnung = "810400WW"
If InStr(ref, "TR-810416_E 2OH") Then GetUrbanProfilbezeichnung = "810400CC"
If InStr(ref, "TR-810416_E 2GO") Then GetUrbanProfilbezeichnung = "810400CC"



			'-----------------------------
			'76 AD     ------------------
			'-----------------------------

If InStr(ref, "P76200_ST2x32") Then GetUrbanProfilbezeichnung = "76200GCC"
If InStr(ref, "P76200_ST2x39") Then GetUrbanProfilbezeichnung = "76200GCC"
If InStr(ref, "P76200_STWS") Then GetUrbanProfilbezeichnung = "76200GWW"
If InStr(ref, "P76200_STWSRAL") Then GetUrbanProfilbezeichnung = "76200GWW"
If InStr(ref, "P76200_STWSREN") Then GetUrbanProfilbezeichnung = "76200GCC"
If InStr(ref, "P76200---1L2x32") Then GetUrbanProfilbezeichnung = "76200A"
If InStr(ref, "P76200---1L2x39") Then GetUrbanProfilbezeichnung = "76200A"
If InStr(ref, "P76200---1LWS") Then GetUrbanProfilbezeichnung = "76200A"
If InStr(ref, "P76200---1LWSRAL") Then GetUrbanProfilbezeichnung = "76200A"
If InStr(ref, "P76200---1LWSREN") Then GetUrbanProfilbezeichnung = "76200A"
If InStr(ref, "P76200---2L24") Then GetUrbanProfilbezeichnung = "76200CCD"
If InStr(ref, "P76200---2L2x32") Then GetUrbanProfilbezeichnung = "76200CCD"
If InStr(ref, "P76200---2L2x39") Then GetUrbanProfilbezeichnung = "76200CCD"
If InStr(ref, "P76200---2L2xRAL") Then GetUrbanProfilbezeichnung = "76200WW"
If InStr(ref, "P76200---2L2xREN") Then GetUrbanProfilbezeichnung = "76200CCD"
If InStr(ref, "P76200---2L31") Then GetUrbanProfilbezeichnung = "76200WCD"
If InStr(ref, "P76200---2L32") Then GetUrbanProfilbezeichnung = "76200WCD"
If InStr(ref, "P76200---2L39") Then GetUrbanProfilbezeichnung = "76200WCD"
If InStr(ref, "P76200---2L53") Then GetUrbanProfilbezeichnung = "76200WCD"
If InStr(ref, "P76200---2LRALWS") Then GetUrbanProfilbezeichnung = "76200A"
If InStr(ref, "P76200---2LRENLK1") Then GetUrbanProfilbezeichnung = "76200CCD"
If InStr(ref, "P76200---2LRENLK3") Then GetUrbanProfilbezeichnung = "76200CCD"
If InStr(ref, "P76200---2LRENLK4") Then GetUrbanProfilbezeichnung = "76200CCD"
If InStr(ref, "P76200---2LRENLK5") Then GetUrbanProfilbezeichnung = "76200CCD"
If InStr(ref, "P76200---2LRENLK6") Then GetUrbanProfilbezeichnung = "76200CCD"
If InStr(ref, "P76200---2LWS") Then GetUrbanProfilbezeichnung = "76200WWD"
If InStr(ref, "P76200---2LWSRAL") Then GetUrbanProfilbezeichnung = "76200WW"
If InStr(ref, "P76200---2LWSREN") Then GetUrbanProfilbezeichnung = "76200CCD"
If InStr(ref, "P76200---2L_ST24") Then GetUrbanProfilbezeichnung = "76200GWCD"
If InStr(ref, "P76200---2L_ST2x32") Then GetUrbanProfilbezeichnung = "76200GCCD"
If InStr(ref, "P76200---2L_ST2x39") Then GetUrbanProfilbezeichnung = "76200GCCD"
If InStr(ref, "P76200---2L_ST2xRAL") Then GetUrbanProfilbezeichnung = "76200GWW"
If InStr(ref, "P76200---2L_ST2xREN") Then GetUrbanProfilbezeichnung = "76200GCCD"
If InStr(ref, "P76200---2L_ST31") Then GetUrbanProfilbezeichnung = "76200GWCD"
If InStr(ref, "P76200---2L_ST32") Then GetUrbanProfilbezeichnung = "76200GWCD"
If InStr(ref, "P76200---2L_ST39") Then GetUrbanProfilbezeichnung = "76200GWCD"
If InStr(ref, "P76200---2L_ST53") Then GetUrbanProfilbezeichnung = "76200GWCD"
If InStr(ref, "P76200---2L_STRALWS") Then GetUrbanProfilbezeichnung = "76200GWW"
If InStr(ref, "P76200---2L_STRENLK1") Then GetUrbanProfilbezeichnung = "76200GWCD"
If InStr(ref, "P76200---2L_STRENLK3") Then GetUrbanProfilbezeichnung = "76200GWCD"
If InStr(ref, "P76200---2L_STRENLK4") Then GetUrbanProfilbezeichnung = "76200GWCD"
If InStr(ref, "P76200---2L_STRENLK5") Then GetUrbanProfilbezeichnung = "76200GWCD"
If InStr(ref, "P76200---2L_STRENLK6") Then GetUrbanProfilbezeichnung = "76200GWCD"
If InStr(ref, "P76200---2L_STWS") Then GetUrbanProfilbezeichnung = "76200GWWD"
If InStr(ref, "P76200---2L_STWSRAL") Then GetUrbanProfilbezeichnung = "76200GWWD"
If InStr(ref, "P76200---2L_STWSREN") Then GetUrbanProfilbezeichnung = "76200GCCD"
If InStr(ref, "P76200---2S24") Then GetUrbanProfilbezeichnung = "76200WCD"
If InStr(ref, "P76200---2S2x32") Then GetUrbanProfilbezeichnung = "76200CCD"
If InStr(ref, "P76200---2S2x39") Then GetUrbanProfilbezeichnung = "76200CCD"
If InStr(ref, "P76200---2S2xRAL") Then GetUrbanProfilbezeichnung = "76200WW"
If InStr(ref, "P76200---2S2xREN") Then GetUrbanProfilbezeichnung = "76200CCD"
If InStr(ref, "P76200---2S31") Then GetUrbanProfilbezeichnung = "76200WCD"
If InStr(ref, "P76200---2S32") Then GetUrbanProfilbezeichnung = "76200WCD"
If InStr(ref, "P76200---2S39") Then GetUrbanProfilbezeichnung = "76200WCD"
If InStr(ref, "P76200---2S53") Then GetUrbanProfilbezeichnung = "76200WCD"
If InStr(ref, "P76200---2SRALWS") Then GetUrbanProfilbezeichnung = "76200WW"
If InStr(ref, "P76200---2SRENLK1") Then GetUrbanProfilbezeichnung = "76200WCD"
If InStr(ref, "P76200---2SRENLK3") Then GetUrbanProfilbezeichnung = "76200WCD"
If InStr(ref, "P76200---2SRENLK4") Then GetUrbanProfilbezeichnung = "76200WCD"
If InStr(ref, "P76200---2SRENLK5") Then GetUrbanProfilbezeichnung = "76200WCD"
If InStr(ref, "P76200---2SRENLK6") Then GetUrbanProfilbezeichnung = "76200WCD"
If InStr(ref, "P76200---2SWS") Then GetUrbanProfilbezeichnung = "76200WWD"
If InStr(ref, "P76200---2SWSRAL") Then GetUrbanProfilbezeichnung = "76200WWD"
If InStr(ref, "P76200---2SWSREN") Then GetUrbanProfilbezeichnung = "76200CCD"
If InStr(ref, "P76200---2S_ST24") Then GetUrbanProfilbezeichnung = "76200GWCD"
If InStr(ref, "P76200---2S_ST2x32") Then GetUrbanProfilbezeichnung = "76200GCCD"
If InStr(ref, "P76200---2S_ST2x39") Then GetUrbanProfilbezeichnung = "76200GCCD"
If InStr(ref, "P76200---2S_ST2xRAL") Then GetUrbanProfilbezeichnung = "76200GWW"
If InStr(ref, "P76200---2S_ST2xREN") Then GetUrbanProfilbezeichnung = "76200GCCD"
If InStr(ref, "P76200---2S_ST31") Then GetUrbanProfilbezeichnung = "76200GWCD"
If InStr(ref, "P76200---2S_ST32") Then GetUrbanProfilbezeichnung = "76200GWCD"
If InStr(ref, "P76200---2S_ST39") Then GetUrbanProfilbezeichnung = "76200GWCD"
If InStr(ref, "P76200---2S_ST53") Then GetUrbanProfilbezeichnung = "76200GWCD"
If InStr(ref, "P76200---2S_STRALWS") Then GetUrbanProfilbezeichnung = "76200GWW"
If InStr(ref, "P76200---2S_STRENLK1") Then GetUrbanProfilbezeichnung = "76200GWCD"
If InStr(ref, "P76200---2S_STRENLK3") Then GetUrbanProfilbezeichnung = "76200GWCD"
If InStr(ref, "P76200---2S_STRENLK4") Then GetUrbanProfilbezeichnung = "76200GWCD"
If InStr(ref, "P76200---2S_STRENLK5") Then GetUrbanProfilbezeichnung = "76200GWCD"
If InStr(ref, "P76200---2S_STRENLK6") Then GetUrbanProfilbezeichnung = "76200GWCD"
If InStr(ref, "P76200---2S_STWS") Then GetUrbanProfilbezeichnung = "76200GWWD"
If InStr(ref, "P76200---2S_STWSRAL") Then GetUrbanProfilbezeichnung = "76200GWCD"
If InStr(ref, "P76200---2S_STWSREN") Then GetUrbanProfilbezeichnung = "76200GCCD"
If InStr(ref, "P76201_ST2x32") Then GetUrbanProfilbezeichnung = "76201GCC"
If InStr(ref, "P76201_ST2x39") Then GetUrbanProfilbezeichnung = "76201GCC"
If InStr(ref, "P76201_STWS") Then GetUrbanProfilbezeichnung = "76201GA"
If InStr(ref, "P76201_STWSRAL") Then GetUrbanProfilbezeichnung = "76201GWW"
If InStr(ref, "P76201_STWSREN") Then GetUrbanProfilbezeichnung = "76201GCC"
If InStr(ref, "P76201---1L2x32") Then GetUrbanProfilbezeichnung = "76201A"
If InStr(ref, "P76201---1L2x39") Then GetUrbanProfilbezeichnung = "76201A"
If InStr(ref, "P76201---1LWS") Then GetUrbanProfilbezeichnung = "76201A"
If InStr(ref, "P76201---1LWSRAL") Then GetUrbanProfilbezeichnung = "76201A"
If InStr(ref, "P76201---1LWSREN") Then GetUrbanProfilbezeichnung = "76201A"
If InStr(ref, "P76201---2L24") Then GetUrbanProfilbezeichnung = "76201WCD"
If InStr(ref, "P76201---2L2x32") Then GetUrbanProfilbezeichnung = "76201CCD"
If InStr(ref, "P76201---2L2x39") Then GetUrbanProfilbezeichnung = "76201CCD"
If InStr(ref, "P76201---2L2xRAL") Then GetUrbanProfilbezeichnung = "76201WW"
If InStr(ref, "P76201---2L2xREN") Then GetUrbanProfilbezeichnung = "76201CCD"
If InStr(ref, "P76201---2L31") Then GetUrbanProfilbezeichnung = "76201WCD"
If InStr(ref, "P76201---2L32") Then GetUrbanProfilbezeichnung = "76201WCD"
If InStr(ref, "P76201---2L39") Then GetUrbanProfilbezeichnung = "76201WCD"
If InStr(ref, "P76201---2L53") Then GetUrbanProfilbezeichnung = "76201WCD"
If InStr(ref, "P76201---2LRALWS") Then GetUrbanProfilbezeichnung = "76201A"
If InStr(ref, "P76201---2LRENLK1") Then GetUrbanProfilbezeichnung = "76201WCD"
If InStr(ref, "P76201---2LRENLK3") Then GetUrbanProfilbezeichnung = "76201WCD"
If InStr(ref, "P76201---2LRENLK4") Then GetUrbanProfilbezeichnung = "76201WCD"
If InStr(ref, "P76201---2LRENLK5") Then GetUrbanProfilbezeichnung = "76201WCD"
If InStr(ref, "P76201---2LRENLK6") Then GetUrbanProfilbezeichnung = "76201WCD"
If InStr(ref, "P76201---2LWS") Then GetUrbanProfilbezeichnung = "76201WWD"
If InStr(ref, "P76201---2LWSRAL") Then GetUrbanProfilbezeichnung = "76201WW"
If InStr(ref, "P76201---2LWSREN") Then GetUrbanProfilbezeichnung = "76201CCD"
If InStr(ref, "P76201---2L_ST24") Then GetUrbanProfilbezeichnung = "76201GWCD"
If InStr(ref, "P76201---2L_ST2x32") Then GetUrbanProfilbezeichnung = "76201GCCD"
If InStr(ref, "P76201---2L_ST2x39") Then GetUrbanProfilbezeichnung = "76201GCCD"
If InStr(ref, "P76201---2L_ST2xRAL") Then GetUrbanProfilbezeichnung = "76201GWW"
If InStr(ref, "P76201---2L_ST2xREN") Then GetUrbanProfilbezeichnung = "76201GCCD"
If InStr(ref, "P76201---2L_ST31") Then GetUrbanProfilbezeichnung = "76201GWCD"
If InStr(ref, "P76201---2L_ST32") Then GetUrbanProfilbezeichnung = "76201GWCD"
If InStr(ref, "P76201---2L_ST39") Then GetUrbanProfilbezeichnung = "76201GWCD"
If InStr(ref, "P76201---2L_ST53") Then GetUrbanProfilbezeichnung = "76201GWCD"
If InStr(ref, "P76201---2L_STRALWS") Then GetUrbanProfilbezeichnung = "76201GWW"
If InStr(ref, "P76201---2L_STRENLK1") Then GetUrbanProfilbezeichnung = "76201GWCD"
If InStr(ref, "P76201---2L_STRENLK3") Then GetUrbanProfilbezeichnung = "76201GWCD"
If InStr(ref, "P76201---2L_STRENLK4") Then GetUrbanProfilbezeichnung = "76201GWCD"
If InStr(ref, "P76201---2L_STRENLK5") Then GetUrbanProfilbezeichnung = "76201GWCD"
If InStr(ref, "P76201---2L_STRENLK6") Then GetUrbanProfilbezeichnung = "76201GWCD"
If InStr(ref, "P76201---2L_STWS") Then GetUrbanProfilbezeichnung = "76201GWWD"
If InStr(ref, "P76201---2L_STWSRAL") Then GetUrbanProfilbezeichnung = "76201GWCD"
If InStr(ref, "P76201---2L_STWSREN") Then GetUrbanProfilbezeichnung = "76201GCCD"
If InStr(ref, "P76201---2S24") Then GetUrbanProfilbezeichnung = "76201WCD"
If InStr(ref, "P76201---2S2x32") Then GetUrbanProfilbezeichnung = "76201CCD"
If InStr(ref, "P76201---2S2x39") Then GetUrbanProfilbezeichnung = "76201CCD"
If InStr(ref, "P76201---2S2xRAL") Then GetUrbanProfilbezeichnung = "76201WW"
If InStr(ref, "P76201---2S2xREN") Then GetUrbanProfilbezeichnung = "76201CCD"
If InStr(ref, "P76201---2S31") Then GetUrbanProfilbezeichnung = "76201WCD"
If InStr(ref, "P76201---2S32") Then GetUrbanProfilbezeichnung = "76201WCD"
If InStr(ref, "P76201---2S39") Then GetUrbanProfilbezeichnung = "76201WCD"
If InStr(ref, "P76201---2S53") Then GetUrbanProfilbezeichnung = "76201WCD"
If InStr(ref, "P76201---2SRALWS") Then GetUrbanProfilbezeichnung = "76201WW"
If InStr(ref, "P76201---2SRENLK1") Then GetUrbanProfilbezeichnung = "76201WCD"
If InStr(ref, "P76201---2SRENLK3") Then GetUrbanProfilbezeichnung = "76201WCD"
If InStr(ref, "P76201---2SRENLK4") Then GetUrbanProfilbezeichnung = "76201WCD"
If InStr(ref, "P76201---2SRENLK5") Then GetUrbanProfilbezeichnung = "76201WCD"
If InStr(ref, "P76201---2SRENLK6") Then GetUrbanProfilbezeichnung = "76201WCD"
If InStr(ref, "P76201---2SWS") Then GetUrbanProfilbezeichnung = "76201WWD"
If InStr(ref, "P76201---2SWSRAL") Then GetUrbanProfilbezeichnung = "76201WWD"
If InStr(ref, "P76201---2SWSREN") Then GetUrbanProfilbezeichnung = "76201CCD"
If InStr(ref, "P76201---2S_ST24") Then GetUrbanProfilbezeichnung = "76201GWCD"
If InStr(ref, "P76201---2S_ST2x32") Then GetUrbanProfilbezeichnung = "76201GCCD"
If InStr(ref, "P76201---2S_ST2x39") Then GetUrbanProfilbezeichnung = "76201GCCD"
If InStr(ref, "P76201---2S_ST2xRAL") Then GetUrbanProfilbezeichnung = "76201GWW"
If InStr(ref, "P76201---2S_ST2xREN") Then GetUrbanProfilbezeichnung = "76201GCCD"
If InStr(ref, "P76201---2S_ST31") Then GetUrbanProfilbezeichnung = "76201GWCD"
If InStr(ref, "P76201---2S_ST32") Then GetUrbanProfilbezeichnung = "76201GWCD"
If InStr(ref, "P76201---2S_ST39") Then GetUrbanProfilbezeichnung = "76201GWCD"
If InStr(ref, "P76201---2S_ST53") Then GetUrbanProfilbezeichnung = "76201GWCD"
If InStr(ref, "P76201---2S_STRALWS") Then GetUrbanProfilbezeichnung = "76201GWW"
If InStr(ref, "P76201---2S_STRENLK1") Then GetUrbanProfilbezeichnung = "76201GWCD"
If InStr(ref, "P76201---2S_STRENLK3") Then GetUrbanProfilbezeichnung = "76201GWCD"
If InStr(ref, "P76201---2S_STRENLK4") Then GetUrbanProfilbezeichnung = "76201GWCD"
If InStr(ref, "P76201---2S_STRENLK5") Then GetUrbanProfilbezeichnung = "76201GWCD"
If InStr(ref, "P76201---2S_STRENLK6") Then GetUrbanProfilbezeichnung = "76201GWCD"
If InStr(ref, "P76201---2S_STWS") Then GetUrbanProfilbezeichnung = "76201GWWD"
If InStr(ref, "P76201---2S_STWSRAL") Then GetUrbanProfilbezeichnung = "76201GWCD"
If InStr(ref, "P76201---2S_STWSREN") Then GetUrbanProfilbezeichnung = "76201GCCD"
If InStr(ref, "P76204_ST2x32") Then GetUrbanProfilbezeichnung = "76204GCC"
If InStr(ref, "P76204_ST2x39") Then GetUrbanProfilbezeichnung = "76204GCC"
If InStr(ref, "P76204_STWS") Then GetUrbanProfilbezeichnung = "76204GA"
If InStr(ref, "P76204_STWSRAL") Then GetUrbanProfilbezeichnung = "76204GWW"
If InStr(ref, "P76204_STWSREN") Then GetUrbanProfilbezeichnung = "76204GCC"
If InStr(ref, "P76204---1L2x32") Then GetUrbanProfilbezeichnung = "76204A"
If InStr(ref, "P76204---1L2x39") Then GetUrbanProfilbezeichnung = "76204A"
If InStr(ref, "P76204---1LWS") Then GetUrbanProfilbezeichnung = "76204A"
If InStr(ref, "P76204---1LWSRAL") Then GetUrbanProfilbezeichnung = "76204A"
If InStr(ref, "P76204---1LWSREN") Then GetUrbanProfilbezeichnung = "76204A"
If InStr(ref, "P76204---2L24") Then GetUrbanProfilbezeichnung = "76204WCD"
If InStr(ref, "P76204---2L2x32") Then GetUrbanProfilbezeichnung = "76204CCD"
If InStr(ref, "P76204---2L2x39") Then GetUrbanProfilbezeichnung = "76204CCD"
If InStr(ref, "P76204---2L2xRAL") Then GetUrbanProfilbezeichnung = "76204WW"
If InStr(ref, "P76204---2L2xREN") Then GetUrbanProfilbezeichnung = "76204CCD"
If InStr(ref, "P76204---2L31") Then GetUrbanProfilbezeichnung = "76204WCD"
If InStr(ref, "P76204---2L32") Then GetUrbanProfilbezeichnung = "76204WCD"
If InStr(ref, "P76204---2L39") Then GetUrbanProfilbezeichnung = "76204WCD"
If InStr(ref, "P76204---2L53") Then GetUrbanProfilbezeichnung = "76204WCD"
If InStr(ref, "P76204---2LRALWS") Then GetUrbanProfilbezeichnung = "76204A"
If InStr(ref, "P76204---2LRENLK1") Then GetUrbanProfilbezeichnung = "76204WCD"
If InStr(ref, "P76204---2LRENLK3") Then GetUrbanProfilbezeichnung = "76204WCD"
If InStr(ref, "P76204---2LRENLK4") Then GetUrbanProfilbezeichnung = "76204WCD"
If InStr(ref, "P76204---2LRENLK5") Then GetUrbanProfilbezeichnung = "76204WCD"
If InStr(ref, "P76204---2LRENLK6") Then GetUrbanProfilbezeichnung = "76204WCD"
If InStr(ref, "P76204---2LWS") Then GetUrbanProfilbezeichnung = "76204WWD"
If InStr(ref, "P76204---2LWSRAL") Then GetUrbanProfilbezeichnung = "76204WW"
If InStr(ref, "P76204---2LWSREN") Then GetUrbanProfilbezeichnung = "76204CCD"
If InStr(ref, "P76204---2L_ST24") Then GetUrbanProfilbezeichnung = "76204GWCD"
If InStr(ref, "P76204---2L_ST2x32") Then GetUrbanProfilbezeichnung = "76204GCCD"
If InStr(ref, "P76204---2L_ST2x39") Then GetUrbanProfilbezeichnung = "76204GCCD"
If InStr(ref, "P76204---2L_ST2xRAL") Then GetUrbanProfilbezeichnung = "76204GWW"
If InStr(ref, "P76204---2L_ST2xREN") Then GetUrbanProfilbezeichnung = "76204GCCD"
If InStr(ref, "P76204---2L_ST31") Then GetUrbanProfilbezeichnung = "76204GWCD"
If InStr(ref, "P76204---2L_ST32") Then GetUrbanProfilbezeichnung = "76204GWCD"
If InStr(ref, "P76204---2L_ST39") Then GetUrbanProfilbezeichnung = "76204GWCD"
If InStr(ref, "P76204---2L_ST53") Then GetUrbanProfilbezeichnung = "76204GWCD"
If InStr(ref, "P76204---2L_STRALWS") Then GetUrbanProfilbezeichnung = "76204GWW"
If InStr(ref, "P76204---2L_STRENLK1") Then GetUrbanProfilbezeichnung = "76204GWCD"
If InStr(ref, "P76204---2L_STRENLK3") Then GetUrbanProfilbezeichnung = "76204GWCD"
If InStr(ref, "P76204---2L_STRENLK4") Then GetUrbanProfilbezeichnung = "76204GWCD"
If InStr(ref, "P76204---2L_STRENLK5") Then GetUrbanProfilbezeichnung = "76204GWCD"
If InStr(ref, "P76204---2L_STRENLK6") Then GetUrbanProfilbezeichnung = "76204GWCD"
If InStr(ref, "P76204---2L_STWS") Then GetUrbanProfilbezeichnung = "76204GWWD"
If InStr(ref, "P76204---2L_STWSRAL") Then GetUrbanProfilbezeichnung = "76204GWW"
If InStr(ref, "P76204---2L_STWSREN") Then GetUrbanProfilbezeichnung = "76204GCCD"
If InStr(ref, "P76204---2S24") Then GetUrbanProfilbezeichnung = "76204WCD"
If InStr(ref, "P76204---2S2x32") Then GetUrbanProfilbezeichnung = "76204CCD"
If InStr(ref, "P76204---2S2x39") Then GetUrbanProfilbezeichnung = "76204CCD"
If InStr(ref, "P76204---2S2xRAL") Then GetUrbanProfilbezeichnung = "76204WW"
If InStr(ref, "P76204---2S2xREN") Then GetUrbanProfilbezeichnung = "76204CCD"
If InStr(ref, "P76204---2S31") Then GetUrbanProfilbezeichnung = "76204WCD"
If InStr(ref, "P76204---2S32") Then GetUrbanProfilbezeichnung = "76204WCD"
If InStr(ref, "P76204---2S39") Then GetUrbanProfilbezeichnung = "76204WCD"
If InStr(ref, "P76204---2S53") Then GetUrbanProfilbezeichnung = "76204WCD"
If InStr(ref, "P76204---2SRALWS") Then GetUrbanProfilbezeichnung = "76204WW"
If InStr(ref, "P76204---2SRENLK1") Then GetUrbanProfilbezeichnung = "76204WCD"
If InStr(ref, "P76204---2SRENLK3") Then GetUrbanProfilbezeichnung = "76204WCD"
If InStr(ref, "P76204---2SRENLK4") Then GetUrbanProfilbezeichnung = "76204WCD"
If InStr(ref, "P76204---2SRENLK5") Then GetUrbanProfilbezeichnung = "76204WCD"
If InStr(ref, "P76204---2SRENLK6") Then GetUrbanProfilbezeichnung = "76204WCD"
If InStr(ref, "P76204---2SWS") Then GetUrbanProfilbezeichnung = "76204WWD"
If InStr(ref, "P76204---2SWSRAL") Then GetUrbanProfilbezeichnung = "76204WW"
If InStr(ref, "P76204---2SWSREN") Then GetUrbanProfilbezeichnung = "76204CCD"
If InStr(ref, "P76204---2S_ST24") Then GetUrbanProfilbezeichnung = "76204GWCD"
If InStr(ref, "P76204---2S_ST2x32") Then GetUrbanProfilbezeichnung = "76204GCCD"
If InStr(ref, "P76204---2S_ST2x39") Then GetUrbanProfilbezeichnung = "76204GCCD"
If InStr(ref, "P76204---2S_ST2xRAL") Then GetUrbanProfilbezeichnung = "76204GWW"
If InStr(ref, "P76204---2S_ST2xREN") Then GetUrbanProfilbezeichnung = "76204GCCD"
If InStr(ref, "P76204---2S_ST31") Then GetUrbanProfilbezeichnung = "76204GWCD"
If InStr(ref, "P76204---2S_ST32") Then GetUrbanProfilbezeichnung = "76204GWCD"
If InStr(ref, "P76204---2S_ST39") Then GetUrbanProfilbezeichnung = "76204GWCD"
If InStr(ref, "P76204---2S_ST53") Then GetUrbanProfilbezeichnung = "76204GWCD"
If InStr(ref, "P76204---2S_STRALWS") Then GetUrbanProfilbezeichnung = "76204GWW"
If InStr(ref, "P76204---2S_STRENLK1") Then GetUrbanProfilbezeichnung = "76204GWCD"
If InStr(ref, "P76204---2S_STRENLK3") Then GetUrbanProfilbezeichnung = "76204GWCD"
If InStr(ref, "P76204---2S_STRENLK4") Then GetUrbanProfilbezeichnung = "76204GWCD"
If InStr(ref, "P76204---2S_STRENLK5") Then GetUrbanProfilbezeichnung = "76204GWCD"
If InStr(ref, "P76204---2S_STRENLK6") Then GetUrbanProfilbezeichnung = "76204GWCD"
If InStr(ref, "P76204---2S_STWS") Then GetUrbanProfilbezeichnung = "76204GWWD"
If InStr(ref, "P76204---2S_STWSRAL") Then GetUrbanProfilbezeichnung = "76204GWW"
If InStr(ref, "P76204---2S_STWSREN") Then GetUrbanProfilbezeichnung = "76204GCCD"
If InStr(ref, "P761012x32") Then GetUrbanProfilbezeichnung = "76101CC"
If InStr(ref, "P761012x39") Then GetUrbanProfilbezeichnung = "76101CC"
If InStr(ref, "P76101WS") Then GetUrbanProfilbezeichnung = "76101A"
If InStr(ref, "P76101WSRAL") Then GetUrbanProfilbezeichnung = "76101WW"
If InStr(ref, "P76101WSREN") Then GetUrbanProfilbezeichnung = "76101CC"
If InStr(ref, "P76101---1L24") Then GetUrbanProfilbezeichnung = "76101WCD"
If InStr(ref, "P76101---1L2x32") Then GetUrbanProfilbezeichnung = "76101CCD"
If InStr(ref, "P76101---1L2x39") Then GetUrbanProfilbezeichnung = "76101CCD"
If InStr(ref, "P76101---1L2xRAL") Then GetUrbanProfilbezeichnung = "76101WW"
If InStr(ref, "P76101---1L2xREN") Then GetUrbanProfilbezeichnung = "76101CCD"
If InStr(ref, "P76101---1L31") Then GetUrbanProfilbezeichnung = "76101WCD"
If InStr(ref, "P76101---1L32") Then GetUrbanProfilbezeichnung = "76101WCD"
If InStr(ref, "P76101---1L39") Then GetUrbanProfilbezeichnung = "76101WCD"
If InStr(ref, "P76101---1L53") Then GetUrbanProfilbezeichnung = "76101WCD"
If InStr(ref, "P76101---1LRALWS") Then GetUrbanProfilbezeichnung = "76101A"
If InStr(ref, "P76101---1LRENLK1") Then GetUrbanProfilbezeichnung = "76101WCD"
If InStr(ref, "P76101---1LRENLK3") Then GetUrbanProfilbezeichnung = "76101WCD"
If InStr(ref, "P76101---1LRENLK4") Then GetUrbanProfilbezeichnung = "76101WCD"
If InStr(ref, "P76101---1LRENLK5") Then GetUrbanProfilbezeichnung = "76101WCD"
If InStr(ref, "P76101---1LRENLK6") Then GetUrbanProfilbezeichnung = "76101WCD"
If InStr(ref, "P76101---1LWS") Then GetUrbanProfilbezeichnung = "76101WWD"
If InStr(ref, "P76101---1LWSRAL") Then GetUrbanProfilbezeichnung = "76101WWD"
If InStr(ref, "P76101---1LWSREN") Then GetUrbanProfilbezeichnung = "76101CCD"
If InStr(ref, "P76101---1S24") Then GetUrbanProfilbezeichnung = "76101WCD"
If InStr(ref, "P76101---1S2x32") Then GetUrbanProfilbezeichnung = "76101CCD"
If InStr(ref, "P76101---1S2x39") Then GetUrbanProfilbezeichnung = "76101CCD"
If InStr(ref, "P76101---1S2xRAL") Then GetUrbanProfilbezeichnung = "76101WW"
If InStr(ref, "P76101---1S2xREN") Then GetUrbanProfilbezeichnung = "76101CCD"
If InStr(ref, "P76101---1S31") Then GetUrbanProfilbezeichnung = "76101WCD"
If InStr(ref, "P76101---1S32") Then GetUrbanProfilbezeichnung = "76101WCD"
If InStr(ref, "P76101---1S39") Then GetUrbanProfilbezeichnung = "76101WCD"
If InStr(ref, "P76101---1S53") Then GetUrbanProfilbezeichnung = "76101WCD"
If InStr(ref, "P76101---1SRALWS") Then GetUrbanProfilbezeichnung = "76101A"
If InStr(ref, "P76101---1SRENLK1") Then GetUrbanProfilbezeichnung = "76101WCD"
If InStr(ref, "P76101---1SRENLK3") Then GetUrbanProfilbezeichnung = "76101WCD"
If InStr(ref, "P76101---1SRENLK4") Then GetUrbanProfilbezeichnung = "76101WCD"
If InStr(ref, "P76101---1SRENLK5") Then GetUrbanProfilbezeichnung = "76101WCD"
If InStr(ref, "P76101---1SRENLK6") Then GetUrbanProfilbezeichnung = "76101WCD"
If InStr(ref, "P76101---1SWS") Then GetUrbanProfilbezeichnung = "76101WWD"
If InStr(ref, "P76101---1SWSRAL") Then GetUrbanProfilbezeichnung = "76101WW"
If InStr(ref, "P76101---1SWSREN") Then GetUrbanProfilbezeichnung = "76101CCD"
If InStr(ref, "P761022x32") Then GetUrbanProfilbezeichnung = "76102CC"
If InStr(ref, "P761022x39") Then GetUrbanProfilbezeichnung = "76102CC"
If InStr(ref, "P76102WS") Then GetUrbanProfilbezeichnung = "76102A"
If InStr(ref, "P76102WSRAL") Then GetUrbanProfilbezeichnung = "76102A"
If InStr(ref, "P76102WSREN") Then GetUrbanProfilbezeichnung = "76102CC"
If InStr(ref, "P76102---1L24") Then GetUrbanProfilbezeichnung = "76102WCD"
If InStr(ref, "P76102---1L2x32") Then GetUrbanProfilbezeichnung = "76102CCD"
If InStr(ref, "P76102---1L2x39") Then GetUrbanProfilbezeichnung = "76102CCD"
If InStr(ref, "P76102---1L2xRAL") Then GetUrbanProfilbezeichnung = "76102A"
If InStr(ref, "P76102---1L2xREN") Then GetUrbanProfilbezeichnung = "76102CCD"
If InStr(ref, "P76102---1L31") Then GetUrbanProfilbezeichnung = "76102WCD"
If InStr(ref, "P76102---1L32") Then GetUrbanProfilbezeichnung = "76102WCD"
If InStr(ref, "P76102---1L39") Then GetUrbanProfilbezeichnung = "76102WCD"
If InStr(ref, "P76102---1L53") Then GetUrbanProfilbezeichnung = "76102WCD"
If InStr(ref, "P76102---1LRALWS") Then GetUrbanProfilbezeichnung = "76102A"
If InStr(ref, "P76102---1LRENLK1") Then GetUrbanProfilbezeichnung = "76102WCD"
If InStr(ref, "P76102---1LRENLK3") Then GetUrbanProfilbezeichnung = "76102WCD"
If InStr(ref, "P76102---1LRENLK4") Then GetUrbanProfilbezeichnung = "76102WCD"
If InStr(ref, "P76102---1LRENLK5") Then GetUrbanProfilbezeichnung = "76102WCD"
If InStr(ref, "P76102---1LRENLK6") Then GetUrbanProfilbezeichnung = "76102WCD"
If InStr(ref, "P76102---1LWS") Then GetUrbanProfilbezeichnung = "76102WWD"
If InStr(ref, "P76102---1LWSRAL") Then GetUrbanProfilbezeichnung = "76102WWD"
If InStr(ref, "P76102---1LWSREN") Then GetUrbanProfilbezeichnung = "76102CCD"
If InStr(ref, "P76102---1S24") Then GetUrbanProfilbezeichnung = "76102WCD"
If InStr(ref, "P76102---1S2x32") Then GetUrbanProfilbezeichnung = "76102CCD"
If InStr(ref, "P76102---1S2x39") Then GetUrbanProfilbezeichnung = "76102CCD"
If InStr(ref, "P76102---1S2xRAL") Then GetUrbanProfilbezeichnung = "76102WW"
If InStr(ref, "P76102---1S2xREN") Then GetUrbanProfilbezeichnung = "76102CCD"
If InStr(ref, "P76102---1S31") Then GetUrbanProfilbezeichnung = "76102WCD"
If InStr(ref, "P76102---1S32") Then GetUrbanProfilbezeichnung = "76102WCD"
If InStr(ref, "P76102---1S39") Then GetUrbanProfilbezeichnung = "76102WCD"
If InStr(ref, "P76102---1S53") Then GetUrbanProfilbezeichnung = "76102WCD"
If InStr(ref, "P76102---1SRALWS") Then GetUrbanProfilbezeichnung = "76102A"
If InStr(ref, "P76102---1SRENLK1") Then GetUrbanProfilbezeichnung = "76102WCD"
If InStr(ref, "P76102---1SRENLK3") Then GetUrbanProfilbezeichnung = "76102WCD"
If InStr(ref, "P76102---1SRENLK4") Then GetUrbanProfilbezeichnung = "76102WCD"
If InStr(ref, "P76102---1SRENLK5") Then GetUrbanProfilbezeichnung = "76102WCD"
If InStr(ref, "P76102---1SRENLK6") Then GetUrbanProfilbezeichnung = "76102WCD"
If InStr(ref, "P76102---1SWS") Then GetUrbanProfilbezeichnung = "76102WWD"
If InStr(ref, "P76102---1SWSRAL") Then GetUrbanProfilbezeichnung = "76102A"
If InStr(ref, "P76102---1SWSREN") Then GetUrbanProfilbezeichnung = "76102CCD"
If InStr(ref, "P761032x32") Then GetUrbanProfilbezeichnung = "76103CC"
If InStr(ref, "P761032x39") Then GetUrbanProfilbezeichnung = "76103CC"
If InStr(ref, "P76103WS") Then GetUrbanProfilbezeichnung = "76103A"
If InStr(ref, "P76103WSRAL") Then GetUrbanProfilbezeichnung = "76103A"
If InStr(ref, "P76103WSREN") Then GetUrbanProfilbezeichnung = "76103CC"
If InStr(ref, "P76103---1L24") Then GetUrbanProfilbezeichnung = "76103WCD"
If InStr(ref, "P76103---1L2x32") Then GetUrbanProfilbezeichnung = "76103CCD"
If InStr(ref, "P76103---1L2x39") Then GetUrbanProfilbezeichnung = "76103CCD"
If InStr(ref, "P76103---1L2xRAL") Then GetUrbanProfilbezeichnung = "76103WW"
If InStr(ref, "P76103---1L2xREN") Then GetUrbanProfilbezeichnung = "76103CCD"
If InStr(ref, "P76103---1L31") Then GetUrbanProfilbezeichnung = "76103WCD"
If InStr(ref, "P76103---1L32") Then GetUrbanProfilbezeichnung = "76103WCD"
If InStr(ref, "P76103---1L39") Then GetUrbanProfilbezeichnung = "76103WCD"
If InStr(ref, "P76103---1L53") Then GetUrbanProfilbezeichnung = "76103WCD"
If InStr(ref, "P76103---1LRALWS") Then GetUrbanProfilbezeichnung = "76103A"
If InStr(ref, "P76103---1LRENLK1") Then GetUrbanProfilbezeichnung = "76103WCD"
If InStr(ref, "P76103---1LRENLK3") Then GetUrbanProfilbezeichnung = "76103WCD"
If InStr(ref, "P76103---1LRENLK4") Then GetUrbanProfilbezeichnung = "76103WCD"
If InStr(ref, "P76103---1LRENLK5") Then GetUrbanProfilbezeichnung = "76103WCD"
If InStr(ref, "P76103---1LRENLK6") Then GetUrbanProfilbezeichnung = "76103WCD"
If InStr(ref, "P76103---1LWS") Then GetUrbanProfilbezeichnung = "76103WWD"
If InStr(ref, "P76103---1LWSRAL") Then GetUrbanProfilbezeichnung = "76103WWD"
If InStr(ref, "P76103---1LWSREN") Then GetUrbanProfilbezeichnung = "76103CCD"
If InStr(ref, "P76103---1S24") Then GetUrbanProfilbezeichnung = "76103WCD"
If InStr(ref, "P76103---1S2x32") Then GetUrbanProfilbezeichnung = "76103CCD"
If InStr(ref, "P76103---1S2x39") Then GetUrbanProfilbezeichnung = "76103CCD"
If InStr(ref, "P76103---1S2xRAL") Then GetUrbanProfilbezeichnung = "76103WW"
If InStr(ref, "P76103---1S2xREN") Then GetUrbanProfilbezeichnung = "76103CCD"
If InStr(ref, "P76103---1S31") Then GetUrbanProfilbezeichnung = "76103WCD"
If InStr(ref, "P76103---1S32") Then GetUrbanProfilbezeichnung = "76103WCD"
If InStr(ref, "P76103---1S39") Then GetUrbanProfilbezeichnung = "76103WCD"
If InStr(ref, "P76103---1S53") Then GetUrbanProfilbezeichnung = "76103WCD"
If InStr(ref, "P76103---1SRALWS") Then GetUrbanProfilbezeichnung = "76103A"
If InStr(ref, "P76103---1SRENLK1") Then GetUrbanProfilbezeichnung = "76103WCD"
If InStr(ref, "P76103---1SRENLK3") Then GetUrbanProfilbezeichnung = "76103WCD"
If InStr(ref, "P76103---1SRENLK4") Then GetUrbanProfilbezeichnung = "76103WCD"
If InStr(ref, "P76103---1SRENLK5") Then GetUrbanProfilbezeichnung = "76103WCD"
If InStr(ref, "P76103---1SRENLK6") Then GetUrbanProfilbezeichnung = "76103WCD"
If InStr(ref, "P76103---1SWS") Then GetUrbanProfilbezeichnung = "76103WWD"
If InStr(ref, "P76103---1SWSRAL") Then GetUrbanProfilbezeichnung = "76103WWD"
If InStr(ref, "P76103---1SWSREN") Then GetUrbanProfilbezeichnung = "76103CCD"

			'-----------------------------
			'76 MD     ------------------
			'-----------------------------

If InStr(ref, "P76171---1L2x32") Then GetUrbanProfilbezeichnung = "76171CC"
If InStr(ref, "P76171---1L2x39") Then GetUrbanProfilbezeichnung = "76171CC"
If InStr(ref, "P76171---1LWS") Then GetUrbanProfilbezeichnung = "76171A"
If InStr(ref, "P76171---1LWSRAL") Then GetUrbanProfilbezeichnung = "76171A"
If InStr(ref, "P76171---1LWSREN") Then GetUrbanProfilbezeichnung = "76171CCD"
If InStr(ref, "P76171---2L24") Then GetUrbanProfilbezeichnung = "76171WCD"
If InStr(ref, "P76171---2L2x32") Then GetUrbanProfilbezeichnung = "76171CCD"
If InStr(ref, "P76171---2L2x39") Then GetUrbanProfilbezeichnung = "76171CCD"
If InStr(ref, "P76171---2L2xRAL") Then GetUrbanProfilbezeichnung = "76171WW"
If InStr(ref, "P76171---2L2xREN") Then GetUrbanProfilbezeichnung = "76171CCD"
If InStr(ref, "P76171---2L31") Then GetUrbanProfilbezeichnung = "76171WCD"
If InStr(ref, "P76171---2L32") Then GetUrbanProfilbezeichnung = "76171WCD"
If InStr(ref, "P76171---2L39") Then GetUrbanProfilbezeichnung = "76171WCD"
If InStr(ref, "P76171---2L53") Then GetUrbanProfilbezeichnung = "76171WCD"
If InStr(ref, "P76171---2LRALWS") Then GetUrbanProfilbezeichnung = "76171WW"
If InStr(ref, "P76171---2LRENLK1") Then GetUrbanProfilbezeichnung = "76171WCD"
If InStr(ref, "P76171---2LRENLK3") Then GetUrbanProfilbezeichnung = "76171WCD"
If InStr(ref, "P76171---2LRENLK4") Then GetUrbanProfilbezeichnung = "76171WCD"
If InStr(ref, "P76171---2LRENLK5") Then GetUrbanProfilbezeichnung = "76171WCD"
If InStr(ref, "P76171---2LRENLK6") Then GetUrbanProfilbezeichnung = "76171WCD"
If InStr(ref, "P76171---2LWS") Then GetUrbanProfilbezeichnung = "76171WWD"
If InStr(ref, "P76171---2LWSRAL") Then GetUrbanProfilbezeichnung = "76171WW"
If InStr(ref, "P76171---2LWSREN") Then GetUrbanProfilbezeichnung = "76171CCD"
If InStr(ref, "P76171---2S24") Then GetUrbanProfilbezeichnung = "76171WCD"
If InStr(ref, "P76171---2S2x32") Then GetUrbanProfilbezeichnung = "76171CCD"
If InStr(ref, "P76171---2S2x39") Then GetUrbanProfilbezeichnung = "76171CCD"
If InStr(ref, "P76171---2S2xRAL") Then GetUrbanProfilbezeichnung = "76171WW"
If InStr(ref, "P76171---2S2xREN") Then GetUrbanProfilbezeichnung = "76171CCD"
If InStr(ref, "P76171---2S31") Then GetUrbanProfilbezeichnung = "76171WCD"
If InStr(ref, "P76171---2S32") Then GetUrbanProfilbezeichnung = "76171WCD"
If InStr(ref, "P76171---2S39") Then GetUrbanProfilbezeichnung = "76171WCD"
If InStr(ref, "P76171---2S53") Then GetUrbanProfilbezeichnung = "76171WCD"
If InStr(ref, "P76171---2SRALWS") Then GetUrbanProfilbezeichnung = "76171WW"
If InStr(ref, "P76171---2SRENLK1") Then GetUrbanProfilbezeichnung = "76171WCD"
If InStr(ref, "P76171---2SRENLK3") Then GetUrbanProfilbezeichnung = "76171WCD"
If InStr(ref, "P76171---2SRENLK4") Then GetUrbanProfilbezeichnung = "76171WCD"
If InStr(ref, "P76171---2SRENLK5") Then GetUrbanProfilbezeichnung = "76171WCD"
If InStr(ref, "P76171---2SRENLK6") Then GetUrbanProfilbezeichnung = "76171WCD"
If InStr(ref, "P76171---2SWS") Then GetUrbanProfilbezeichnung = "76171WCD"
If InStr(ref, "P76171---2SWSRAL") Then GetUrbanProfilbezeichnung = "76171WCD"
If InStr(ref, "P76171---2SWSREN") Then GetUrbanProfilbezeichnung = "76171CCD"
If InStr(ref, "P76172---1L2x32") Then GetUrbanProfilbezeichnung = "76172A"
If InStr(ref, "P76172---1L2x39") Then GetUrbanProfilbezeichnung = "76172A"
If InStr(ref, "P76172---1LWS") Then GetUrbanProfilbezeichnung = "76172A"
If InStr(ref, "P76172---1LWSRAL") Then GetUrbanProfilbezeichnung = "76172A"
If InStr(ref, "P76172---1LWSREN") Then GetUrbanProfilbezeichnung = "76172CC"
If InStr(ref, "P76172---2L24") Then GetUrbanProfilbezeichnung = "76172WCD"
If InStr(ref, "P76172---2L2x32") Then GetUrbanProfilbezeichnung = "76172CCD"
If InStr(ref, "P76172---2L2x39") Then GetUrbanProfilbezeichnung = "76172CCD"
If InStr(ref, "P76172---2L2xRAL") Then GetUrbanProfilbezeichnung = "76172WW"
If InStr(ref, "P76172---2L2xREN") Then GetUrbanProfilbezeichnung = "76172CCD"
If InStr(ref, "P76172---2L31") Then GetUrbanProfilbezeichnung = "76172WCD"
If InStr(ref, "P76172---2L32") Then GetUrbanProfilbezeichnung = "76172WCD"
If InStr(ref, "P76172---2L39") Then GetUrbanProfilbezeichnung = "76172WCD"
If InStr(ref, "P76172---2L53") Then GetUrbanProfilbezeichnung = "76172WCD"
If InStr(ref, "P76172---2LRALWS") Then GetUrbanProfilbezeichnung = "76172WW"
If InStr(ref, "P76172---2LRENLK1") Then GetUrbanProfilbezeichnung = "76172WCD"
If InStr(ref, "P76172---2LRENLK3") Then GetUrbanProfilbezeichnung = "76172WCD"
If InStr(ref, "P76172---2LRENLK4") Then GetUrbanProfilbezeichnung = "76172WCD"
If InStr(ref, "P76172---2LRENLK5") Then GetUrbanProfilbezeichnung = "76172WCD"
If InStr(ref, "P76172---2LRENLK6") Then GetUrbanProfilbezeichnung = "76172WCD"
If InStr(ref, "P76172---2LWS") Then GetUrbanProfilbezeichnung = "76172WWD"
If InStr(ref, "P76172---2LWSRAL") Then GetUrbanProfilbezeichnung = "76172WW"
If InStr(ref, "P76172---2LWSREN") Then GetUrbanProfilbezeichnung = "76172CCD"
If InStr(ref, "P76172---2S24") Then GetUrbanProfilbezeichnung = "76172WCD"
If InStr(ref, "P76172---2S2x32") Then GetUrbanProfilbezeichnung = "76172CCD"
If InStr(ref, "P76172---2S2x39") Then GetUrbanProfilbezeichnung = "76172CCD"
If InStr(ref, "P76172---2S2xRAL") Then GetUrbanProfilbezeichnung = "76172WW"
If InStr(ref, "P76172---2S2xREN") Then GetUrbanProfilbezeichnung = "76172CCD"
If InStr(ref, "P76172---2S31") Then GetUrbanProfilbezeichnung = "76172WCD"
If InStr(ref, "P76172---2S32") Then GetUrbanProfilbezeichnung = "76172WCD"
If InStr(ref, "P76172---2S39") Then GetUrbanProfilbezeichnung = "76172WCD"
If InStr(ref, "P76172---2S53") Then GetUrbanProfilbezeichnung = "76172WCD"
If InStr(ref, "P76172---2SRALWS") Then GetUrbanProfilbezeichnung = "76172WW"
If InStr(ref, "P76172---2SRENLK1") Then GetUrbanProfilbezeichnung = "76172WCD"
If InStr(ref, "P76172---2SRENLK3") Then GetUrbanProfilbezeichnung = "76172WCD"
If InStr(ref, "P76172---2SRENLK4") Then GetUrbanProfilbezeichnung = "76172WCD"
If InStr(ref, "P76172---2SRENLK5") Then GetUrbanProfilbezeichnung = "76172WCD"
If InStr(ref, "P76172---2SRENLK6") Then GetUrbanProfilbezeichnung = "76172WCD"
If InStr(ref, "P76172---2SWS") Then GetUrbanProfilbezeichnung = "76172WWD"
If InStr(ref, "P76172---2SWSRAL") Then GetUrbanProfilbezeichnung = "76172WW"
If InStr(ref, "P76172---2SWSREN") Then GetUrbanProfilbezeichnung = "76172CCD"
If InStr(ref, "P76173---1L2x32") Then GetUrbanProfilbezeichnung = "76173CC"
If InStr(ref, "P76173---1L2x39") Then GetUrbanProfilbezeichnung = "76173CC"
If InStr(ref, "P76173---1LWS") Then GetUrbanProfilbezeichnung = "76173A"
If InStr(ref, "P76173---1LWSRAL") Then GetUrbanProfilbezeichnung = "76173A"
If InStr(ref, "P76173---1LWSREN") Then GetUrbanProfilbezeichnung = "76173CC"
If InStr(ref, "P76173---2L24") Then GetUrbanProfilbezeichnung = "76173WCD"
If InStr(ref, "P76173---2L2x32") Then GetUrbanProfilbezeichnung = "76173CCD"
If InStr(ref, "P76173---2L2x39") Then GetUrbanProfilbezeichnung = "76173CCD"
If InStr(ref, "P76173---2L2xRAL") Then GetUrbanProfilbezeichnung = "76173WW"
If InStr(ref, "P76173---2L2xREN") Then GetUrbanProfilbezeichnung = "76173CCD"
If InStr(ref, "P76173---2L31") Then GetUrbanProfilbezeichnung = "76173WCD"
If InStr(ref, "P76173---2L32") Then GetUrbanProfilbezeichnung = "76173WCD"
If InStr(ref, "P76173---2L39") Then GetUrbanProfilbezeichnung = "76173WCD"
If InStr(ref, "P76173---2L53") Then GetUrbanProfilbezeichnung = "76173WCD"
If InStr(ref, "P76173---2LRALWS") Then GetUrbanProfilbezeichnung = "76173WW"
If InStr(ref, "P76173---2LRENLK1") Then GetUrbanProfilbezeichnung = "76173WCD"
If InStr(ref, "P76173---2LRENLK3") Then GetUrbanProfilbezeichnung = "76173WCD"
If InStr(ref, "P76173---2LRENLK4") Then GetUrbanProfilbezeichnung = "76173WCD"
If InStr(ref, "P76173---2LRENLK5") Then GetUrbanProfilbezeichnung = "76173WCD"
If InStr(ref, "P76173---2LRENLK6") Then GetUrbanProfilbezeichnung = "76173WCD"
If InStr(ref, "P76173---2LWS") Then GetUrbanProfilbezeichnung = "76173WWD"
If InStr(ref, "P76173---2LWSRAL") Then GetUrbanProfilbezeichnung = "76173A"
If InStr(ref, "P76173---2LWSREN") Then GetUrbanProfilbezeichnung = "76173CCD"
If InStr(ref, "P76173---2S24") Then GetUrbanProfilbezeichnung = "76173WCD"
If InStr(ref, "P76173---2S2x32") Then GetUrbanProfilbezeichnung = "76173CCD"
If InStr(ref, "P76173---2S2x39") Then GetUrbanProfilbezeichnung = "76173CCD"
If InStr(ref, "P76173---2S2xRAL") Then GetUrbanProfilbezeichnung = "76173WW"
If InStr(ref, "P76173---2S2xREN") Then GetUrbanProfilbezeichnung = "76173CCD"
If InStr(ref, "P76173---2S31") Then GetUrbanProfilbezeichnung = "76173WCD"
If InStr(ref, "P76173---2S32") Then GetUrbanProfilbezeichnung = "76173WCD"
If InStr(ref, "P76173---2S39") Then GetUrbanProfilbezeichnung = "76173WCD"
If InStr(ref, "P76173---2S53") Then GetUrbanProfilbezeichnung = "76173WCD"
If InStr(ref, "P76173---2SRALWS") Then GetUrbanProfilbezeichnung = "76173WW"
If InStr(ref, "P76173---2SRENLK1") Then GetUrbanProfilbezeichnung = "76173WCD"
If InStr(ref, "P76173---2SRENLK3") Then GetUrbanProfilbezeichnung = "76173WCD"
If InStr(ref, "P76173---2SRENLK4") Then GetUrbanProfilbezeichnung = "76173WCD"
If InStr(ref, "P76173---2SRENLK5") Then GetUrbanProfilbezeichnung = "76173WCD"
If InStr(ref, "P76173---2SRENLK6") Then GetUrbanProfilbezeichnung = "76173WCD"
If InStr(ref, "P76173---2SWS") Then GetUrbanProfilbezeichnung = "76173WWD"
If InStr(ref, "P76173---2SWSRAL") Then GetUrbanProfilbezeichnung = "76173A"
If InStr(ref, "P76173---2SWSREN") Then GetUrbanProfilbezeichnung = "76173CCD"
If InStr(ref, "P76270_ST2x32") Then GetUrbanProfilbezeichnung = "76270GCCD"
If InStr(ref, "P76270_ST2x39") Then GetUrbanProfilbezeichnung = "76270GCCD"
If InStr(ref, "P76270_STWS") Then GetUrbanProfilbezeichnung = "76270GWWD"
If InStr(ref, "P76270_STWSRAL") Then GetUrbanProfilbezeichnung = "76270GA"
If InStr(ref, "P76270_STWSREN") Then GetUrbanProfilbezeichnung = "76270GCCD"
If InStr(ref, "P76270---1L2x32") Then GetUrbanProfilbezeichnung = "76270CC"
If InStr(ref, "P76270---1L2x39") Then GetUrbanProfilbezeichnung = "76270CC"
If InStr(ref, "P76270---1LWS") Then GetUrbanProfilbezeichnung = "76270A"
If InStr(ref, "P76270---1LWSRAL") Then GetUrbanProfilbezeichnung = "76270A"
If InStr(ref, "P76270---1LWSREN") Then GetUrbanProfilbezeichnung = "76270CC"
If InStr(ref, "P76270---2L_ST24") Then GetUrbanProfilbezeichnung = "76270GWCD"
If InStr(ref, "P76270---2L_ST2x32") Then GetUrbanProfilbezeichnung = "76270GCCD"
If InStr(ref, "P76270---2L_ST2x39") Then GetUrbanProfilbezeichnung = "76270GCCD"
If InStr(ref, "P76270---2L_ST2xRAL") Then GetUrbanProfilbezeichnung = "76270GWW"
If InStr(ref, "P76270---2L_ST2xREN") Then GetUrbanProfilbezeichnung = "76270GCCD"
If InStr(ref, "P76270---2L_ST31") Then GetUrbanProfilbezeichnung = "76270GWCD"
If InStr(ref, "P76270---2L_ST32") Then GetUrbanProfilbezeichnung = "76270GWCD"
If InStr(ref, "P76270---2L_ST39") Then GetUrbanProfilbezeichnung = "76270GWCD"
If InStr(ref, "P76270---2L_ST53") Then GetUrbanProfilbezeichnung = "76270GWCD"
If InStr(ref, "P76270---2L_STRALWS") Then GetUrbanProfilbezeichnung = "76270GWW"
If InStr(ref, "P76270---2L_STRENLK1") Then GetUrbanProfilbezeichnung = "76270GWCD"
If InStr(ref, "P76270---2L_STRENLK3") Then GetUrbanProfilbezeichnung = "76270GWCD"
If InStr(ref, "P76270---2L_STRENLK4") Then GetUrbanProfilbezeichnung = "76270GWCD"
If InStr(ref, "P76270---2L_STRENLK5") Then GetUrbanProfilbezeichnung = "76270GWCD"
If InStr(ref, "P76270---2L_STRENLK6") Then GetUrbanProfilbezeichnung = "76270GWCD"
If InStr(ref, "P76270---2L_STWS") Then GetUrbanProfilbezeichnung = "76270GWWD"
If InStr(ref, "P76270---2L_STWSRAL") Then GetUrbanProfilbezeichnung = "76270GWW"
If InStr(ref, "P76270---2L_STWSREN") Then GetUrbanProfilbezeichnung = "76270GCCD"
If InStr(ref, "P76270---2L24") Then GetUrbanProfilbezeichnung = "76270WCD"
If InStr(ref, "P76270---2L2x32") Then GetUrbanProfilbezeichnung = "76270CCD"
If InStr(ref, "P76270---2L2x39") Then GetUrbanProfilbezeichnung = "76270CCD"
If InStr(ref, "P76270---2L2xRAL") Then GetUrbanProfilbezeichnung = "76270WW"
If InStr(ref, "P76270---2L2xREN") Then GetUrbanProfilbezeichnung = "76270CCD"
If InStr(ref, "P76270---2L31") Then GetUrbanProfilbezeichnung = "76270WCD"
If InStr(ref, "P76270---2L32") Then GetUrbanProfilbezeichnung = "76270WCD"
If InStr(ref, "P76270---2L39") Then GetUrbanProfilbezeichnung = "76270WCD"
If InStr(ref, "P76270---2L53") Then GetUrbanProfilbezeichnung = "76270WCD"
If InStr(ref, "P76270---2LRALWS") Then GetUrbanProfilbezeichnung = "76270WW"
If InStr(ref, "P76270---2LRENLK1") Then GetUrbanProfilbezeichnung = "76270WCD"
If InStr(ref, "P76270---2LRENLK3") Then GetUrbanProfilbezeichnung = "76270WCD"
If InStr(ref, "P76270---2LRENLK4") Then GetUrbanProfilbezeichnung = "76270WCD"
If InStr(ref, "P76270---2LRENLK5") Then GetUrbanProfilbezeichnung = "76270WCD"
If InStr(ref, "P76270---2LRENLK6") Then GetUrbanProfilbezeichnung = "76270WCD"
If InStr(ref, "P76270---2LWS") Then GetUrbanProfilbezeichnung = "76270WWD"
If InStr(ref, "P76270---2LWSRAL") Then GetUrbanProfilbezeichnung = "76270WW"
If InStr(ref, "P76270---2LWSREN") Then GetUrbanProfilbezeichnung = "76270CCD"
If InStr(ref, "P76270---2S_ST24") Then GetUrbanProfilbezeichnung = "76270GWCD"
If InStr(ref, "P76270---2S_ST2x32") Then GetUrbanProfilbezeichnung = "76270GCCD"
If InStr(ref, "P76270---2S_ST2x39") Then GetUrbanProfilbezeichnung = "76270GCCD"
If InStr(ref, "P76270---2S_ST2xRAL") Then GetUrbanProfilbezeichnung = "76270GWW"
If InStr(ref, "P76270---2S_ST2xREN") Then GetUrbanProfilbezeichnung = "76270GCCD"
If InStr(ref, "P76270---2S_ST31") Then GetUrbanProfilbezeichnung = "76270GWCD"
If InStr(ref, "P76270---2S_ST32") Then GetUrbanProfilbezeichnung = "76270GWCD"
If InStr(ref, "P76270---2S_ST39") Then GetUrbanProfilbezeichnung = "76270GWCD"
If InStr(ref, "P76270---2S_ST53") Then GetUrbanProfilbezeichnung = "76270GWCD"
If InStr(ref, "P76270---2S_STRALWS") Then GetUrbanProfilbezeichnung = "76270GWW"
If InStr(ref, "P76270---2S_STRENLK1") Then GetUrbanProfilbezeichnung = "76270GWCD"
If InStr(ref, "P76270---2S_STRENLK3") Then GetUrbanProfilbezeichnung = "76270GWCD"
If InStr(ref, "P76270---2S_STRENLK4") Then GetUrbanProfilbezeichnung = "76270GWCD"
If InStr(ref, "P76270---2S_STRENLK5") Then GetUrbanProfilbezeichnung = "76270GWCD"
If InStr(ref, "P76270---2S_STRENLK6") Then GetUrbanProfilbezeichnung = "76270GWCD"
If InStr(ref, "P76270---2S_STWS") Then GetUrbanProfilbezeichnung = "76270GWWD"
If InStr(ref, "P76270---2S_STWSRAL") Then GetUrbanProfilbezeichnung = "76270GWW"
If InStr(ref, "P76270---2S_STWSREN") Then GetUrbanProfilbezeichnung = "76270GCCD"
If InStr(ref, "P76270---2S24") Then GetUrbanProfilbezeichnung = "76270WCD"
If InStr(ref, "P76270---2S2x32") Then GetUrbanProfilbezeichnung = "76270CCD"
If InStr(ref, "P76270---2S2x39") Then GetUrbanProfilbezeichnung = "76270CCD"
If InStr(ref, "P76270---2S2xRAL") Then GetUrbanProfilbezeichnung = "76270WW"
If InStr(ref, "P76270---2S2xREN") Then GetUrbanProfilbezeichnung = "76270CCD"
If InStr(ref, "P76270---2S31") Then GetUrbanProfilbezeichnung = "76270WCD"
If InStr(ref, "P76270---2S32") Then GetUrbanProfilbezeichnung = "76270WCD"
If InStr(ref, "P76270---2S39") Then GetUrbanProfilbezeichnung = "76270WCD"
If InStr(ref, "P76270---2S53") Then GetUrbanProfilbezeichnung = "76270WCD"
If InStr(ref, "P76270---2SRALWS") Then GetUrbanProfilbezeichnung = "76270WW"
If InStr(ref, "P76270---2SRENLK1") Then GetUrbanProfilbezeichnung = "76270WCD"
If InStr(ref, "P76270---2SRENLK3") Then GetUrbanProfilbezeichnung = "76270WCD"
If InStr(ref, "P76270---2SRENLK4") Then GetUrbanProfilbezeichnung = "76270WCD"
If InStr(ref, "P76270---2SRENLK5") Then GetUrbanProfilbezeichnung = "76270WCD"
If InStr(ref, "P76270---2SRENLK6") Then GetUrbanProfilbezeichnung = "76270WCD"
If InStr(ref, "P76270---2SWS") Then GetUrbanProfilbezeichnung = "76270WWD"
If InStr(ref, "P76270---2SWSRAL") Then GetUrbanProfilbezeichnung = "76270WW"
If InStr(ref, "P76270---2SWSREN") Then GetUrbanProfilbezeichnung = "76270CCD"
If InStr(ref, "P76271_ST2x32") Then GetUrbanProfilbezeichnung = "76271GCC"
If InStr(ref, "P76271_ST2x39") Then GetUrbanProfilbezeichnung = "76271GCC"
If InStr(ref, "P76271_STWS") Then GetUrbanProfilbezeichnung = "76271GA"
If InStr(ref, "P76271_STWSRAL") Then GetUrbanProfilbezeichnung = "76271GA"
If InStr(ref, "P76271_STWSREN") Then GetUrbanProfilbezeichnung = "76271GCC"
If InStr(ref, "P76271---1L2x32") Then GetUrbanProfilbezeichnung = "76271CC"
If InStr(ref, "P76271---1L2x39") Then GetUrbanProfilbezeichnung = "76271CC"
If InStr(ref, "P76271---1LWS") Then GetUrbanProfilbezeichnung = "76271A"
If InStr(ref, "P76271---1LWSRAL") Then GetUrbanProfilbezeichnung = "76271A"
If InStr(ref, "P76271---1LWSREN") Then GetUrbanProfilbezeichnung = "76271CC"
If InStr(ref, "P76271---2L_ST24") Then GetUrbanProfilbezeichnung = "76271GWCD"
If InStr(ref, "P76271---2L_ST2x32") Then GetUrbanProfilbezeichnung = "76271GCCD"
If InStr(ref, "P76271---2L_ST2x39") Then GetUrbanProfilbezeichnung = "76271GCCD"
If InStr(ref, "P76271---2L_ST2xRAL") Then GetUrbanProfilbezeichnung = "76271GWW"
If InStr(ref, "P76271---2L_ST2xREN") Then GetUrbanProfilbezeichnung = "76271GCCD"
If InStr(ref, "P76271---2L_ST31") Then GetUrbanProfilbezeichnung = "76271GWCD"
If InStr(ref, "P76271---2L_ST32") Then GetUrbanProfilbezeichnung = "76271GWCD"
If InStr(ref, "P76271---2L_ST39") Then GetUrbanProfilbezeichnung = "76271GWCD"
If InStr(ref, "P76271---2L_ST53") Then GetUrbanProfilbezeichnung = "76271GWCD"
If InStr(ref, "P76271---2L_STRALWS") Then GetUrbanProfilbezeichnung = "76271GWW"
If InStr(ref, "P76271---2L_STRENLK1") Then GetUrbanProfilbezeichnung = "76271GWCD"
If InStr(ref, "P76271---2L_STRENLK3") Then GetUrbanProfilbezeichnung = "76271GWCD"
If InStr(ref, "P76271---2L_STRENLK4") Then GetUrbanProfilbezeichnung = "76271GWCD"
If InStr(ref, "P76271---2L_STRENLK5") Then GetUrbanProfilbezeichnung = "76271GWCD"
If InStr(ref, "P76271---2L_STRENLK6") Then GetUrbanProfilbezeichnung = "76271GWCD"
If InStr(ref, "P76271---2L_STWS") Then GetUrbanProfilbezeichnung = "76271GWWD"
If InStr(ref, "P76271---2L_STWSRAL") Then GetUrbanProfilbezeichnung = "76271GWW"
If InStr(ref, "P76271---2L_STWSREN") Then GetUrbanProfilbezeichnung = "76271GCCD"
If InStr(ref, "P76271---2L24") Then GetUrbanProfilbezeichnung = "76271WCD"
If InStr(ref, "P76271---2L2x32") Then GetUrbanProfilbezeichnung = "76271CCD"
If InStr(ref, "P76271---2L2x39") Then GetUrbanProfilbezeichnung = "76271CCD"
If InStr(ref, "P76271---2L2xRAL") Then GetUrbanProfilbezeichnung = "76271WW"
If InStr(ref, "P76271---2L2xREN") Then GetUrbanProfilbezeichnung = "76271CCD"
If InStr(ref, "P76271---2L31") Then GetUrbanProfilbezeichnung = "76271WCD"
If InStr(ref, "P76271---2L32") Then GetUrbanProfilbezeichnung = "76271WCD"
If InStr(ref, "P76271---2L39") Then GetUrbanProfilbezeichnung = "76271WCD"
If InStr(ref, "P76271---2L53") Then GetUrbanProfilbezeichnung = "76271WCD"
If InStr(ref, "P76271---2LRALWS") Then GetUrbanProfilbezeichnung = "76271WW"
If InStr(ref, "P76271---2LRENLK1") Then GetUrbanProfilbezeichnung = "76271WCD"
If InStr(ref, "P76271---2LRENLK3") Then GetUrbanProfilbezeichnung = "76271WCD"
If InStr(ref, "P76271---2LRENLK4") Then GetUrbanProfilbezeichnung = "76271WCD"
If InStr(ref, "P76271---2LRENLK5") Then GetUrbanProfilbezeichnung = "76271WCD"
If InStr(ref, "P76271---2LRENLK6") Then GetUrbanProfilbezeichnung = "76271WCD"
If InStr(ref, "P76271---2LWS") Then GetUrbanProfilbezeichnung = "76271WWD"
If InStr(ref, "P76271---2LWSRAL") Then GetUrbanProfilbezeichnung = "76271WW"
If InStr(ref, "P76271---2LWSREN") Then GetUrbanProfilbezeichnung = "76271CCD"
If InStr(ref, "P76271---2S_ST24") Then GetUrbanProfilbezeichnung = "76271GWCD"
If InStr(ref, "P76271---2S_ST2x32") Then GetUrbanProfilbezeichnung = "76271GCCD"
If InStr(ref, "P76271---2S_ST2x39") Then GetUrbanProfilbezeichnung = "76271GCCD"
If InStr(ref, "P76271---2S_ST2xRAL") Then GetUrbanProfilbezeichnung = "76271GWW"
If InStr(ref, "P76271---2S_ST2xREN") Then GetUrbanProfilbezeichnung = "76271GCCD"
If InStr(ref, "P76271---2S_ST31") Then GetUrbanProfilbezeichnung = "76271GWCD"
If InStr(ref, "P76271---2S_ST32") Then GetUrbanProfilbezeichnung = "76271GWCD"
If InStr(ref, "P76271---2S_ST39") Then GetUrbanProfilbezeichnung = "76271GWCD"
If InStr(ref, "P76271---2S_ST53") Then GetUrbanProfilbezeichnung = "76271GWCD"
If InStr(ref, "P76271---2S_STRALWS") Then GetUrbanProfilbezeichnung = "76271GWW"
If InStr(ref, "P76271---2S_STRENLK1") Then GetUrbanProfilbezeichnung = "76271GWCD"
If InStr(ref, "P76271---2S_STRENLK3") Then GetUrbanProfilbezeichnung = "76271GWCD"
If InStr(ref, "P76271---2S_STRENLK4") Then GetUrbanProfilbezeichnung = "76271GWCD"
If InStr(ref, "P76271---2S_STRENLK5") Then GetUrbanProfilbezeichnung = "76271GWCD"
If InStr(ref, "P76271---2S_STRENLK6") Then GetUrbanProfilbezeichnung = "76271GWCD"
If InStr(ref, "P76271---2S_STWS") Then GetUrbanProfilbezeichnung = "76271GWWS"
If InStr(ref, "P76271---2S_STWSRAL") Then GetUrbanProfilbezeichnung = "76271GWW"
If InStr(ref, "P76271---2S_STWSREN") Then GetUrbanProfilbezeichnung = "76271GCCD"
If InStr(ref, "P76271---2S24") Then GetUrbanProfilbezeichnung = "76271WCD"
If InStr(ref, "P76271---2S2x32") Then GetUrbanProfilbezeichnung = "76271CCD"
If InStr(ref, "P76271---2S2x39") Then GetUrbanProfilbezeichnung = "76271CCD"
If InStr(ref, "P76271---2S2xRAL") Then GetUrbanProfilbezeichnung = "76271WW"
If InStr(ref, "P76271---2S2xREN") Then GetUrbanProfilbezeichnung = "76271CCD"
If InStr(ref, "P76271---2S31") Then GetUrbanProfilbezeichnung = "76271WCD"
If InStr(ref, "P76271---2S32") Then GetUrbanProfilbezeichnung = "76271WCD"
If InStr(ref, "P76271---2S39") Then GetUrbanProfilbezeichnung = "76271WCD"
If InStr(ref, "P76271---2S53") Then GetUrbanProfilbezeichnung = "76271WCD"
If InStr(ref, "P76271---2SRALWS") Then GetUrbanProfilbezeichnung = "76271WW"
If InStr(ref, "P76271---2SRENLK1") Then GetUrbanProfilbezeichnung = "76271WCD"
If InStr(ref, "P76271---2SRENLK3") Then GetUrbanProfilbezeichnung = "76271WCD"
If InStr(ref, "P76271---2SRENLK4") Then GetUrbanProfilbezeichnung = "76271WCD"
If InStr(ref, "P76271---2SRENLK5") Then GetUrbanProfilbezeichnung = "76271WCD"
If InStr(ref, "P76271---2SRENLK6") Then GetUrbanProfilbezeichnung = "76271WCD"
If InStr(ref, "P76271---2SWS") Then GetUrbanProfilbezeichnung = "76271WWD"
If InStr(ref, "P76271---2SWSRAL") Then GetUrbanProfilbezeichnung = "76271WW"
If InStr(ref, "P76271---2SWSREN") Then GetUrbanProfilbezeichnung = "76271CCD"
If InStr(ref, "P76272_ST2x32") Then GetUrbanProfilbezeichnung = "76272GCC"
If InStr(ref, "P76272_ST2x39") Then GetUrbanProfilbezeichnung = "76272GCC"
If InStr(ref, "P76272_STWS") Then GetUrbanProfilbezeichnung = "76272GA"
If InStr(ref, "P76272_STWSRAL") Then GetUrbanProfilbezeichnung = "76272GA"
If InStr(ref, "P76272_STWSREN") Then GetUrbanProfilbezeichnung = "76272GCC"
If InStr(ref, "P76272---1L2x32") Then GetUrbanProfilbezeichnung = "76272CC"
If InStr(ref, "P76272---1L2x39") Then GetUrbanProfilbezeichnung = "76272CC"
If InStr(ref, "P76272---1LWS") Then GetUrbanProfilbezeichnung = "76272A"
If InStr(ref, "P76272---1LWSRAL") Then GetUrbanProfilbezeichnung = "76272A"
If InStr(ref, "P76272---1LWSREN") Then GetUrbanProfilbezeichnung = "76272CC"
If InStr(ref, "P76272---2L_ST24") Then GetUrbanProfilbezeichnung = "76272GWCD"
If InStr(ref, "P76272---2L_ST2x32") Then GetUrbanProfilbezeichnung = "76272GCCD"
If InStr(ref, "P76272---2L_ST2x39") Then GetUrbanProfilbezeichnung = "76272GCCD"
If InStr(ref, "P76272---2L_ST2xRAL") Then GetUrbanProfilbezeichnung = "76272GWW"
If InStr(ref, "P76272---2L_ST2xREN") Then GetUrbanProfilbezeichnung = "76272GCCD"
If InStr(ref, "P76272---2L_ST31") Then GetUrbanProfilbezeichnung = "76272GWCD"
If InStr(ref, "P76272---2L_ST32") Then GetUrbanProfilbezeichnung = "76272GWCD"
If InStr(ref, "P76272---2L_ST39") Then GetUrbanProfilbezeichnung = "76272GWCD"
If InStr(ref, "P76272---2L_ST53") Then GetUrbanProfilbezeichnung = "76272GWCD"
If InStr(ref, "P76272---2L_STRALWS") Then GetUrbanProfilbezeichnung = "76272GWW"
If InStr(ref, "P76272---2L_STRENLK1") Then GetUrbanProfilbezeichnung = "76272GWCD"
If InStr(ref, "P76272---2L_STRENLK3") Then GetUrbanProfilbezeichnung = "76272GWCD"
If InStr(ref, "P76272---2L_STRENLK4") Then GetUrbanProfilbezeichnung = "76272GWCD"
If InStr(ref, "P76272---2L_STRENLK5") Then GetUrbanProfilbezeichnung = "76272GWCD"
If InStr(ref, "P76272---2L_STRENLK6") Then GetUrbanProfilbezeichnung = "76272GWCD"
If InStr(ref, "P76272---2L_STWS") Then GetUrbanProfilbezeichnung = "76272GWWD"
If InStr(ref, "P76272---2L_STWSRAL") Then GetUrbanProfilbezeichnung = "76272GWW"
If InStr(ref, "P76272---2L_STWSREN") Then GetUrbanProfilbezeichnung = "76272GCCD"
If InStr(ref, "P76272---2L24") Then GetUrbanProfilbezeichnung = "76272WCD"
If InStr(ref, "P76272---2L2x32") Then GetUrbanProfilbezeichnung = "76272CCD"
If InStr(ref, "P76272---2L2x39") Then GetUrbanProfilbezeichnung = "76272CCD"
If InStr(ref, "P76272---2L2xRAL") Then GetUrbanProfilbezeichnung = "76272WW"
If InStr(ref, "P76272---2L2xREN") Then GetUrbanProfilbezeichnung = "76272CCD"
If InStr(ref, "P76272---2L31") Then GetUrbanProfilbezeichnung = "76272WCD"
If InStr(ref, "P76272---2L32") Then GetUrbanProfilbezeichnung = "76272WCD"
If InStr(ref, "P76272---2L39") Then GetUrbanProfilbezeichnung = "76272WCD"
If InStr(ref, "P76272---2L53") Then GetUrbanProfilbezeichnung = "76272WCD"
If InStr(ref, "P76272---2LRALWS") Then GetUrbanProfilbezeichnung = "76272WW"
If InStr(ref, "P76272---2LRENLK1") Then GetUrbanProfilbezeichnung = "76272WCD"
If InStr(ref, "P76272---2LRENLK3") Then GetUrbanProfilbezeichnung = "76272WCD"
If InStr(ref, "P76272---2LRENLK4") Then GetUrbanProfilbezeichnung = "76272WCD"
If InStr(ref, "P76272---2LRENLK5") Then GetUrbanProfilbezeichnung = "76272WCD"
If InStr(ref, "P76272---2LRENLK6") Then GetUrbanProfilbezeichnung = "76272WCd"
If InStr(ref, "P76272---2LWS") Then GetUrbanProfilbezeichnung = "76272WWD"
If InStr(ref, "P76272---2LWSRAL") Then GetUrbanProfilbezeichnung = "76272WW"
If InStr(ref, "P76272---2LWSREN") Then GetUrbanProfilbezeichnung = "76272CCD"
If InStr(ref, "P76272---2S_ST24") Then GetUrbanProfilbezeichnung = "76272GWCD"
If InStr(ref, "P76272---2S_ST2x32") Then GetUrbanProfilbezeichnung = "76272GCCD"
If InStr(ref, "P76272---2S_ST2x39") Then GetUrbanProfilbezeichnung = "76272GCCD"
If InStr(ref, "P76272---2S_ST2xRAL") Then GetUrbanProfilbezeichnung = "76272GWW"
If InStr(ref, "P76272---2S_ST2xREN") Then GetUrbanProfilbezeichnung = "76272GCCD"
If InStr(ref, "P76272---2S_ST31") Then GetUrbanProfilbezeichnung = "76272GWCD"
If InStr(ref, "P76272---2S_ST32") Then GetUrbanProfilbezeichnung = "76272GWCD"
If InStr(ref, "P76272---2S_ST39") Then GetUrbanProfilbezeichnung = "76272GWCD"
If InStr(ref, "P76272---2S_ST53") Then GetUrbanProfilbezeichnung = "76272GWCD"
If InStr(ref, "P76272---2S_STRALWS") Then GetUrbanProfilbezeichnung = "76272GWW"
If InStr(ref, "P76272---2S_STRENLK1") Then GetUrbanProfilbezeichnung = "76272GWCD"
If InStr(ref, "P76272---2S_STRENLK3") Then GetUrbanProfilbezeichnung = "76272GWCD"
If InStr(ref, "P76272---2S_STRENLK4") Then GetUrbanProfilbezeichnung = "76272GWCD"
If InStr(ref, "P76272---2S_STRENLK5") Then GetUrbanProfilbezeichnung = "76272GWCD"
If InStr(ref, "P76272---2S_STRENLK6") Then GetUrbanProfilbezeichnung = "76272GWCD"
If InStr(ref, "P76272---2S_STWS") Then GetUrbanProfilbezeichnung = "76272GWWD"
If InStr(ref, "P76272---2S_STWSRAL") Then GetUrbanProfilbezeichnung = "76272GWW"
If InStr(ref, "P76272---2S_STWSREN") Then GetUrbanProfilbezeichnung = "76272GCCD"
If InStr(ref, "P76272---2S24") Then GetUrbanProfilbezeichnung = "76272WCD"
If InStr(ref, "P76272---2S2x32") Then GetUrbanProfilbezeichnung = "76272CCD"
If InStr(ref, "P76272---2S2x39") Then GetUrbanProfilbezeichnung = "76272CCD"
If InStr(ref, "P76272---2S2xRAL") Then GetUrbanProfilbezeichnung = "76272WW"
If InStr(ref, "P76272---2S2xREN") Then GetUrbanProfilbezeichnung = "76272CCD"
If InStr(ref, "P76272---2S31") Then GetUrbanProfilbezeichnung = "76272WCD"
If InStr(ref, "P76272---2S32") Then GetUrbanProfilbezeichnung = "76272WCD"
If InStr(ref, "P76272---2S39") Then GetUrbanProfilbezeichnung = "76272WCD"
If InStr(ref, "P76272---2S53") Then GetUrbanProfilbezeichnung = "76272WCD"
If InStr(ref, "P76272---2SRALWS") Then GetUrbanProfilbezeichnung = "76272WW"
If InStr(ref, "P76272---2SRENLK1") Then GetUrbanProfilbezeichnung = "76272WCD"
If InStr(ref, "P76272---2SRENLK3") Then GetUrbanProfilbezeichnung = "76272WCD"
If InStr(ref, "P76272---2SRENLK4") Then GetUrbanProfilbezeichnung = "76272WCD"
If InStr(ref, "P76272---2SRENLK5") Then GetUrbanProfilbezeichnung = "76272WCD"
If InStr(ref, "P76272---2SRENLK6") Then GetUrbanProfilbezeichnung = "76272WCD"
If InStr(ref, "P76272---2SWS") Then GetUrbanProfilbezeichnung = "76272WWD"
If InStr(ref, "P76272---2SWSRAL") Then GetUrbanProfilbezeichnung = "76272WW"
If InStr(ref, "P76272---2SWSREN") Then GetUrbanProfilbezeichnung = "76272CCD"

If InStr(ref, "P76102_E24") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102_E2x32") Then GetUrbanProfilbezeichnung = "76102CC"
If InStr(ref, "P76102_E2x39") Then GetUrbanProfilbezeichnung = "76102CC"
If InStr(ref, "P76102_E2xRAL") Then GetUrbanProfilbezeichnung = "76102WW"
If InStr(ref, "P76102_E2xREN") Then GetUrbanProfilbezeichnung = "76102CC"
If InStr(ref, "P76102_E31") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102_E32") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102_E39") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102_E53") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102_ERALWS") Then GetUrbanProfilbezeichnung = "76102WW"
If InStr(ref, "P76102_ERENLK1") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102_ERENLK3") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102_ERENLK4") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102_ERENLK5") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102_ERENLK6") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102_EWS") Then GetUrbanProfilbezeichnung = "76102WW"
If InStr(ref, "P76102_EWSRAL") Then GetUrbanProfilbezeichnung = "76102WW"
If InStr(ref, "P76102_EWSREN") Then GetUrbanProfilbezeichnung = "76102CC"
If InStr(ref, "P76102_EA24") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102_EA2x32") Then GetUrbanProfilbezeichnung = "76102CC"
If InStr(ref, "P76102_EA2x39") Then GetUrbanProfilbezeichnung = "76102CC"
If InStr(ref, "P76102_EA2xRAL") Then GetUrbanProfilbezeichnung = "76102WW"
If InStr(ref, "P76102_EA2xREN") Then GetUrbanProfilbezeichnung = "76102CC"
If InStr(ref, "P76102_EA31") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102_EA32") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102_EA39") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102_EA53") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102_EARALWS") Then GetUrbanProfilbezeichnung = "76102WW"
If InStr(ref, "P76102_EARENLK1") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102_EARENLK3") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102_EARENLK4") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102_EARENLK5") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102_EARENLK6") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102_EAWS") Then GetUrbanProfilbezeichnung = "76102WW"
If InStr(ref, "P76102_EAWSRAL") Then GetUrbanProfilbezeichnung = "76102WW"
If InStr(ref, "P76102_EAWSREN") Then GetUrbanProfilbezeichnung = "76102CC"
If InStr(ref, "P76102---1L_E24") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102---1L_E2x32") Then GetUrbanProfilbezeichnung = "76102CC"
If InStr(ref, "P76102---1L_E2x39") Then GetUrbanProfilbezeichnung = "76102CC"
If InStr(ref, "P76102---1L_E2xRAL") Then GetUrbanProfilbezeichnung = "76102WW"
If InStr(ref, "P76102---1L_E2xREN") Then GetUrbanProfilbezeichnung = "76102CC"
If InStr(ref, "P76102---1L_E31") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102---1L_E32") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102---1L_E39") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102---1L_E53") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102---1L_ERALWS") Then GetUrbanProfilbezeichnung = "76102CC"
If InStr(ref, "P76102---1L_ERENLK1") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102---1L_ERENLK3") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102---1L_ERENLK4") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102---1L_ERENLK5") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102---1L_ERENLK6") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102---1L_EWS") Then GetUrbanProfilbezeichnung = "76102WW"
If InStr(ref, "P76102---1L_EWSRAL") Then GetUrbanProfilbezeichnung = "76102WW"
If InStr(ref, "P76102---1L_EWSREN") Then GetUrbanProfilbezeichnung = "76102CC"
If InStr(ref, "P76102---1L_EA24") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102---1L_EA2x32") Then GetUrbanProfilbezeichnung = "76102CC"
If InStr(ref, "P76102---1L_EA2x39") Then GetUrbanProfilbezeichnung = "76102CC"
If InStr(ref, "P76102---1L_EA2xRAL") Then GetUrbanProfilbezeichnung = "76102WW"
If InStr(ref, "P76102---1L_EA2xREN") Then GetUrbanProfilbezeichnung = "76102CC"
If InStr(ref, "P76102---1L_EA31") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102---1L_EA32") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102---1L_EA39") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102---1L_EA53") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102---1L_EARALWS") Then GetUrbanProfilbezeichnung = "76102WW"
If InStr(ref, "P76102---1L_EARENLK1") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102---1L_EARENLK3") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102---1L_EARENLK4") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102---1L_EARENLK5") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102---1L_EARENLK6") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102---1L_EAWS") Then GetUrbanProfilbezeichnung = "76102WW"
If InStr(ref, "P76102---1L_EAWSRAL") Then GetUrbanProfilbezeichnung = "76102WW"
If InStr(ref, "P76102---1L_EAWSREN") Then GetUrbanProfilbezeichnung = "76102CC"
If InStr(ref, "P76102---1S_E24") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102---1S_E2x32") Then GetUrbanProfilbezeichnung = "76102CC"
If InStr(ref, "P76102---1S_E2x39") Then GetUrbanProfilbezeichnung = "76102CC"
If InStr(ref, "P76102---1S_E2xRAL") Then GetUrbanProfilbezeichnung = "76102WW"
If InStr(ref, "P76102---1S_E2xREN") Then GetUrbanProfilbezeichnung = "76102CC"
If InStr(ref, "P76102---1S_E31") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102---1S_E32") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102---1S_E39") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102---1S_E53") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102---1S_ERALWS") Then GetUrbanProfilbezeichnung = "76102WW"
If InStr(ref, "P76102---1S_ERENLK1") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102---1S_ERENLK3") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102---1S_ERENLK4") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102---1S_ERENLK5") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102---1S_ERENLK6") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102---1S_EWS") Then GetUrbanProfilbezeichnung = "76102WW"
If InStr(ref, "P76102---1S_EWSRAL") Then GetUrbanProfilbezeichnung = "76102WW"
If InStr(ref, "P76102---1S_EWSREN") Then GetUrbanProfilbezeichnung = "76102CC"
If InStr(ref, "P76102---1S_EA24") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102---1S_EA2x32") Then GetUrbanProfilbezeichnung = "76102CC"
If InStr(ref, "P76102---1S_EA2x39") Then GetUrbanProfilbezeichnung = "76102CC"
If InStr(ref, "P76102---1S_EA2xRAL") Then GetUrbanProfilbezeichnung = "76102WW"
If InStr(ref, "P76102---1S_EA2xREN") Then GetUrbanProfilbezeichnung = "76102CC"
If InStr(ref, "P76102---1S_EA31") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102---1S_EA32") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102---1S_EA39") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102---1S_EA53") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102---1S_EARALWS") Then GetUrbanProfilbezeichnung = "76102WW"
If InStr(ref, "P76102---1S_EARENLK1") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102---1S_EARENLK3") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102---1S_EARENLK4") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102---1S_EARENLK5") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102---1S_EARENLK6") Then GetUrbanProfilbezeichnung = "76102WC"
If InStr(ref, "P76102---1S_EAWS") Then GetUrbanProfilbezeichnung = "76102WW"
If InStr(ref, "P76102---1S_EAWSRAL") Then GetUrbanProfilbezeichnung = "76102WW"
If InStr(ref, "P76102---1S_EAWSREN") Then GetUrbanProfilbezeichnung = "76102CC"
If InStr(ref, "P76103_E24") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103_E2x32") Then GetUrbanProfilbezeichnung = "76103CC"
If InStr(ref, "P76103_E2x39") Then GetUrbanProfilbezeichnung = "76103CC"
If InStr(ref, "P76103_E2xRAL") Then GetUrbanProfilbezeichnung = "76103WW"
If InStr(ref, "P76103_E2xREN") Then GetUrbanProfilbezeichnung = "76103CC"
If InStr(ref, "P76103_E31") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103_E32") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103_E39") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103_E53") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103_ERALWS") Then GetUrbanProfilbezeichnung = "76103WW"
If InStr(ref, "P76103_ERENLK1") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103_ERENLK3") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103_ERENLK4") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103_ERENLK5") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103_ERENLK6") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103_EWS") Then GetUrbanProfilbezeichnung = "76103WW"
If InStr(ref, "P76103_EWSRAL") Then GetUrbanProfilbezeichnung = "76103WW"
If InStr(ref, "P76103_EWSREN") Then GetUrbanProfilbezeichnung = "76103CC"
If InStr(ref, "P76103_EA24") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103_EA2x32") Then GetUrbanProfilbezeichnung = "76103CC"
If InStr(ref, "P76103_EA2x39") Then GetUrbanProfilbezeichnung = "76103CC"
If InStr(ref, "P76103_EA2xRAL") Then GetUrbanProfilbezeichnung = "76103WW"
If InStr(ref, "P76103_EA2xREN") Then GetUrbanProfilbezeichnung = "76103CC"
If InStr(ref, "P76103_EA31") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103_EA32") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103_EA39") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103_EA53") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103_EARALWS") Then GetUrbanProfilbezeichnung = "76103WW"
If InStr(ref, "P76103_EARENLK1") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103_EARENLK3") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103_EARENLK4") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103_EARENLK5") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103_EARENLK6") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103_EAWS") Then GetUrbanProfilbezeichnung = "76103WW"
If InStr(ref, "P76103_EAWSRAL") Then GetUrbanProfilbezeichnung = "76103WW"
If InStr(ref, "P76103_EAWSREN") Then GetUrbanProfilbezeichnung = "76103CC"
If InStr(ref, "P76103---1L_E24") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103---1L_E2x32") Then GetUrbanProfilbezeichnung = "76103CC"
If InStr(ref, "P76103---1L_E2x39") Then GetUrbanProfilbezeichnung = "76103CC"
If InStr(ref, "P76103---1L_E2xRAL") Then GetUrbanProfilbezeichnung = "76103WW"
If InStr(ref, "P76103---1L_E2xREN") Then GetUrbanProfilbezeichnung = "76103CC"
If InStr(ref, "P76103---1L_E31") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103---1L_E32") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103---1L_E39") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103---1L_E53") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103---1L_ERALWS") Then GetUrbanProfilbezeichnung = "76103WW"
If InStr(ref, "P76103---1L_ERENLK1") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103---1L_ERENLK3") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103---1L_ERENLK4") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103---1L_ERENLK5") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103---1L_ERENLK6") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103---1L_EWS") Then GetUrbanProfilbezeichnung = "76103WW"
If InStr(ref, "P76103---1L_EWSRAL") Then GetUrbanProfilbezeichnung = "76103WW"
If InStr(ref, "P76103---1L_EWSREN") Then GetUrbanProfilbezeichnung = "76103CC"
If InStr(ref, "P76103---1L_EA24") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103---1L_EA2x32") Then GetUrbanProfilbezeichnung = "76103CC"
If InStr(ref, "P76103---1L_EA2x39") Then GetUrbanProfilbezeichnung = "76103CC"
If InStr(ref, "P76103---1L_EA2xRAL") Then GetUrbanProfilbezeichnung = "76103WW"
If InStr(ref, "P76103---1L_EA2xREN") Then GetUrbanProfilbezeichnung = "76103CC"
If InStr(ref, "P76103---1L_EA31") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103---1L_EA32") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103---1L_EA39") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103---1L_EA53") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103---1L_EARALWS") Then GetUrbanProfilbezeichnung = "76103WW"
If InStr(ref, "P76103---1L_EARENLK1") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103---1L_EARENLK3") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103---1L_EARENLK4") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103---1L_EARENLK5") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103---1L_EARENLK6") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103---1L_EAWS") Then GetUrbanProfilbezeichnung = "76103WW"
If InStr(ref, "P76103---1L_EAWSRAL") Then GetUrbanProfilbezeichnung = "76103WW"
If InStr(ref, "P76103---1L_EAWSREN") Then GetUrbanProfilbezeichnung = "76103CC"
If InStr(ref, "P76103---1S_E24") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103---1S_E2x32") Then GetUrbanProfilbezeichnung = "76103CC"
If InStr(ref, "P76103---1S_E2x39") Then GetUrbanProfilbezeichnung = "76103CC"
If InStr(ref, "P76103---1S_E2xRAL") Then GetUrbanProfilbezeichnung = "76103WW"
If InStr(ref, "P76103---1S_E2xREN") Then GetUrbanProfilbezeichnung = "76103CC"
If InStr(ref, "P76103---1S_E31") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103---1S_E32") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103---1S_E39") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103---1S_E53") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103---1S_ERALWS") Then GetUrbanProfilbezeichnung = "76103WW"
If InStr(ref, "P76103---1S_ERENLK1") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103---1S_ERENLK3") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103---1S_ERENLK4") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103---1S_ERENLK5") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103---1S_ERENLK6") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103---1S_EWS") Then GetUrbanProfilbezeichnung = "76103WW"
If InStr(ref, "P76103---1S_EWSRAL") Then GetUrbanProfilbezeichnung = "76103WW"
If InStr(ref, "P76103---1S_EWSREN") Then GetUrbanProfilbezeichnung = "76103CC"
If InStr(ref, "P76103---1S_EA24") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103---1S_EA2x32") Then GetUrbanProfilbezeichnung = "76103CC"
If InStr(ref, "P76103---1S_EA2x39") Then GetUrbanProfilbezeichnung = "76103CC"
If InStr(ref, "P76103---1S_EA2xRAL") Then GetUrbanProfilbezeichnung = "76103WW"
If InStr(ref, "P76103---1S_EA2xREN") Then GetUrbanProfilbezeichnung = "76103CC"
If InStr(ref, "P76103---1S_EA31") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103---1S_EA32") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103---1S_EA39") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103---1S_EA53") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103---1S_EARALWS") Then GetUrbanProfilbezeichnung = "76103WW"
If InStr(ref, "P76103---1S_EARENLK1") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103---1S_EARENLK3") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103---1S_EARENLK4") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103---1S_EARENLK5") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103---1S_EARENLK6") Then GetUrbanProfilbezeichnung = "76103WC"
If InStr(ref, "P76103---1S_EAWS") Then GetUrbanProfilbezeichnung = "76103WW"
If InStr(ref, "P76103---1S_EAWSRAL") Then GetUrbanProfilbezeichnung = "76103WW"
If InStr(ref, "P76103---1S_EAWSREN") Then GetUrbanProfilbezeichnung = "76103CC"
If InStr(ref, "P76206---1L2x32") Then GetUrbanProfilbezeichnung = "76206CC"
If InStr(ref, "P76206---1L2x39") Then GetUrbanProfilbezeichnung = "76206CC"
If InStr(ref, "P76206---1LWS") Then GetUrbanProfilbezeichnung = "76206WW"
If InStr(ref, "P76206---1LWSRAL") Then GetUrbanProfilbezeichnung = "76206WW"
If InStr(ref, "P76206---1LWSREN") Then GetUrbanProfilbezeichnung = "76206CC"
If InStr(ref, "P76206---1L_ST2x32") Then GetUrbanProfilbezeichnung = "76206CC"
If InStr(ref, "P76206---1L_ST2x39") Then GetUrbanProfilbezeichnung = "76206CC"
If InStr(ref, "P76206---1L_STWS") Then GetUrbanProfilbezeichnung = "76206WW"
If InStr(ref, "P76206---1L_STWSRAL") Then GetUrbanProfilbezeichnung = "76206WW"
If InStr(ref, "P76206---1L_STWSREN") Then GetUrbanProfilbezeichnung = "76206CC"
If InStr(ref, "P76206---1S24") Then GetUrbanProfilbezeichnung = "76206WC"
If InStr(ref, "P76206---1S2x32") Then GetUrbanProfilbezeichnung = "76206CC"
If InStr(ref, "P76206---1S2x39") Then GetUrbanProfilbezeichnung = "76206CC"
If InStr(ref, "P76206---1S2xRAL") Then GetUrbanProfilbezeichnung = "76206WW"
If InStr(ref, "P76206---1S2xREN") Then GetUrbanProfilbezeichnung = "76206CC"
If InStr(ref, "P76206---1S31") Then GetUrbanProfilbezeichnung = "76206WC"
If InStr(ref, "P76206---1S32") Then GetUrbanProfilbezeichnung = "76206WC"
If InStr(ref, "P76206---1S39") Then GetUrbanProfilbezeichnung = "76206WC"
If InStr(ref, "P76206---1S53") Then GetUrbanProfilbezeichnung = "76206WC"
If InStr(ref, "P76206---1SRALWS") Then GetUrbanProfilbezeichnung = "76206WW"
If InStr(ref, "P76206---1SRENLK1") Then GetUrbanProfilbezeichnung = "76206WC"
If InStr(ref, "P76206---1SRENLK3") Then GetUrbanProfilbezeichnung = "76206WC"
If InStr(ref, "P76206---1SRENLK4") Then GetUrbanProfilbezeichnung = "76206WC"
If InStr(ref, "P76206---1SRENLK5") Then GetUrbanProfilbezeichnung = "76206WC"
If InStr(ref, "P76206---1SRENLK6") Then GetUrbanProfilbezeichnung = "76206WC"
If InStr(ref, "P76206---1SWS") Then GetUrbanProfilbezeichnung = "76206WW"
If InStr(ref, "P76206---1SWSRAL") Then GetUrbanProfilbezeichnung = "76206WW"
If InStr(ref, "P76206---1SWSREN") Then GetUrbanProfilbezeichnung = "76206CC"
If InStr(ref, "P76206---1S_ST24") Then GetUrbanProfilbezeichnung = "76206WC"
If InStr(ref, "P76206---1S_ST2x32") Then GetUrbanProfilbezeichnung = "76206CC"
If InStr(ref, "P76206---1S_ST2x39") Then GetUrbanProfilbezeichnung = "76206CC"
If InStr(ref, "P76206---1S_ST2xRAL") Then GetUrbanProfilbezeichnung = "76206WW"
If InStr(ref, "P76206---1S_ST2xREN") Then GetUrbanProfilbezeichnung = "76206CC"
If InStr(ref, "P76206---1S_ST31") Then GetUrbanProfilbezeichnung = "76206WC"
If InStr(ref, "P76206---1S_ST32") Then GetUrbanProfilbezeichnung = "76206WC"
If InStr(ref, "P76206---1S_ST39") Then GetUrbanProfilbezeichnung = "76206WC"
If InStr(ref, "P76206---1S_ST53") Then GetUrbanProfilbezeichnung = "76206WC"
If InStr(ref, "P76206---1S_STRALWS") Then GetUrbanProfilbezeichnung = "76206WW"
If InStr(ref, "P76206---1S_STRENLK1") Then GetUrbanProfilbezeichnung = "76206WC"
If InStr(ref, "P76206---1S_STRENLK3") Then GetUrbanProfilbezeichnung = "76206WC"
If InStr(ref, "P76206---1S_STRENLK4") Then GetUrbanProfilbezeichnung = "76206WC"
If InStr(ref, "P76206---1S_STRENLK5") Then GetUrbanProfilbezeichnung = "76206WC"
If InStr(ref, "P76206---1S_STRENLK6") Then GetUrbanProfilbezeichnung = "76206WC"
If InStr(ref, "P76206---1S_STWS") Then GetUrbanProfilbezeichnung = "76206WW"
If InStr(ref, "P76206---1S_STWSRAL") Then GetUrbanProfilbezeichnung = "76206WW"
If InStr(ref, "P76206---1S_STWSREN") Then GetUrbanProfilbezeichnung = "76206CC"
If InStr(ref, "P76206---2L24") Then GetUrbanProfilbezeichnung = "76206WCD"
If InStr(ref, "P76206---2L2x32") Then GetUrbanProfilbezeichnung = "76206CCD"
If InStr(ref, "P76206---2L2x39") Then GetUrbanProfilbezeichnung = "76206CCD"
If InStr(ref, "P76206---2L2xRAL") Then GetUrbanProfilbezeichnung = "76206WW"
If InStr(ref, "P76206---2L2xREN") Then GetUrbanProfilbezeichnung = "76206CCD"
If InStr(ref, "P76206---2L31") Then GetUrbanProfilbezeichnung = "76206WCD"
If InStr(ref, "P76206---2L32") Then GetUrbanProfilbezeichnung = "76206WCD"
If InStr(ref, "P76206---2L39") Then GetUrbanProfilbezeichnung = "76206WCD"
If InStr(ref, "P76206---2L53") Then GetUrbanProfilbezeichnung = "76206WCD"
If InStr(ref, "P76206---2LRALWS") Then GetUrbanProfilbezeichnung = "76206WW"
If InStr(ref, "P76206---2LRENLK1") Then GetUrbanProfilbezeichnung = "76206WCD"
If InStr(ref, "P76206---2LRENLK3") Then GetUrbanProfilbezeichnung = "76206WCD"
If InStr(ref, "P76206---2LRENLK4") Then GetUrbanProfilbezeichnung = "76206WCD"
If InStr(ref, "P76206---2LRENLK5") Then GetUrbanProfilbezeichnung = "76206WCD"
If InStr(ref, "P76206---2LRENLK6") Then GetUrbanProfilbezeichnung = "76206WCD"
If InStr(ref, "P76206---2LWS") Then GetUrbanProfilbezeichnung = "76206WWD"
If InStr(ref, "P76206---2LWSRAL") Then GetUrbanProfilbezeichnung = "76206WWD"
If InStr(ref, "P76206---2LWSREN") Then GetUrbanProfilbezeichnung = "76206CCD"
If InStr(ref, "P76206---2L_ST24") Then GetUrbanProfilbezeichnung = "76206WCD"
If InStr(ref, "P76206---2L_ST2x32") Then GetUrbanProfilbezeichnung = "76206CCD"
If InStr(ref, "P76206---2L_ST2x39") Then GetUrbanProfilbezeichnung = "76206CCD"
If InStr(ref, "P76206---2L_ST2xRAL") Then GetUrbanProfilbezeichnung = "76206WW"
If InStr(ref, "P76206---2L_ST2xREN") Then GetUrbanProfilbezeichnung = "76206CCD"
If InStr(ref, "P76206---2L_ST31") Then GetUrbanProfilbezeichnung = "76206WCD"
If InStr(ref, "P76206---2L_ST32") Then GetUrbanProfilbezeichnung = "76206WCD"
If InStr(ref, "P76206---2L_ST39") Then GetUrbanProfilbezeichnung = "76206WCD"
If InStr(ref, "P76206---2L_ST53") Then GetUrbanProfilbezeichnung = "76206WCD"
If InStr(ref, "P76206---2L_STRALWS") Then GetUrbanProfilbezeichnung = "76206WW"
If InStr(ref, "P76206---2L_STRENLK1") Then GetUrbanProfilbezeichnung = "76206WCD"
If InStr(ref, "P76206---2L_STRENLK3") Then GetUrbanProfilbezeichnung = "76206WCD"
If InStr(ref, "P76206---2L_STRENLK4") Then GetUrbanProfilbezeichnung = "76206WCD"
If InStr(ref, "P76206---2L_STRENLK5") Then GetUrbanProfilbezeichnung = "76206WCD"
If InStr(ref, "P76206---2L_STRENLK6") Then GetUrbanProfilbezeichnung = "76206WCD"
If InStr(ref, "P76206---2L_STWS") Then GetUrbanProfilbezeichnung = "76206WWD"
If InStr(ref, "P76206---2L_STWSRAL") Then GetUrbanProfilbezeichnung = "76206WWD"
If InStr(ref, "P76206---2L_STWSREN") Then GetUrbanProfilbezeichnung = "76206CCD"
If InStr(ref, "P76206---2S24") Then GetUrbanProfilbezeichnung = "76206WCD"
If InStr(ref, "P76206---2S2x32") Then GetUrbanProfilbezeichnung = "76206CCD"
If InStr(ref, "P76206---2S2x39") Then GetUrbanProfilbezeichnung = "76206CCD"
If InStr(ref, "P76206---2S2xRAL") Then GetUrbanProfilbezeichnung = "76206WW"
If InStr(ref, "P76206---2S2xREN") Then GetUrbanProfilbezeichnung = "76206CCD"
If InStr(ref, "P76206---2S31") Then GetUrbanProfilbezeichnung = "76206WCD"
If InStr(ref, "P76206---2S32") Then GetUrbanProfilbezeichnung = "76206WCD"
If InStr(ref, "P76206---2S39") Then GetUrbanProfilbezeichnung = "76206WCD"
If InStr(ref, "P76206---2S53") Then GetUrbanProfilbezeichnung = "76206WCD"
If InStr(ref, "P76206---2SRALWS") Then GetUrbanProfilbezeichnung = "76206WW"
If InStr(ref, "P76206---2SRENLK1") Then GetUrbanProfilbezeichnung = "76206WCD"
If InStr(ref, "P76206---2SRENLK3") Then GetUrbanProfilbezeichnung = "76206WCD"
If InStr(ref, "P76206---2SRENLK4") Then GetUrbanProfilbezeichnung = "76206WCD"
If InStr(ref, "P76206---2SRENLK5") Then GetUrbanProfilbezeichnung = "76206WCD"
If InStr(ref, "P76206---2SRENLK6") Then GetUrbanProfilbezeichnung = "76206WCD"
If InStr(ref, "P76206---2SWS") Then GetUrbanProfilbezeichnung = "76206WWD"
If InStr(ref, "P76206---2SWSRAL") Then GetUrbanProfilbezeichnung = "76206WWD"
If InStr(ref, "P76206---2SWSREN") Then GetUrbanProfilbezeichnung = "76206CCD"
If InStr(ref, "P76206---2S_ST24") Then GetUrbanProfilbezeichnung = "76206WCD"
If InStr(ref, "P76206---2S_ST2x32") Then GetUrbanProfilbezeichnung = "76206CCD"
If InStr(ref, "P76206---2S_ST2x39") Then GetUrbanProfilbezeichnung = "76206CCD"
If InStr(ref, "P76206---2S_ST2xRAL") Then GetUrbanProfilbezeichnung = "76206WW"
If InStr(ref, "P76206---2S_ST2xREN") Then GetUrbanProfilbezeichnung = "76206CCD"
If InStr(ref, "P76206---2S_ST31") Then GetUrbanProfilbezeichnung = "76206WCD"
If InStr(ref, "P76206---2S_ST32") Then GetUrbanProfilbezeichnung = "76206WCD"
If InStr(ref, "P76206---2S_ST39") Then GetUrbanProfilbezeichnung = "76206WCD"
If InStr(ref, "P76206---2S_ST53") Then GetUrbanProfilbezeichnung = "76206WCD"
If InStr(ref, "P76206---2S_STRALWS") Then GetUrbanProfilbezeichnung = "76206WW"
If InStr(ref, "P76206---2S_STRENLK1") Then GetUrbanProfilbezeichnung = "76206WCD"
If InStr(ref, "P76206---2S_STRENLK3") Then GetUrbanProfilbezeichnung = "76206WCD"
If InStr(ref, "P76206---2S_STRENLK4") Then GetUrbanProfilbezeichnung = "76206WCD"
If InStr(ref, "P76206---2S_STRENLK5") Then GetUrbanProfilbezeichnung = "76206WCD"
If InStr(ref, "P76206---2S_STRENLK6") Then GetUrbanProfilbezeichnung = "76206WCD"
If InStr(ref, "P76206---2S_STWS") Then GetUrbanProfilbezeichnung = "76206WWD"
If InStr(ref, "P76206---2S_STWSRAL") Then GetUrbanProfilbezeichnung = "76206WWD"
If InStr(ref, "P76206---2S_STWSREN") Then GetUrbanProfilbezeichnung = "76206CCD"
If InStr(ref, "P762072x32") Then GetUrbanProfilbezeichnung = "76207CC"
If InStr(ref, "P7620732") Then GetUrbanProfilbezeichnung = "76207WC"
If InStr(ref, "P76207WS") Then GetUrbanProfilbezeichnung = "76207WW"
If InStr(ref, "P76207WSRAL") Then GetUrbanProfilbezeichnung = "76207WW"
If InStr(ref, "P76207WSREN") Then GetUrbanProfilbezeichnung = "76207CC"
If InStr(ref, "P76207---2L24") Then GetUrbanProfilbezeichnung = "76207WCD"
If InStr(ref, "P76207---2L2x32") Then GetUrbanProfilbezeichnung = "76207CCD"
If InStr(ref, "P76207---2L2x39") Then GetUrbanProfilbezeichnung = "76207CCD"
If InStr(ref, "P76207---2L2xRAL") Then GetUrbanProfilbezeichnung = "76207WW"
If InStr(ref, "P76207---2L2xREN") Then GetUrbanProfilbezeichnung = "76207CCD"
If InStr(ref, "P76207---2L31") Then GetUrbanProfilbezeichnung = "76207WCD"
If InStr(ref, "P76207---2L32") Then GetUrbanProfilbezeichnung = "76207WCD"
If InStr(ref, "P76207---2L39") Then GetUrbanProfilbezeichnung = "76207WCD"
If InStr(ref, "P76207---2L53") Then GetUrbanProfilbezeichnung = "76207WCD"
If InStr(ref, "P76207---2LRALWS") Then GetUrbanProfilbezeichnung = "76207WW"
If InStr(ref, "P76207---2LRENLK1") Then GetUrbanProfilbezeichnung = "76207WCD"
If InStr(ref, "P76207---2LRENLK3") Then GetUrbanProfilbezeichnung = "76207WCD"
If InStr(ref, "P76207---2LRENLK4") Then GetUrbanProfilbezeichnung = "76207WCD"
If InStr(ref, "P76207---2LRENLK5") Then GetUrbanProfilbezeichnung = "76207WCD"
If InStr(ref, "P76207---2LRENLK6") Then GetUrbanProfilbezeichnung = "76207WCD"
If InStr(ref, "P76207---2LWS") Then GetUrbanProfilbezeichnung = "76207WWD"
If InStr(ref, "P76207---2LWSRAL") Then GetUrbanProfilbezeichnung = "76207WWD"
If InStr(ref, "P76207---2LWSREN") Then GetUrbanProfilbezeichnung = "76207CCD"
If InStr(ref, "P76207---2S24") Then GetUrbanProfilbezeichnung = "76207WCD"
If InStr(ref, "P76207---2S2x32") Then GetUrbanProfilbezeichnung = "76207CCD"
If InStr(ref, "P76207---2S2x39") Then GetUrbanProfilbezeichnung = "76207CCD"
If InStr(ref, "P76207---2S2xRAL") Then GetUrbanProfilbezeichnung = "76207WW"
If InStr(ref, "P76207---2S2xREN") Then GetUrbanProfilbezeichnung = "76207CCD"
If InStr(ref, "P76207---2S31") Then GetUrbanProfilbezeichnung = "76207WCD"
If InStr(ref, "P76207---2S32") Then GetUrbanProfilbezeichnung = "76207WCD"
If InStr(ref, "P76207---2S39") Then GetUrbanProfilbezeichnung = "76207WCD"
If InStr(ref, "P76207---2S53") Then GetUrbanProfilbezeichnung = "76207WCD"
If InStr(ref, "P76207---2SRALWS") Then GetUrbanProfilbezeichnung = "76207WW"
If InStr(ref, "P76207---2SRENLK1") Then GetUrbanProfilbezeichnung = "76207WCD"
If InStr(ref, "P76207---2SRENLK3") Then GetUrbanProfilbezeichnung = "76207WCD"
If InStr(ref, "P76207---2SRENLK4") Then GetUrbanProfilbezeichnung = "76207WCD"
If InStr(ref, "P76207---2SRENLK5") Then GetUrbanProfilbezeichnung = "76207WCD"
If InStr(ref, "P76207---2SRENLK6") Then GetUrbanProfilbezeichnung = "76207WCD"
If InStr(ref, "P76207---2SWS") Then GetUrbanProfilbezeichnung = "76207WWD"
If InStr(ref, "P76207---2SWSRAL") Then GetUrbanProfilbezeichnung = "76207WWD"
If InStr(ref, "P76207---2SWSREN") Then GetUrbanProfilbezeichnung = "76207CCD"



			'-----------------------------
			'Kömmerling     ------------------
			'-----------------------------

If InStr(ref, "KO-2600 654.G") Then GetUrbanProfilbezeichnung = "2600WWD"
If InStr(ref, "KO-2600 RALaus") Then GetUrbanProfilbezeichnung = "2600WW"
If InStr(ref, "KO-2600 RALbeid") Then GetUrbanProfilbezeichnung = "2600WW"
If InStr(ref, "KO-2600 RALinn") Then GetUrbanProfilbezeichnung = "2600WWD"
If InStr(ref, "KO-2601 032.D") Then GetUrbanProfilbezeichnung = "2601CCD"
If InStr(ref, "KO-2601 041.D") Then GetUrbanProfilbezeichnung = "2601CCD"
If InStr(ref, "KO-2601 224.D") Then GetUrbanProfilbezeichnung = "2601WCD"
If InStr(ref, "KO-2601 231.D") Then GetUrbanProfilbezeichnung = "2601WCD"
If InStr(ref, "KO-2601 232.D") Then GetUrbanProfilbezeichnung = "2601WCD"
If InStr(ref, "KO-2601 237.D") Then GetUrbanProfilbezeichnung = "2601WCD"
If InStr(ref, "KO-2601 241.D") Then GetUrbanProfilbezeichnung = "2601WCD"
If InStr(ref, "KO-2601 253.D") Then GetUrbanProfilbezeichnung = "2601WCD"
If InStr(ref, "KO-2601 654.G") Then GetUrbanProfilbezeichnung = "2601WWD"
If InStr(ref, "KO-2601 RALaus") Then GetUrbanProfilbezeichnung = "2601A"
If InStr(ref, "KO-2601 RALbeid") Then GetUrbanProfilbezeichnung = "2601A"
If InStr(ref, "KO-2601 RALinn") Then GetUrbanProfilbezeichnung = "2601WWD"
If InStr(ref, "KO-2601 RENauß") Then GetUrbanProfilbezeichnung = "2601WCD"
If InStr(ref, "KO-2601 RENbeid") Then GetUrbanProfilbezeichnung = "2601CCD"
If InStr(ref, "KO-2601_AC 032") Then GetUrbanProfilbezeichnung = "2601A"
If InStr(ref, "KO-2601_AC 041") Then GetUrbanProfilbezeichnung = "2601A"
If InStr(ref, "KO-2601_AC 654") Then GetUrbanProfilbezeichnung = "2601A"
If InStr(ref, "KO-2601_AC RALinn") Then GetUrbanProfilbezeichnung = "2601A"
If InStr(ref, "KO-2606 032.D") Then GetUrbanProfilbezeichnung = "2606CCD"
If InStr(ref, "KO-2606 041.D") Then GetUrbanProfilbezeichnung = "2606CCD"
If InStr(ref, "KO-2606 224.D") Then GetUrbanProfilbezeichnung = "2606WCD"
If InStr(ref, "KO-2606 231.D") Then GetUrbanProfilbezeichnung = "2606WCD"
If InStr(ref, "KO-2606 232.D") Then GetUrbanProfilbezeichnung = "2606WCD"
If InStr(ref, "KO-2606 237.D") Then GetUrbanProfilbezeichnung = "2606WCD"
If InStr(ref, "KO-2606 241.D") Then GetUrbanProfilbezeichnung = "2606WCD"
If InStr(ref, "KO-2606 253.D") Then GetUrbanProfilbezeichnung = "2606WCD"
If InStr(ref, "KO-2606 654.G") Then GetUrbanProfilbezeichnung = "2606WWD"
If InStr(ref, "KO-2606 RALaus") Then GetUrbanProfilbezeichnung = "2606A"
If InStr(ref, "KO-2606 RALbeid") Then GetUrbanProfilbezeichnung = "2606A"
If InStr(ref, "KO-2606 RALinn") Then GetUrbanProfilbezeichnung = "2606WWD"
If InStr(ref, "KO-2606 RENauß") Then GetUrbanProfilbezeichnung = "2606WCD"
If InStr(ref, "KO-2606 RENbeid") Then GetUrbanProfilbezeichnung = "2606CCD"
If InStr(ref, "KO-2606_AC 032") Then GetUrbanProfilbezeichnung = "2606A"
If InStr(ref, "KO-2606_AC 041") Then GetUrbanProfilbezeichnung = "2606A"
If InStr(ref, "KO-2606_AC 654") Then GetUrbanProfilbezeichnung = "2606A"
If InStr(ref, "KO-2606_AC RALinn") Then GetUrbanProfilbezeichnung = "2606A"
If InStr(ref, "KO-2617 032.D") Then GetUrbanProfilbezeichnung = "2617CCD"
If InStr(ref, "KO-2617 041.D") Then GetUrbanProfilbezeichnung = "2617CCD"
If InStr(ref, "KO-2617 224.D") Then GetUrbanProfilbezeichnung = "2617WCD"
If InStr(ref, "KO-2617 231.D") Then GetUrbanProfilbezeichnung = "2617WCD"
If InStr(ref, "KO-2617 232.D") Then GetUrbanProfilbezeichnung = "2617WCD"
If InStr(ref, "KO-2617 237.D") Then GetUrbanProfilbezeichnung = "2617WCD"
If InStr(ref, "KO-2617 241.D") Then GetUrbanProfilbezeichnung = "2617WCD"
If InStr(ref, "KO-2617 253.D") Then GetUrbanProfilbezeichnung = "2617WCD"
If InStr(ref, "KO-2617 654.G") Then GetUrbanProfilbezeichnung = "2617WWD"
If InStr(ref, "KO-2617 RALauss") Then GetUrbanProfilbezeichnung = "2617WWD"
If InStr(ref, "KO-2617 RALbeids") Then GetUrbanProfilbezeichnung = "2617WW"
If InStr(ref, "KO-2617 RALin") Then GetUrbanProfilbezeichnung = "2617WWD"
If InStr(ref, "KO-2617 RENauß") Then GetUrbanProfilbezeichnung = "2617WCD"
If InStr(ref, "KO-2617 RENbeid") Then GetUrbanProfilbezeichnung = "2617CCD"
If InStr(ref, "KO-2617_AC 032") Then GetUrbanProfilbezeichnung = "2617A"
If InStr(ref, "KO-2617_AC 041") Then GetUrbanProfilbezeichnung = "2617A"
If InStr(ref, "KO-2617_AC 654") Then GetUrbanProfilbezeichnung = "2617A"
If InStr(ref, "KO-2617_AC RALauss") Then GetUrbanProfilbezeichnung = "2617A"
If InStr(ref, "KO-2617_AC RALbeids") Then GetUrbanProfilbezeichnung = "2617A"
If InStr(ref, "KO-2617_AC RALin") Then GetUrbanProfilbezeichnung = "2617A"
If InStr(ref, "KO-2617_AC RENbeid") Then GetUrbanProfilbezeichnung = "2617A"
If InStr(ref, "KO-2617_G 032.D") Then GetUrbanProfilbezeichnung = "2617GCCD"
If InStr(ref, "KO-2617_G 041.D") Then GetUrbanProfilbezeichnung = "2617GCCD"
If InStr(ref, "KO-2617_G 224.D") Then GetUrbanProfilbezeichnung = "2617GWCD"
If InStr(ref, "KO-2617_G 231.D") Then GetUrbanProfilbezeichnung = "2617GWCD"
If InStr(ref, "KO-2617_G 232.D") Then GetUrbanProfilbezeichnung = "2617GWCD"
If InStr(ref, "KO-2617_G 237.D") Then GetUrbanProfilbezeichnung = "2617GWCD"
If InStr(ref, "KO-2617_G 241.D") Then GetUrbanProfilbezeichnung = "2617GWCD"
If InStr(ref, "KO-2617_G 253.D") Then GetUrbanProfilbezeichnung = "2617GWCD"
If InStr(ref, "KO-2617_G 654.G") Then GetUrbanProfilbezeichnung = "2617GWWD"
If InStr(ref, "KO-2617_G RALauss") Then GetUrbanProfilbezeichnung = "2617GWWD"
If InStr(ref, "KO-2617_G RALbeids") Then GetUrbanProfilbezeichnung = "2617GWW"
If InStr(ref, "KO-2617_G RALin") Then GetUrbanProfilbezeichnung = "2617GWWD"
If InStr(ref, "KO-2617_G RENauß") Then GetUrbanProfilbezeichnung = "2617GWCD"
If InStr(ref, "KO-2617_G RENbeid") Then GetUrbanProfilbezeichnung = "2617GCCD"
If InStr(ref, "KO-2617_G_AC 032") Then GetUrbanProfilbezeichnung = "2617GA"
If InStr(ref, "KO-2617_G_AC 041") Then GetUrbanProfilbezeichnung = "2617GA"
If InStr(ref, "KO-2617_G_AC 654") Then GetUrbanProfilbezeichnung = "2617GA"
If InStr(ref, "KO-2617_G_AC RALauss") Then GetUrbanProfilbezeichnung = "2617GA"
If InStr(ref, "KO-2617_G_AC RALbeids") Then GetUrbanProfilbezeichnung = "2617GA"
If InStr(ref, "KO-2617_G_AC RALin") Then GetUrbanProfilbezeichnung = "2617GA"
If InStr(ref, "KO-2617_G_AC RENbeid") Then GetUrbanProfilbezeichnung = "2617GA"
If InStr(ref, "KO-2618 032.D") Then GetUrbanProfilbezeichnung = "2618CCD"
If InStr(ref, "KO-2618 041.D") Then GetUrbanProfilbezeichnung = "2618CCD"
If InStr(ref, "KO-2618 224.D") Then GetUrbanProfilbezeichnung = "2618WCD"
If InStr(ref, "KO-2618 231.D") Then GetUrbanProfilbezeichnung = "2618WCD"
If InStr(ref, "KO-2618 232.D") Then GetUrbanProfilbezeichnung = "2618WCD"
If InStr(ref, "KO-2618 237.D") Then GetUrbanProfilbezeichnung = "2618WCD"
If InStr(ref, "KO-2618 241.D") Then GetUrbanProfilbezeichnung = "2618WCD"
If InStr(ref, "KO-2618 253.D") Then GetUrbanProfilbezeichnung = "2618WCD"
If InStr(ref, "KO-2618 654.G") Then GetUrbanProfilbezeichnung = "2618WWD"
If InStr(ref, "KO-2618 RALauss") Then GetUrbanProfilbezeichnung = "2618WWD"
If InStr(ref, "KO-2618 RALbeids") Then GetUrbanProfilbezeichnung = "2618WW"
If InStr(ref, "KO-2618 RALin") Then GetUrbanProfilbezeichnung = "2618WWD"
If InStr(ref, "KO-2618 RENauß") Then GetUrbanProfilbezeichnung = "2618WCD"
If InStr(ref, "KO-2618 RENbeid") Then GetUrbanProfilbezeichnung = "2618CCD"
If InStr(ref, "KO-2618_AC 032") Then GetUrbanProfilbezeichnung = "2618A"
If InStr(ref, "KO-2618_AC 041") Then GetUrbanProfilbezeichnung = "2618A"
If InStr(ref, "KO-2618_AC 654") Then GetUrbanProfilbezeichnung = "2618A"
If InStr(ref, "KO-2618_AC RALauss") Then GetUrbanProfilbezeichnung = "2618A"
If InStr(ref, "KO-2618_AC RALbeids") Then GetUrbanProfilbezeichnung = "2618A"
If InStr(ref, "KO-2618_AC RALin") Then GetUrbanProfilbezeichnung = "2618A"
If InStr(ref, "KO-2618_AC RALinn") Then GetUrbanProfilbezeichnung = "2618A"
If InStr(ref, "KO-2618_AC RENbeid") Then GetUrbanProfilbezeichnung = "2618A"
If InStr(ref, "KO-2618_G 032.D") Then GetUrbanProfilbezeichnung = "2618GCCD"
If InStr(ref, "KO-2618_G 041.D") Then GetUrbanProfilbezeichnung = "2618GCCD"
If InStr(ref, "KO-2618_G 224.D") Then GetUrbanProfilbezeichnung = "2618GWCD"
If InStr(ref, "KO-2618_G 231.D") Then GetUrbanProfilbezeichnung = "2618GWCD"
If InStr(ref, "KO-2618_G 232.D") Then GetUrbanProfilbezeichnung = "2618GWCD"
If InStr(ref, "KO-2618_G 237.D") Then GetUrbanProfilbezeichnung = "2618GWCD"
If InStr(ref, "KO-2618_G 241.D") Then GetUrbanProfilbezeichnung = "2618GWCD"
If InStr(ref, "KO-2618_G 253.D") Then GetUrbanProfilbezeichnung = "2618GWCD"
If InStr(ref, "KO-2618_G 654.G") Then GetUrbanProfilbezeichnung = "2618GWWD"
If InStr(ref, "KO-2618_G RALauss") Then GetUrbanProfilbezeichnung = "2618GWWD"
If InStr(ref, "KO-2618_G RALbeids") Then GetUrbanProfilbezeichnung = "2618GWW"
If InStr(ref, "KO-2618_G RALin") Then GetUrbanProfilbezeichnung = "2618GWWD"
If InStr(ref, "KO-2618_G RENauß") Then GetUrbanProfilbezeichnung = "2618GWCD"
If InStr(ref, "KO-2618_G RENbeid") Then GetUrbanProfilbezeichnung = "2618GCCD"
If InStr(ref, "KO-2618_G_AC 032") Then GetUrbanProfilbezeichnung = "2618GA"
If InStr(ref, "KO-2618_G_AC 041") Then GetUrbanProfilbezeichnung = "2618GA"
If InStr(ref, "KO-2618_G_AC 654") Then GetUrbanProfilbezeichnung = "2618GA"
If InStr(ref, "KO-2618_G_AC RALauss") Then GetUrbanProfilbezeichnung = "2618GA"
If InStr(ref, "KO-2618_G_AC RALbeids") Then GetUrbanProfilbezeichnung = "2618GA"
If InStr(ref, "KO-2618_G_AC RALin") Then GetUrbanProfilbezeichnung = "2618GA"
If InStr(ref, "KO-2618_G_AC RALinn") Then GetUrbanProfilbezeichnung = "2618GA"
If InStr(ref, "KO-2618_G_AC RENbeid") Then GetUrbanProfilbezeichnung = "2618GA"
If InStr(ref, "KO-2645 032.D") Then GetUrbanProfilbezeichnung = "2645CCD"
If InStr(ref, "KO-2645 041.D") Then GetUrbanProfilbezeichnung = "2645CCD"
If InStr(ref, "KO-2645 224.D") Then GetUrbanProfilbezeichnung = "2645WCD"
If InStr(ref, "KO-2645 231.D") Then GetUrbanProfilbezeichnung = "2645WCD"
If InStr(ref, "KO-2645 232.D") Then GetUrbanProfilbezeichnung = "2645WCD"
If InStr(ref, "KO-2645 237.D") Then GetUrbanProfilbezeichnung = "2645WCD"
If InStr(ref, "KO-2645 241.D") Then GetUrbanProfilbezeichnung = "2645WCD"
If InStr(ref, "KO-2645 253.D") Then GetUrbanProfilbezeichnung = "2645WCD"
If InStr(ref, "KO-2645 654.G") Then GetUrbanProfilbezeichnung = "2645WWD"
If InStr(ref, "KO-2645 RALaus") Then GetUrbanProfilbezeichnung = "2645WWD"
If InStr(ref, "KO-2645 RALbeid") Then GetUrbanProfilbezeichnung = "2645WW"
If InStr(ref, "KO-2645 RALinn") Then GetUrbanProfilbezeichnung = "2645WWD"
If InStr(ref, "KO-2645 RENauß") Then GetUrbanProfilbezeichnung = "2645WCD"
If InStr(ref, "KO-2645 RENbeid") Then GetUrbanProfilbezeichnung = "2645CCD"
If InStr(ref, "KO-2645_AC 032") Then GetUrbanProfilbezeichnung = "2645A"
If InStr(ref, "KO-2645_AC 041") Then GetUrbanProfilbezeichnung = "2645A"
If InStr(ref, "KO-2645_AC 654") Then GetUrbanProfilbezeichnung = "2645A"
If InStr(ref, "KO-2645_AC RALinn") Then GetUrbanProfilbezeichnung = "2645A"
If InStr(ref, "KO-2621_GS 032.D") Then GetUrbanProfilbezeichnung = "2621CCD"
If InStr(ref, "KO-2621_GS 041.D") Then GetUrbanProfilbezeichnung = "2621CCD"
If InStr(ref, "KO-2621_GS 224.D") Then GetUrbanProfilbezeichnung = "2621WCD"
If InStr(ref, "KO-2621_GS 231.D") Then GetUrbanProfilbezeichnung = "2621WCD"
If InStr(ref, "KO-2621_GS 232.D") Then GetUrbanProfilbezeichnung = "2621WCD"
If InStr(ref, "KO-2621_GS 237.D") Then GetUrbanProfilbezeichnung = "2621WCD"
If InStr(ref, "KO-2621_GS 241.D") Then GetUrbanProfilbezeichnung = "2621WCD"
If InStr(ref, "KO-2621_GS 253.D") Then GetUrbanProfilbezeichnung = "2621WCD"
If InStr(ref, "KO-2621_GS 654.G") Then GetUrbanProfilbezeichnung = "2621WWD"
If InStr(ref, "KO-2621_GS RALaus") Then GetUrbanProfilbezeichnung = "2621WWD"
If InStr(ref, "KO-2621_GS RALbeid") Then GetUrbanProfilbezeichnung = "2621WW"
If InStr(ref, "KO-2621_GS RALinn") Then GetUrbanProfilbezeichnung = "2621WWD"
If InStr(ref, "KO-2621_GS RENauß") Then GetUrbanProfilbezeichnung = "2621WCD"
If InStr(ref, "KO-2621_GS RENbeid") Then GetUrbanProfilbezeichnung = "2621CCD"
If InStr(ref, "KO-2621_GS_AC 032") Then GetUrbanProfilbezeichnung = "2621A"
If InStr(ref, "KO-2621_GS_AC 041") Then GetUrbanProfilbezeichnung = "2621A"
If InStr(ref, "KO-2621_GS_AC 654") Then GetUrbanProfilbezeichnung = "2621A"
If InStr(ref, "KO-2621_GS_AC RALinn") Then GetUrbanProfilbezeichnung = "2621A"
If InStr(ref, "KO-2621_GS_R 032.D") Then GetUrbanProfilbezeichnung = "2621CCD"
If InStr(ref, "KO-2621_GS_R 041.D") Then GetUrbanProfilbezeichnung = "2621CCD"
If InStr(ref, "KO-2621_GS_R 224.D") Then GetUrbanProfilbezeichnung = "2621WCD"
If InStr(ref, "KO-2621_GS_R 231.D") Then GetUrbanProfilbezeichnung = "2621WCD"
If InStr(ref, "KO-2621_GS_R 232.D") Then GetUrbanProfilbezeichnung = "2621WCD"
If InStr(ref, "KO-2621_GS_R 237.D") Then GetUrbanProfilbezeichnung = "2621WCD"
If InStr(ref, "KO-2621_GS_R 241.D") Then GetUrbanProfilbezeichnung = "2621WCD"
If InStr(ref, "KO-2621_GS_R 253.D") Then GetUrbanProfilbezeichnung = "2621WCD"
If InStr(ref, "KO-2621_GS_R 654.G") Then GetUrbanProfilbezeichnung = "2621WWD"
If InStr(ref, "KO-2621_GS_R RALaus") Then GetUrbanProfilbezeichnung = "2621WWD"
If InStr(ref, "KO-2621_GS_R RALbeid") Then GetUrbanProfilbezeichnung = "2621WW"
If InStr(ref, "KO-2621_GS_R RALinn") Then GetUrbanProfilbezeichnung = "2621WWD"
If InStr(ref, "KO-2621_GS_R RENauß") Then GetUrbanProfilbezeichnung = "2621WCD"
If InStr(ref, "KO-2621_GS_R RENbeid") Then GetUrbanProfilbezeichnung = "2621CCD"


			'-----------------------------
			'Aluplast     ------------------
			'-----------------------------
				If InStr(ref, "AP-140003 RALaus") Then GetUrbanProfilbezeichnung = "140003WW"
If InStr(ref, "AP-140003 RALbeid") Then GetUrbanProfilbezeichnung = "140003WW"
If InStr(ref, "AP-140003 RALinn") Then GetUrbanProfilbezeichnung = "140003WW"
If InStr(ref, "AP-140003 RENauß") Then GetUrbanProfilbezeichnung = "140003WC"
If InStr(ref, "AP-140003 RENbeid") Then GetUrbanProfilbezeichnung = "140003CC"
If InStr(ref, "AP-140303 201") Then GetUrbanProfilbezeichnung = "140003WC"
If InStr(ref, "AP-140303 205") Then GetUrbanProfilbezeichnung = "140003WC"
If InStr(ref, "AP-140303 215") Then GetUrbanProfilbezeichnung = "140003WC"
If InStr(ref, "AP-140303 217") Then GetUrbanProfilbezeichnung = "140003WC"
If InStr(ref, "AP-140303 223") Then GetUrbanProfilbezeichnung = "140003WC"
If InStr(ref, "AP-140303 243") Then GetUrbanProfilbezeichnung = "140003WC"
If InStr(ref, "AP-140303 245") Then GetUrbanProfilbezeichnung = "140003WC"
If InStr(ref, "AP-140403 000") Then GetUrbanProfilbezeichnung = "140003WW"
If InStr(ref, "AP-141303 123") Then GetUrbanProfilbezeichnung = "140003CC"
If InStr(ref, "AP-143303 115") Then GetUrbanProfilbezeichnung = "140003CC"
If InStr(ref, "AP-140003_A RALaus") Then GetUrbanProfilbezeichnung = "140003WW"
If InStr(ref, "AP-140003_A RALbeid") Then GetUrbanProfilbezeichnung = "140003WW"
If InStr(ref, "AP-140003_A RALinn") Then GetUrbanProfilbezeichnung = "140003WW"
If InStr(ref, "AP-140003_A RENauß") Then GetUrbanProfilbezeichnung = "140003CC"
If InStr(ref, "AP-140003_A RENbeid") Then GetUrbanProfilbezeichnung = "140003CC"
If InStr(ref, "AP-140303_A 201") Then GetUrbanProfilbezeichnung = "140003CC"
If InStr(ref, "AP-140303_A 205") Then GetUrbanProfilbezeichnung = "140003CC"
If InStr(ref, "AP-140303_A 215") Then GetUrbanProfilbezeichnung = "140003CC"
If InStr(ref, "AP-140303_A 217") Then GetUrbanProfilbezeichnung = "140003CC"
If InStr(ref, "AP-140303_A 223") Then GetUrbanProfilbezeichnung = "140003CC"
If InStr(ref, "AP-140303_A 243") Then GetUrbanProfilbezeichnung = "140003CC"
If InStr(ref, "AP-140303_A 245") Then GetUrbanProfilbezeichnung = "140003CC"
If InStr(ref, "AP-140403_A 000") Then GetUrbanProfilbezeichnung = "140003WW"
If InStr(ref, "AP-141303_A 123") Then GetUrbanProfilbezeichnung = "140003CC"
If InStr(ref, "AP-143303_A 115") Then GetUrbanProfilbezeichnung = "140003CC"
If InStr(ref, "AP-140003_AC RALinn") Then GetUrbanProfilbezeichnung = "140003A"
If InStr(ref, "AP-140003_AC RENbeid") Then GetUrbanProfilbezeichnung = "140003A"
If InStr(ref, "AP-140403TS 000") Then GetUrbanProfilbezeichnung = "140003WW"
If InStr(ref, "AP-141303TS 123") Then GetUrbanProfilbezeichnung = "140003A"
If InStr(ref, "AP-143303TS 115") Then GetUrbanProfilbezeichnung = "140003A"
If InStr(ref, "AP-140033 RALaus") Then GetUrbanProfilbezeichnung = "140933WW"
If InStr(ref, "AP-140033 RALbeid") Then GetUrbanProfilbezeichnung = "140933WW"
If InStr(ref, "AP-140033 RALinn") Then GetUrbanProfilbezeichnung = "140933WWD"
If InStr(ref, "AP-140033 RENauß") Then GetUrbanProfilbezeichnung = "140933WCD"
If InStr(ref, "AP-140033 RENbeid") Then GetUrbanProfilbezeichnung = "140933CCD"
If InStr(ref, "AP-140333 201") Then GetUrbanProfilbezeichnung = "140933WCD"
If InStr(ref, "AP-140333 205") Then GetUrbanProfilbezeichnung = "140933WCD"
If InStr(ref, "AP-140333 215") Then GetUrbanProfilbezeichnung = "140933WCD"
If InStr(ref, "AP-140333 217") Then GetUrbanProfilbezeichnung = "140933WCD"
If InStr(ref, "AP-140333 223") Then GetUrbanProfilbezeichnung = "140933WCD"
If InStr(ref, "AP-140333 243") Then GetUrbanProfilbezeichnung = "140933WCD"
If InStr(ref, "AP-140333 245") Then GetUrbanProfilbezeichnung = "140933WCD"
If InStr(ref, "AP-140433 000") Then GetUrbanProfilbezeichnung = "140933WWD"
If InStr(ref, "AP-143333 115") Then GetUrbanProfilbezeichnung = "140933CCD"
If InStr(ref, "AP-141333 123") Then GetUrbanProfilbezeichnung = "140933CCD"
If InStr(ref, "AP-140033_AC RALinn") Then GetUrbanProfilbezeichnung = "140933A"
If InStr(ref, "AP-140033_AC RENbeid") Then GetUrbanProfilbezeichnung = "140933A"
If InStr(ref, "AP-140433TS 000") Then GetUrbanProfilbezeichnung = "140933A"
If InStr(ref, "AP-141333TS 123") Then GetUrbanProfilbezeichnung = "140933A"
If InStr(ref, "AP-143333TS 115") Then GetUrbanProfilbezeichnung = "140933A"
If InStr(ref, "AP-140033_BR RALaus") Then GetUrbanProfilbezeichnung = "140933WW"
If InStr(ref, "AP-140033_BR RALbeid") Then GetUrbanProfilbezeichnung = "140933WW"
If InStr(ref, "AP-140033_BR RALinn") Then GetUrbanProfilbezeichnung = "140933WW"
If InStr(ref, "AP-140033_BR RENauß") Then GetUrbanProfilbezeichnung = "140933WC"
If InStr(ref, "AP-140033_BR RENbeid") Then GetUrbanProfilbezeichnung = "140933CC"
If InStr(ref, "AP-140333_ST 201") Then GetUrbanProfilbezeichnung = "140933WC"
If InStr(ref, "AP-140333_ST 205") Then GetUrbanProfilbezeichnung = "140933WC"
If InStr(ref, "AP-140333_ST 215") Then GetUrbanProfilbezeichnung = "140933WC"
If InStr(ref, "AP-140333_ST 217") Then GetUrbanProfilbezeichnung = "140933WC"
If InStr(ref, "AP-140333_ST 223") Then GetUrbanProfilbezeichnung = "140933WC"
If InStr(ref, "AP-140333_ST 243") Then GetUrbanProfilbezeichnung = "140933WC"
If InStr(ref, "AP-140333_ST 245") Then GetUrbanProfilbezeichnung = "140933WC"
If InStr(ref, "AP-140433_ST 000") Then GetUrbanProfilbezeichnung = "140933WW"
If InStr(ref, "AP-141333_ST 123") Then GetUrbanProfilbezeichnung = "140933CC"
If InStr(ref, "AP-143333_ST 115") Then GetUrbanProfilbezeichnung = "140933CC"
If InStr(ref, "AP-140033_BR_A 000") Then GetUrbanProfilbezeichnung = "140933WW"
If InStr(ref, "AP-140033_BR_A 115") Then GetUrbanProfilbezeichnung = "140933CC"
If InStr(ref, "AP-140033_BR_A 123") Then GetUrbanProfilbezeichnung = "140933CC"
If InStr(ref, "AP-140033_BR_A 201") Then GetUrbanProfilbezeichnung = "140933WC"
If InStr(ref, "AP-140033_BR_A 205") Then GetUrbanProfilbezeichnung = "140933WC"
If InStr(ref, "AP-140033_BR_A 215") Then GetUrbanProfilbezeichnung = "140933WC"
If InStr(ref, "AP-140033_BR_A 217") Then GetUrbanProfilbezeichnung = "140933WC"
If InStr(ref, "AP-140033_BR_A 223") Then GetUrbanProfilbezeichnung = "140933WC"
If InStr(ref, "AP-140033_BR_A 243") Then GetUrbanProfilbezeichnung = "140933WC"
If InStr(ref, "AP-140033_BR_A 245") Then GetUrbanProfilbezeichnung = "140933WC"
If InStr(ref, "AP-140033_BR_A RALaus") Then GetUrbanProfilbezeichnung = "140933WW"
If InStr(ref, "AP-140033_BR_A RALbeid") Then GetUrbanProfilbezeichnung = "140933WW"
If InStr(ref, "AP-140033_BR_A RALinn") Then GetUrbanProfilbezeichnung = "140933WW"
If InStr(ref, "AP-140033_BR_A RENauß") Then GetUrbanProfilbezeichnung = "140933WC"
If InStr(ref, "AP-140033_BR_A RENbeid") Then GetUrbanProfilbezeichnung = "140933CC"
If InStr(ref, "AP-140033_FÜD 000") Then GetUrbanProfilbezeichnung = "140933FWW"
If InStr(ref, "AP-140033_FÜD 115") Then GetUrbanProfilbezeichnung = "140933FCC"
If InStr(ref, "AP-140033_FÜD 123") Then GetUrbanProfilbezeichnung = "140933FCC"
If InStr(ref, "AP-140033_FÜD 201") Then GetUrbanProfilbezeichnung = "140933FWC"
If InStr(ref, "AP-140033_FÜD 205") Then GetUrbanProfilbezeichnung = "140933FWC"
If InStr(ref, "AP-140033_FÜD 215") Then GetUrbanProfilbezeichnung = "140933FWC"
If InStr(ref, "AP-140033_FÜD 217") Then GetUrbanProfilbezeichnung = "140933FWC"
If InStr(ref, "AP-140033_FÜD 223") Then GetUrbanProfilbezeichnung = "140933FWC"
If InStr(ref, "AP-140033_FÜD 243") Then GetUrbanProfilbezeichnung = "140933FWC"
If InStr(ref, "AP-140033_FÜD 245") Then GetUrbanProfilbezeichnung = "140933FWC"
If InStr(ref, "AP-140033_FÜD RALaus") Then GetUrbanProfilbezeichnung = "140933FWW"
If InStr(ref, "AP-140033_FÜD RALbeid") Then GetUrbanProfilbezeichnung = "140933FWW"
If InStr(ref, "AP-140033_FÜD RALinn") Then GetUrbanProfilbezeichnung = "140933FWW"
If InStr(ref, "AP-140033_FÜD RENauß") Then GetUrbanProfilbezeichnung = "140933FWC"
If InStr(ref, "AP-140033_FÜD RENbeid") Then GetUrbanProfilbezeichnung = "140933FCC"
If InStr(ref, "AP-140035 RALaus") Then GetUrbanProfilbezeichnung = "140935WW"
If InStr(ref, "AP-140035 RALbeid") Then GetUrbanProfilbezeichnung = "140935WW"
If InStr(ref, "AP-140035 RALinn") Then GetUrbanProfilbezeichnung = "140935WWD"
If InStr(ref, "AP-140035 RENbeid") Then GetUrbanProfilbezeichnung = "140935CCD"
If InStr(ref, "AP-140435 000") Then GetUrbanProfilbezeichnung = "140935WWD"
If InStr(ref, "AP-140035_FÜD 000") Then GetUrbanProfilbezeichnung = "140935WW"
If InStr(ref, "AP-140035_FÜD RALaus") Then GetUrbanProfilbezeichnung = "140935WW"
If InStr(ref, "AP-140035_FÜD RALbeid") Then GetUrbanProfilbezeichnung = "140935WW"
If InStr(ref, "AP-140035_FÜD RALinn") Then GetUrbanProfilbezeichnung = "140935WW"
If InStr(ref, "AP-140035_FÜD RENbeid") Then GetUrbanProfilbezeichnung = "140935CC"
If InStr(ref, "AP-140035 115") Then GetUrbanProfilbezeichnung = "140935CC"
If InStr(ref, "AP-140035 123") Then GetUrbanProfilbezeichnung = "140935CC"
If InStr(ref, "AP-140035 201") Then GetUrbanProfilbezeichnung = "140935WC"
If InStr(ref, "AP-140035 205") Then GetUrbanProfilbezeichnung = "140935WC"
If InStr(ref, "AP-140035 215") Then GetUrbanProfilbezeichnung = "140935WC"
If InStr(ref, "AP-140035 217") Then GetUrbanProfilbezeichnung = "140935WC"
If InStr(ref, "AP-140035 223") Then GetUrbanProfilbezeichnung = "140935WC"
If InStr(ref, "AP-140035 243") Then GetUrbanProfilbezeichnung = "140935WC"
If InStr(ref, "AP-140035 245") Then GetUrbanProfilbezeichnung = "140935WC"


		End Function
	
		Dim KaempferZaehler

		Function InitKaempferZaehler()
			InitKaempferZaehler = ""
			KaempferZaehler = 0
		End Function

		Function IncKaempferZaehler()
			IncKaempferZaehler = ""
			KaempferZaehler = KaempferZaehler + 1
		End Function

		Function GetKaempferZaehler()
			GetKaempferZaehler = KaempferZaehler
		End Function
		
	]]></msxsl:script>
	<xsl:param name="set" select="'1'"/>
	<xsl:param name="machine" select="'1'"/>
	<xsl:param name="separation" select="'1'"/>
	<xsl:param name="WithESL" select="'1'"/>
	<xsl:param name="Anschlagrichtung" select="'rechts'"/>
	<xsl:param name="Abfragemass" select="'250'"/>
	<!-- separation = 1  Rahmen-->
	<!-- separation = 2  Flügel -->
	<!-- separation = *  Rahmen+Flügel -->
	<xsl:variable name="lot" select="/ProductionLot/@productionLotNumber"/>
	
	<xsl:template match="/">
		<xsl:value-of select="script:SetLocaleFirst()"/>
		<xsl:apply-templates select="descendant::ProductionSet[@ProductionSetNumber=$set] "/>
		<xsl:value-of select="script:SetLocaleEnd()"/>
	</xsl:template>
	
	<xsl:template match="ProductionSet">
		<xsl:apply-templates select="Squares"/>
	</xsl:template>
	
	<xsl:template match="Squares">
		<xsl:choose>
			<xsl:when test="$separation='1'">
				<!---->
				<xsl:apply-templates select="FRAME[count(child::Piece) &gt; 0]">
					<xsl:sort select ="Piece[2]/@container" order ="ascending" data-type ="number"/>
					<xsl:sort select ="Piece[2]/@slot" order ="ascending" data-type ="number"/>
				</xsl:apply-templates>
			</xsl:when>
			<xsl:when test="$separation='2'">
				<xsl:apply-templates select="FRAME/SASH[count(child::Piece) &gt; 0]">
					<xsl:sort select ="Piece[2]/@container" order ="ascending" data-type ="number"/>
					<xsl:sort select ="Piece[2]/@slot" order ="ascending" data-type ="number"/>
				</xsl:apply-templates>
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates select="FRAME[count(child::Piece) &gt; 0]">
					<xsl:sort select ="Piece[2]/@container" order ="ascending" data-type ="number"/>
					<xsl:sort select ="Piece[2]/@slot" order ="ascending" data-type ="number"/>
				</xsl:apply-templates>
				<xsl:apply-templates select="FRAME/SASH[count(child::Piece) &gt; 0]">
					<xsl:sort select ="Piece[2]/@container" order ="ascending" data-type ="number"/>
					<xsl:sort select ="Piece[2]/@slot" order ="ascending" data-type ="number"/>
				</xsl:apply-templates>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template match="FRAME">
		<!--SD20110221	min Breite: 310 mm Höhe: 250 mm
						max Breite: 3000 mm Höhe: 2450 mm-->
		<!--<xsl:if test="	MEASURE/@swidth &gt; 310 and MEASURE/@sheight &gt; 250 and ((MEASURE/@swidth &lt;3000 and MEASURE/@sheight &lt; 2450) or 
						(MEASURE/@sheight &lt;3000 and MEASURE/@swidth &lt; 2450))">-->
			
			<!-- RK160128#29252 Rahmeninformationen bei Model_AO vertikal spiegeln -->
			<xsl:variable name="Modell_AO">
				<xsl:choose>
					<xsl:when test="count(child::Piece/descendant::Operation[@name='MODELL_AO']) &gt;0"><xsl:value-of select="1"/></xsl:when>
					<xsl:otherwise><xsl:value-of select="0"/></xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			
			
			<xsl:variable name="cnt" select="script:IncCnt()"/>
			<!--Teile-ID (5-30) Eindeutige Teileidentifikation-->
			<!--6st Los & 2st Set & 4st squareNumberInSet & 2st Lage -->
			<xsl:variable name="Barcode">
				<xsl:value-of select="concat(format-number($lot,'000000'),format-number($set,'00'),format-number(@squareNumberInSet,'0000'),'00')"/>
			</xsl:variable>
			<xsl:value-of select="concat(script:RFill(string($Barcode),'0','14'),';')"/>
			<!--Teile-ID2 (5-30) Eindeutige Teile-ID bei Doppelschweißen-->
			<!---->
			<xsl:variable name="Barcode2">
				<xsl:value-of select="''"/>
			</xsl:variable>
			<xsl:value-of select="concat(script:RFill(string($Barcode2),' ','14'),';')"/>
			<!--Los (10) Losbezeichnung-->
			<!--6st Los & 2st Set & 00-->
			<xsl:variable name="Losbezeichnung">
				<xsl:value-of select="concat(format-number($lot,'000000'),format-number($set,'00'),'00')"/>
			</xsl:variable>
			<xsl:value-of select="concat(script:RFill(string($Losbezeichnung),string('0'),'20'),';')"/>
			<!-- Lospos (3) Position im Los-->
			<!--3st squareNumberInSet-->
			<xsl:variable name="Lospos">
				<xsl:value-of select="format-number(@squareNumberInSet,'000')"/>
			</xsl:variable>
			<xsl:value-of select="concat(script:LFill(string($Lospos),'0','3'),';')"/>
			<!--Auftrag (20) Auftragsbezeichnung -->
			<!--20st orderNumber-->
			<xsl:variable name="Auftrag">
				<xsl:value-of select="script:LFill(string(@orderNumber),string(' '), '20')"/>
			</xsl:variable>
			<xsl:value-of select="concat($Auftrag,';')"/>
			<!--Auftragspos (3) Position im Auftrag-->
			<!--3st sortOrder-->
			<xsl:variable name="Auftragspos">
				<xsl:value-of select="script:LFill(string(@sortOrder),' ','3')"/>
			</xsl:variable>
			<xsl:value-of select="concat($Auftragspos,';')"/>
			<!--Breite (7) Rahmenbreite(3) [mm] [xxxx.xx]-->
			<xsl:value-of select="concat(format-number(MEASURE/@swidth,'0000.00'),';')"/>
			<!--Höhe   (7) Rahmenlänge (3) [mm] [xxxx.xx]-->
			<xsl:value-of select="concat(format-number(MEASURE/@sheight,'0000.00'),';')"/>
			<!--Profil oben (12) Profilbezeichnung oberes(3) Profil-->
			<!--6st material & URBAN-Frabkennung & Dichtungskennung-->
			<xsl:call-template name="Profile">
				<xsl:with-param name="refFin" select="Piece[@angle=180]/@material"/>
				<xsl:with-param name="refBase" select="Piece[@angle=180]/@materialbase"/>
			</xsl:call-template>
			<!--Profil unten (12) Profilbezeichnung unteres(3) Profil-->
			<xsl:call-template name="Profile">
				<xsl:with-param name="refFin" select="Piece[@angle=360]/@material"/>
				<xsl:with-param name="refBase" select="Piece[@angle=360]/@materialbase"/>
			</xsl:call-template>
			<xsl:choose>
				<!-- RK160128#29252 Rahmeninformationen bei Model_AO vertikal spiegeln -->
				<xsl:when test="$Modell_AO=1">
					<!--Profil links (12) Profilbezeichnung linkes(3) Profil-->
					<xsl:call-template name="Profile">
						<xsl:with-param name="refFin" select="Piece[@angle=90]/@material"/>
						<xsl:with-param name="refBase" select="Piece[@angle=90]/@materialbase"/>
					</xsl:call-template>
					<!--Profil rechts (12) Profilbezeichnung rechtes(3) Profil-->
					<xsl:call-template name="Profile">
						<xsl:with-param name="refFin" select="Piece[@angle=270]/@material"/>
						<xsl:with-param name="refBase" select="Piece[@angle=270]/@materialbase"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<!--Profil links (12) Profilbezeichnung linkes(3) Profil-->
					<xsl:call-template name="Profile">
						<xsl:with-param name="refFin" select="Piece[@angle=270]/@material"/>
						<xsl:with-param name="refBase" select="Piece[@angle=270]/@materialbase"/>
					</xsl:call-template>
					<!--Profil rechts (12) Profilbezeichnung rechtes(3) Profil-->
					<xsl:call-template name="Profile">
						<xsl:with-param name="refFin" select="Piece[@angle=90]/@material"/>
						<xsl:with-param name="refBase" select="Piece[@angle=90]/@materialbase"/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:value-of select="script:InitVerarbeiteteKaempfer()"/>
			<!--Initialisieren des ESL-Arrays-->
			<xsl:value-of select="script:InitESL()"/>
			<xsl:value-of select="script:InitKaempferZaehler()"/>
			<!--SD20101118 geschweißte Teiler haben die Kennzeichnung "_GS"-->
			<!--horizontale Teiler-->
			<xsl:variable name="HorizontaleTeiler">
				<xsl:for-each select="Piece[@angle = 270 or @angel = 90]/Delimitation[contains(@MullionBaseRef,'KO-2621_GS')]">
					<!--nach verechneter X-Porsition sortieren-->
					<xsl:sort select ="@MullionRealPos" order ="descending" data-type ="number"/>
					<xsl:variable name="zuBearbeitenderKaempfer">
						<!--prüft ob der Kämpfer noch oder schon bearbeitet ist-->
						<xsl:value-of select="script:CheckVerarbeiteteKaempfer(string(@MullionProfilePieceId))"/>
					</xsl:variable>
					<xsl:if test="$zuBearbeitenderKaempfer = 1">
						<xsl:value-of select="script:IncKaempferZaehler()"/>
						<!--Kämpferprofil1H(4) AN 12 Horizontal Profilbezeichnung Kämpfer 1 (von unten)-->
						<xsl:call-template name="Profile">
							<xsl:with-param name="refFin" select="@MullionMaterial"/>
							<xsl:with-param name="refBase" select="@MullionBaseRef"/>
						</xsl:call-template>
						<!--Kämpfermass1H(4) N 7 Horizontal Kämpfermass 1 (von unten) [xxxx.xx]-->
						<xsl:value-of select="concat(format-number(@MullionRealPos,'0000.00'),';')"/>
						<!--Kämpfertyp1H(4) N 1 Horizontal Kämpfertyp Kämpfer 1(6)-->
						<xsl:value-of select="script:GetKaempfertyp(string(@MullionMaterial), string(ancestor::Piece/@angle))"/>
						<xsl:value-of select="';#'"/>
						<!--SD20110124 setzen der ESL, SL für Teiler -->
						<xsl:variable name="ang">
							<xsl:value-of select="@angle"/>
						</xsl:variable>
						<xsl:variable name="len">
							<xsl:value-of select="@lengthWithWelding"/>
						</xsl:variable>
						<xsl:if test="script:GetKaempferZaehler() = 2">
							<xsl:for-each select="Operations/Operation[contains(@name,'ESL_')]">
								<!--Side, gibt an auf welcher Seite vom Teiler die Bearbeitung sitzt-->
								<!--bie Teiler = 90° 1 = links / 2 = rechts-->
								<xsl:variable name="Side">
									<xsl:value-of select="@side"/>
								</xsl:variable>
								<!--<xsl:value-of select="concat('    Test:  ',$ang,'   ',$len,'    ')"/>-->
								<xsl:choose>
									<xsl:when test="contains(@name,'ESL_SL')">
										<xsl:if test="@X &lt; $Abfragemass or ($len - @X &lt; $Abfragemass)">
											<xsl:choose>
												<!--K1.1o/r-->
												<xsl:when test="$ang = 360 and $len - @X &lt; $Abfragemass and $Side = 1">
													<xsl:value-of select="script:setESL('5','4')"/>
												</xsl:when>
												<xsl:when test="$ang = 180 and @X &lt; $Abfragemass and $Side = 2">
													<xsl:value-of select="script:setESL('5','4')"/>
												</xsl:when>
												<!--K1.2o/r-->
												<xsl:when test="$ang = 360 and $len - @X &lt; $Abfragemass and $Side = 2">
													<xsl:value-of select="script:setESL('6','4')"/>
												</xsl:when>
												<xsl:when test="$ang = 180 and @X &lt; $Abfragemass and $Side = 1">
													<xsl:value-of select="script:setESL('6','4')"/>
												</xsl:when>
												<!--K1.1u/l-->
												<xsl:when test="$ang = 360 and @X &lt; $Abfragemass and $Side = 1">
													<xsl:value-of select="script:setESL('7','4')"/>
												</xsl:when>
												<xsl:when test="$ang = 180 and $len - @X &lt; $Abfragemass and $Side = 2">
													<xsl:value-of select="script:setESL('7','4')"/>
												</xsl:when>
												<!--K1.2u/l-->
												<xsl:when test="$ang = 360 and @X &lt; $Abfragemass and $Side = 2">
													<xsl:value-of select="script:setESL('8','4')"/>
												</xsl:when>
												<xsl:when test="$ang = 180 and $len - @X &lt; $Abfragemass and $Side = 1">
													<xsl:value-of select="script:setESL('8','4')"/>
												</xsl:when>
											</xsl:choose>
										</xsl:if>
									</xsl:when>
									<xsl:when test ="contains(@name,'ESL_')">
										<xsl:if test="@X &lt; $Abfragemass or ($len - @X &lt; $Abfragemass)">
											<xsl:choose>
												<!--K1.1o/r-->
												<xsl:when test="$ang = 360 and $len - @X &lt; $Abfragemass and $Side = 1">
													<xsl:value-of select="script:setESL('5','2')"/>
												</xsl:when>
												<xsl:when test="$ang = 180 and @X &lt; $Abfragemass and $Side = 2">
													<xsl:value-of select="script:setESL('5','2')"/>
												</xsl:when>
												<!--K1.2o/r-->
												<xsl:when test="$ang = 360 and $len - @X &lt; $Abfragemass and $Side = 2">
													<xsl:value-of select="script:setESL('6','2')"/>
												</xsl:when>
												<xsl:when test="$ang = 180 and @X &lt; $Abfragemass and $Side = 1">
													<xsl:value-of select="script:setESL('6','2')"/>
												</xsl:when>
												<!--K1.1u/l-->
												<xsl:when test="$ang = 360 and @X &lt; $Abfragemass and $Side = 1">
													<xsl:value-of select="script:setESL('7','2')"/>
												</xsl:when>
												<xsl:when test="$ang = 180 and $len - @X &lt; $Abfragemass and $Side = 2">
													<xsl:value-of select="script:setESL('7','2')"/>
												</xsl:when>
												<!--K1.2u/l-->
												<xsl:when test="$ang = 360 and @X &lt; $Abfragemass and $Side = 2">
													<xsl:value-of select="script:setESL('8','2')"/>
												</xsl:when>
												<xsl:when test="$ang = 180 and $len - @X &lt; $Abfragemass and $Side = 1">
													<xsl:value-of select="script:setESL('8','2')"/>
												</xsl:when>
											</xsl:choose>
										</xsl:if>
									</xsl:when>
								</xsl:choose>
							</xsl:for-each>
						</xsl:if>

						<xsl:if test ="script:GetKaempferZaehler() = 1">
							<xsl:for-each select ="Operations/Operation[contains(@name,'ESL_')]">
								<!--Side, gibt an auf welcher Seite vom Teiler die Bearbeitung sitzt-->
								<!--bie Teiler = 90° 1 = links / 2 = rechts-->
								<xsl:variable name ="Side">
									<xsl:value-of select ="@side"/>
								</xsl:variable>

								<!--<xsl:value-of select="concat('    Test:  ',$ang,'   ',$len,'    ')"/>-->
								<xsl:choose>
									<xsl:when test ="contains(@name,'ESL_SL')">
										<xsl:if test="@X &lt; $Abfragemass or ($len - @X &lt; $Abfragemass)">
											<xsl:choose>
												<!--K2.1o/r-->
												<xsl:when test="$ang = 90 and $len - @X &lt; $Abfragemass and $Side = 1">
													<xsl:value-of select="script:setESL('9','3')"/>
												</xsl:when>
												<xsl:when test="$ang = 270 and @X &lt; $Abfragemass and $Side = 2">
													<xsl:value-of select="script:setESL('9','3')"/>
												</xsl:when>
												<!--K2.2o/r-->
												<xsl:when test="$ang = 90 and $len - @X &lt; $Abfragemass and $Side = 2">
													<xsl:value-of select="script:setESL('10','3')"/>
												</xsl:when>
												<xsl:when test="$ang = 270 and @X &lt; $Abfragemass and $Side = 1">
													<xsl:value-of select="script:setESL('10','3')"/>
												</xsl:when>
												<!--K2.1u/l-->
												<xsl:when test="$ang = 90 and @X &lt; $Abfragemass and $Side = 1">
													<xsl:value-of select="script:setESL('11','3')"/>
												</xsl:when>
												<xsl:when test="$ang = 270 and $len - @X &lt; $Abfragemass and $Side = 2">
													<xsl:value-of select="script:setESL('11','3')"/>
												</xsl:when>
												<!--K2.2u/l-->
												<xsl:when test="$ang = 90 and @X &lt; $Abfragemass and $Side = 2">
													<xsl:value-of select="script:setESL('12','3')"/>
												</xsl:when>
												<xsl:when test="$ang = 270 and $len - @X &lt; $Abfragemass and $Side = 1">
													<xsl:value-of select="script:setESL('12','3')"/>
												</xsl:when>
											</xsl:choose>
										</xsl:if>
									</xsl:when>
									<xsl:when test ="contains(@name,'ESL_')">
										<xsl:if test="@X &lt; $Abfragemass or ($len - @X &lt; $Abfragemass)">
											<xsl:choose>
												<!--K2.1o/r-->
												<xsl:when test="$ang = 90 and $len - @X &lt; $Abfragemass and $Side = 1">
													<xsl:value-of select="script:setESL('9','1')"/>
												</xsl:when>
												<xsl:when test="$ang = 270 and @X &lt; $Abfragemass and $Side = 2">
													<xsl:value-of select="script:setESL('9','1')"/>
												</xsl:when>
												<!--K2.2o/r-->
												<xsl:when test="$ang = 90 and $len - @X &lt; $Abfragemass and $Side = 2">
													<xsl:value-of select="script:setESL('10','1')"/>
												</xsl:when>
												<xsl:when test="$ang = 270 and @X &lt; $Abfragemass and $Side = 1">
													<xsl:value-of select="script:setESL('10','1')"/>
												</xsl:when>
												<!--K2.1u/l-->
												<xsl:when test="$ang = 90 and @X &lt; $Abfragemass and $Side = 1">
													<xsl:value-of select="script:setESL('11','1')"/>
												</xsl:when>
												<xsl:when test="$ang = 270 and $len - @X &lt; $Abfragemass and $Side = 2">
													<xsl:value-of select="script:setESL('11','1')"/>
												</xsl:when>
												<!--K2.2u/l-->
												<xsl:when test="$ang = 90 and @X &lt; $Abfragemass and $Side = 2">
													<xsl:value-of select="script:setESL('12','1')"/>
												</xsl:when>
												<xsl:when test="$ang = 270 and $len - @X &lt; $Abfragemass and $Side = 1">
													<xsl:value-of select="script:setESL('12','1')"/>
												</xsl:when>
											</xsl:choose>
										</xsl:if>
									</xsl:when>
								</xsl:choose>
							</xsl:for-each>
						</xsl:if>
					</xsl:if>
				</xsl:for-each>
			</xsl:variable>
			<!--<xsl:value-of select ="concat('HorizontaleTeiler: ',$HorizontaleTeiler,'HorizontaleTeiler')"/>-->
			<xsl:value-of select="script:InitKaempferZaehler()"/>
			<!--SD20101118 geschweißte Teiler haben die Kennzeichnung "_GS"-->
			<!--vertikale Teiler-->
			<xsl:variable name="VertikaleTeiler">
				<xsl:for-each select="Piece[@angle = 360 or @angel = 180]/Delimitation[contains(@MullionBaseRef,'KO-2621_GS')]">
					<!--nach verechneter X-Porsition sortieren-->
					<xsl:sort select ="@MullionRealPos" order ="descending" data-type ="number"/>
					<xsl:variable name="zuBearbeitenderKaempfer">
						<!--prüft ob der Kämpfer noch oder schon bearbeitet ist-->
						<xsl:value-of select="script:CheckVerarbeiteteKaempfer(string(@MullionProfilePieceId))"/>
					</xsl:variable>
					<xsl:if test="$zuBearbeitenderKaempfer = 1">
						<xsl:value-of select="script:IncKaempferZaehler()"/>
						<!--Kämpferprofil1V(4) AN 12 Vertikal Profilbezeichnung Kämpfer 1 (von links)-->
						<xsl:call-template name="Profile">
							<xsl:with-param name="refFin" select="@MullionMaterial"/>
							<xsl:with-param name="refBase" select="@MullionBaseRef"/>
						</xsl:call-template>
						<!--Kämpfermass1V(4) N 7 Vertikal Kämpfermass 1 (von links) [xxxx.xx]-->
						<xsl:value-of select="concat(format-number(@MullionRealPos,'0000.00'),';')"/>
						<!--Kämpfertyp1V(4) N 1 Vertikal Kämpfertyp Kämpfer 1(6)-->
						<xsl:value-of select="script:GetKaempfertyp(string(@MullionMaterial), string(ancestor::Piece/@angle))"/>
						<xsl:value-of select="';#'"/>
						<!--SD20110124 setzen der ESL, SL für Teiler -->
						<xsl:variable name="ang">
							<xsl:value-of select="@angle"/>
						</xsl:variable>
						<xsl:variable name="len">
							<xsl:value-of select="@lengthWithWelding"/>
						</xsl:variable>
						
						<xsl:if test ="script:GetKaempferZaehler() = 2">
							<xsl:for-each select ="Operations/Operation[contains(@name,'ESL_')]">
								<!--Side, gibt an auf welcher Seite vom Teiler die Bearbeitung sitzt-->
								<!--bie Teiler = 90° 1 = links / 2 = rechts-->
								<xsl:variable name ="Side">
									<xsl:value-of select ="@side"/>
								</xsl:variable>
								<!--<xsl:value-of select="concat('    Test:  ',$ang,'   ',$len,'    ')"/>-->
								<xsl:choose>
									<xsl:when test ="contains(@name,'ESL_SL')">
										<xsl:if test="@X &lt; $Abfragemass or ($len - @X &lt; $Abfragemass)">
											<xsl:choose>
												<!--K1.1o/r-->
												<xsl:when test="$ang = 90 and $len - @X &lt; $Abfragemass and $Side = 1">
													<xsl:value-of select="script:setESL('5','3')"/>
												</xsl:when>
												<xsl:when test="$ang = 270 and @X &lt; $Abfragemass and $Side = 2">
													<xsl:value-of select="script:setESL('5','3')"/>
												</xsl:when>
												<!--K1.2o/r-->
												<xsl:when test="$ang = 90 and $len - @X &lt; $Abfragemass and $Side = 2">
													<xsl:value-of select="script:setESL('6','3')"/>
												</xsl:when>
												<xsl:when test="$ang = 270 and @X &lt; $Abfragemass and $Side = 1">
													<xsl:value-of select="script:setESL('6','3')"/>
												</xsl:when>
												<!--K1.1u/l-->
												<xsl:when test="$ang = 90 and @X &lt; $Abfragemass and $Side = 1">
													<xsl:value-of select="script:setESL('7','3')"/>
												</xsl:when>
												<xsl:when test="$ang = 270 and $len - @X &lt; $Abfragemass and $Side = 2">
													<xsl:value-of select="script:setESL('7','3')"/>
												</xsl:when>
												<!--K1.2u/l-->
												<xsl:when test="$ang = 90 and @X &lt; $Abfragemass and $Side = 2">
													<xsl:value-of select="script:setESL('8','3')"/>
												</xsl:when>
												<xsl:when test="$ang = 270 and $len - @X &lt; $Abfragemass and $Side = 1">
													<xsl:value-of select="script:setESL('8','3')"/>
												</xsl:when>
											</xsl:choose>
										</xsl:if>
									</xsl:when>
									<xsl:when test ="contains(@name,'ESL_')">
										<xsl:if test="@X &lt; $Abfragemass or ($len - @X &lt; $Abfragemass)">
											<xsl:choose>
												<!--K1.1o/r-->
												<xsl:when test="$ang = 90 and $len - @X &lt; $Abfragemass and $Side = 1">
													<xsl:value-of select="script:setESL('5','1')"/>
												</xsl:when>
												<xsl:when test="$ang = 270 and @X &lt; $Abfragemass and $Side = 2">
													<xsl:value-of select="script:setESL('5','1')"/>
												</xsl:when>
												<!--K1.2o/r-->
												<xsl:when test="$ang = 90 and $len - @X &lt; $Abfragemass and $Side = 2">
													<xsl:value-of select="script:setESL('6','1')"/>
												</xsl:when>
												<xsl:when test="$ang = 270 and @X &lt; $Abfragemass and $Side = 1">
													<xsl:value-of select="script:setESL('6','1')"/>
												</xsl:when>
												<!--K1.1u/l-->
												<xsl:when test="$ang = 90 and @X &lt; $Abfragemass and $Side = 1">
													<xsl:value-of select="script:setESL('7','1')"/>
												</xsl:when>
												<xsl:when test="$ang = 270 and $len - @X &lt; $Abfragemass and $Side = 2">
													<xsl:value-of select="script:setESL('7','1')"/>
												</xsl:when>
												<!--K1.2u/l-->
												<xsl:when test="$ang = 90 and @X &lt; $Abfragemass and $Side = 2">
													<xsl:value-of select="script:setESL('8','1')"/>
												</xsl:when>
												<xsl:when test="$ang = 270 and $len - @X &lt; $Abfragemass and $Side = 1">
													<xsl:value-of select="script:setESL('8','1')"/>
												</xsl:when>
											</xsl:choose>
										</xsl:if>
									</xsl:when>
								</xsl:choose>
							</xsl:for-each>
						</xsl:if>
					
						<xsl:if test ="script:GetKaempferZaehler() = 1">
							<xsl:for-each select ="Operations/Operation[contains(@name,'ESL_')]">
								<!--Side, gibt an auf welcher Seite vom Teiler die Bearbeitung sitzt-->
								<!--bie Teiler = 90° 1 = links / 2 = rechts-->
								<xsl:variable name ="Side">
									<xsl:value-of select ="@side"/>
								</xsl:variable>
								
								<!--<xsl:value-of select="concat('    Test:  ',$ang,'   ',$len,'    ')"/>-->
								<xsl:choose>
									<xsl:when test ="contains(@name,'ESL_SL')">
										<xsl:if test="@X &lt; $Abfragemass or ($len - @X &lt; $Abfragemass)">
											<xsl:choose>
												<!--K2.1o/r-->
												<xsl:when test="$ang = 90 and $len - @X &lt; $Abfragemass and $Side = 1">
													<xsl:value-of select="script:setESL('9','3')"/>
												</xsl:when>
												<xsl:when test="$ang = 270 and @X &lt; $Abfragemass and $Side = 2">
													<xsl:value-of select="script:setESL('9','3')"/>
												</xsl:when>
												<!--K2.2o/r-->
												<xsl:when test="$ang = 90 and $len - @X &lt; $Abfragemass and $Side = 2">
													<xsl:value-of select="script:setESL('10','3')"/>
												</xsl:when>
												<xsl:when test="$ang = 270 and @X &lt; $Abfragemass and $Side = 1">
													<xsl:value-of select="script:setESL('10','3')"/>
												</xsl:when>
												<!--K2.1u/l-->
												<xsl:when test="$ang = 90 and @X &lt; $Abfragemass and $Side = 1">
													<xsl:value-of select="script:setESL('11','3')"/>
												</xsl:when>
												<xsl:when test="$ang = 270 and $len - @X &lt; $Abfragemass and $Side = 2">
													<xsl:value-of select="script:setESL('11','3')"/>
												</xsl:when>
												<!--K2.2u/l-->
												<xsl:when test="$ang = 90 and @X &lt; $Abfragemass and $Side = 2">
													<xsl:value-of select="script:setESL('12','3')"/>
												</xsl:when>
												<xsl:when test="$ang = 270 and $len - @X &lt; $Abfragemass and $Side = 1">
													<xsl:value-of select="script:setESL('12','3')"/>
												</xsl:when>
											</xsl:choose>
										</xsl:if>
									</xsl:when>
									<xsl:when test ="contains(@name,'ESL_')">
										<xsl:if test="@X &lt; $Abfragemass or ($len - @X &lt; $Abfragemass)">
											<xsl:choose>
												<!--K2.1o/r-->
												<xsl:when test="$ang = 90 and $len - @X &lt; $Abfragemass and $Side = 1">
													<xsl:value-of select="script:setESL('9','1')"/>
												</xsl:when>
												<xsl:when test="$ang = 270 and @X &lt; $Abfragemass and $Side = 2">
													<xsl:value-of select="script:setESL('9','1')"/>
												</xsl:when>
												<!--K2.2o/r-->
												<xsl:when test="$ang = 90 and $len - @X &lt; $Abfragemass and $Side = 2">
													<xsl:value-of select="script:setESL('10','1')"/>
												</xsl:when>
												<xsl:when test="$ang = 270 and @X &lt; $Abfragemass and $Side = 1">
													<xsl:value-of select="script:setESL('10','1')"/>
												</xsl:when>
												<!--K2.1u/l-->
												<xsl:when test="$ang = 90 and @X &lt; $Abfragemass and $Side = 1">
													<xsl:value-of select="script:setESL('11','1')"/>
												</xsl:when>
												<xsl:when test="$ang = 270 and $len - @X &lt; $Abfragemass and $Side = 2">
													<xsl:value-of select="script:setESL('11','1')"/>
												</xsl:when>
												<!--K2.2u/l-->
												<xsl:when test="$ang = 90 and @X &lt; $Abfragemass and $Side = 2">
													<xsl:value-of select="script:setESL('12','1')"/>
												</xsl:when>
												<xsl:when test="$ang = 270 and $len - @X &lt; $Abfragemass and $Side = 1">
													<xsl:value-of select="script:setESL('12','1')"/>
												</xsl:when>
											</xsl:choose>
										</xsl:if>
									</xsl:when>
								</xsl:choose>
							</xsl:for-each>
						</xsl:if>
					</xsl:if>
				</xsl:for-each>
			</xsl:variable>
			<!--<xsl:value-of select ="concat('Vertikale Teiler: ',$VertikaleTeiler,'Vertikale Teiler')"/>-->
			<xsl:value-of select="script:getKaempfer(string($HorizontaleTeiler),string($VertikaleTeiler))"/>
			<!--Kämpferprofil2H(4) AN 12 Horizontal Profilbezeichnung Kämpfer 2 (von unten)-->
			<!--Kämpfermass2H(4) N 7 Horizontal Kämpfermass 2 (von unten) [xxxx.xx]-->
			<!--Kämpfertyp2H(4) (1) Horizontal Kämpfertyp Kämpfer 2(6)-->
			<!--Kämpferprofil2V(4) AN 12 Vertikal Profilbezeichnung Kämpfer 2 (von links)-->
			<!--Kämpfermass2V(4) N 7 Vertikal Kämpfermass 2 (von links) [xxxx.xx]-->
			<!--Kämpfertyp2V(4) N 1 Vertikal Kämpfertyp Kämpfer 2(6)-->
			<!-- Wagen- Fachnummer -->
			<xsl:choose>
				<xsl:when test="Piece[2]/@slot &gt;0">
					<xsl:value-of select="concat(format-number(Piece[2]/@container,'000'),';')"/>
					<xsl:value-of select="concat(format-number(Piece[2]/@slot,'000'),';')"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="'000;000;'"/>
				</xsl:otherwise>
			</xsl:choose>
			<!-- Kennung Rahmen=1 / Flügel=0 -->
			<xsl:value-of select="'1;'"/>
			<!-- Entnahmen automatisch=0 / manuell=1 -->
			<xsl:value-of select="'0;'"/>
			<!-- Eck- und Scherenlager Code -->
			<xsl:choose>
				<xsl:when test="$WithESL='1'">
					<xsl:apply-templates select="Piece/Operations/descendant::Operation[contains(@name,'ESL_')]"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="script:InitESL()"/>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:variable name="esl">
				<xsl:value-of select="script:GetESL()"/>
			</xsl:variable>
			<xsl:value-of select="concat(script:RFill(string($esl),'0','12'),';')"/>
			<!--Vorgabedrehung (1) 
			0=Maschine ermittelt Drehung,
			1=90° 
			2=180° 
			3=270° 
			4=0° 
			(gegen den Uhrzeigersinn)-->
			<!--SD20101117 ist OK laut Hr. Wiest E-Mail: "Antw: 1. Testdatensätze URBAN AKS6400"-->
			<xsl:variable name="Drehung">
				<xsl:value-of select="'0'"/>
			</xsl:variable>
			<xsl:value-of select="concat($Drehung,';')"/>
			<!--Stulpkennung (1)
				0-nicht sägen
				1-sägen
			-->
			<xsl:variable name="Stulpkennung">
				<xsl:value-of select="0"/>
			</xsl:variable>
			<xsl:value-of select="concat($Stulpkennung,';')"/>
			<!--SD20110125 Nacharbeit(1) - N hinzugefügt-->
			<!--0 = Standard / 1 = Lampe nach verputzen-->
			<xsl:variable name="Nacharbeitskennung">
				<xsl:choose>
					<xsl:when test="descendant::Piece[Operations/Operation[@name = 'Speziell' or @name = 'Speziell2' or @name = 'Speziell76F']]">
						<xsl:value-of select="'1'"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="'0'"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:value-of select="concat($Nacharbeitskennung,';')"/>
			<xsl:text>&#xA;</xsl:text>
		<!--</xsl:if>-->
	</xsl:template>
	
	<xsl:template match="SASH">
		<!--SD20110221	min Breite: 310 mm Höhe: 250 mm
						max Breite: 3000 mm Höhe: 2450 mm-->
		<!--Angepasst von ThKö-->
		<!--<xsl:if test="	MEASURE/@swidth &gt; 310 and 
						MEASURE/@sheight &gt; 250 and 
						((MEASURE/@swidth &lt;3000 and 
						MEASURE/@sheight &lt; 2450) or 
						(MEASURE/@sheight &lt;3000 and 
						MEASURE/@swidth &lt; 2450))					">-->
			<xsl:variable name="cnt" select="script:IncCnt()"/>
			<!--Teile-ID (5-30) Eindeutige Teileidentifikation-->
			<!--6st Los & 2st Set & 4st squareNumberInSet & 2st Lage -->
			<xsl:variable name="Barcode">
				<xsl:value-of select="concat(format-number($lot,'000000'),format-number($set,'00'),format-number(@squareNumberInSet,'0000'),'00')"/>
			</xsl:variable>
			<xsl:value-of select="concat(script:RFill(string($Barcode),string(0),string(14)),';')"/>
			<!--Teile-ID2 (5-30) Eindeutige Teile-ID bei Doppelschweißen-->
			<!---->
			<xsl:variable name="Barcode2">
				<xsl:value-of select="''"/>
			</xsl:variable>
			<xsl:value-of select="concat(script:RFill(string($Barcode2),' ','14'),';')"/>
			<!--Los (20) Losbezeichnung-->
			<!--6st Los & 2st Set & 00-->
			<xsl:variable name="Losbezeichnung">
				<xsl:value-of select="concat(format-number($lot,'000000'),format-number($set,'00'),'00')"/>
			</xsl:variable>
			<xsl:value-of select="concat(script:RFill(string($Losbezeichnung),string('0'),'20'),';')"/>
			<!-- Lospos (3) Position im Los-->
			<!--3st squareNumberInSet-->
			<xsl:variable name="Lospos">
				<xsl:value-of select="format-number(@squareNumberInSet,'000')"/>
			</xsl:variable>
			<xsl:value-of select="concat(script:LFill(string($Lospos),'0','3'),';')"/>
			<!--Auftrag (20) Auftragsbezeichnung -->
			<!--20st orderNumber-->
			<xsl:variable name="Auftrag">
				<xsl:value-of select="script:LFill(string(parent::FRAME/@orderNumber),string(' '), '20')"/>
			</xsl:variable>
			<xsl:value-of select="concat($Auftrag,';')"/>
			<!--Auftragspos (3) Position im Auftrag-->
			<!--3st sortOrder-->
			<xsl:variable name="Auftragspos">
				<xsl:value-of select="script:LFill(string(parent::FRAME/@sortOrder),' ','3')"/>
			</xsl:variable>
			<xsl:value-of select="concat($Auftragspos,';')"/>
			<!--Breite (7) Flügelbreite(3) [mm] [xxxx.xx]-->
			<xsl:variable name="Swidth">
				<xsl:value-of select="MEASURE/@swidth"/>
			</xsl:variable>
			<xsl:value-of select="concat(format-number($Swidth,'0000.00'),';')"/>
			<!--Höhe   (7) Flügellänge (3) [mm] [xxxx.xx]-->
			<xsl:value-of select="concat(format-number(MEASURE/@sheight,'0000.00'),';')"/>
			<!--Profil oben (12) Profilbezeichnung oberes(3) Profil-->
			<!--6st material & URBAN-Frabkennung & Dichtungskennung-->
			<xsl:call-template name="Profile">
				<xsl:with-param name="refFin" select="Piece[@angle=180]/@material"/>
				<xsl:with-param name="refBase" select="Piece[@angle=180]/@materialbase"/>
			</xsl:call-template>
			<!--Profil unten (12) Profilbezeichnung unteres(3) Profil-->
			<xsl:call-template name="Profile">
				<xsl:with-param name="refFin" select="Piece[@angle =360]/@material"/>
				<xsl:with-param name="refBase" select="Piece[@angle=360]/@materialbase"/>
			</xsl:call-template>
			<!--Profil links (12) Profilbezeichnung linkes(3) Profil-->
			<xsl:call-template name="Profile">
				<xsl:with-param name="refFin" select="Piece[@angle=270]/@material"/>
				<xsl:with-param name="refBase" select="Piece[@angle=270]/@materialbase"/>
			</xsl:call-template>
			<!--Profil rechts (12) Profilbezeichnung rechtes(3) Profil-->
			<xsl:call-template name="Profile">
				<xsl:with-param name="refFin" select="Piece[@angle=90]/@material"/>
				<xsl:with-param name="refBase" select="Piece[@angle=90]/@materialbase"/>
			</xsl:call-template>
			<xsl:value-of select="script:InitVerarbeiteteKaempfer()"/>
			<!--Initialisieren des ESL-Arrays-->
			<xsl:value-of select="script:InitESL()"/>
			<!--SD20101118 geschweißte Teiler haben die Kennzeichnung "_GS"-->
			<!--horizontale Teiler-->
			<xsl:variable name="HorizontaleTeiler">
				<xsl:for-each select="Piece[@angle = 270 or @angel = 90]/Delimitation[contains(@MullionBaseRef,'KO-2621_GS')]">
					<!--nach verechneter X-Porsition sortieren-->
					<xsl:sort select ="@MullionRealPos" order ="descending" data-type ="number"/>
					<xsl:variable name="zuBearbeitenderKaempfer">
						<!--prüft ob der Kämpfer noch oder schon bearbeitet ist-->
						<xsl:value-of select="script:CheckVerarbeiteteKaempfer(string(@MullionProfilePieceId))"/>
					</xsl:variable>
					<xsl:if test="$zuBearbeitenderKaempfer = 1">
						<!--Kämpferprofil1H(4) AN 12 Horizontal Profilbezeichnung Kämpfer 1 (von unten)-->
						<xsl:call-template name="Profile">
							<xsl:with-param name="refFin" select="@MullionMaterial"/>
							<xsl:with-param name="refBase" select="@MullionBaseRef"/>
						</xsl:call-template>
						<!--Kämpfermass1H(4) N 7 Horizontal Kämpfermass 1 (von unten) [xxxx.xx]-->
						<xsl:value-of select="concat(format-number(@MullionRealPos,'0000.00'),';')"/>
						<!--Kämpfertyp1H(4) N 1 Horizontal Kämpfertyp Kämpfer 1(6)-->
						<xsl:value-of select="script:GetKaempfertyp(string(@MullionMaterial), string(ancestor::Piece/@angle))"/>
						<xsl:value-of select="';#'"/>
					</xsl:if>
				</xsl:for-each>
			</xsl:variable>
			<!--<xsl:value-of select ="concat('HorizontaleTeiler: ',$HorizontaleTeiler,'HorizontaleTeiler')"/>-->
			<!--SD20101118 geschweißte Teiler haben die Kennzeichnung "_GS"-->
			<!--vertikale Teiler-->
			<xsl:variable name="VertikaleTeiler">
				<xsl:for-each select="Piece[@angle = 360 or @angel = 180]/Delimitation[contains(@MullionBaseRef,'KO-2621_GS')]">
					<!--nach verechneter X-Porsition sortieren-->
					<xsl:sort select ="@MullionRealPos" order ="descending" data-type ="number"/>
					<xsl:variable name="zuBearbeitenderKaempfer">
						<!--prüft ob der Kämpfer noch oder schon bearbeitet ist-->
						<xsl:value-of select="script:CheckVerarbeiteteKaempfer(string(@MullionProfilePieceId))"/>
					</xsl:variable>
					<xsl:if test="$zuBearbeitenderKaempfer = 1">
						<!--Kämpferprofil1V(4) AN 12 Vertikal Profilbezeichnung Kämpfer 1 (von links)-->
						<xsl:call-template name="Profile">
							<xsl:with-param name="refFin" select="@MullionMaterial"/>
							<xsl:with-param name="refBase" select="@MullionBaseRef"/>
						</xsl:call-template>
						<!--Kämpfermass1V(4) N 7 Vertikal Kämpfermass 1 (von links) [xxxx.xx]-->
						<xsl:value-of select="concat(format-number(@MullionRealPos,'0000.00'),';')"/>
						<!--Kämpfertyp1V(4) N 1 Vertikal Kämpfertyp Kämpfer 1(6)-->
						<xsl:value-of select="script:GetKaempfertyp(string(@MullionMaterial), string(ancestor::Piece/@angle))"/>
						<xsl:value-of select="';#'"/>
					</xsl:if>
				</xsl:for-each>
			</xsl:variable>
			<!--<xsl:value-of select ="concat('Vertikale Teiler: ',$VertikaleTeiler,'Vertikale Teiler')"/>-->
			<xsl:value-of select="script:getKaempfer(string($HorizontaleTeiler),string($VertikaleTeiler))"/>
			<!--Kämpferprofil2H(4) AN 12 Horizontal Profilbezeichnung Kämpfer 2 (von unten)-->
			<!--Kämpfermass2H(4) N 7 Horizontal Kämpfermass 2 (von unten) [xxxx.xx]-->
			<!--Kämpfertyp2H(4) (1) Horizontal Kämpfertyp Kämpfer 2(6)-->
			<!--Kämpferprofil2V(4) AN 12 Vertikal Profilbezeichnung Kämpfer 2 (von links)-->
			<!--Kämpfermass2V(4) N 7 Vertikal Kämpfermass 2 (von links) [xxxx.xx]-->
			<!--Kämpfertyp2V(4) N 1 Vertikal Kämpfertyp Kämpfer 2(6)-->
			<!-- Wagen- Fachnummer -->
			<xsl:choose>
				<xsl:when test="Piece[2]/@slot &gt;0">
					<xsl:value-of select="concat(format-number(Piece[2]/@container,'000'),';')"/>
					<xsl:value-of select="concat(format-number(Piece[2]/@slot,'000'),';')"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="'000;000;'"/>
				</xsl:otherwise>
			</xsl:choose>
			<!-- Kennung Rahmen=1 / Flügel=0 -->
			<xsl:value-of select="'0;'"/>
			<!-- Entnahmen automatisch=0 / manuell=1 -->
			<!--SD20101116 ist OK laut Hr. Wiest-->
			<xsl:value-of select="'0;'"/>
			<!-- Eck- und Scherenlager Code -->
			<xsl:value-of select="script:InitESL()"/>
			<xsl:if test="$WithESL='1'">
				<xsl:apply-templates select="Piece/descendant::Operation[contains(@name,'ESL_')]"/>
			</xsl:if>
			<xsl:variable name="esl">
				<xsl:value-of select="script:GetESL()"/>
			</xsl:variable>
			<xsl:value-of select="concat(script:RFill(string($esl),'0','12'),';')"/>
			<!--Vorgabedrehung (1) 
			0=Maschine ermittelt Drehung,
			1=90° 
			2=180° 
			3=270° 
			4=0° 
			(gegen den Uhrzeigersinn)-->
			<!--SD20101117 ist OK laut Hr. Wiest E-Mail: "Antw: 1. Testdatensätze URBAN AKS6400"-->
			<!--SD20101123 laut Herr Pfahl: "WG: Datensatz  Urban Maschine" 
			Schweißmaschine Linksanschlag 
			Stulp Links : Vorgabedrehung 2
			Stulp Rechts : Vorgabedrehung 4

			Schweißmaschine Rechtsanschlag
			Stulp Links : Vorgabedrehung 4
			Stulp Rechts : Vorgabedrehung 2
			-->
			<xsl:variable name="Drehung">
				<xsl:choose>
					<!--Braunau-->
					<xsl:when test="$Anschlagrichtung = 'links'">
						<xsl:choose>
							<!--Stulp rechts-->
							<xsl:when test="@rebatePosition = '1'">
								<xsl:value-of select="'4'"/>
							</xsl:when>
							<!--Stulp links-->
							<xsl:when test="@rebatePosition = '3'">
								<xsl:value-of select="'2'"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="'0'"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<!--Thaya-->
					<xsl:when test="$Anschlagrichtung = 'rechts'">
						<xsl:choose>
							<!--Stulp rechts-->
							<xsl:when test="@rebatePosition = '1'">
								<xsl:value-of select="'2'"/>
							</xsl:when>
							<!--Stulp links-->
							<xsl:when test="@rebatePosition = '3'">
								<xsl:value-of select="'4'"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="'0'"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
				</xsl:choose>
			</xsl:variable>
			<xsl:value-of select="concat($Drehung,';')"/>
			<!--Stulpkennung (1)
				0-nicht sägen
				1-sägen
				erkennbar an materialesbas enthält '_G'
			-->
			<xsl:variable name="Stulpkennung">
				<xsl:variable name="wirdGesaegt">
					<xsl:for-each select="descendant::Piece">
						<xsl:choose>
							<xsl:when test="contains(@materialbase,'_G')">
								<xsl:value-of select="'1'"/>
							</xsl:when>
              <xsl:when test="contains(@materialbase,'_ST')">
                <xsl:value-of select="'1'"/>
              </xsl:when>
						</xsl:choose>
					</xsl:for-each>
				</xsl:variable>
				<xsl:choose>
					<xsl:when test="format-number($wirdGesaegt,'0') &gt; 0">
						<xsl:value-of select="1"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="0"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:value-of select="concat($Stulpkennung,';')"/>
			<!--SD20110125 Nacharbeit(1) - N hinzugefügt-->
			<!--0 = Standard / 1 = Lampe nach verputzen-->
			<xsl:variable name="Nacharbeitskennung">
				<xsl:choose>
					<xsl:when test="descendant::Piece[Operations/Operation[@name = 'Speziell' or @name = 'Speziell2'or @name = 'Speziell3' or @name = 'Speziell76F']]">
						<xsl:value-of select="'1'"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="'0'"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:value-of select="concat($Nacharbeitskennung,';')"/>
			<xsl:text>&#xA;</xsl:text>
		<!--</xsl:if>-->
		
	</xsl:template>
	
	<xsl:template name="Profile">
		<xsl:param name="refFin"/>
		<xsl:param name="refBase"/>
		<xsl:variable name="ref">
			<!--<xsl:value-of select="script:GetUrbanProfilbezeichnung(string($refFin),string($refBase))"/>-->
			<xsl:value-of select="script:GetUrbanProfilbezeichnung(string($refFin))"/>
		</xsl:variable>
		<xsl:value-of select="concat(script:RFill(string($ref),' ',string(12)),';')"/>
	</xsl:template>
	
	<xsl:template match="Operation">
		<xsl:variable name="ang">
			<xsl:value-of select="ancestor::Piece/@angle"/>
		</xsl:variable>
		<xsl:variable name="len">
			<xsl:choose>
				<xsl:when test="$ang mod 180 = 0">
					<xsl:value-of select="ancestor::FRAME[1]/MEASURE/@swidth"/>
				</xsl:when>
				<xsl:when test="$ang  = 90 or $ang = 270">
					<xsl:value-of select="ancestor::FRAME[1]/MEASURE/@sheight"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="0"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<!--<xsl:value-of select="concat('    Test:  ',$ang,'   ',$len,'    ')"/>-->
		<!--<xsl:if test="$len &gt; 0">-->
		<xsl:choose>
			<xsl:when test="contains(@name,'ESL_SL')">
				<!--<xsl:text>&#xA;</xsl:text>
					<xsl:value-of select ="'ESL_SL'"/>
					<xsl:text>&#xA;</xsl:text>-->
				<xsl:if test="@X &lt; $Abfragemass or ($len - @X &lt; $Abfragemass)">
					<xsl:choose>
						<xsl:when test="$ang = 270 and @X &lt; $Abfragemass">
							<xsl:value-of select="script:setESL('1','3')"/>
						</xsl:when>
						<xsl:when test="$ang = 270 and $len - @X &lt; $Abfragemass">
							<xsl:value-of select="script:setESL('3','3')"/>
						</xsl:when>
						<xsl:when test="$ang mod 360 = 0 and @X &lt; $Abfragemass">
							<xsl:value-of select="script:setESL('3','4')"/>
						</xsl:when>
						<xsl:when test="$ang mod 360 = 0 and $len - @X &lt; $Abfragemass">
							<xsl:value-of select="script:setESL('4','4')"/>
						</xsl:when>
						<xsl:when test="$ang  = 90 and @X &lt; $Abfragemass">
							<xsl:value-of select="script:setESL('4','3')"/>
						</xsl:when>
						<xsl:when test="$ang = 90 and $len - @X &lt; $Abfragemass">
							<xsl:value-of select="script:setESL('2','3')"/>
						</xsl:when>
						<xsl:when test="$ang = 180 and @X &lt; $Abfragemass">
							<xsl:value-of select="script:setESL('2','4')"/>
						</xsl:when>
						<xsl:when test="$ang = 180 and $len - @X &lt; $Abfragemass">
							<xsl:value-of select="script:setESL('1','4')"/>
						</xsl:when>
					</xsl:choose>
				</xsl:if>
			</xsl:when>
			<xsl:when test="contains(@name,'ESL_')">
				<!--<xsl:text>&#xA;</xsl:text>
					<xsl:value-of select ="'ESL_'"/>
					<xsl:text>&#xA;</xsl:text>-->
				<xsl:choose>
					<xsl:when test="$ang = 270 and @X &lt; $Abfragemass">
						<xsl:value-of select="script:setESL('1','1')"/>
					</xsl:when>
					<xsl:when test="$ang = 270 and $len - @X &lt; $Abfragemass">
						<xsl:value-of select="script:setESL('3','1')"/>
					</xsl:when>
					<xsl:when test="$ang mod 360 = 0 and @X &lt; $Abfragemass">
						<xsl:value-of select="script:setESL('3','2')"/>
					</xsl:when>
					<xsl:when test="$ang mod 360 = 0 and $len - @X &lt; $Abfragemass">
						<xsl:value-of select="script:setESL('4','2')"/>
					</xsl:when>
					<xsl:when test="$ang  = 90 and @X &lt; $Abfragemass">
						<xsl:value-of select="script:setESL('4','1')"/>
					</xsl:when>
					<xsl:when test="$ang = 90 and $len - @X &lt; $Abfragemass">
						<xsl:value-of select="script:setESL('2','1')"/>
					</xsl:when>
					<xsl:when test="$ang = 180 and @X &lt; $Abfragemass">
						<xsl:value-of select="script:setESL('2','2')"/>
					</xsl:when>
					<xsl:when test="$ang = 180 and $len - @X &lt; $Abfragemass">
						<xsl:value-of select="script:setESL('1','2')"/>
					</xsl:when>
				</xsl:choose>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>
