<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:dsc="ModelDescriptive" 
xmlns:draw="ModelDraw" xmlns:math="urn:schemas-cagle-com:math" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:script="Myscript">
	<xsl:output method="xml" version="1.0" omit-xml-declaration="yes"  encoding="UTF-8" indent="yes"/>

	<!-- _______________________________________________________ Root ____________________________________________________________ -->
	
	<msxsl:script language="VBScript" implements-prefix="script"><![CDATA[

		Dim currentLocale
		' Get the current locale
		currentLocale = GetLocale

		Function SetLocaleFirst()
		  Dim original
		  original = SetLocale("en-gb")
		  SetLocaleFirst = ""
  		End Function
	
		Function SetLocaleEnd()
			Dim original
			original = SetLocale(currentLocale)
			SetLocaleEnd = ""
		End Function

            Function GetDecDel()
                GetDecDel = Mid(CStr(3 / 2), 2, 1)
            End Function
	
            Function GetPolylineParam(path, point, value)
            Dim v
            Dim p
            Dim sP
            Dim a
            GetPolylineParam = 0
            If Not IsNumeric(point) Then Exit Function Else p = CLng(point)
            v = Split(path, "P")
            If UBound(v) = 5 Then
                sP = v(p)
                a = Split(sP, " ")
                If UCase(value) = "X" Then GetPolylineParam = a(0)
                If UCase(value) = "Y" Then GetPolylineParam = a(1)
            End If
            End Function
            
	 ]]></msxsl:script>
	
	<xsl:template match="/">
		<xsl:comment>UsedDecimal: <xsl:value-of select="script:GetDecDel()"/></xsl:comment>
		<xsl:value-of select="script:SetLocaleFirst()"/>
		<xsl:comment>SettedDecimal: <xsl:value-of select="script:GetDecDel()"/></xsl:comment>

		<xsl:apply-templates select="ProductionLot"/>

		<xsl:value-of select="script:SetLocaleEnd()"/>
	</xsl:template>
	
	<xsl:template match="ProductionLot">
		<ProductionLot>
			<xsl:attribute name="productionLotNumber"><xsl:value-of select="@productionLotNumber"/></xsl:attribute>
			<xsl:apply-templates select="ProductionSet"/>
		</ProductionLot>
	</xsl:template>
	
	<xsl:template match="ProductionSet">
		<ProductionSet>
			<xsl:attribute name="ProductionSetNumber"><xsl:value-of select="@productionSetNumber"/></xsl:attribute>
			<xsl:apply-templates select="descendant::Squares"></xsl:apply-templates>
		</ProductionSet>
	</xsl:template>
	
	<xsl:template match="Squares">
		<Squares>
			<xsl:apply-templates select="descendant::SquareInstance">
				<!--SD20110221 sortierung hinzugefügt-->
				<xsl:sort select ="@squareNumberInSet" order ="ascending" data-type ="number"/>
			</xsl:apply-templates>
		</Squares>
	</xsl:template>
	
	<xsl:template match="SquareInstance">	
		<xsl:variable name="iNumber" select="parent::Square/@salesDocumentNumber"/>
		<xsl:variable name="iVersion" select="parent::Square/@salesDocumentVersion"/>
		<xsl:variable name="iPosition" select="parent::Square/@salesDocumentPosition"/>
		<xsl:variable name="iSortOrder" select="//descendant::SalesDocument[@salesDocumentNumber = $iNumber and 
							@salesDocumentVersion=$iVersion]/SalesDocumentPosition[@position=$iPosition]/@sortOrder"/>
		<xsl:variable name="iNomenclatur" select="//descendant::SalesDocument[@salesDocumentNumber = $iNumber and 
							@salesDocumentVersion=$iVersion]/SalesDocumentPosition[@position=$iPosition]/@nomenclature"/>
		<xsl:variable name="iSubModel" select="parent::Square/@subModel"/>
		<xsl:variable name="iInstance" select="@salesDocumentInstance" />
		<xsl:variable name="iSquareNumberInSet"  select="@squareNumberInSet"/>
		<xsl:variable name="iSquareNumberInWindow"  select="parent::Square/@squareNumberInWindow"/>
		<xsl:variable name="iSquareNumber" select="parent::Square/@id " />
		
		<xsl:for-each select="ancestor::ProductionSet/SubWindows/SubWindow">
			<xsl:if test="@salesDocumentNumber=$iNumber and @salesDocumentVersion=$iVersion and  @salesDocumentPosition=$iPosition  and  @salesDocumentInstance=$iInstance and @subModel = $iSubModel">
				<xsl:apply-templates select="/ProductionLot/ProductionModels/ProductionModel[@salesDocumentNumber = $iNumber and @salesDocumentVersion =$iVersion and @salesDocumentPosition =$iPosition]/Model" >
					<xsl:with-param name="iSQ" select="$iSquareNumber" />
					<xsl:with-param name="iNumber" select="$iNumber"/>
					<xsl:with-param name="iVersion" select="$iVersion"/>
					<xsl:with-param name="iPosition" select="$iPosition"/>		
					<xsl:with-param name="iSortOrder" select="$iSortOrder"/>		
					<xsl:with-param name="iNomenclatur" select="$iNomenclatur"/>		
					<xsl:with-param name="iInstance" select="$iInstance" />
					<xsl:with-param name="iSquareNumberInWindow"  select="$iSquareNumberInWindow"/>
					<xsl:with-param name="iSquareNumberInSet"  select="$iSquareNumberInSet"/>
					<xsl:with-param name="iOrderNumber" select="/descendant::SalesDocuments/SalesDocument[@salesDocumentNumber=$iNumber and @salesDocumentVersion=$iVersion]/@orderNumber"/>		
					<xsl:with-param  name="subModel" ><xsl:value-of select="@subModel"/></xsl:with-param>
					<xsl:with-param  name="vSetPosition" ><xsl:value-of select="position() +1 - @subModel"/></xsl:with-param>
				</xsl:apply-templates>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	
	<xsl:template match="Model">		
		<xsl:param name="iSQ"/>
		<xsl:param name="iNumber"/>
		<xsl:param name="iVersion"/>
		<xsl:param name="iPosition"/>		
		<xsl:param name="iSortOrder"/>		
		<xsl:param name="iNomenclatur"/>		
		<xsl:param name="iInstance"/>
		<xsl:param name="iOrderNumber"/>		
		<xsl:param name="iSquareNumberInWindow"/>
		<xsl:param name="iSquareNumberInSet"/>
		<xsl:param name="subModel"/>
		<xsl:param name="vSetPosition"/>
		
		<xsl:variable name="ProductionType" select="dsc:Model/dsc:ProductionType/@name"/>
		<xsl:variable name="frameRef" select="dsc:Model/dsc:Defs/dsc:Materials/child::dsc:Material[@Role='FRAME']/@ref"/>
		<xsl:variable name="hasFrame">
			<xsl:choose>
				<xsl:when test="descendant::dsc:Hole/dsc:Rod[@material=$frameRef]/dsc:Square/@generate=1"><xsl:value-of select="1"/></xsl:when>
				<xsl:otherwise><xsl:value-of select="0"/></xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		
		<xsl:for-each select="descendant::dsc:Hole/dsc:Rod/dsc:Square[@id = $iSQ]">
			<xsl:variable name="profile" select="parent::dsc:Rod/@material" />
			<xsl:variable name="role" select="/ProductionLot/ProductionModels/ProductionModel/Model/dsc:Model
						/dsc:Defs/dsc:Materials/dsc:Material[@ref = $profile]/@Role"/>
			<!--xsl:if test="$iSQ='SQ1'"-->
			<xsl:if test="($hasFrame=1 and $role='FRAME') or ($hasFrame=0 and $role='SASH')">
				<FRAME> <!--! IF role of square(id=SQ1) a sash THEN fill informations in node "FRAME" -->
					<xsl:attribute name="ProductionType"><xsl:value-of select="$ProductionType"/></xsl:attribute>
					<xsl:attribute name="id"><xsl:value-of select="$iSQ" /></xsl:attribute>
					<xsl:attribute name="setPosition"><xsl:value-of select="$vSetPosition"/></xsl:attribute>
					<xsl:attribute name="squareNumberInWindow"><xsl:value-of select="$iSquareNumberInWindow"/></xsl:attribute>
					<xsl:attribute name="squareNumberInSet"><xsl:value-of select="$iSquareNumberInSet"/></xsl:attribute>
					<xsl:attribute name="salesDocumentNumber"><xsl:value-of select="$iNumber"/></xsl:attribute>
					<xsl:attribute name="salesDocumentVersion"><xsl:value-of select="$iVersion"/></xsl:attribute>
					<xsl:attribute name="salesDocumentPosition"><xsl:value-of select="$iPosition"/></xsl:attribute>
					<xsl:attribute name="sortOrder"><xsl:value-of select="$iSortOrder"/></xsl:attribute>
					<xsl:attribute name="nomenclatrura"><xsl:value-of select="$iNomenclatur"/></xsl:attribute>
					<xsl:attribute name="salesDocumentInstance"><xsl:value-of select="$iInstance"/></xsl:attribute>
					<xsl:attribute name="orderNumber"><xsl:value-of select="$iOrderNumber"/></xsl:attribute>
					<xsl:attribute name="subModel"><xsl:value-of select="$subModel"/></xsl:attribute>
					<xsl:if test="$role='FRAME'">
						<xsl:attribute name="holescount"><xsl:value-of select="count(dsc:PhysicHole)" /></xsl:attribute>
						<xsl:attribute name="delemitation">
							<xsl:choose>
								<xsl:when test="count(dsc:PhysicHole) &gt; 1">
									<xsl:value-of select="ancestor::dsc:Hole[1]/dsc:Hole/descendant::dsc:Delimitation[1]/@orientation"/>
								</xsl:when>
								<xsl:otherwise><xsl:value-of select="''"/></xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
				
						<MEASURE>
							<xsl:attribute name="X"><xsl:value-of select="'0'"/></xsl:attribute>
							<xsl:attribute name="Y"><xsl:value-of select="'0'"/></xsl:attribute>
							<xsl:attribute name="width"><xsl:value-of select="format-number(@width,'0.0')"/></xsl:attribute>
							<xsl:attribute name="height"><xsl:value-of select="format-number(@height,'0.0')"/></xsl:attribute>
							<xsl:attribute name="swidth"><xsl:value-of select="format-number(@width,'0.0')"/></xsl:attribute>
							<xsl:attribute name="sheight"><xsl:value-of select="format-number(@height,'0.0')"/></xsl:attribute>
						</MEASURE>
					
						<xsl:apply-templates select="parent::dsc:Rod/dsc:ProfilePiece">
							<xsl:with-param name="iNumber"><xsl:value-of select="$iNumber"/></xsl:with-param>
							<xsl:with-param name="iVersion"><xsl:value-of select="$iVersion"/></xsl:with-param>
							<xsl:with-param name="iPosition"><xsl:value-of select="$iPosition"/></xsl:with-param>
							<xsl:with-param name="iInstance"><xsl:value-of select="$iInstance"/></xsl:with-param>
						</xsl:apply-templates>
					
						<xsl:variable name="minX">
							<xsl:for-each select="dsc:PhysicHole">
								<xsl:sort select="@x" data-type="number"/>
								<xsl:if test="position()=1">
									<xsl:choose>
										<xsl:when test="format-number(@x,'0.0')='NaN'"><xsl:value-of select="0"/></xsl:when>
										<xsl:otherwise><xsl:value-of select="@x"/></xsl:otherwise>
									</xsl:choose>
								</xsl:if>
							</xsl:for-each>
						</xsl:variable>
						<xsl:variable name="minY">
							<xsl:for-each select="dsc:PhysicHole">
								<xsl:sort select="@y" data-type="number"/>
								<xsl:if test="position()=1">
									<xsl:choose>
										<xsl:when test="format-number(@y,'0.0')='NaN'"><xsl:value-of select="0"/></xsl:when>
										<xsl:otherwise><xsl:value-of select="@y"/></xsl:otherwise>
									</xsl:choose>
								</xsl:if>
							</xsl:for-each>
						</xsl:variable>
						
						<xsl:apply-templates select="dsc:PhysicHole">
							<xsl:with-param name="iNumber"><xsl:value-of select="$iNumber"/></xsl:with-param>
							<xsl:with-param name="iVersion"><xsl:value-of select="$iVersion"/></xsl:with-param>
							<xsl:with-param name="iPosition"><xsl:value-of select="$iPosition"/></xsl:with-param>
							<xsl:with-param name="iInstance"><xsl:value-of select="$iInstance"/></xsl:with-param>
							<xsl:with-param name="subModel"><xsl:value-of select="$subModel"/></xsl:with-param>
							<xsl:with-param name="sqmatu" select="parent::dsc:Rod/dsc:ProfilePiece[1]/@material"/>
							<xsl:with-param name="sqmatr" select="parent::dsc:Rod/dsc:ProfilePiece[2]/@material"/>
							<xsl:with-param name="sqmato" select="parent::dsc:Rod/dsc:ProfilePiece[3]/@material"/>
							<xsl:with-param name="sqmatl" select="parent::dsc:Rod/dsc:ProfilePiece[4]/@material"/>
							<xsl:with-param name="sqwidth" select="format-number(@width,'0.0')"/>
							<xsl:with-param name="sqheight" select="format-number(@height,'0.0')"/>
							<xsl:with-param name="minX" select="$minX"/>
							<xsl:with-param name="minY" select="$minY"/>
						</xsl:apply-templates>
						
						<xsl:apply-templates select="parent::dsc:Rod/parent::dsc:Hole/descendant::dsc:Hole[dsc:Opening]"  mode="sash">
							<xsl:sort select="@id" />
							<xsl:with-param name="oX" select="format-number(@x,'0.0')" />
							<xsl:with-param name="oY" select="format-number(@y,'0.0')" />
							<xsl:with-param name="below" select="parent::dsc:Rod/dsc:ProfilePiece[1]/@material" />
							<xsl:with-param name="right" select="parent::dsc:Rod/dsc:ProfilePiece[2]/@material" />
							<xsl:with-param name="top" select="parent::dsc:Rod/dsc:ProfilePiece[3]/@material" />
							<xsl:with-param name="left" select="parent::dsc:Rod/dsc:ProfilePiece[4]/@material" />
							<xsl:with-param name="iNumber"><xsl:value-of select="$iNumber"/></xsl:with-param>
							<xsl:with-param name="iVersion"><xsl:value-of select="$iVersion"/></xsl:with-param>
							<xsl:with-param name="iPosition"><xsl:value-of select="$iPosition"/></xsl:with-param>
							<xsl:with-param name="iInstance"><xsl:value-of select="$iInstance"/></xsl:with-param>
							<xsl:with-param name="subModel"><xsl:value-of select="$subModel"/></xsl:with-param>
						</xsl:apply-templates>
					</xsl:if>
					<xsl:if test="$role='SASH'">
						<xsl:apply-templates select="parent::dsc:Rod/parent::dsc:Hole/descendant-or-self::dsc:Hole[dsc:Opening]"  mode="sash">
							<xsl:sort select="@id" />
							<xsl:with-param name="oX" select="format-number(@x,'0.0')" />
							<xsl:with-param name="oY" select="format-number(@y,'0.0')" />
							<xsl:with-param name="below" select="parent::dsc:Rod/dsc:ProfilePiece[1]/@material" />
							<xsl:with-param name="right" select="parent::dsc:Rod/dsc:ProfilePiece[2]/@material" />
							<xsl:with-param name="top" select="parent::dsc:Rod/dsc:ProfilePiece[3]/@material" />
							<xsl:with-param name="left" select="parent::dsc:Rod/dsc:ProfilePiece[4]/@material" />
							<xsl:with-param name="iNumber"><xsl:value-of select="$iNumber"/></xsl:with-param>
							<xsl:with-param name="iVersion"><xsl:value-of select="$iVersion"/></xsl:with-param>
							<xsl:with-param name="iPosition"><xsl:value-of select="$iPosition"/></xsl:with-param>
							<xsl:with-param name="iInstance"><xsl:value-of select="$iInstance"/></xsl:with-param>
							<xsl:with-param name="subModel"><xsl:value-of select="$subModel"/></xsl:with-param>
						</xsl:apply-templates>
					</xsl:if>
				</FRAME>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>

	<xsl:template match="dsc:ProfilePiece"	>
		<xsl:param name="iNumber" />
		<xsl:param name="iVersion" />
		<xsl:param name="iPosition" />
		<xsl:param name="iInstance" />
		<Piece>
			<xsl:attribute name ="id">
				<xsl:value-of select ="@id"/>
			</xsl:attribute>
			<xsl:variable name ="material">
				<xsl:choose>
					<xsl:when test="contains(@material,'SPROSSE')">
						<xsl:value-of select="concat('GE',substring(@material,8))"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="@material"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:attribute name="material">
				<xsl:value-of select ="$material"/>
			</xsl:attribute>
			<xsl:variable name ="MaterialBase">
				<xsl:value-of select ="/ProductionLot/ProductionModels/ProductionModel/Model/dsc:Model/dsc:Defs/dsc:Materials/dsc:Material[@ref = $material]/@baseRef"/>
			</xsl:variable>
			<xsl:attribute name="materialbase">
				<xsl:value-of select ="$MaterialBase"/>
			</xsl:attribute>
			<xsl:variable name ="angle" select ="@angle"/>
			<xsl:attribute name="angle"><xsl:value-of select="$angle"/></xsl:attribute>
			<xsl:variable name="gmid"><xsl:value-of select="dsc:GeneratedMaterial/@id"/></xsl:variable>
			<xsl:attribute name="container"><xsl:value-of select="/ProductionLot/ProductionSet/Machines/Machine[@machineId='1' or @machineId='2']
					/descendant::CutInstance[@number=$iNumber and @version=$iVersion and 
					@position=$iPosition and @instance=$iInstance and @idPiece=$gmid]/@container"/>
			</xsl:attribute>
			<xsl:attribute name="slot"><xsl:value-of select="/ProductionLot/ProductionSet/Machines/Machine[@machineId='1' or @machineId='2']
					/descendant::CutInstance[@number=$iNumber and @version=$iVersion and 
					@position=$iPosition and @instance=$iInstance and @idPiece=$gmid]/@slot"/>
			</xsl:attribute>
			<xsl:attribute name="length"><xsl:value-of select="@long"/></xsl:attribute>
			<!--SD20101004 lengthWithWelding hinzugefügt-->
			<xsl:attribute name ="lengthWithWelding">
				<xsl:value-of select ="@lengthWithWelding"/>
			</xsl:attribute>
			<!--SD20101004 lengthWithoutWelding hinzugefügt-->
			<xsl:attribute name ="lengthWithoutWelding">
				<xsl:value-of select ="@lengthWithoutWelding"/>
			</xsl:attribute>
			
			<!--SD20100830 MatWidth-->
			
			<xsl:attribute name ="matWidth">
				<xsl:value-of select ="/ProductionLot/ProductionModels/ProductionModel/Model/dsc:Model/dsc:Defs/dsc:Profiles/dsc:Profile[@ref=$MaterialBase]/@width"/>
			</xsl:attribute>
			<!--SD20100824 Delimitation hinzugefügt -->
			<xsl:for-each select ="ancestor::dsc:Hole/dsc:Rod/dsc:ProfilePiece[@angle = $angle]/dsc:MullionsPositions/child::*">
				<Delimitation>
					<xsl:variable name ="MullionProfilePieceId">
						<xsl:value-of select ="@idPiece"/>
					</xsl:variable>
					<xsl:attribute name ="MullionProfilePieceId">
						<xsl:value-of select ="$MullionProfilePieceId"/>
					</xsl:attribute>
					<xsl:attribute name ="MullionPos">
						<xsl:value-of select ="@pos"/>
					</xsl:attribute>
					<xsl:attribute name ="MullionRealPos">
						<xsl:choose>
							<xsl:when test ="$angle = 360">
								<xsl:value-of select ="@pos"/>
							</xsl:when>
							<xsl:when test ="$angle = 90">
								<xsl:value-of select ="@pos"/>
							</xsl:when>
							<xsl:when test ="$angle = 180">
								<xsl:value-of select ="ancestor::dsc:ProfilePiece/@lengthWithoutWelding - @pos"/>
							</xsl:when>
							<xsl:when test ="$angle = 270">
								<xsl:value-of select ="ancestor::dsc:ProfilePiece/@lengthWithoutWelding - @pos"/>
							</xsl:when>
						</xsl:choose>
					</xsl:attribute>
					<xsl:attribute name ="MullionX">
						<xsl:value-of select ="@x"/>
					</xsl:attribute>
					<xsl:attribute name ="MullionY">
						<xsl:value-of select ="@y"/>
					</xsl:attribute>
					<xsl:variable name ="MullionMaterial">
						<xsl:value-of select ="ancestor::dsc:Hole/descendant::dsc:Hole/dsc:Delimitation/dsc:Rod/dsc:ProfilePiece[@id = $MullionProfilePieceId]/@material"/>
					</xsl:variable>
					<xsl:attribute name ="MullionMaterial">
						<xsl:value-of select ="$MullionMaterial"/>
					</xsl:attribute>
					<xsl:variable name ="baseRef">
						<xsl:value-of select ="/ProductionLot/ProductionModels/ProductionModel/Model/dsc:Model/dsc:Defs/dsc:Materials/dsc:Material[@ref = $MullionMaterial ]/@baseRef"/>
					</xsl:variable>
					<xsl:attribute name ="MullionBaseRef">
						<xsl:value-of select ="$baseRef"/>
					</xsl:attribute>
					<xsl:attribute name ="MullionWidth">
						<xsl:value-of select ="/ProductionLot/ProductionModels/ProductionModel/Model/dsc:Model/dsc:Defs/dsc:Profiles/dsc:Profile[@ref=$baseRef]/@width"/>
					</xsl:attribute>
				</Delimitation>
			</xsl:for-each>
			
			<Operations>
				<xsl:call-template name="Operations"><xsl:with-param name="gmid" select="$gmid"/></xsl:call-template>
			</Operations>
		</Piece>
	</xsl:template>
	
	<xsl:template match="dsc:PhysicHole">
		<xsl:param name="iNumber"/>
		<xsl:param name="iVersion"/>
		<xsl:param name="iPosition"/>
		<xsl:param name="iInstance"/>
		<xsl:param name="subModel"/>
		<xsl:param name="sqmatu"/>
		<xsl:param name="sqmatr"/>
		<xsl:param name="sqmato"/>
		<xsl:param name="sqmatl"/>
		<xsl:param name="sqwidth"/>
		<xsl:param name="sqheight"/>
		<xsl:param name="minX"/>
		<xsl:param name="minY"/>

		<PhysicalHoles>
			<xsl:variable name="bottom">
				<xsl:choose>
					<xsl:when test="format-number(@y,'0.0')='NaN'"><xsl:value-of select="format-number(0,'0.0')"/></xsl:when>
					<xsl:otherwise><xsl:value-of select="format-number(@y - $minY,'0.0')"/></xsl:otherwise>
				</xsl:choose>
			</xsl:variable> 
			<xsl:variable name="top" select="format-number($bottom + @height,'0.0')"/>
			<xsl:variable name="left">
				<xsl:choose>
					<xsl:when test="format-number(@x,'0.0')='NaN'"><xsl:value-of select="format-number(0,'0.0')"/></xsl:when>
					<xsl:otherwise><xsl:value-of select="format-number(@x - $minX,'0.0')"/></xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:variable name="right" select="format-number($left + @width,'0.0')"/>
			<xsl:attribute name="minX"><xsl:value-of select="$minX"/></xsl:attribute>
			<xsl:attribute name="minY"><xsl:value-of select="$minY"/></xsl:attribute>
			<xsl:attribute name="id"><xsl:value-of select="@id"/></xsl:attribute>
			<xsl:attribute name="glass"><xsl:value-of select="@glass"/></xsl:attribute>
			<xsl:attribute name="width"><xsl:value-of select="format-number(@width,'0.0')"/></xsl:attribute>
			<xsl:attribute name="height"><xsl:value-of select="format-number(@height,'0.0')"/></xsl:attribute>
			<xsl:attribute name="bottom"><xsl:value-of select="$bottom"/></xsl:attribute>
			<xsl:attribute name="top"><xsl:value-of select="$top"/></xsl:attribute>
			<xsl:attribute name="left"><xsl:value-of select="$left"/></xsl:attribute>
			<xsl:attribute name="right"><xsl:value-of select="$right"/></xsl:attribute>
			<Perfil>
				<xsl:attribute name="pos"><xsl:value-of select="'0'"/></xsl:attribute>
				<xsl:variable name="matB">
					<xsl:choose>
						<xsl:when test="$bottom = 0"><xsl:value-of select="$sqmatu"/></xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="ancestor::dsc:Hole[1]/descendant::dsc:Delimitation
									[@orientation='horizontal' and format-number(@ordinate,'0.0') = $bottom]/dsc:Rod/@material"/>	
						</xsl:otherwise>
					</xsl:choose>	
				</xsl:variable>
				<xsl:variable name="material">
					<xsl:choose>
						<xsl:when test="contains($matB,'SPROSSE')">
							<xsl:value-of select="concat('GE',substring($matB,8))"/>
						</xsl:when>
						<xsl:otherwise><xsl:value-of select="$matB"/></xsl:otherwise>
					</xsl:choose>				
				</xsl:variable>
				<xsl:attribute name="material"><xsl:value-of select="$material"/></xsl:attribute>
				<xsl:call-template name="MatProperties">
					<xsl:with-param name="mat"  select="$material"/>
				</xsl:call-template>
				
				<xsl:attribute name ="PPID">
					<xsl:value-of select ="@id"/>
				</xsl:attribute>
				
			</Perfil>
			<Perfil>
				<xsl:attribute name="pos"><xsl:value-of select="'1'"/></xsl:attribute>
				<xsl:variable name="matR">
					<xsl:choose>
						<xsl:when test="$right = $sqwidth"><xsl:value-of select="$sqmatr"/></xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="ancestor::dsc:Hole[1]/descendant::dsc:Delimitation
									[@orientation='vertical' and format-number(@ordinate,'0.0') = $right]/dsc:Rod/@material"/>	
						</xsl:otherwise>
					</xsl:choose>	
				</xsl:variable>
				<xsl:variable name="material">
					<xsl:choose>
						<xsl:when test="contains($matR,'SPROSSE')">
							<xsl:value-of select="concat('GE',substring($matR,8))"/>
						</xsl:when>
						<xsl:otherwise><xsl:value-of select="$matR"/></xsl:otherwise>
					</xsl:choose>				
				</xsl:variable>
				<xsl:attribute name="material"><xsl:value-of select="$material"/></xsl:attribute>
				<xsl:call-template name="MatProperties">
					<xsl:with-param name="mat"  select="$material"/>
				</xsl:call-template>

				<xsl:attribute name ="PPID">
					<xsl:value-of select ="@id"/>
				</xsl:attribute>
			</Perfil>
			<Perfil>
				<xsl:attribute name="pos">
					<xsl:value-of select="'2'"/>
				</xsl:attribute>
				<xsl:variable name="matT">
					<xsl:choose>
						<xsl:when test="$top = $sqheight">
							<xsl:value-of select="$sqmato"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="ancestor::dsc:Hole[1]/descendant::dsc:Delimitation
									[@orientation='horizontal' and format-number(@ordinate,'0.0') = $top]/dsc:Rod/@material"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="material">
					<xsl:choose>
						<xsl:when test="contains($matT,'SPROSSE')">
							<xsl:value-of select="concat('GE',substring($matT,8))"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$matT"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:attribute name="material">
					<xsl:value-of select="$material"/>
				</xsl:attribute>
				<xsl:call-template name="MatProperties">
					<xsl:with-param name="mat"  select="$material"/>
				</xsl:call-template>


				<xsl:attribute name ="PPID">
					<xsl:value-of select ="@id"/>
				</xsl:attribute>
			</Perfil>
			<Perfil>
				<xsl:attribute name="pos"><xsl:value-of select="'3'"/></xsl:attribute>
				<xsl:variable name="matL">
					<xsl:choose>
						<xsl:when test="$left = 0"><xsl:value-of select="$sqmatl"/></xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="ancestor::dsc:Hole[1]/descendant::dsc:Delimitation
									[@orientation='vertical' and format-number(@ordinate,'0.0') = $left]/dsc:Rod/@material"/>	
						</xsl:otherwise>
					</xsl:choose>	
				</xsl:variable>
				<xsl:variable name="material">
					<xsl:choose>
						<xsl:when test="contains($matL,'SPROSSE')">
							<xsl:value-of select="concat('GE',substring($matL,8))"/>
						</xsl:when>
						<xsl:otherwise><xsl:value-of select="$matL"/></xsl:otherwise>
					</xsl:choose>				
				</xsl:variable>
				<xsl:attribute name="material"><xsl:value-of select="$material"/></xsl:attribute>
				<xsl:call-template name="MatProperties">
					<xsl:with-param name="mat"  select="$material"/>
				</xsl:call-template>

				<xsl:attribute name ="PPID">
					<xsl:value-of select ="@id"/>
				</xsl:attribute>
			</Perfil>
			<xsl:if test="@glass">
				<xsl:variable name="id" select="@glass"/>
				<xsl:variable name="glass" select="/ProductionLot/descendant::Glass[@number = $iNumber and @version =$iVersion and 
												    @position =$iPosition and @subModel=$subModel and @id=$id]"/>
				<xsl:variable name="elementName" select="ancestor::dsc:Hole[1]/descendant::dsc:Glass[@id=$id]/@elementName"/>
				<Glass>
					<xsl:attribute name="id"><xsl:value-of select="$glass/@id"/></xsl:attribute>
					<xsl:attribute name="elementName"><xsl:value-of select="$elementName"/></xsl:attribute>
					<xsl:attribute name="type"><xsl:value-of select="$glass/@type"/></xsl:attribute>
					<xsl:attribute name="reference"><xsl:value-of select="$glass/@reference"/></xsl:attribute>
					<xsl:attribute name="color"><xsl:value-of select="$glass/@color"/></xsl:attribute>
					<xsl:attribute name="thickness"><xsl:value-of select="$glass/@thickness"/></xsl:attribute>
					<xsl:attribute name="width"><xsl:value-of select="$glass/@width"/></xsl:attribute>
					<xsl:attribute name="height"><xsl:value-of select="$glass/@height"/></xsl:attribute>
				</Glass>
			</xsl:if>
		</PhysicalHoles>
	</xsl:template>
	
	<xsl:template match="dsc:Hole" mode="sash">
		<xsl:param name="iNumber"/>
		<xsl:param name="iVersion"/>
		<xsl:param name="iPosition"/>
		<xsl:param name="iInstance"/>
		<xsl:param name="subModel"/>
		<xsl:param name="oX"/>
		<xsl:param name="oY"/>
		<xsl:param name="below"/>
		<xsl:param name="right"/>
		<xsl:param name="top"/>
		<xsl:param name="left"/>
		<xsl:variable name="holeID" select="@id"/>
		
		<xsl:if test="count(dsc:Rod/dsc:Square) &gt; 0">
			<SASH>
				<xsl:attribute name="id"><xsl:value-of select="dsc:Rod/dsc:Square/@id"/></xsl:attribute>
				<xsl:attribute name="squareNumberInSet"><xsl:value-of select="/ProductionLot/ProductionSet/Squares/
						Square[@salesDocumentNumber=$iNumber and @salesDocumentVersion=$iVersion and 
						@salesDocumentPosition =$iPosition and @id=current()/dsc:Rod/dsc:Square/@id]/SquareInstance[@salesDocumentInstance=$iInstance]/@squareNumberInSet"/>
				</xsl:attribute> 
				<xsl:attribute name="fittingCode"><xsl:value-of select="dsc:Rod/dsc:Square/@fittingCode"/></xsl:attribute>
				<xsl:attribute name="prefOpenCode">
					<xsl:value-of select="ancestor::dsc:Model/child::dsc:Param/descendant::PrefOpenCode[@holeId=$holeID]/@prefOpenCode"/>
				</xsl:attribute>
				<xsl:attribute name="handleHeight"><xsl:value-of select="dsc:Opening/dsc:Handle/@height"/></xsl:attribute>
				<xsl:attribute name="sashRebateWidth" ><xsl:value-of select="@sashRebateWidth" /></xsl:attribute>
				<xsl:attribute name="sashRebateHeight" ><xsl:value-of select="@sashRebateHeight" /></xsl:attribute>
				<xsl:attribute name="holescount"><xsl:value-of select="count(dsc:Rod/dsc:Square/dsc:PhysicHole)" /></xsl:attribute>
				<xsl:attribute name="delemitation">
					<xsl:choose>
						<xsl:when test="count(dsc:Rod/dsc:Square/dsc:PhysicHole) &gt; 1">
							<xsl:value-of select="dsc:Delimitation/@orientation"/>
						</xsl:when>
						<xsl:otherwise><xsl:value-of select="''"/></xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>
				<xsl:attribute name="rebatePosition"><xsl:value-of select="dsc:Rod/dsc:Square/@rebatePosition"/></xsl:attribute>
				<MEASURE>
					<xsl:attribute name="X"><xsl:value-of select="format-number(script:GetPolylineParam(string
								(dsc:MatrixPolyline/POLYLINE2D/@path), string(1), 'X') - $oX,'0.0')"/>
					</xsl:attribute>
					<xsl:attribute name="Y"><xsl:value-of select="format-number(script:GetPolylineParam(string
								(dsc:MatrixPolyline/POLYLINE2D/@path), string(1), 'Y')  - $oY,'0.0')"/>
					</xsl:attribute>
					<xsl:attribute name="width"><xsl:value-of select="format-number(script:GetPolylineParam(string
								(dsc:MatrixPolyline/POLYLINE2D/@path), string(2), 'X') - script:GetPolylineParam(string
								(dsc:MatrixPolyline/POLYLINE2D/@path), string(1), 'X'),'0.0')"/>
					</xsl:attribute>
					<xsl:attribute name="height"><xsl:value-of select="format-number(script:GetPolylineParam(string
								(dsc:MatrixPolyline/POLYLINE2D/@path), string(3), 'Y') - script:GetPolylineParam(string
								(dsc:MatrixPolyline/POLYLINE2D/@path), string(2), 'Y'),'0.0')"/>
					</xsl:attribute>			
					<xsl:attribute name="swidth"><xsl:value-of select="format-number(dsc:Rod/dsc:Square/@width,'0.0')"/></xsl:attribute>
					<xsl:attribute name="sheight"><xsl:value-of select="format-number(dsc:Rod/dsc:Square/@height,'0.0')"/></xsl:attribute>
				</MEASURE>
			
				<xsl:call-template name="Piece" >
					<xsl:with-param name="orientagen" select="'horizontal'"/>
					<xsl:with-param name="ordinate" select="format-number(script:GetPolylineParam(string
								(dsc:MatrixPolyline/POLYLINE2D/@path), string(1), 'Y') - $oY,'0.0')" />
					<xsl:with-param name="frame" select="$below" />
					<xsl:with-param name="sash" select="dsc:Rod/dsc:ProfilePiece[1]/@material" />
					<xsl:with-param name="angle" select="dsc:Rod/dsc:ProfilePiece[1]/@angle" />
					<xsl:with-param name="gmid" select="dsc:Rod/dsc:ProfilePiece[1]/dsc:GeneratedMaterial/@id" />
					<xsl:with-param name="iNumber"><xsl:value-of select="$iNumber"/></xsl:with-param>
					<xsl:with-param name="iVersion"><xsl:value-of select="$iVersion"/></xsl:with-param>
					<xsl:with-param name="iPosition"><xsl:value-of select="$iPosition"/></xsl:with-param>
					<xsl:with-param name="iInstance"><xsl:value-of select="$iInstance"/></xsl:with-param>
				</xsl:call-template>
				<xsl:call-template name="Piece" >
					<xsl:with-param name="orientagen" select="'vertical'"/>
					<xsl:with-param name="ordinate" select="format-number(script:GetPolylineParam(string
								(dsc:MatrixPolyline/POLYLINE2D/@path), string(2), 'X') - $oX,'0.0')" />
					<xsl:with-param name="frame" select="$right" />
					<xsl:with-param name="sash" select="dsc:Rod/dsc:ProfilePiece[2]/@material" />
					<xsl:with-param name="angle" select="dsc:Rod/dsc:ProfilePiece[2]/@angle" />					
					<xsl:with-param name="gmid" select="dsc:Rod/dsc:ProfilePiece[2]/dsc:GeneratedMaterial/@id" />
					<xsl:with-param name="iNumber"><xsl:value-of select="$iNumber"/></xsl:with-param>
					<xsl:with-param name="iVersion"><xsl:value-of select="$iVersion"/></xsl:with-param>
					<xsl:with-param name="iPosition"><xsl:value-of select="$iPosition"/></xsl:with-param>
					<xsl:with-param name="iInstance"><xsl:value-of select="$iInstance"/></xsl:with-param>
				</xsl:call-template>
				<xsl:call-template name="Piece" >
					<xsl:with-param name="orientagen" select="'horizontal'"/>
					<xsl:with-param name="ordinate" select="format-number(script:GetPolylineParam(string
								(dsc:MatrixPolyline/POLYLINE2D/@path), string(3), 'Y') - $oY,'0.0')" />
					<xsl:with-param name="frame" select="$top" />
					<xsl:with-param name="sash" select="dsc:Rod/dsc:ProfilePiece[3]/@material" />
					<xsl:with-param name="angle" select="dsc:Rod/dsc:ProfilePiece[3]/@angle" />					
					<xsl:with-param name="gmid" select="dsc:Rod/dsc:ProfilePiece[3]/dsc:GeneratedMaterial/@id" />
					<xsl:with-param name="iNumber"><xsl:value-of select="$iNumber"/></xsl:with-param>
					<xsl:with-param name="iVersion"><xsl:value-of select="$iVersion"/></xsl:with-param>
					<xsl:with-param name="iPosition"><xsl:value-of select="$iPosition"/></xsl:with-param>
					<xsl:with-param name="iInstance"><xsl:value-of select="$iInstance"/></xsl:with-param>
				</xsl:call-template>
				<xsl:call-template name="Piece" >
					<xsl:with-param name="orientagen" select="'vertical'"/>
					<xsl:with-param name="ordinate" select="format-number(script:GetPolylineParam(string
								(dsc:MatrixPolyline/POLYLINE2D/@path), string(1), 'X') - $oX,'0.0')" />
					<xsl:with-param name="frame" select="$left" />
					<xsl:with-param name="sash" select="dsc:Rod/dsc:ProfilePiece[4]/@material" />
					<xsl:with-param name="angle" select="dsc:Rod/dsc:ProfilePiece[4]/@angle" />					
					<xsl:with-param name="gmid" select="dsc:Rod/dsc:ProfilePiece[4]/dsc:GeneratedMaterial/@id" />
					<xsl:with-param name="iNumber"><xsl:value-of select="$iNumber"/></xsl:with-param>
					<xsl:with-param name="iVersion"><xsl:value-of select="$iVersion"/></xsl:with-param>
					<xsl:with-param name="iPosition"><xsl:value-of select="$iPosition"/></xsl:with-param>
					<xsl:with-param name="iInstance"><xsl:value-of select="$iInstance"/></xsl:with-param>
				</xsl:call-template>
				<xsl:variable name="minX">
					<xsl:for-each select="dsc:Rod/dsc:Square/dsc:PhysicHole">
						<xsl:sort select="@x" data-type="number"/>
						<xsl:if test="position()=1">
							<xsl:choose>
								<xsl:when test="format-number(@x,'0.0')='NaN'"><xsl:value-of select="0"/></xsl:when>
								<xsl:otherwise><xsl:value-of select="@x"/></xsl:otherwise>
							</xsl:choose>
						</xsl:if>
					</xsl:for-each>
				</xsl:variable>
				<xsl:variable name="minY">
					<xsl:for-each select="dsc:Rod/dsc:Square/dsc:PhysicHole">
						<xsl:sort select="@y" data-type="number"/>
						<xsl:if test="position()=1">
							<xsl:choose>
								<xsl:when test="format-number(@y,'0.0')='NaN'"><xsl:value-of select="0"/></xsl:when>
								<xsl:otherwise><xsl:value-of select="@y"/></xsl:otherwise>
							</xsl:choose>
						</xsl:if>
					</xsl:for-each>
				</xsl:variable>
				
				<xsl:apply-templates select="dsc:Rod/dsc:Square/dsc:PhysicHole">
					<xsl:with-param name="iNumber"><xsl:value-of select="$iNumber"/></xsl:with-param>
					<xsl:with-param name="iVersion"><xsl:value-of select="$iVersion"/></xsl:with-param>
					<xsl:with-param name="iPosition"><xsl:value-of select="$iPosition"/></xsl:with-param>
					<xsl:with-param name="iInstance"><xsl:value-of select="$iInstance"/></xsl:with-param>
					<xsl:with-param name="subModel"><xsl:value-of select="$subModel"/></xsl:with-param>
					<xsl:with-param name="sqmatu" select="dsc:Rod/dsc:ProfilePiece[1]/@material"/>
					<xsl:with-param name="sqmatr" select="dsc:Rod/dsc:ProfilePiece[2]/@material"/>
					<xsl:with-param name="sqmato" select="dsc:Rod/dsc:ProfilePiece[3]/@material"/>
					<xsl:with-param name="sqmatl" select="dsc:Rod/dsc:ProfilePiece[4]/@material"/>
					<xsl:with-param name="sqwidth" select="format-number(dsc:Rod/dsc:Square/@width,'0.0')"/>
					<xsl:with-param name="sqheight" select="format-number(dsc:Rod/dsc:Square/@height,'0.0')"/>
					<xsl:with-param name="minX" select="$minX"/>
					<xsl:with-param name="minY" select="$minY"/>
				</xsl:apply-templates>
			</SASH>
		</xsl:if>
	</xsl:template>
	
	<xsl:template name="Piece">
		<xsl:param name="orientagen"/>
		<xsl:param name="ordinate" />
		<xsl:param name="frame" />
		<xsl:param name="sash" />
		<xsl:param name="angle" />
		<xsl:param name="gmid"/>
		<xsl:param name="iNumber" />
		<xsl:param name="iVersion" />
		<xsl:param name="iPosition" />
		<xsl:param name="iInstance" />

		<Piece>
			<xsl:variable name="adj">
				<xsl:choose>
					<xsl:when test="ancestor::dsc:Hole/dsc:Delimitation[@orientation = $orientagen and format-number(@ordinate,'0.0') = $ordinate]">
						<xsl:value-of select="ancestor::dsc:Hole/dsc:Delimitation[@orientation = $orientagen and format-number(@ordinate,'0.0')  = $ordinate]/dsc:Rod/@material" />					
					</xsl:when>
					<xsl:otherwise><xsl:value-of select="$frame" /></xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:attribute name="adjacent" >
				<xsl:choose>
					<xsl:when test="contains($adj,'SPROSSE')">
						<xsl:value-of select="concat('GE',substring($adj,8))"/>
					</xsl:when>
					<xsl:otherwise><xsl:value-of select="$adj"/></xsl:otherwise>
				</xsl:choose>				
			</xsl:attribute>
			<xsl:variable name ="material">
				<xsl:choose>
					<xsl:when test="contains($sash,'SPROSSE')">
						<xsl:value-of select="concat('GE',substring($sash,8))"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$sash"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:attribute name="material" >
				<xsl:value-of select ="$material"/>
			</xsl:attribute>

			<xsl:variable name ="MaterialBase">
				<xsl:value-of select ="/ProductionLot/ProductionModels/ProductionModel/Model/dsc:Model/dsc:Defs/dsc:Materials/dsc:Material[@ref = $material]/@baseRef"/>
			</xsl:variable>

			<xsl:attribute name="materialbase">
				<xsl:value-of select ="$MaterialBase"/>
			</xsl:attribute>
			
			<xsl:attribute name="angle" ><xsl:value-of select="$angle" /></xsl:attribute>
			<xsl:attribute name="container"><xsl:value-of select="/ProductionLot/ProductionSet/Machines/Machine[@machineId='1' or @machineId='2']
						/descendant::CutInstance[@number=$iNumber and @version=$iVersion and @position=$iPosition 
						and @instance=$iInstance and @idPiece=$gmid]/@container"/>
			</xsl:attribute>
			<xsl:attribute name="slot"><xsl:value-of select="/ProductionLot/ProductionSet/Machines/Machine[@machineId='1' or @machineId='2']
						/descendant::CutInstance[@number=$iNumber and @version=$iVersion and @position=$iPosition 
						and @instance=$iInstance and @idPiece=$gmid]/@slot"/>
			</xsl:attribute>
			<!--SD20101004 weldedLength hinzugefügt -->
			<xsl:variable name ="weldAddedA">
				<xsl:value-of select ="/ProductionLot/ProductionSet/Machines/Machine[@machineId='1']
						/descendant::CutInstance[@number=$iNumber and @version=$iVersion and @position=$iPosition 
						and @instance=$iInstance and @idPiece=$gmid]/@weldAddedA"/>
			</xsl:variable>
			<xsl:variable name ="weldAddedB">
				<xsl:value-of select ="/ProductionLot/ProductionSet/Machines/Machine[@machineId='1']
						/descendant::CutInstance[@number=$iNumber and @version=$iVersion and @position=$iPosition 
						and @instance=$iInstance and @idPiece=$gmid]/@weldAddedB"/>
			</xsl:variable>
			<xsl:variable name ="lengthWithoutWelding">
				<xsl:value-of select="/ProductionLot/ProductionSet/Machines/Machine[@machineId='1']/descendant::CutInstance[@number=$iNumber and @version=$iVersion and @position=$iPosition and @instance=$iInstance and @idPiece=$gmid]/ancestor::CutPiece/@length"/>
			</xsl:variable>
			
			<xsl:attribute name="weldedLength">
				<xsl:value-of select ="$lengthWithoutWelding - $weldAddedA - $weldAddedB "/>
			</xsl:attribute>
			<!--SD20101004 lengthWithoutWelding hinzugefügt -->
			<xsl:attribute name="lengthWithoutWelding">
				<xsl:value-of select ="$lengthWithoutWelding"/>
			</xsl:attribute>

			<!--SD20100830 MatWidth-->
			<xsl:attribute name ="matWidth">
				<xsl:value-of select ="/ProductionLot/ProductionModels/ProductionModel/Model/dsc:Model/dsc:Defs/dsc:Profiles/dsc:Profile[@ref=$MaterialBase]/@width"/>
			</xsl:attribute>
			
			<xsl:copy-of select="dsc:Rod/dsc:ProfilePiece[child::dsc:GeneratedMaterial/@id=$gmid]/dsc:GeneratedPieces"/>
			<xsl:apply-templates select="dsc:Rod/dsc:ProfilePiece[child::dsc:GeneratedMaterial/@id=$gmid]/dsc:GeneratedPieces/child::dsc:GeneratedPiece"/>
			
			<!--SD20100824 Delimitation hinzugefügt -->
			<xsl:for-each select ="dsc:Rod/dsc:ProfilePiece[@angle = $angle]/dsc:MullionsPositions/child::*">
				<Delimitation>
					<xsl:variable name ="MullionProfilePieceId">
						<xsl:value-of select ="@idPiece"/>
					</xsl:variable>
					<xsl:attribute name ="MullionProfilePieceId">
						<xsl:value-of select ="$MullionProfilePieceId"/>
					</xsl:attribute>
					<xsl:attribute name ="MullionPos">
						<xsl:value-of select ="@pos"/>
					</xsl:attribute>
					<xsl:attribute name ="MullionRealPos">
						<xsl:choose>
							<xsl:when test ="$angle = 360">
								<xsl:value-of select ="@pos"/>
							</xsl:when>
							<xsl:when test ="$angle = 90">
								<xsl:value-of select ="@pos"/>
							</xsl:when>
							<xsl:when test ="$angle = 180">
								<xsl:value-of select ="ancestor::dsc:ProfilePiece/@lengthWithoutWelding - @pos"/>
							</xsl:when>
							<xsl:when test ="$angle = 270">
								<xsl:value-of select ="ancestor::dsc:ProfilePiece/@lengthWithoutWelding - @pos"/>
							</xsl:when>
						</xsl:choose>
					</xsl:attribute>
					<xsl:attribute name ="MullionX">
						<xsl:value-of select ="@x"/>
					</xsl:attribute>
					<xsl:attribute name ="MullionY">
						<xsl:value-of select ="@y"/>
					</xsl:attribute>
					<xsl:variable name ="MullionMaterial">
						<xsl:value-of select ="ancestor::dsc:Hole/descendant::dsc:Hole/dsc:Delimitation/dsc:Rod/dsc:ProfilePiece[@id = $MullionProfilePieceId]/@material"/>
					</xsl:variable>
					<xsl:attribute name ="MullionMaterial">
						<xsl:value-of select ="$MullionMaterial"/>
					</xsl:attribute>
					<xsl:variable name ="baseRef">
						<xsl:value-of select ="/ProductionLot/ProductionModels/ProductionModel/Model/dsc:Model/dsc:Defs/dsc:Materials/dsc:Material[@ref = $MullionMaterial ]/@baseRef"/>
					</xsl:variable>
					<xsl:attribute name ="MullionBaseRef">
						<xsl:value-of select ="$baseRef"/>
					</xsl:attribute>
					<xsl:attribute name ="MullionWidth">
						<xsl:value-of select ="/ProductionLot/ProductionModels/ProductionModel/Model/dsc:Model/dsc:Defs/dsc:Profiles/dsc:Profile[@ref=$baseRef]/@width"/>
					</xsl:attribute>
				</Delimitation>
			</xsl:for-each>
						   
			<Operations>
				<xsl:call-template name="Operations"><xsl:with-param name="gmid" select="$gmid"/></xsl:call-template>
			</Operations>
		</Piece>
	</xsl:template>
	
	<xsl:template match="dsc:GeneratedPiece">
		<xsl:variable name="mat" select="@material"/>
		<FittingMaterial>
			<xsl:attribute name="material"><xsl:value-of select="$mat"/></xsl:attribute>
			<xsl:attribute name="description"><xsl:value-of select="ancestor::dsc:Model/dsc:Defs/descendant::dsc:Material[contains(@ref,$mat)]/@description"/></xsl:attribute>
		</FittingMaterial>
	</xsl:template>
	
		<xsl:template name="Operations">
		<xsl:param name="gmid"/>
		<xsl:for-each select="descendant::dsc:GeneratedMaterial[@id=$gmid]/dsc:Machinings/descendant::dsc:Machining[@Operate='1' and @Phase !='Incompatible']">
			<Operation>
				<xsl:attribute name="name"><xsl:value-of select="dsc:Shape/@name"/></xsl:attribute>
				<xsl:attribute name="description"><xsl:value-of select="@description"/></xsl:attribute>
				<xsl:attribute name="X"><xsl:value-of select="dsc:PieceCenter3D/@X"/></xsl:attribute>
				<xsl:attribute name="Y"><xsl:value-of select="dsc:PieceCenter3D/@Y"/></xsl:attribute>
				<xsl:attribute name="Z"><xsl:value-of select="dsc:PieceCenter3D/@Z"/></xsl:attribute>
				<xsl:attribute name="depth">
					<xsl:choose>
						<xsl:when test="@Depth &gt; 0"><xsl:value-of select="format-number(@Depth,'0.00')"/></xsl:when>
						<xsl:otherwise><xsl:value-of select="0"/></xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>
				<xsl:attribute name="side"><xsl:value-of select="@Side"/></xsl:attribute>
				<xsl:attribute name="angleplane"><xsl:value-of select="@angleplane"/></xsl:attribute>
				<xsl:attribute name="tool"><xsl:value-of select="@Tool"/></xsl:attribute>
				<xsl:attribute name="Rotation"><xsl:value-of select="@Rotation"/></xsl:attribute>
				<xsl:attribute name="HorizontalInversion"><xsl:value-of select="@HorizontalInversion"/></xsl:attribute>
				<xsl:attribute name="VerticalInversion"><xsl:value-of select="@VerticalInversion"/></xsl:attribute>
				<xsl:attribute name="inverted"><xsl:value-of select="parent::dsc:Machinings/@inverted"/></xsl:attribute>
				<xsl:for-each select="descendant::dsc:Shape/Variable">
					<Variable>
						<xsl:attribute name="name"><xsl:value-of select="@Name"/></xsl:attribute>
						<xsl:attribute name="value"><xsl:value-of select="@Value"/></xsl:attribute>
					</Variable>
				</xsl:for-each>			
			</Operation>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="MatProperties">
		<xsl:param name="mat"/>
		<xsl:param name="varProfRef"/>
		<xsl:variable name="baseRef" select="/ProductionLot/ProductionModels/ProductionModel/Model/dsc:Model
						/dsc:Defs/dsc:Materials/dsc:Material[@ref = $mat]/@baseRef"/>
		<xsl:variable name="color" select="/ProductionLot/ProductionModels/ProductionModel/Model/dsc:Model
						/dsc:Defs/dsc:Materials/dsc:Material[@ref = $mat]/@color"/>
		<xsl:variable name="complColor" select="/ProductionLot/ProductionModels/ProductionModel/Model/dsc:Model
						/dsc:Defs/dsc:Colors/dsc:Color[@ref=$color]/@complex"/>
		<xsl:choose>
			<xsl:when test="$complColor = 1">
				<xsl:attribute name="color"><xsl:value-of select="/ProductionLot/ProductionModels/ProductionModel/Model/dsc:Model
						/dsc:Defs/dsc:Colors/dsc:Color[@ref=$color]/@originalName"/></xsl:attribute>
				<xsl:attribute name="innerColor"><xsl:value-of select="/ProductionLot/ProductionModels/ProductionModel/Model/dsc:Model
						/dsc:Defs/dsc:Colors/dsc:Color[@ref=$color]/@inner"/></xsl:attribute>
				<xsl:attribute name="outerColor"><xsl:value-of select="/ProductionLot/ProductionModels/ProductionModel/Model/dsc:Model
						/dsc:Defs/dsc:Colors/dsc:Color[@ref=$color]/@outer"/></xsl:attribute>
			</xsl:when>
			<xsl:otherwise>
				<xsl:attribute name="color"><xsl:value-of select="$color"/></xsl:attribute>
				<xsl:attribute name="innerColor"><xsl:value-of select="$color"/></xsl:attribute>
				<xsl:attribute name="outerColor"><xsl:value-of select="$color"/></xsl:attribute>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:variable name="profRef">
			<xsl:choose>
				<xsl:when test="contains($varProfRef,']')"><xsl:value-of select="$varProfRef"/></xsl:when>
				<xsl:otherwise><xsl:value-of select="$baseRef"/></xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:attribute name="matWidth"><xsl:value-of select="/ProductionLot/ProductionModels/ProductionModel/Model/dsc:Model
						/dsc:Defs/dsc:Profiles/dsc:Profile[@ref=$profRef]/@width"/></xsl:attribute>
		<xsl:attribute name="matInnerWidth"><xsl:value-of select="/ProductionLot/ProductionModels/ProductionModel/Model/dsc:Model
						/dsc:Defs/dsc:Profiles/dsc:Profile[@ref=$profRef]/@innerWidth"/></xsl:attribute>
		<xsl:attribute name="matInnerBody"><xsl:value-of select="/ProductionLot/ProductionModels/ProductionModel/Model/dsc:Model
						/dsc:Defs/dsc:Profiles/dsc:Profile[@ref=$profRef]/@innerBody"/></xsl:attribute>
		<xsl:attribute name="matOuterWidth"><xsl:value-of select="/ProductionLot/ProductionModels/ProductionModel/Model/dsc:Model
						/dsc:Defs/dsc:Profiles/dsc:Profile[@ref=$profRef]/@outerWidth"/></xsl:attribute>
		<xsl:attribute name="matOuterBody"><xsl:value-of select="/ProductionLot/ProductionModels/ProductionModel/Model/dsc:Model
						/dsc:Defs/dsc:Profiles/dsc:Profile[@ref=$profRef]/@outerBody"/></xsl:attribute>
		<xsl:attribute name="matHeight"><xsl:value-of select="/ProductionLot/ProductionModels/ProductionModel/Model/dsc:Model
						/dsc:Defs/dsc:Profiles/dsc:Profile[@ref=$profRef]/@height"/></xsl:attribute>
	</xsl:template>

</xsl:stylesheet>
