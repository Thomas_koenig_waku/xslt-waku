<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:script="Myscript" extension-element-prefixes="script">
	<xsl:output version="4.0" omit-xml-declaration="yes" indent="yes" method="text"/>
	<!-- _______________________________________________________ Root ____________________________________________________________ -->
	<msxsl:script language="VBScript" implements-prefix="script"><![CDATA[ 

            Function RFill(name, sign, times)
                Dim sStr
                sStr = name & String(times, sign)
                RFill = Left(sStr, times)
            End Function

            Function LFill(name, sign, times)
              Dim s
              s = String(times, sign) & name
              LFill = Right(s, times)
            End Function

            Function GetFeeldNo(fitting)
                Dim v
                Dim u
                GetFeeldNo = "0"
                v = Split(fitting, "#")
                    u = UBound(v)
                If u > 0 Then
                    GetFeeldNo = CStr(v(0))
                End If
            End Function

             Function GetLongVal(val)
                Dim sVal
                Dim v
                Dim d
                If Not IsNumeric(val) Then GetLongVal = CStr(0): Exit Function
                sVal = Replace(val, ",", ".")
                v = Split(sVal, ".")
                If UBound(v) >= 1 Then
                    d = CInt(Left(v(1), 1))
                    If d >= 5 Then sVal = CStr(CLng(v(0)) + 1) Else sVal = CStr(CLng(v(0)))
                Else
                    sVal = CStr(CLng(v(0)))
                End If
                GetLongVal = sVal
            End Function

	       ]]></msxsl:script>
	<!-- PARAMS USED TO OBTAIN EXTERNAL VB INFORMATION -->
	<xsl:param name="set" select="'1'"/>
	<xsl:param name="machine" select="'0'"/>
	<!-- role 0=all	1=FRAME	2=SASH	3=MULLION -->
	<xsl:param name="role" select="0"/>
	<xsl:template match="/">
		<xsl:apply-templates select="descendant::ProductionSet[@ProductionSetNumber=$set]/Machine[@machineId &gt; $machine]"/>
	</xsl:template>
	<xsl:template match="Machine">
		<xsl:variable name="RodCnt" select="count(Rod)"/>
		<xsl:if test="count(Rod) &gt; 0">
			<xsl:choose>
				<xsl:when test="$role=1">
					<xsl:apply-templates select="descendant::Rod[@rol='FRAME']/CutPiece">
				            <xsl:sort select="ancestor::Rod/@setNo"/>
				            <xsl:sort select="descendant::CutInstance/@container" data-type="number"/>
						<xsl:sort select="descendant::CutInstance/@slot" data-type="number"/>
						<xsl:sort select="descendant::CutInstance/@squareNumberInSet" data-type="number"/>
						<xsl:sort select="descendant::CutInstance/@angle" data-type="number"/>
					</xsl:apply-templates>
				</xsl:when>
				<xsl:when test="$role=2">
					<xsl:apply-templates select="descendant::Rod[@rol='SASH']/CutPiece">
				            <xsl:sort select="ancestor::Rod/@setNo"/>
				            <xsl:sort select="descendant::CutInstance/@container" data-type="number"/>
						<xsl:sort select="descendant::CutInstance/@slot" data-type="number"/>
						<xsl:sort select="descendant::CutInstance/@squareNumberInSet" data-type="number"/>
						<xsl:sort select="descendant::CutInstance/@angle" data-type="number"/>
					</xsl:apply-templates>
				</xsl:when>
				<xsl:when test="$role=3">
					<xsl:apply-templates select="descendant::Rod[@rol='MULLION']/CutPiece">
				            <xsl:sort select="ancestor::Rod/@setNo"/>
				            <xsl:sort select="descendant::CutInstance/@container" data-type="number"/>
						<xsl:sort select="descendant::CutInstance/@slot" data-type="number"/>
						<xsl:sort select="descendant::CutInstance/@squareNumberInSet" data-type="number"/>
						<xsl:sort select="descendant::CutInstance/@angle" data-type="number"/>
					</xsl:apply-templates>
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates select="descendant::Rod/CutPiece">
				            <xsl:sort select="ancestor::Rod/@setNo"/>
				            <xsl:sort select="descendant::CutInstance/@container" data-type="number"/>
						<xsl:sort select="descendant::CutInstance/@slot" data-type="number"/>
						<xsl:sort select="descendant::CutInstance/@squareNumberInSet" data-type="number"/>
						<xsl:sort select="descendant::CutInstance/@angle" data-type="number"/>
					</xsl:apply-templates>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
	</xsl:template>
	<xsl:template match="CutPiece">
		<xsl:for-each select="CutInstance">
<!--			<xsl:value-of select="concat('Num:',script:LFill(string(@number),string(0),string(10)))"/>
			<xsl:value-of select="concat('Pos.B.:',script:LFill(string(@nomenclature),string(0),string(3)))"/>
			<xsl:value-of select="concat('SQNrInSet:',script:LFill(string(@squareNumberInSet),string(0),string(2)))"/>
			<xsl:value-of select="concat('FitCode:',format-number(script:GetFeeldNo(string(@fittingCode)),'00'))"/>
			<xsl:value-of select="concat(' L:',format-number(script:GetLongVal(string(parent::CutPiece/@length * 10)),'00000'))"/>
			<xsl:value-of select="concat(' W:',script:LFill(string(@angle),string(0),string(3)))"/>
			<xsl:value-of select="concat(' Rol:',script:LFill(string(ancestor::Rod/@rol),string(' '),string(10)))"/>
			<xsl:value-of select="concat(' Besch:',script:RFill(string(ancestor::Rod/@Description),string(' '),string(10)))"/>
      			<xsl:value-of select="concat(' SetNo:',script:LFill(string(ancestor::Rod/@setNo),string(0),string(3)))"/> -->
			<xsl:value-of select="concat(' Rol:',script:LFill(string(ancestor::Rod/@rol),string(' '),string(10)))"/>
			<xsl:value-of select="concat('Inst:',script:LFill(string(@instance),string(0),string(2)))"/>
			<xsl:value-of select="concat(' W:',script:LFill(string(@angle),string(0),string(3)))"/>
      			<xsl:value-of select="concat(' ID:',@Id)"/>
      			<xsl:value-of select="concat(' Cont:',script:LFill(string(@container),string(0),string(3)))"/>
			<xsl:value-of select="concat(' Slot:',@slot)"/>
			<xsl:if test="descendant::Steel">
				<xsl:value-of select="concat(' StGM:',descendant::Steel/@Id)"/>
				<xsl:value-of select="concat(' StRef:',descendant::Steel/@reference)"/>
			</xsl:if>
			<xsl:text>&#xA;</xsl:text>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>
