<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:dsc="ModelDescriptive" xmlns:thd="Model3D" xmlns:script="MyScript" xmlns:msxsl="urn:schemas-microsoft-com:xslt" extension-element-prefixes="dsc thd">
	<xsl:output method="xml" version="1.0" omit-xml-declaration="yes" encoding="UTF-8" indent="yes"/>
	<msxsl:script language="VBScript" implements-prefix="script"><![CDATA[ 
			
			
	
					
			
		Dim currentLocale
		' Get the current locale
		currentLocale = GetLocale

		Function SetLocaleFirst()
		  Dim original
		  original = SetLocale("en-gb")
		  SetLocaleFirst = ""
  		End Function
	
		Function SetLocaleEnd()
			Dim original
			original = SetLocale(currentLocale)
			SetLocaleEnd = ""
		End Function

            Function GetDecDel()
                GetDecDel = Mid(CStr(3 / 2), 2, 1)
            End Function
		
            Function LongInterior(longitud, anguloA, anguloB, anchoP)
                LongInterior = CStr(longitud)
                
               If anguloA = 0 or anguloB = 0 then
                	exit function
                end if 

                
                If anguloA = 90 and anguloB = 90 then
                	exit function
                end if 

		if 	anguloA = 90    then
			LongInterior = longitud -  anchoP / FormatNumber(Tan(3.1416 * anguloB / 180), 2)
			exit function
		end if

		if 	anguloB = 90    then
			LongInterior = longitud -  anchoP / FormatNumber(Tan(3.1416 * anguloA / 180), 2)
			exit function
		end if                
                
                 LongInterior = longitud - anchoP / Round(Tan(3.1416 * anguloA / 180), 2) - anchoP / FormatNumber(Tan(3.1416 * anguloB / 180), 2)
                
                LongInterior = Round(LongInterior, 2)
            End Function
       
            Dim stSl1
            Dim stSl2
            Dim stSl3
            Dim stSl4
            Dim stSl5
            Dim stSl6
            Dim stSl7
            Dim stSl8
            Dim stSl9
            Dim stSl10
            
            Function IncStSlot(setNo)
                Dim setN
                IncStSlot = 0
                If Not IsNumeric(setNo) Then Exit Function
                setN = CLng(setNo)
                Select Case setN
                    Case 1: stSl1 = stSl1 + 1: IncStSlot = stSl1
                    Case 2: stSl2 = stSl2 + 1: IncStSlot = stSl2
                    Case 3: stSl3 = stSl3 + 1: IncStSlot = stSl3
                    Case 4: stSl4 = stSl4 + 1: IncStSlot = stSl4
                    Case 5: stSl5 = stSl5 + 1: IncStSlot = stSl5
                    Case 6: stSl6 = stSl6 + 1: IncStSlot = stSl6
                    Case 7: stSl7 = stSl7 + 1: IncStSlot = stSl7
                    Case 8: stSl8 = stSl8 + 1: IncStSlot = stSl8
                    Case 9: stSl9 = stSl9 + 1: IncStSlot = stSl9
                    Case 10: stSl10 = stSl10 + 1: IncStSlot = stSl10
                End Select
            End Function
            
            Function SetStSlotsBack()
                SetStSlotsBack = 0
                stSl1 = 0
                stSl2 = 0
                stSl3 = 0
                stSl4 = 0
                stSl5 = 0
                stSl6 = 0
                stSl7 = 0
                stSl8 = 0
                stSl9 = 0
                stSl10 = 0
                SetStSlotsBack = stSl1
            End Function
      
      Function FormatNo(No, Offs, L)
			Dim sNo
			Dim sZero
			Dim i
			If Not IsNumeric(No) Then No = 0
			sNo = CStr(CInt(No) + Offs)
			For i = 1 To L - Len(sNo)
				sZero = sZero & "0"
			Next
			FormatNo = sZero & sNo
		End Function
      
      Function PieceCode(MId, SqId, Inst, PId, Sys, Role, sClass, Ref, Ang, Own)
        'PieceCode= hSort & MId & SqId & Inst & PId & Sys & Role & Ref & Ang & Own
        Dim lAng
        Dim sSort
        Dim lMuOffs
        Dim v
        Dim sKind
        
        'RK120628 nach Analyse der Beispieldatensätze wird bei Waku folgendermaßen sortiert
        '1. Rahmen aller Positionen (L,R,O,U)
        '2. Rahmenteiler aller Positionen (PF, KF)
        '3. RVB aller Positionen
        '4. Rehmeneinleger
        '5. Flügel aller Positionen (L,R,O,U)
        '6. Flügeleinleger
        '7. Sprossen aller Positionen (h, v)
        'RK120628 die Reihenfolge der Bearbeitungsseiten lasse ich vorerst mal im Teilecode,
        'da diese noch nicht vollständig abgeklärt ist
        'Profilbezeichnung zerlegen um das Kürzel für die MatBez zu selektieren
        
        PieceCode = "X"
        If sClass = "Dummy" Then Exit Function
        v = Split(Ref, "-")
        If UBound(v) < 1 Then Exit Function
        lAng = CInt(Ang)
        sSort = FormatNo(MId, 0, 3) & FormatNo(Inst, 0, 3) & FormatNo(SqId, 0, 2)
        Select Case Sys
            Case "W78 Solid", "W97 Vision", "W87 Modul", "W97 Modul", "W106 Contur"
                Select Case v(0)
                    Case "BR"
                        If sClass <> "Schwelle" Then
                            If (lAng > 180) And (lAng < 360) Then PieceCode = "1" & sSort & FormatNo(PId, 1, 3) & "AIABA090"
                            If (lAng > 0) And (lAng < 180) Then PieceCode = "1" & sSort & FormatNo(PId, 41, 3) & "IABAA031"
                            If lAng = 180 Then PieceCode = "1" & sSort & "91" & "AIABA090"
                            If lAng = 360 Then PieceCode = "1" & sSort & "92" & "IABAA001"
                        End If
                    Case "KF", "PF"
                        If (lAng = 360) Or (lAng < 180) Then PieceCode = "2" & sSort & FormatNo(PId, 1, 3) & "IABAA001"
                        If (lAng >= 180) And (lAng < 360) Then PieceCode = "2" & sSort & FormatNo(PId, 51, 3) & "AIABA000"
                    Case "VB"
                        If (lAng = 360) Or (lAng = 180) Then PieceCode = "3" & sSort & FormatNo(PId, 301, 3) & "IABAA001"
                        If (lAng <> 180) And (lAng <> 360) Then PieceCode = "3" & sSort & FormatNo(PId, 351, 3) & "AIABA000"
                    Case "RE"
                        If (lAng = 360) Or (lAng = 180) Then PieceCode = "4" & sSort & FormatNo(PId, 1, 3) & "IABAA001"
                        If (lAng <> 180) And (lAng <> 360) Then PieceCode = "4" & sSort & FormatNo(PId, 51, 3) & "AIABA000"
                    Case "FL", "VLB"
                        If (lAng > 180) And (lAng < 360) Then PieceCode = "5" & sSort & FormatNo(PId, 1, 3) & "AIABA090"
                        If (lAng > 0) And (lAng < 180) Then PieceCode = "5" & sSort & FormatNo(PId, 41, 3) & "IABAA031"
                        If lAng = 180 Then PieceCode = "5" & sSort & "91" & "AIABA090"
                        If lAng = 360 Then PieceCode = "5" & sSort & "92" & "IABAA001"
                    Case "FE"
                        If (lAng = 360) Or (lAng = 180) Then PieceCode = "6" & sSort & FormatNo(PId, 1, 3) & "IABAA001"
                        If (lAng <> 180) And (lAng <> 360) Then PieceCode = "6" & sSort & FormatNo(PId, 51, 3) & "AIABA000"
                    Case "GS"
                        If (lAng = 360) Or (lAng = 180) Then PieceCode = "7" & sSort & FormatNo(PId, 1, 3) & "IABAA001"
                        If (lAng <> 180) And (lAng <> 360) Then PieceCode = "7" & sSort & FormatNo(PId, 51, 3) & "AIABA000"
                    Case "KP"
                End Select
            Case Else
                Select Case Role
                    Case "FRAME BROADENING"
                    Case "FRAME"
                    Case "SASH"
                    Case "MULLION"
                    Case Else
                End Select
        End Select
    'PieceCode=(hSort) & MId & SqId & Inst & PId & Sys & Role & Ref & Ang & Own
    End Function

            
		]]></msxsl:script>
	<!-- _______________________________________________________ Root ____________________________________________________________ -->
	<!-- all pieces of main profiles (frame, sash aso) have a modified slot number (slot*10 + position in slot) -->
	<!-- for recalculation of correct slot number we need all main machineId's -->
	<xsl:param name="mainMachineIds" select="'#1#'"/>
	<xsl:variable name="RolRefuerzo">
		<xsl:value-of select="'REINFORCEMENT'"/>
	</xsl:variable>
	<xsl:template match="/">
		<xsl:comment>UsedDecimal: <xsl:value-of select="script:GetDecDel()"/>
		</xsl:comment>
		<xsl:value-of select="script:SetLocaleFirst()"/>
		<xsl:comment>SettedDecimal: <xsl:value-of select="script:GetDecDel()"/>
		</xsl:comment>
		<xsl:apply-templates select="ProductionLot"/>
		<xsl:value-of select="script:SetLocaleEnd()"/>
	</xsl:template>
	<xsl:template match="ProductionLot">
		<ProductionLot>
			<xsl:attribute name="ProductionLot"><xsl:value-of select="@productionLotNumber"/></xsl:attribute>
			<xsl:attribute name="description"><xsl:value-of select="@description"/></xsl:attribute>
			<xsl:apply-templates select="ProductionSet"/>
		</ProductionLot>
	</xsl:template>
	<xsl:template match="ProductionSet">
		<ProductionSet>
			<xsl:attribute name="ProductionSetNumber"><xsl:value-of select="@productionSetNumber"/></xsl:attribute>
			<xsl:apply-templates select="Machines/Machine"/>
		</ProductionSet>
	</xsl:template>
	<xsl:template match="Machine">
		<Machine>
			<xsl:attribute name="machineId"><xsl:value-of select="@machineId"/></xsl:attribute>
			<xsl:apply-templates select="Reference/Rod">
				<xsl:with-param name="machId" select="@machineId"/>
			</xsl:apply-templates>
		</Machine>
	</xsl:template>
	<xsl:template match="Rod">
		<xsl:param name="machId"/>
		<!-- RK090106 Fachnummern für Stahl, in Zuschnittreihenfolge erzeugen -->
		<!-- Wenn erster Stab MId 103,104,105 dann die Fachnummern auf 0 setzen -->
		<xsl:if test="@number=1 and ($machId=103 or $machId=104 or $machId=105)">
			<xsl:variable name="stSl" select="script:SetStSlotsBack()"/>
		</xsl:if>
		<xsl:variable name="iNumber" select="CutPiece/CutInstance/@number"/>
		<xsl:variable name="iVersion" select="CutPiece/CutInstance/@version"/>
		<xsl:variable name="iPosition" select="CutPiece/CutInstance/@position"/>
		<xsl:variable name="mref">
			<xsl:value-of select="parent::Reference/@materialReference"/>
		</xsl:variable>
		<xsl:variable name="MaterialDef" select="/ProductionLot/ProductionModels/ProductionModel [(@salesDocumentNumber=$iNumber) and (@salesDocumentVersion=$iVersion) and (@salesDocumentPosition=												$iPosition)]/Model/dsc:Model/dsc:Defs/dsc:Materials/dsc:Material[@ref=$mref]"/>
		<xsl:variable name="ref">
			<!--xsl:value-of select="//dsc:Defs/dsc:Materials/dsc:Material[@ref=$mref]/@baseRef"></xsl:value-of-->
			<xsl:value-of select="$MaterialDef/@baseRef"/>
		</xsl:variable>
		<xsl:variable name="Profile" select="/ProductionLot/ProductionModels/ProductionModel [(@salesDocumentNumber=$iNumber) and (@salesDocumentVersion=$iVersion) and (@salesDocumentPosition=												$iPosition)]/Model/dsc:Model/dsc:Defs/dsc:Profiles/dsc:Profile/thd:PROFILE[@ID=$ref]"/>
		<xsl:variable name="rol">
			<!--xsl:value-of select="//dsc:Defs/dsc:Materials/dsc:Material[@ref=$mref]/@Role"></xsl:value-of-->
			<xsl:value-of select="$MaterialDef/@Role"/>
		</xsl:variable>
		<xsl:variable name="class">
			<xsl:value-of select="$MaterialDef/@class"/>
		</xsl:variable>
		<xsl:variable name="height">
			<!--xsl:value-of select="//dsc:Defs/dsc:Profiles/dsc:Profile/thd:PROFILE[@ID=$ref]/@Height"></xsl:value-of-->
			<xsl:value-of select="$Profile/@Height"/>
		</xsl:variable>
		<xsl:variable name="width">
			<!--xsl:value-of select="//dsc:Defs/dsc:Profiles/dsc:Profile/thd:PROFILE[@ID=$ref]/@Width"></xsl:value-of-->
			<xsl:value-of select="$Profile/@Width"/>
		</xsl:variable>
		<xsl:variable name="Description">
			<!--xsl:value-of select="//dsc:Defs/dsc:Materials/dsc:Material[@ref=$mref]/@description"></xsl:value-of-->
			<xsl:value-of select="$MaterialDef/@description"/>
		</xsl:variable>
		<xsl:variable name="innerWidth">
			<!--xsl:value-of select="//dsc:Defs/dsc:Profiles/dsc:Profile[@ref=$ref]/@innerWidth"></xsl:value-of-->
			<xsl:value-of select="$Profile/parent::dsc:Profile/@innerWidth"/>
		</xsl:variable>
		<xsl:variable name="innerBody">
			<!--xsl:value-of select="//dsc:Defs/dsc:Profiles/dsc:Profile[@ref=$ref]/@innerBody"></xsl:value-of-->
			<xsl:value-of select="$Profile/parent::dsc:Profile/@innerBody"/>
		</xsl:variable>
		<xsl:variable name="outerWidth">
			<!--xsl:value-of select="//dsc:Defs/dsc:Profiles/dsc:Profile[@ref=$ref]/@outerWidth"></xsl:value-of-->
			<xsl:value-of select="$Profile/parent::dsc:Profile/@outerWidth"/>
		</xsl:variable>
		<xsl:variable name="outerBody">
			<!--xsl:value-of select="//dsc:Defs/dsc:Profiles/dsc:Profile[@ref=$ref]/@outerBody"></xsl:value-of-->
			<xsl:value-of select="$Profile/parent::dsc:Profile/@outerBody"/>
		</xsl:variable>
		<!-- KST -->
		<xsl:variable name="color">
			<xsl:value-of select="parent::Reference/@color"/>
		</xsl:variable>
		<xsl:variable name="complColor" select="/ProductionLot/ProductionModels/ProductionModel/Model/dsc:Model/dsc:Defs/dsc:Colors/dsc:Color[@ref=$color]/@complex"/>
		<!--xsl:choose>
			<xsl:when test="$complColor = 1">
				<xsl:variable name="innerColor">
					<xsl:value-of select="/ProductionLot/ProductionModels/ProductionModel/Model/dsc:Model	/dsc:Defs/dsc:Colors/dsc:Color[@ref=$color]/@inner"/>				
				</xsl:variable>				
				<xsl:variable name="outerColor">
					<xsl:value-of select="/ProductionLot/ProductionModels/ProductionModel/Model/dsc:Model	/dsc:Defs/dsc:Colors/dsc:Color[@ref=$color]/@outer"/>				
				</xsl:variable>											
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="innerColor">
					<xsl:value-of select="$color"/>
				</xsl:variable>				
				<xsl:variable name="outerColor">
					<xsl:value-of select="$color"/>
				</xsl:variable>				
			</xsl:otherwise>
		</xsl:choose-->
		<xsl:variable name="innerColor">
			<xsl:choose>
				<xsl:when test="$complColor = 1">
					<xsl:variable name="matColor" select="$MaterialDef/@color"/>
					<!--xsl:value-of select="/ProductionLot/ProductionModels/ProductionModel/Model/dsc:Model/dsc:Defs/dsc:Colors/dsc:Color[@ref=$color]/@inner"/-->
					<xsl:value-of select="/ProductionLot/ProductionModels/ProductionModel/Model/dsc:Model/dsc:Defs/dsc:Colors/dsc:Color[@ref=$matColor]/@inner"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$color"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="outerColor">
			<xsl:choose>
				<xsl:when test="$complColor = 1">
					<xsl:variable name="matColor" select="$MaterialDef/@color"/>
					<!--xsl:value-of select="/ProductionLot/ProductionModels/ProductionModel/Model/dsc:Model/dsc:Defs/dsc:Colors/dsc:Color[@ref=$color]/@outer"/-->
					<xsl:value-of select="/ProductionLot/ProductionModels/ProductionModel/Model/dsc:Model/dsc:Defs/dsc:Colors/dsc:Color[@ref=$matColor]/@outer"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$color"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<!--xsl:if test="$rol!=$RolRefuerzo"-->
		<Rod>
			<xsl:attribute name="number"><xsl:value-of select="@number"/></xsl:attribute>
			<xsl:attribute name="instances"><xsl:value-of select="@instances"/></xsl:attribute>
			<xsl:attribute name="piecesNumber"><xsl:value-of select="@piecesNumber"/></xsl:attribute>
			<xsl:attribute name="reference"><xsl:value-of select="$ref"/></xsl:attribute>
			<xsl:attribute name="finalReference"><xsl:value-of select="$mref"/></xsl:attribute>
			<xsl:attribute name="rol"><xsl:value-of select="$rol"/></xsl:attribute>
			<xsl:attribute name="class"><xsl:value-of select="$class"/></xsl:attribute>
			<xsl:attribute name="Description"><xsl:value-of select="$Description"/></xsl:attribute>
			<!--xsl:attribute name="color"><xsl:value-of select="parent::Reference/@color"/></xsl:attribute-->
			<xsl:attribute name="color"><xsl:value-of select="$color"/></xsl:attribute>
			<xsl:attribute name="IsComplex"><xsl:value-of select="$complColor"/></xsl:attribute>
			<xsl:attribute name="innerColor"><xsl:value-of select="/ProductionLot/ProductionModels/ProductionModel/Model/dsc:Model/dsc:Defs/dsc:Colors/dsc:Color[@ref=$innerColor]/@description"/></xsl:attribute>
			<xsl:attribute name="outerColor"><xsl:value-of select="/ProductionLot/ProductionModels/ProductionModel/Model/dsc:Model/dsc:Defs/dsc:Colors/dsc:Color[@ref=$outerColor]/@description"/></xsl:attribute>
			<xsl:attribute name="length"><xsl:value-of select="@length"/></xsl:attribute>
			<xsl:attribute name="height"><xsl:value-of select="$height"/></xsl:attribute>
			<xsl:attribute name="width"><xsl:value-of select="$width"/></xsl:attribute>
			<xsl:attribute name="innerWidth"><xsl:value-of select="$innerWidth"/></xsl:attribute>
			<xsl:attribute name="innerBody"><xsl:value-of select="$innerBody"/></xsl:attribute>
			<xsl:attribute name="outerWidth"><xsl:value-of select="$outerWidth"/></xsl:attribute>
			<xsl:attribute name="outerBody"><xsl:value-of select="$outerBody"/></xsl:attribute>
			<xsl:attribute name="isWaste"><xsl:value-of select="@isWaste"/></xsl:attribute>
			<xsl:attribute name="kindwaste"><xsl:value-of select="@kindWaste"/></xsl:attribute>
			<xsl:attribute name="waste"><xsl:value-of select="@waste"/></xsl:attribute>
			<xsl:attribute name="inverted"><xsl:value-of select="parent::Reference/@mirrorHorizontalForMachining"/></xsl:attribute>
			<xsl:variable name="setNo">
				<xsl:choose>
					<xsl:when test="$machId = 21 or $machId = 22 or $machId = 23 or $machId = 103 or $machId = 104 or $machId = 105">
						<xsl:choose>
							<xsl:when test="@setNo &gt; 1">
								<xsl:value-of select="@setNo"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="1"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="0"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:attribute name="setNo"><xsl:value-of select="$setNo"/></xsl:attribute>
			<xsl:apply-templates select="child::CutPiece">
				<xsl:with-param name="baseref" select="$ref"/>
				<xsl:with-param name="perfheight" select="$height"/>
				<xsl:with-param name="machId" select="$machId"/>
				<xsl:with-param name="setNo" select="$setNo"/>
			</xsl:apply-templates>
		</Rod>
		<!--/xsl:if-->
	</xsl:template>
	<xsl:template match="CutPiece">
		<xsl:param name="baseref"/>
		<xsl:param name="perfheight"/>
		<xsl:param name="machId"/>
		<xsl:param name="setNo"/>
		<CutPiece>
			<xsl:attribute name="number"><xsl:value-of select="@number"/></xsl:attribute>
			<xsl:attribute name="instances"><xsl:value-of select="@instances"/></xsl:attribute>
			<xsl:attribute name="length"><xsl:value-of select="@length"/></xsl:attribute>
			<xsl:attribute name="angleA"><xsl:value-of select="@angleA"/></xsl:attribute>
			<xsl:attribute name="angleB"><xsl:value-of select="@angleB"/></xsl:attribute>
			<xsl:apply-templates select="child::CutInstance">
				<xsl:with-param name="baseref" select="$baseref"/>
				<xsl:with-param name="perfheight" select="$perfheight"/>
				<xsl:with-param name="machId" select="$machId"/>
				<xsl:with-param name="setNo" select="$setNo"/>
			</xsl:apply-templates>
		</CutPiece>
	</xsl:template>
	<xsl:template match="CutInstance">
		<xsl:param name="baseref"/>
		<xsl:param name="perfheight"/>
		<xsl:param name="machId"/>
		<xsl:param name="setNo"/>
		<xsl:variable name="iNumber">
			<xsl:value-of select="@number"/>
		</xsl:variable>
		<xsl:variable name="iVersion">
			<xsl:value-of select="@version"/>
		</xsl:variable>
		<xsl:variable name="iPosition">
			<xsl:value-of select="@position"/>
		</xsl:variable>
		<xsl:variable name="iInstance">
			<xsl:value-of select="@instance"/>
		</xsl:variable>
		<xsl:variable name="orderNumber">
			<!--xsl:value-of select="/descendant::SalesDocuments/SalesDocument[@salesDocumentNumber= $iNumber and @salesDocumentVersion = $iVersion]/@orderNumber"/-->
			<xsl:value-of select="/ProductionLot/SalesDocuments/SalesDocument[@salesDocumentNumber= $iNumber and @salesDocumentVersion = $iVersion]/@orderNumber"/>
		</xsl:variable>
		<xsl:variable name="userData1">
			<xsl:value-of select="/ProductionLot/SalesDocuments/SalesDocument[@salesDocumentNumber= $iNumber and @salesDocumentVersion = $iVersion]/@userData1"/>
		</xsl:variable>
		<xsl:variable name="userData2">
			<xsl:value-of select="/ProductionLot/SalesDocuments/SalesDocument[@salesDocumentNumber= $iNumber and @salesDocumentVersion = $iVersion]/@userData2"/>
		</xsl:variable>
		<xsl:variable name="reference">
			<!--xsl:value-of select="/descendant::SalesDocuments/SalesDocument[@salesDocumentNumber= $iNumber and @salesDocumentVersion = $iVersion]/@reference"/-->
			<xsl:value-of select="/ProductionLot/SalesDocuments/SalesDocument[@salesDocumentNumber= $iNumber and @salesDocumentVersion = $iVersion]/@reference"/>
		</xsl:variable>
		<xsl:variable name="sortOrder">
			<!--xsl:value-of select="/descendant::SalesDocuments/SalesDocument[@salesDocumentNumber= $iNumber and @salesDocumentVersion = $iVersion]/SalesDocumentPosition				[@position=$iPosition]/@sortOrder"/-->
			<xsl:value-of select="/ProductionLot/SalesDocuments/SalesDocument[@salesDocumentNumber= $iNumber and @salesDocumentVersion = $iVersion]/SalesDocumentPosition[@position=$iPosition]/@sortOrder"/>
		</xsl:variable>
		<xsl:variable name="nomenclature">
			<!--xsl:value-of select="//descendant::SalesDocument[@salesDocumentNumber = $iNumber and @salesDocumentVersion=$iVersion]/SalesDocumentPosition						[@position=$iPosition]/@nomenclature"/-->
			<xsl:value-of select="/ProductionLot/SalesDocuments/SalesDocument[@salesDocumentNumber = $iNumber and @salesDocumentVersion=$iVersion]/SalesDocumentPosition[@position=$iPosition]/@nomenclature"/>
		</xsl:variable>
		<xsl:variable name="customerName">
			<xsl:value-of select="/ProductionLot/SalesDocuments/SalesDocument[@salesDocumentNumber= $iNumber and @salesDocumentVersion = $iVersion]/@customerName"/>
		</xsl:variable>
		<xsl:variable name="shipTo">
			<xsl:value-of select="/ProductionLot/SalesDocuments/SalesDocument[@salesDocumentNumber= $iNumber and @salesDocumentVersion = $iVersion]/@shipTo"/>
		</xsl:variable>
		<!--SD20110407 hinzugefügt-->
		<xsl:variable name="prefOpenCode">
			<xsl:value-of select="/ProductionLot/ProductionModels/ProductionModel[@salesDocumentNumber=$iNumber and @salesDocumentVersion=$iVersion and @salesDocumentPosition=$iPosition]/Model/dsc:Model/dsc:Param/PrefCoDescriptive/PrefOpenCodes/PrefOpenCode/@prefOpenCode"/>
		</xsl:variable>
		<xsl:variable name="SalesmanName">
			<xsl:value-of select="/ProductionLot/SalesDocuments/SalesDocument[@salesDocumentNumber= $iNumber and @salesDocumentVersion = $iVersion]/@SalesmanName"/>
		</xsl:variable>
		<xsl:variable name="iGM">
			<xsl:value-of select="@idPiece"/>
		</xsl:variable>
		<xsl:variable name="posInSquare">
			<xsl:value-of select="@positionInSquare"/>
		</xsl:variable>
		<!--xsl:variable name="tmpRef"><xsl:value-of select="parent::CutPiece/parent::Rod/parent::Reference/@materialReference"/></xsl:variable-->
		<xsl:variable name="matref">
			<!--xsl:value-of select="//dsc:Defs/dsc:Materials/dsc:Material[@ref=$tmpRef]/@baseRef"></xsl:value-of-->
			<xsl:value-of select="$baseref"/>
		</xsl:variable>
		<xsl:variable name="length">
			<xsl:value-of select="parent::CutPiece/@length"/>
		</xsl:variable>
		<xsl:variable name="weldAddedA">
			<xsl:choose>
				<xsl:when test="format-number(@weldAddedA,'0') ='NaN' ">
					<xsl:value-of select="0"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="@weldAddedA"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="weldAddedB">
			<xsl:choose>
				<xsl:when test="format-number(@weldAddedB,'0') ='NaN' ">
					<xsl:value-of select="0"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="@weldAddedB"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="cutAngleA">
			<xsl:value-of select="parent::CutPiece/@angleA"/>
		</xsl:variable>
		<xsl:variable name="cutAngleB">
			<xsl:value-of select="parent::CutPiece/@angleB"/>
		</xsl:variable>
		<xsl:variable name="height">
			<!--xsl:value-of select="format-number(//dsc:Defs/dsc:Profiles/dsc:Profile/thd:PROFILE[@ID=$matref]/@Height,'0')"/-->
			<xsl:value-of select="format-number($perfheight,'0')"/>
		</xsl:variable>
		<!-- added by ilja 22.11.2004 -->
		<xsl:variable name="sqid" select="@squareId"/>
		<xsl:variable name="ps" select="ancestor::ProductionSet/@productionSetNumber"/>
		<!-- end ilja -->
		<xsl:variable name="innerLength">
			<!--<xsl:value-of select="script:Longinterior(string($length),string($cutAngleA),string($cutAngleB),string($height))"/>-->
			<xsl:choose>
				<xsl:when test="$height = 'NaN'">
					<xsl:value-of select="0"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="script:Longinterior(string($length),string($cutAngleA),string($cutAngleB),string($height))"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="instance" select="@instance"/>
		<CutInstance>
			<xsl:variable name="sdnumber" select="@number"/>
			<xsl:variable name="sdversion" select="@version"/>
			<xsl:variable name="sdposition" select="@position"/>
			<xsl:variable name="sdinstance" select="@instance"/>
			<xsl:variable name="sdsubmodel" select="@subModel"/>
			<xsl:variable name="productionSet" select="ancestor::ProductionSet/@productionSetNumber"/>
			<xsl:attribute name="Id"><xsl:value-of select="@idPiece"/></xsl:attribute>
			<xsl:attribute name="absoluteNumber"><xsl:value-of select="@absoluteNumber"/></xsl:attribute>
			<xsl:attribute name="number"><xsl:value-of select="@number"/></xsl:attribute>
			<xsl:attribute name="version"><xsl:value-of select="@version"/></xsl:attribute>
			<xsl:attribute name="position"><xsl:value-of select="@position"/></xsl:attribute>
			<xsl:for-each select="ancestor::ProductionSet/SubWindows/SubWindow">
				<xsl:if test="@salesDocumentNumber=$sdnumber and @salesDocumentVersion=$sdversion and  @salesDocumentPosition=$sdposition  and @salesDocumentInstance=$sdinstance">
					<xsl:attribute name="setPosition"><xsl:value-of select="position() +1 - @subModel"/></xsl:attribute>
				</xsl:if>
			</xsl:for-each>
			<xsl:attribute name="sortOrder"><xsl:value-of select="$sortOrder"/></xsl:attribute>
			<xsl:attribute name="nomenclature"><xsl:value-of select="$nomenclature"/></xsl:attribute>
			<xsl:attribute name="instance"><xsl:value-of select="$instance"/></xsl:attribute>
			<xsl:attribute name="orderNumber"><xsl:value-of select="$orderNumber"/></xsl:attribute>
			<xsl:attribute name="userData1"><xsl:value-of select="$userData1"/></xsl:attribute>
			<xsl:attribute name="userData2"><xsl:value-of select="$userData2"/></xsl:attribute>
			<xsl:attribute name="reference"><xsl:value-of select="$reference"/></xsl:attribute>
			<xsl:attribute name="shipTo"><xsl:value-of select="$shipTo"/></xsl:attribute>
			<!--SD20110407 hinzugefügt-->
			<xsl:attribute name="prefOpenCode"><xsl:value-of select="$prefOpenCode"/></xsl:attribute>
			<xsl:attribute name="salesmanName"><xsl:value-of select="$SalesmanName"/></xsl:attribute>
			<xsl:attribute name="InnerLength"><xsl:value-of select="$innerLength"/></xsl:attribute>
			<xsl:attribute name="weldAddedA"><xsl:value-of select="$weldAddedA"/></xsl:attribute>
			<xsl:attribute name="weldAddedB"><xsl:value-of select="$weldAddedB"/></xsl:attribute>
			<xsl:attribute name="weldedLength"><xsl:value-of select="$length   -  $weldAddedA   -  $weldAddedB"/></xsl:attribute>
			<xsl:if test="@incrementA">
				<xsl:attribute name="incrementA"><xsl:value-of select="@incrementA"/></xsl:attribute>
			</xsl:if>
			<xsl:if test="@incrementB">
				<xsl:attribute name="incrementB"><xsl:value-of select="@incrementB"/></xsl:attribute>
			</xsl:if>
			<xsl:variable name="GMNode" select="/ProductionLot/ProductionModels/ProductionModel[@salesDocumentNumber =$iNumber and 	@salesDocumentVersion =$iVersion and @salesDocumentPosition =$iPosition]/descendant::dsc:Contour/descendant::dsc:GeneratedMaterial[@id=$iGM]"/>
			<xsl:variable name="material" select="$GMNode/@material"/>
			<!-- KOE20130522 variable Breite (beisägen) -->
			<xsl:attribute name="height"><xsl:value-of select="$GMNode/@height"/></xsl:attribute>
			<!--xsl:if test="$GMNode/@method='self' and $GMNode/parent::dsc:ProfilePiece/child::dsc:GeneratedMaterial[@method='Raw Material']"-->
			<xsl:if test="$GMNode/@rawMaterialGenerator='1'">
				<xsl:variable name="rawGM" select="$GMNode/parent::*/child::dsc:GeneratedMaterial[@rawMaterialGeneratorId=$iGM]"/>
				<xsl:if test="$rawGM">
					<xsl:attribute name="rawGMId"><xsl:value-of select="$rawGM/@id"/></xsl:attribute>
					<xsl:attribute name="rawRef"><xsl:value-of select="$rawGM/@material"/></xsl:attribute>
					<xsl:attribute name="rawColor"><xsl:value-of select="$rawGM/@color"/></xsl:attribute>
					<xsl:attribute name="rawLength"><xsl:value-of select="$rawGM/@length"/></xsl:attribute>
				</xsl:if>
			</xsl:if>
			<!-- RK110201 Fachnummern für Stahl vom PVC- Vaterprofil holen -->
			<xsl:variable name="fatherProfGMID">
				<xsl:value-of select="$GMNode/ancestor::dsc:ProfilePiece/child::dsc:GeneratedMaterial[@method='self']/@id"/>
			</xsl:variable>
			<xsl:attribute name="fatherProfGMID"><xsl:value-of select="$fatherProfGMID"/></xsl:attribute>
			<xsl:variable name="fatherMaterial">
				<xsl:value-of select="$GMNode/ancestor::dsc:ProfilePiece/child::dsc:GeneratedMaterial[@method='self']/@material"/>
			</xsl:variable>
			<xsl:attribute name="fatherMaterial"><xsl:value-of select="$fatherMaterial"/></xsl:attribute>
			<xsl:variable name="fatherRole">
				<xsl:value-of select="//dsc:Defs/dsc:Materials/dsc:Material[@ref=$fatherMaterial]/@Role"/>
			</xsl:variable>
			<xsl:attribute name="fatherRole"><xsl:value-of select="$fatherRole"/></xsl:attribute>
			<xsl:attribute name="system"><xsl:value-of select="$GMNode/ancestor::dsc:Model/@system"/></xsl:attribute>
			<xsl:variable name="masterMaterial">
				<xsl:value-of select="$GMNode/ancestor::dsc:PerimetralRods/parent::dsc:Rod/@material"/>
			</xsl:variable>
			<xsl:attribute name="masterRolle"><xsl:value-of select="$GMNode/ancestor::dsc:Model/dsc:Defs/dsc:Materials/dsc:Material[@ref=$masterMaterial]/@Role"/></xsl:attribute>
			<xsl:variable name="Container">
				<xsl:value-of select="@container"/>
			</xsl:variable>
			<xsl:variable name="tmpContainer"><!--xsl:choose>
					<xsl:when test ="string-length($Container) &gt; 0">
						<xsl:value-of select ="$Container"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select ="'-1'"/>
					</xsl:otherwise>
				</xsl:choose-->
				<xsl:choose><xsl:when test="$machId=3"><xsl:value-of select="ancestor::Machines/child::Machine[@machineId=1 or @machineId=2]/
												descendant::CutInstance
												[@number = $iNumber and 
												@version = $iVersion and 
												@position = $iPosition and 
												@instance=$sdinstance and
												@idPiece=$fatherProfGMID and 
												@angle= @angle]
												/@container"/></xsl:when>
												<xsl:when test="$machId=13"><xsl:value-of select="ancestor::Machines/child::Machine[@machineId=1 or @machineId=2 or @machineId=15]/
												descendant::CutInstance
												[@number = $iNumber and 
												@version = $iVersion and 
												@position = $iPosition and 
												@instance=$sdinstance and
												@idPiece=$fatherProfGMID and 
												@angle= @angle]
												/@container"/></xsl:when><xsl:when test="$machId=33"><xsl:value-of select="ancestor::Machines/child::Machine[@machineId=18]/
												descendant::CutInstance
												[@number = $iNumber and 
												@version = $iVersion and 
												@position = $iPosition and 
												@instance=$sdinstance and
												@idPiece=$fatherProfGMID and 
												@angle= @angle]
												/@container"/></xsl:when><xsl:when test="$machId=34"><xsl:value-of select="ancestor::Machines/child::Machine[@machineId=15]/
												descendant::CutInstance
												[@number = $iNumber and 
												@version = $iVersion and 
												@position = $iPosition and 
												@instance=$sdinstance and
												@idPiece=$fatherProfGMID and 
												@angle= @angle]
												/@container"/></xsl:when><xsl:when test="$machId=35"><xsl:value-of select="@container"/></xsl:when>
												<xsl:otherwise><xsl:value-of select="@container"/></xsl:otherwise>
												</xsl:choose>
			</xsl:variable >
			<xsl:attribute name="container">
				<xsl:choose>
					<xsl:when test="$tmpContainer != ''"><xsl:value-of select="$tmpContainer"/></xsl:when>
					<xsl:otherwise><xsl:value-of select="'0'"/></xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<!--xsl:variable name ="Slot"><xsl:value-of select="@slot"/></xsl:variable-->
			<xsl:variable name="Slot">
				<xsl:choose>
					<xsl:when test="$machId=3">
						<xsl:value-of select="ancestor::Machines/child::Machine[@machineId=1 or @machineId=2]/
												descendant::CutInstance
												[@number = $iNumber and 
												@version = $iVersion and 
												@position = $iPosition and 
												@instance=$sdinstance and
												@idPiece=$fatherProfGMID and 
												@angle= @angle]
												/@slot"/>
					</xsl:when>
					<xsl:when test="$machId=13">
						<xsl:value-of select="ancestor::Machines/child::Machine[@machineId=1 or @machineId=2 or @machineId=15]/
												descendant::CutInstance
												[@number = $iNumber and 
												@version = $iVersion and 
												@position = $iPosition and 
												@instance=$sdinstance and
												@idPiece=$fatherProfGMID and 
												@angle= @angle]
												/@slot"/>
					</xsl:when>
					<xsl:when test="$machId=33">
						<xsl:value-of select="ancestor::Machines/child::Machine[@machineId=18]/
												descendant::CutInstance
												[@number = $iNumber and 
												@version = $iVersion and 
												@position = $iPosition and 
												@instance=$sdinstance and
												@idPiece=$fatherProfGMID and 
												@angle= @angle]
												/@slot"/>
					</xsl:when>
					<xsl:when test="$machId=34">
						<xsl:value-of select="ancestor::Machines/child::Machine[@machineId=15]/
												descendant::CutInstance
												[@number = $iNumber and 
												@version = $iVersion and 
												@position = $iPosition and 
												@instance=$sdinstance and
												@idPiece=$fatherProfGMID and 
												@angle= @angle]
												/@slot"/>
					</xsl:when>
					<xsl:when test="$machId=35">
						<xsl:value-of select="@slot"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="@slot"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:attribute name="slot"><xsl:choose><xsl:when test="string-length($Slot) &gt; 0"><xsl:value-of select="$Slot"/></xsl:when><xsl:otherwise><xsl:value-of select="'-1'"/></xsl:otherwise></xsl:choose></xsl:attribute>
			<xsl:variable name="angle">
				<xsl:choose>
					<xsl:when test="@angle = -1">
						<xsl:choose>
							<xsl:when test="$GMNode/parent::*/parent::dsc:Stick">
								<xsl:choose>
									<xsl:when test="$GMNode/parent::*/parent::dsc:Stick/@orientation = 'horizontal'">
										<xsl:value-of select="360"/>
									</xsl:when>
									<xsl:when test="$GMNode/parent::*/parent::dsc:Stick/@orientation = 'vertical'">
										<xsl:value-of select="90"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="@angle"/>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="@angle"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="@angle"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:attribute name="angle"><xsl:value-of select="$angle"/></xsl:attribute>
			<xsl:attribute name="positionInSquare"><xsl:value-of select="$posInSquare"/></xsl:attribute>
			<xsl:attribute name="parameter"><xsl:value-of select="@parameter"/></xsl:attribute>
			<!--xsl:for-each select="//descendant::Model"-->
			<xsl:for-each select="/ProductionLot/ProductionModels/ProductionModel/Model">
				<xsl:if test="parent::ProductionModel[@salesDocumentNumber =$iNumber] and parent::ProductionModel[@salesDocumentVersion =$iVersion] and parent::ProductionModel[@salesDocumentPosition =$iPosition] ">
					<!-- added by ilja 22.11.2004 -->
					<!--xsl:attribute name="fittingCode"><xsl:value-of select="descendant::dsc:Model/descendant::dsc:Square[@id = $sqid]/@fittingCode"/></xsl:attribute-->
					<xsl:attribute name="fittingCode"><xsl:value-of select="dsc:Model/descendant::dsc:Square[@id = $sqid]/@fittingCode"/></xsl:attribute>
					<xsl:attribute name="squareId"><xsl:value-of select="$sqid"/></xsl:attribute>
					<xsl:attribute name="squareNumberInSet"><!--xsl:value-of select="//ProductionLot/ProductionSet[@productionSetNumber = $ps]/Squares/
					Square[@salesDocumentNumber = $iNumber and @salesDocumentVersion=$iVersion and @salesDocumentPosition =$iPosition and @id = 				$sqid]/SquareInstance[@salesDocumentInstance=$iInstance]/@squareNumberInSet" /--><xsl:value-of select="/ProductionLot/ProductionSet[@productionSetNumber = $ps]/Squares/
					Square[@salesDocumentNumber = $iNumber and @salesDocumentVersion=$iVersion and @salesDocumentPosition =$iPosition and @id = $sqid]/SquareInstance[@salesDocumentInstance=$iInstance]/@squareNumberInSet"/></xsl:attribute>
					<xsl:variable name="parentSqId">
						<xsl:value-of select="descendant::dsc:Square[@id=$sqid]/@parentSquare"/>
					</xsl:variable>
					<xsl:attribute name="parentSqId"><xsl:value-of select="$parentSqId"/></xsl:attribute>
					<xsl:variable name="parentSqNoInSet" select="/ProductionLot/ProductionSet[@productionSetNumber = $ps]/Squares/
					Square[@salesDocumentNumber = $iNumber and @salesDocumentVersion=$iVersion and @salesDocumentPosition =$iPosition and @id = $parentSqId]/SquareInstance[@salesDocumentInstance=$iInstance]/@squareNumberInSet"/>
					<xsl:attribute name="parentSqNoInSet"><xsl:value-of select="$parentSqNoInSet"/></xsl:attribute>
					<!--     { @salesDocumentNumber = $iNumber and @salesDocumentVersion=$iVersion and @salesDocumentPosition =$iPosition and}                                end ilja -->
					<xsl:variable name="rebatePos">
						<xsl:value-of select="descendant::dsc:GeneratedMaterial[@id=$iGM]/parent::dsc:ProfilePiece/parent::dsc:Rod/descendant::dsc:Square/@rebatePosition"/>
					</xsl:variable>
					<xsl:variable name="orgRef">
						<xsl:value-of select="descendant::dsc:GeneratedMaterial[@id=$iGM]/parent::dsc:ProfilePiece/@material"/>
					</xsl:variable>
					<xsl:variable name="orgClass">
						<xsl:value-of select="child::dsc:Model/child::dsc:Defs/descendant::dsc:Material[@ref=$orgRef]/@class"/>
					</xsl:variable>
					<xsl:variable name="hasStulp">
						<!--  0 not has stulp 1 has stulp -->
						<xsl:choose>
							<xsl:when test="$rebatePos=$posInSquare">
								<xsl:value-of select="1"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="0"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					<xsl:attribute name="orgRef"><xsl:value-of select="$orgRef"/></xsl:attribute>
					<xsl:attribute name="orgClass"><xsl:value-of select="$orgClass"/></xsl:attribute>
					<xsl:attribute name="hasStulp"><xsl:value-of select="$hasStulp"/></xsl:attribute>
					<xsl:attribute name="customerName"><!--xsl:value-of select="//descendant::SalesDocument[@salesDocumentNumber = $iNumber and @salesDocumentVersion=$iVersion]/@customerName"/--><xsl:value-of select="$customerName"/></xsl:attribute>
					<xsl:attribute name="modelName"><!--xsl:value-of select="descendant::dsc:Model/@name"/--><xsl:value-of select="dsc:Model/@name"/></xsl:attribute>
					<xsl:variable name="IstHolzModell">
						<xsl:choose>
							<xsl:when test="contains(child::dsc:Model/child::dsc:Defs/dsc:Options/dsc:Option[@option='000 Materialsystem']/@value,'Holz')">
								<xsl:value-of select="1"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="0"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					<xsl:attribute name="specialOperations"><xsl:choose><xsl:when test="$GMNode/parent::*/parent::*/parent::dsc:GeorgianBar"><xsl:variable name="glassNumber" select="$GMNode/parent::*/parent::*/parent::dsc:GeorgianBar/parent::dsc:Glass/@number"/><xsl:variable name="fieldNumber" select="dsc:Model/dsc:Param/PrefCoDescriptive/WorkSheetInfo/Model/Frames/Frame/Glasses/Glass[@Nr=$glassNumber]/@FieldNumber"/><xsl:value-of select="concat($fieldNumber,'#',$GMNode/parent::*/parent::*/parent::dsc:GeorgianBar/@type)"/></xsl:when><xsl:when test="$IstHolzModell = 1"><xsl:variable name="SCM_PROG"><xsl:if test="$GMNode/dsc:Machinings/descendant::dsc:Machining/dsc:Shape[@name='SCM_PROG']"><xsl:value-of select="concat('SCM_PROG#',$GMNode/dsc:Machinings/descendant::dsc:Machining/dsc:Shape[@name='SCM_PROG']/parent::*/@description)"/></xsl:if></xsl:variable><xsl:variable name="EPA" select="$GMNode/dsc:Machinings/descendant::dsc:Machining/dsc:Shape[@name='EPA']/parent::*/@description"/><xsl:variable name="EPB" select="$GMNode/dsc:Machinings/descendant::dsc:Machining/dsc:Shape[@name='EPB']/parent::*/@description"/><xsl:variable name="LPA" select="$GMNode/dsc:Machinings/descendant::dsc:Machining/dsc:Shape[@name='LPA']/parent::*/@description"/><xsl:variable name="LPI" select="$GMNode/dsc:Machinings/descendant::dsc:Machining/dsc:Shape[@name='LPI']/parent::*/@description"/><xsl:value-of select="concat($SCM_PROG,'$',$EPA,'$',$EPB,'$',$LPA,'$',$LPI)"/></xsl:when></xsl:choose></xsl:attribute>
					<xsl:if test="$IstHolzModell = 1">
						<!-- 
						iMId = SetPosition
						SqNo = squareNumberInWindow
						iPId = dsc:ProfilePiece/@order 
						-->
						<xsl:variable name="setPosition">
							<xsl:for-each select="/ProductionLot/ProductionSet[@productionSetNumber=$productionSet]/SubWindows/SubWindow[@subModel='1']">
								<xsl:if test="@salesDocumentNumber=$sdnumber and @salesDocumentVersion=$sdversion and  @salesDocumentPosition=$sdposition  and @salesDocumentInstance=$sdinstance">
									<xsl:value-of select="position()"/>
								</xsl:if>
							</xsl:for-each>
						</xsl:variable>
						<xsl:variable name="SqNo" select="substring-after($sqid,'SQ')"/>
						<xsl:variable name="iPId" select="$GMNode/parent::dsc:ProfilePiece/@order"/>
						<xsl:variable name="sMSys" select="dsc:Model/@system"/>
						<xsl:variable name="sRole" select="//dsc:Defs/dsc:Materials/dsc:Material[@ref=$material]/@Role"/>
						<xsl:variable name="sClass" select="//dsc:Defs/dsc:Materials/dsc:Material[@ref=$material]/@class"/>
						<xsl:variable name="piececode" select="script:PieceCode(string($setPosition),string($SqNo),string($instance),string($iPId),string($sMSys),string($sRole),string($sClass),string( 		$material),string($angle),string($sqid))"/>
						<xsl:attribute name="pieceCodeWood"><xsl:value-of select="$piececode"/></xsl:attribute>
					</xsl:if>
					<xsl:if test="descendant::dsc:GeneratedMaterial[@id=$iGM]/dsc:Machinings/descendant::dsc:Machining[@Operate='1' and @Phase !='Incompatible'][dsc:Shape/@name='HECHT#BEARBEITUNG']">
						<xsl:attribute name="hechtbearbeitung"><xsl:for-each select="descendant::dsc:GeneratedMaterial[@id=$iGM]/dsc:Machinings/descendant::dsc:Machining[@Operate='1' and @Phase !='Incompatible'][dsc:Shape/@name='HECHT#BEARBEITUNG']"><xsl:sort select="dsc:PieceCenter3D/@Z" data-type="number"/><xsl:sort select="dsc:PieceCenter3D/@X" data-type="number"/><xsl:value-of select="@Tool"/><xsl:value-of select="format-number(dsc:PieceCenter3D/@X,'0000')"/></xsl:for-each></xsl:attribute>
					</xsl:if>
					<Operations>
						<xsl:for-each select="descendant::dsc:GeneratedMaterial[@id=$iGM]/dsc:Machinings/descendant::dsc:Machining[@Operate='1' and @Phase !='Incompatible']">
							<xsl:if test="dsc:PieceCenter3D/@X &lt;= ancestor::dsc:GeneratedMaterial[1]/@length and dsc:PieceCenter3D/@X &gt;= 0">
								<Operation>
									<xsl:attribute name="name"><xsl:value-of select="dsc:Shape/@name"/></xsl:attribute>
									<xsl:attribute name="description"><xsl:value-of select="@description"/></xsl:attribute>
									<xsl:attribute name="X"><xsl:value-of select="dsc:PieceCenter3D/@X"/></xsl:attribute>
									<xsl:attribute name="Y"><xsl:value-of select="dsc:PieceCenter3D/@Y"/></xsl:attribute>
									<xsl:attribute name="Z"><xsl:value-of select="dsc:PieceCenter3D/@Z"/></xsl:attribute>
									<xsl:attribute name="depth"><xsl:choose><xsl:when test="@Depth &gt; 0"><xsl:value-of select="format-number(@Depth,'0.00')"/></xsl:when><xsl:otherwise><xsl:value-of select="0"/></xsl:otherwise></xsl:choose></xsl:attribute>
									<xsl:attribute name="side"><xsl:value-of select="@Side"/></xsl:attribute>
									<xsl:attribute name="angleplane"><xsl:value-of select="@angleplane"/></xsl:attribute>
									<xsl:attribute name="tool"><xsl:value-of select="@Tool"/></xsl:attribute>
									<xsl:attribute name="Rotation"><xsl:value-of select="@Rotation"/></xsl:attribute>
									<xsl:attribute name="HorizontalInversion"><xsl:value-of select="@HorizontalInversion"/></xsl:attribute>
									<xsl:attribute name="VerticalInversion"><xsl:value-of select="@VerticalInversion"/></xsl:attribute>
									<xsl:attribute name="inverted"><xsl:value-of select="parent::dsc:Machinings/@inverted"/></xsl:attribute>
									<xsl:for-each select="descendant::dsc:Shape/Variable">
										<Variable>
											<xsl:attribute name="name"><xsl:value-of select="@Name"/></xsl:attribute>
											<xsl:attribute name="value"><xsl:value-of select="@Value"/></xsl:attribute>
										</Variable>
									</xsl:for-each>
								</Operation>
							</xsl:if>
						</xsl:for-each>
					</Operations>
					<Steels>
						<xsl:for-each select="descendant::dsc:GeneratedMaterial[@id=$iGM]/parent::*/dsc:GeneratedProfilePieces/dsc:ProfilePiece/dsc:GeneratedMaterial">
							<xsl:variable name="ref">
								<xsl:value-of select="@material"/>
							</xsl:variable>
							<xsl:variable name="rol" select="//dsc:Defs/dsc:Materials/dsc:Material[@ref=$ref]/@Role"/>
							<xsl:if test="$rol=$RolRefuerzo">
								<Steel>
									<xsl:attribute name="Id"><xsl:value-of select="@id"/></xsl:attribute>
									<xsl:attribute name="reference"><xsl:value-of select="$ref"/></xsl:attribute>
									<!--SD20110526 Class hinzugefügt, wegen der Eisenübersetzung-->
									<xsl:attribute name="class"><xsl:value-of select="//dsc:Defs/dsc:Materials/dsc:Material[@ref=$ref]/@class"/></xsl:attribute>
									<xsl:attribute name="length"><xsl:value-of select="@length"/></xsl:attribute>
								</Steel>
							</xsl:if>
						</xsl:for-each>
					</Steels>
				</xsl:if>
			</xsl:for-each>
		</CutInstance>
	</xsl:template>
</xsl:stylesheet>
