<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="C:\Prefco\PrefCoUserProjekte\Programmers\rkrause\XSL\Brand\xsltSTURTZBaz.xslt"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:script="Myscript" extension-element-prefixes="script">
	<xsl:output version="4.0" omit-xml-declaration="yes" indent="yes" method="text"/>
	<!-- _______________________________________________________ Root ____________________________________________________________ -->
	<msxsl:script language="VBScript" implements-prefix="script"><![CDATA[ 
	     
		Dim currentLocale
		' Get the current locale
		currentLocale = GetLocale

		Function SetLocaleFirst()
		  Dim original
		  original = SetLocale("en-gb")
		  SetLocaleFirst = ""
  		End Function
	
		Function SetLocaleEnd()
			Dim original
			original = SetLocale(currentLocale)
			SetLocaleEnd = ""
		End Function

        Function RFill(name, sign, times)
            Dim sStr
            sStr = name & String(times, sign)
            RFill = Left(sStr, times)
        End Function
        
        Function LFill(name, sign, times)
          Dim s
          s = String(times, sign) & name
          LFill = Right(s, times)
        End Function
            
	       ]]></msxsl:script>
	
	<!-- Parameter die beim Starten des Prozessors übergeben werden können -->
	<xsl:param name="set" select="'1'"/>
	<xsl:param name="separation" select="'2'"/>
	<!--1 - Rahmen
		2 - Flügel
		3 - Rest - nicht Rahmen/Flügel
		-->
	<xsl:variable name="machineId" select="'#13#'"/>
	

	<xsl:variable name ="HechtBearbeitungen">
		<!--@Waku - hier bitte selbstständig erweitern und uns immer die aktuell geänderte zu senden-->
		<xsl:value-of select ="'
								'"/>
	</xsl:variable>
	<xsl:variable name ="Spaltenueberschrift">
		<xsl:value-of select ="'Pos;Stueckzahl;Benennung;Laenge;Bemerkung;'"/>
		<xsl:text>&#xA;</xsl:text>
	</xsl:variable>
	<xsl:variable name="lot" select="ProductionLot/@ProductionLot"/>
	
	<xsl:template match="/">

		<xsl:value-of select="script:SetLocaleFirst()"/>
		<xsl:value-of select ="$Spaltenueberschrift"></xsl:value-of>
		<xsl:apply-templates select="descendant::ProductionSet[@ProductionSetNumber=$set]/Machine[contains($machineId, concat('#', @machineId,'#'))]"/>

		<xsl:value-of select="script:SetLocaleEnd()"/>

	</xsl:template>

	<xsl:template match="Machine">
		<xsl:variable name ="MachineId" select ="@machineId"/>
		<xsl:apply-templates select ="descendant::Rod">
			<xsl:with-param name ="MachineId" select ="$MachineId"/>
		</xsl:apply-templates>
		
	</xsl:template>

	<xsl:template match="Rod">
		<xsl:param name ="MachineId"/>
		<xsl:apply-templates select ="CutPiece">
			<xsl:with-param name ="MachineId" select ="$MachineId"/>
			<xsl:with-param name ="reference" select ="@reference"/>
			<xsl:with-param name ="color" select ="@color"/>
		</xsl:apply-templates>
	</xsl:template>

	<xsl:template match="CutPiece">
		<xsl:param name ="MachineId"/>
		<xsl:param name ="reference"/>
		<xsl:param name ="color"/>
		<xsl:param name ="length" select ="@length"/>

		<xsl:choose>
			<xsl:when test="$separation = 1">
				<xsl:apply-templates select ="CutInstance[@fatherRole = 'FRAME']">
					<xsl:with-param name ="MachineId" select ="$MachineId"/>
					<xsl:with-param name ="reference" select ="$reference"/>
					<xsl:with-param name ="color" select ="$color"/>
					<xsl:with-param name ="length" select ="$length"/>
				</xsl:apply-templates>
				<xsl:apply-templates select ="CutInstance[@fatherRole = 'MULLION']">
					<xsl:with-param name ="MachineId" select ="$MachineId"/>
					<xsl:with-param name ="reference" select ="$reference"/>
					<xsl:with-param name ="color" select ="$color"/>
					<xsl:with-param name ="length" select ="$length"/>
				</xsl:apply-templates>
			</xsl:when>
			<xsl:when test="$separation = 2">
				<xsl:apply-templates select ="CutInstance[@fatherRole = 'SASH']">
					<xsl:with-param name ="MachineId" select ="$MachineId"/>
					<xsl:with-param name ="reference" select ="$reference"/>
					<xsl:with-param name ="color" select ="$color"/>
					<xsl:with-param name ="length" select ="$length"/>
				</xsl:apply-templates>
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates select ="CutInstance[@fatherRole != 'SASH' and @fatherRole != 'FRAME']">
					<xsl:with-param name ="MachineId" select ="$MachineId"/>
					<xsl:with-param name ="reference" select ="$reference"/>
					<xsl:with-param name ="color" select ="$color"/>
					<xsl:with-param name ="length" select ="$length"/>
				</xsl:apply-templates>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="CutInstance">
		<xsl:param name ="MachineId"/>
		<xsl:param name ="reference"/>
		<xsl:param name ="color"/>
		<xsl:param name ="length"/>
		<!--Position-->
		<xsl:variable name ="Position">
			<xsl:value-of select ="concat(@orderNumber,'/',@nomenclature)"/>
		</xsl:variable>
		<xsl:value-of select ="concat($Position,';')"/>
		<!--Anzahl-->
		<xsl:variable name ="Anzahl">
			<xsl:value-of select ="'1'"/>
		</xsl:variable>
		<xsl:value-of select ="concat($Anzahl,';')"/>
		<!--Benennung-->
		<xsl:variable name ="Benennung">
			<xsl:value-of select ="concat($reference,' ',$color)"/>
		</xsl:variable>
		<!-- RK150324#28252 lt. Thomas solle die Länge um 100 verkürzt werden wenn in der reference "fix" enthalten ist -->
		<xsl:value-of select ="concat($Benennung,';')"/>
		<!--Laenge-->
		<!-- RK150324#28252 lt. Thomas solle die Länge um 100 verkürzt werden wenn in der reference "fix" enthalten ist -->
		<xsl:variable name ="Laenge">
			<xsl:choose>
				<xsl:when test="contains($reference,'fix')"><xsl:value-of select ="$length - 100"/></xsl:when>
				<xsl:otherwise><xsl:value-of select ="$length"/></xsl:otherwise>
			</xsl:choose>
			
		</xsl:variable>
		<xsl:value-of select ="concat($Laenge,';')"/>
		<!--Bemerkung-->
		
		<xsl:variable name ="Lage">
			<xsl:choose>
				<xsl:when test ="@angle = 90">
					<xsl:value-of select ="'R'"/>
				</xsl:when>
				<xsl:when test ="@angle = 180">
					<xsl:value-of select ="'O'"/>
				</xsl:when>
				<xsl:when test ="@angle = 270">
					<xsl:value-of select ="'L'"/>
				</xsl:when>
				<xsl:when test ="@angle = 360 or @angle = 0">
					<xsl:value-of select ="'U'"/>
				</xsl:when>
			</xsl:choose>
		</xsl:variable>
		
		<xsl:variable name ="StahlKlinkschnitt">
			<xsl:for-each select ="Operations/Operation[contains('Stahl#Klinkschnitt',@name)]">
				<xsl:sort select ="@X" data-type ="number" order ="descending"/>
				<xsl:if test ="position() != 1">
					<xsl:value-of select ="'K-L'"/>
					<xsl:value-of select ="@X"/>
					<xsl:value-of select ="'-R'"/>
					<xsl:value-of select ="$Laenge - @X"/>
				</xsl:if>
			</xsl:for-each>
		</xsl:variable>
		
		<!--Bearbeitungen-->
		<xsl:variable name ="Bearbeitungen">
			<xsl:for-each select ="Operations/Operation[contains($HechtBearbeitungen,concat('#',@name,'#'))]">
				<xsl:if test ="position() = 1">
					<xsl:value-of select ="'Bearb.!'"/>
				</xsl:if>
			</xsl:for-each>
		</xsl:variable>
		
		<xsl:variable name ="Wagen">
			<xsl:value-of select ="@container"/>
		</xsl:variable>
		<xsl:variable name ="Fach">
			<xsl:value-of select ="@slot"/>
		</xsl:variable>
			
		<xsl:variable name ="Bemerkung">	
			<xsl:value-of select ="concat($Lage ,' ',$StahlKlinkschnitt,$Bearbeitungen,' ','W',$Wagen,' F', $Fach)"/>
		</xsl:variable>
		<xsl:value-of select ="concat($Bemerkung,';')"/>
		<xsl:text>&#xA;</xsl:text>
	</xsl:template>
</xsl:stylesheet>
